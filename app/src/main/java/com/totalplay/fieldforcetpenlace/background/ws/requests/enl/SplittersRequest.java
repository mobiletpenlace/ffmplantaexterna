package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 1/6/17.
 * FieldForceManagement-Android
 */
public class SplittersRequest extends BaseRequest {

    @SerializedName("latitud")
    public String latitude;
    @SerializedName("longitud")
    public String longitude;
    public String radio;
    @SerializedName("nombre")
    public String name;
}
