package com.totalplay.fieldforcetpenlace.background.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.utils.Prefs;

/**
 * Created by TotalPlay on 12/11/16.
 * EMP
 */

public class MessagingFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = MessagingFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String token = FirebaseInstanceId.getInstance().getToken();
        Prefs.instance().putString(Keys.FCM_TOKEN, token);
        Log.d(TAG, token);
    }

}
