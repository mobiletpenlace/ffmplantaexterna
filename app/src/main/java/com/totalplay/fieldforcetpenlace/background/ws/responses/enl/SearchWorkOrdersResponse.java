package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by NS-TOTALPLAY on 08/02/2017.
 */

public class SearchWorkOrdersResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("result")
    public String resultId;
    @SerializedName("resultDescripcion")
    public String descriptionResult;
    @SerializedName("Ot")
    public ArrayList<WorkOrder> workOrders = new ArrayList<>();
}
