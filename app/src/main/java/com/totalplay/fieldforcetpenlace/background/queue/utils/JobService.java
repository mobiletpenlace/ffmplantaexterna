package com.totalplay.fieldforcetpenlace.background.queue.utils;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.scheduling.FrameworkJobSchedulerService;
import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;


/**
 * Created by totalplay on 3/7/17.
 * EMP
 */

public class JobService extends FrameworkJobSchedulerService {
    @NonNull
    @Override
    protected JobManager getJobManager() {
        return QueueManager.instance();
    }
}
