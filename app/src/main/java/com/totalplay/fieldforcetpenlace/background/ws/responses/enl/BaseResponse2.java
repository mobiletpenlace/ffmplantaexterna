package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 11/16/16.
 */
public class BaseResponse2 extends BaseResponse{

    @SerializedName("Result")
    public String result;
    @SerializedName("ResultDescription")
    public String resultDescription;
    @SerializedName("IdResult")
    public String resultId;
    @SerializedName("Description")
    public String description;

}
