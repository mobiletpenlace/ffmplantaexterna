package com.totalplay.fieldforcetpenlace.background.queue.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

/**
 * Created by totalplay on 3/8/17.
 * EMP
 */

abstract class DataJob extends Job {

    DataJob(Params params) {
        super(params);
    }

    @Override
    public void onAdded() {

    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        if (throwable instanceof NullPointerException) {
            return RetryConstraint.CANCEL;
        }
        return RetryConstraint.RETRY;
    }
}
