package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;

/**
 * Created by totalplay on 11/28/16.
 * Total
 */

public class PaymentEngineResponse implements Serializable, WSBaseResponseInterface {

    @SerializedName("BrmResult")
    public BrmResult brmResult;
    @SerializedName("BankResponse")
    public BankResponse bankResponse;

    public class BrmResult{

        @SerializedName("Response")
        public Response response;

        public class Response{
            @SerializedName("idResult")
            public String idResult;
            @SerializedName("code")
            public String code;
            @SerializedName("description")
            public String description;
        }

    }

    public class BankResponse{
        @SerializedName("TxId")
        public String txId;
        @SerializedName("Response")
        public BankResponse.Response response;
        @SerializedName("BankingAuthorizerResponse")
        public BankingAuthorizerResponse bankingAuthorizerResponse;

        public class Response{
            @SerializedName("code")
            public String code;
            @SerializedName("description")
            public String description;
        }

        public class BankingAuthorizerResponse{
            @SerializedName("affiliation")
            public String affiliation;
            @SerializedName("affiliationName")
            public String affiliationName;
            @SerializedName("authorizationCode")
            public String authorizationCode;
            @SerializedName("bankResponseCode")
            public String bankResponseCode;
            @SerializedName("descriptionResponse")
            public String descriptionResponse;
            @SerializedName("standardResponseCode")
            public String standardResponseCode;
            @SerializedName("transactionControlNumber")
            public String transactionControlNumber;
            @SerializedName("transactionDate")
            public String transactionDate;
        }
    }

}
