package com.totalplay.fieldforcetpenlace.background.ws;

import com.totalplay.background.BaseDefinition;
import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.utils.Prefs;


/**
 * Created by jorgehdezvilla on 29/08/17.
 * FFM
 */

@SuppressWarnings({"all"})
public class WSManager extends BaseWSManager {

    public static boolean DEBUG_ENABLED = true;

    public static WSManager init() {
        return new WSManager();
    }

    @Override
    public boolean getDebugEnabled() {
        return DEBUG_ENABLED;
    }

    @Override
    public boolean getErrorDebugEnabled() {
        return false;
    }

    public class WS {
        public static final String CRASH = "crash";
        public static final String LOGIN = "login";
        public static final String VERSION_APP = "versionApp";
        public static final String SEARCH_EMPLOYEE = "searchEmployee";
        public static final String ASSISTANTS = "assistants";
        public static final String WORK_ASSISTANTS_TEAM = "workAssistantsTeam";
        public static final String SAVE_WORK_ASSISTANTS_TEAM = "saveWorkAssistantsTeam";
        public static final String CHANGE_STATUS = "changeStatus";
        public static final String CANCEL_WORK_ORDER = "cancelWorkOrder";
        public static final String WORK_ORDERS = "workOrders";
        public static final String SPLITTERS = "splitters";
        public static final String ADD_SPLITERS = "findSplitter";
        public static final String UPDATE_SPLITTER = "updateSplitter";
        public static final String GET_MATERIALS = "consultingMaterials";
        public static final String DISCOUNT_MATERIALS = "discountMaterials";
        public static final String FINISH_WORK_ORDER = "affectation";
        public static final String SAVE_ARRIVE_TIME = "arriveTime";
        public static final String VALIDATE_WORK_ORDER = "validWorkOrder";
        public static final String UPDATE_TIME = "updateTime";
        public static final String SAVE_IMAGE = "saveImageBD";
        public static final String SURVEY = "testapp";
        public static final String QUOTATION = "quotation";
        public static final String GET_EQUIPMENTS = "getEquipment";
        public static final String GENERATE_DNS = "generateDNS";
        public static final String PAYMENT = "payment";
        public static final String CASH_PAYMENT = "cashPayment";
        public static final String GET_FLAGS = "getFlags";
        public static final String ACTIVATION = "activation";
        public static final String AUTO_FIND = "autoFind";
        public static final String VALIDATE_SERIAL_NUMBER = "validateSerialNumber";
        public static final String GET_MAC = "getMac";
        public static final String UPDATE_EQUIPMENT = "updateEquipment";
        public static final String SETTING_EQUIPMENT = "settingEquipment";
        public static final String UPDATE_DN = "updateDN";
        public static final String SEND_FLAG = "sendFlag";
        public static final String FINISH_STATUS = "finishStatus";
        public static final String CONFIG_SMD = "getConfigSDM";
        public static final String VALIDATE_APP = "validaCuentaENL";
        public static final String CHANGE_OPERATOR_STATUS = "changeOperatorStatus";
        public static final String GET_SETTINGS_EQUIPMENTS = "getSettingsEuipments";
        public static final String DOWNLOAD_FILE = "downloadFile";
    }

    @Override
    protected BaseDefinition getDefinition() {
        return null;
    }

    @Override
    protected String getJsonDebug(String requestUrl) {
        String json = "";
        switch (requestUrl) {
            case WS.VERSION_APP:
                json = "{\n" +
                        "   version: \"1.0.0.6\"\n" +
                        "}";
                break;
            case WS.LOGIN:
                json = "{\n" +
                        "    \"IdResult\": \"0\",\n" +
                        "    \"Result\": \"0\",\n" +
                        "    \"ResultDescription\": \"Usuario correcto.\",\n" +
                        "    \"IdOperario\": \"4919\",\n" +
                        "    \"Propietarios\": \"1\",\n" +
                        "    \"Primer_Acceso\": \"true\"\n" +
                        "}";
                break;
            case WS.PAYMENT:
                json = "{\n" +
                        "    \"BrmResult\": {\n" +
                        "        \"Response\": {\n" +
                        "            \"idResult\": \"17103112230401376\",\n" +
                        "            \"code\": \"0\",\n" +
                        "            \"description\": \"Account not found\"\n" +
                        "        },\n" +
                        "        \"BrmResponse\": {\n" +
                        "            \"Poid\": null,\n" +
                        "            \"Code\": null,\n" +
                        "            \"Description\": null,\n" +
                        "            \"Account_obj\": null,\n" +
                        "            \"Event\": {\n" +
                        "                \"Event_obj\": null,\n" +
                        "                \"Result\": null\n" +
                        "            },\n" +
                        "            \"Created_T\": null,\n" +
                        "            \"Effective_T\": null,\n" +
                        "            \"Event_No\": null,\n" +
                        "            \"Item_No\": null,\n" +
                        "            \"Item_obj\": null,\n" +
                        "            \"Payment\": {\n" +
                        "                \"Account_No\": null,\n" +
                        "                \"Ach\": null,\n" +
                        "                \"Amount\": null,\n" +
                        "                \"Bill_No\": null,\n" +
                        "                \"UserStatus\": null,\n" +
                        "                \"Trans_id\": null,\n" +
                        "                \"Sub_trans_id\": null\n" +
                        "            },\n" +
                        "            \"Payment_failde_info\": {\n" +
                        "                \"Payment_trans_id\": null\n" +
                        "            }\n" +
                        "        }\n" +
                        "    },\n" +
                        "    \"SaveCardResponse\": {\n" +
                        "        \"Response\": {\n" +
                        "            \"code\": null,\n" +
                        "            \"result\": null,\n" +
                        "            \"description\": null\n" +
                        "        },\n" +
                        "        \"CardsResponsitoryResponse\": {\n" +
                        "            \"airTimeSequence\": null,\n" +
                        "            \"crmCode\": null,\n" +
                        "            \"crmDescription\": null,\n" +
                        "            \"Ticket\": null\n" +
                        "        }\n" +
                        "    },\n" +
                        "    \"BankResponse\": {\n" +
                        "        \"TxId\": \"17103112230401376\",\n" +
                        "        \"Response\": {\n" +
                        "            \"code\": \"0\",\n" +
                        "            \"description\": \"Account not found\"\n" +
                        "        },\n" +
                        "        \"BankingAuthorizerResponse\": {\n" +
                        "            \"affiliation\": 17103112230401376,\n" +
                        "            \"affiliationName\": 17103112230401376,\n" +
                        "            \"authorizationCode\": 17103112230401376,\n" +
                        "            \"bankResponseCode\": 00,\n" +
                        "            \"descriptionResponse\": 17103112230401376,\n" +
                        "            \"standardResponseCode\": 17103112230401376,\n" +
                        "            \"transactionControlNumber\": 17103112230401376,\n" +
                        "            \"transactionDate\": 17103112230401376\n" +
                        "        }\n" +
                        "    }\n" +
                        "}";
                break;
            case WS.GET_SETTINGS_EQUIPMENTS:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultDescription\": \"Operación Exitosa\",\n" +
                        "    \"informacionEquipo\": [\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvTMAQ\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "            \"modeloEquipo\": \"8245h\",\n" +
                        "            \"tipo\": \"ONT\",\n" +
                        "            \"numeroSerie\": null,\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvVMAQ\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHLtMAO\",\n" +
                        "            \"modeloEquipo\": \"Smart 1500\",\n" +
                        "            \"tipo\": \"UPS\",\n" +
                        "            \"numeroSerie\": null,\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvWMAQ\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHLuMAO\",\n" +
                        "            \"modeloEquipo\": \"TL-SF1024\",\n" +
                        "            \"tipo\": \"Switch\",\n" +
                        "            \"numeroSerie\": null,\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvXMAQ\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWT2MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Jfjf\",\n" +
                        "            \"mac\": \"A3:56:\",\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvYMAQ\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHLwMAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Hfjfn\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvZMAQ\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWT4MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Ufnfbf\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvaMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWT5MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Nnjj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvbMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHLzMAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Juj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvcMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHM0MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Huj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvdMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHM1MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Yujj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epveMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHM2MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Hjkk\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvfMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHM3MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Hjk\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvgMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHM4MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"V mkh\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvhMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTCMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Gjkk\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epviMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTDMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Gjkjh\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvjMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTEMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Hjjn\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvkMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHM8MAO\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Guu\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvlMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTGMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Hjjj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvmMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTHMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Bzncj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvnMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMBMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Yujb\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvoMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMCMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Tujj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvpMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMDMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"5656757\",\n" +
                        "            \"mac\": \"45:54:54:44\",\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvqMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMEMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Yujj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvrMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTMMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Yjnn\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvsMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMGMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Vjjj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvtMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMHMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Tu jnb\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvuMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMIMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Yujb\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvvMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTQMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"6uhbb\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvwMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTRMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Hhbj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvxMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMLMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Gbbb\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvyMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMMMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Ghjj\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epvzMAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000008LWTUMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Yjjnn\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epw0MAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005CHMOMA4\",\n" +
                        "            \"modeloEquipo\": \"GXP1610\",\n" +
                        "            \"tipo\": \"Tel IP\",\n" +
                        "            \"numeroSerie\": \"Hjjn\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": {\n" +
                        "                \"modelo\": [\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    },\n" +
                        "                    {\n" +
                        "                        \"id_modelo\": \"a0SQ0000005C3hDMAS\",\n" +
                        "                        \"nombre_modelo\": \"8245h\"\n" +
                        "                    }\n" +
                        "                ]\n" +
                        "            }\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"idPlanServicio\": \"a0IQ0000008epw1MAA\",\n" +
                        "            \"idEquipoModelo\": \"a0SQ0000005C3hEMAS\",\n" +
                        "            \"modeloEquipo\": \"PBX Enlace\",\n" +
                        "            \"tipo\": \"PBX\",\n" +
                        "            \"numeroSerie\": \"Kgkgkf\",\n" +
                        "            \"mac\": null,\n" +
                        "            \"modelos_disponibles\": null\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}";
                break;
            case WS.WORK_ORDERS:
                if (Prefs.instance().string(Keys.FLOW_TYPE).equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
                    json = "{\n" +
                            "    \"result\": \"0\",\n" +
                            "    \"resultDescripcion\": \"Operacion exitosa\",\n" +
                            "    \"Ot\": [\n" +
                            "        {\n" +
                            "            \"Id_Operario\": \"4848\",\n" +
                            "            \"NumeroOT\": \"463\",\n" +
                            "            \"NumeroTicket\": \" \",\n" +
                            "            \"NumeroOS\": \"OS-01964\",\n" +
                            "            \"NumeroCuenta\": \"0210001063\",\n" +
                            "            \"NombreCliente\": \"TEST ENLACE- OPORTUNIDAD  ENLACE\",\n" +
                            "            \"NombreContacto\": \"TEST MIDDLEWARE DIRZO BATMAN\",\n" +
                            "            \"CanalVenta\": \"Página web\",\n" +
                            "            \"Calle\": \"periferico sur\",\n" +
                            "            \"NInterior\": \"null\",\n" +
                            "            \"NExterior\": \"4119\",\n" +
                            "            \"Colonia\": \"Fuentes del Pedregal\",\n" +
                            "            \"Municipio\": \"Tlalpan\",\n" +
                            "            \"Ciudad\": \"Ciudad de Mexico\",\n" +
                            "            \"CodigoPostal\": \"14141\",\n" +
                            "            \"Referencia\": \"na\",\n" +
                            "            \"entreCalles\": \"null\",\n" +
                            "            \"Lat\": \"19.37224624655047\",\n" +
                            "            \"Long1\": \"-99.15061789849244\",\n" +
                            "            \"TelefonoContacto\": \"5548945646\",\n" +
                            "            \"TelefonoEmpresa\": \"5556456456\",\n" +
                            "            \"Extencion\": null,\n" +
                            "            \"PaqueteContratado\": \"CONEXION 4 Analógico D\",\n" +
                            "            \"Cluser\": \"CIUDAD DE MEXICO\",\n" +
                            "            \"IdEstatusOT\": \"2\",\n" +
                            "            \"DescEstatusOT\": \"ASIGNADA\",\n" +
                            "            \"TipoIntervencion\": \"48\",\n" +
                            "            \"SubTipoIntervencion\": \"49\",\n" +
                            "            \"idEstadoOT\": \"9\",\n" +
                            "            \"descEstadoOT\": \"PROGRAMADA PERO NO INICIADA\",\n" +
                            "            \"idMotivoOT\": \"23\",\n" +
                            "            \"DescMotivoOT\": \"ASIGNADO POR DESPACHO\",\n" +
                            "            \"UnidadNegocio\": \"ENLACE\",\n" +
                            "            \"DiasRetraso\": \"-1\",\n" +
                            "            \"VISITAS\": \"0\",\n" +
                            "            \"idPropietario\": \"1\",\n" +
                            "            \"hora_compromiso\": \"20:00:00\",\n" +
                            "            \"url_imagenes\": \"Ventas/Enlace/0210001063/\",\n" +
                            "            \"url_checkList\": \"https://msstest.totalplay.com.mx/TFE/DownloadFiles/dGZldXNlcjp0ZjNwNHNzdzByZA==/L1ZlbnRhcy9FbmxhY2UvMDIxMDAwMTA2My9Eb2N1bWVudG9zL2NoZWNrTGlzdC54bHN4\",\n" +
                            "            \"tiempoEstimado\": \"120\"\n" +
                            "        }\n" +
                            "    ]\n" +
                            "}";
                } else {
                    json = "{\n" +
                            "    \"result\": \"0\",\n" +
                            "    \"resultDescripcion\": \"Operacion exitosa\",\n" +
                            "    \"Ot\": [\n" +
                            "        {\n" +
                            "            \"Id_Operario\": \"4848\",\n" +
                            "            \"NumeroOT\": \"463\",\n" +
                            "            \"NumeroTicket\": \" \",\n" +
                            "            \"NumeroOS\": \"OS-01964\",\n" +
                            "            \"NumeroCuenta\": \"0210001063\",\n" +
                            "            \"NombreCliente\": \"TEST ENLACE- OPORTUNIDAD  ENLACE\",\n" +
                            "            \"NombreContacto\": \"TEST MIDDLEWARE DIRZO BATMAN\",\n" +
                            "            \"CanalVenta\": \"Página web\",\n" +
                            "            \"Calle\": \"periferico sur\",\n" +
                            "            \"NInterior\": \"null\",\n" +
                            "            \"NExterior\": \"4119\",\n" +
                            "            \"Colonia\": \"Fuentes del Pedregal\",\n" +
                            "            \"Municipio\": \"Tlalpan\",\n" +
                            "            \"Ciudad\": \"Ciudad de Mexico\",\n" +
                            "            \"CodigoPostal\": \"14141\",\n" +
                            "            \"Referencia\": \"na\",\n" +
                            "            \"entreCalles\": \"null\",\n" +
                            "            \"Lat\": \"19.37224624655047\",\n" +
                            "            \"Long1\": \"-99.15061789849244\",\n" +
                            "            \"TelefonoContacto\": \"5548945646\",\n" +
                            "            \"TelefonoEmpresa\": \"5556456456\",\n" +
                            "            \"Extencion\": null,\n" +
                            "            \"PaqueteContratado\": \"CONEXION 4 Analógico D\",\n" +
                            "            \"Cluser\": \"CIUDAD DE MEXICO\",\n" +
                            "            \"IdEstatusOT\": \"2\",\n" +
                            "            \"DescEstatusOT\": \"ASIGNADA\",\n" +
                            "            \"TipoIntervencion\": \"48\",\n" +
                            "            \"SubTipoIntervencion\": \"49\",\n" +
                            "            \"idEstadoOT\": \"9\",\n" +
                            "            \"descEstadoOT\": \"PROGRAMADA PERO NO INICIADA\",\n" +
                            "            \"idMotivoOT\": \"23\",\n" +
                            "            \"DescMotivoOT\": \"ASIGNADO POR DESPACHO\",\n" +
                            "            \"UnidadNegocio\": \"ENLACE\",\n" +
                            "            \"DiasRetraso\": \"-1\",\n" +
                            "            \"VISITAS\": \"0\",\n" +
                            "            \"idPropietario\": \"7\",\n" +
                            "            \"hora_compromiso\": \"20:00:00\",\n" +
                            "            \"url_imagenes\": \"Ventas/Enlace/0210001063/\",\n" +
                            "            \"url_checkList\": \"https://msstest.totalplay.com.mx/TFE/DownloadFiles/dGZldXNlcjp0ZjNwNHNzdzByZA==/L1ZlbnRhcy9FbmxhY2UvMDIxMDAwMTA2My9Eb2N1bWVudG9zL2NoZWNrTGlzdC54bHN4\",\n" +
                            "            \"tiempoEstimado\": \"120\"\n" +
                            "        }\n" +
                            "    ]\n" +
                            "}";
                }
                break;
            case WS.CHANGE_OPERATOR_STATUS:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultdescription\": \"Operacion exitosa.\"\n" +
                        "}";
                break;
            case WS.ASSISTANTS:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultdescription\": \"Operacion exitosa.\",\n" +
                        "    \"Auxiliares\": [\n" +
                        "        {\n" +
                        "            \"ID_Auxiliar\": \"5\",\n" +
                        "            \"Nombre_Auxiliar\": \"SIN AUXILIAR \"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"ID_Auxiliar\": \"4901\",\n" +
                        "            \"Nombre_Auxiliar\": \"ARTURO VARGAS RODRIGUEZ\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"ID_Auxiliar\": \"4931\",\n" +
                        "            \"Nombre_Auxiliar\": \"MARIO HERNANDEZ PEREZ\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"ID_Auxiliar\": \"4932\",\n" +
                        "            \"Nombre_Auxiliar\": \"CESAR ANGUIANO ROSALES\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"ID_Auxiliar\": \"4933\",\n" +
                        "            \"Nombre_Auxiliar\": \"RUBEN CERON GARCIA\"\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}";
                break;
            case WS.WORK_ASSISTANTS_TEAM:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultdescription\": \"Operacion exitosa.\",\n" +
                        "    \"ID_Operario\": \"4848\",\n" +
                        "    \"Nombre_Operario\": \"PRUEBA APP USER\",\n" +
                        "    \"Car_Info\": [\n" +
                        "        {\n" +
                        "            \"ID_Car\": null,\n" +
                        "            \"Modelo\": null,\n" +
                        "            \"Marca\": null,\n" +
                        "            \"Placa\": null\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"Auxiliares\": [\n" +
                        "    ]\n" +
                        "}";
                break;
            case WS.GET_FLAGS:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultdescription\": \"Operacion exitosa.\",\n" +
                        "    \"Flags\": [\n" +
                        "        {\n" +
                        "            \"Flag\": [\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"1\",\n" +
                        "                    \"Descripcion_Flag\": \"EQUIPOS CONFIGURADOS\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"2\",\n" +
                        "                    \"Descripcion_Flag\": \"DNS CONFIGURADOS\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"3\",\n" +
                        "                    \"Descripcion_Flag\": \"CUENTA ACTIVADA\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"4\",\n" +
                        "                    \"Descripcion_Flag\": \"CUENTA PAGADA\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"5\",\n" +
                        "                    \"Descripcion_Flag\": \"EVIDENCIA ENVIADA\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"6\",\n" +
                        "                    \"Descripcion_Flag\": \"MARK ASSISTANT\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"7\",\n" +
                        "                    \"Descripcion_Flag\": \"SPLITTER ASIGNADO\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"8\",\n" +
                        "                    \"Descripcion_Flag\": \"TIEMPO ESTIMADO\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"9\",\n" +
                        "                    \"Descripcion_Flag\": \"REFERENCIAS URBANAS\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"ID_Flag\": \"10\",\n" +
                        "                    \"Descripcion_Flag\": \"TERMINA ENCUESTA\",\n" +
                        "                    \"On_Off\": \"FALSE\"\n" +
                        "                }\n" +
                        "            ]\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}";
                break;
            case WS.QUOTATION:
                json = "{\"result\":\"0\",\"resultDescription\":\"Operacion exitosa\",\"CotizacionName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"ProntoPago\":\"600.0\",\"MegasSubida\":\"31.0\",\"MegasBajada\":\"310.0\",\"DNsTotales\":\"10.0\",\"PrimeraRenta\":\"true\",\"CotPlanServicios\":[{\"IdCotPlanServicio\":\"a0IQ0000008dKlTMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35438\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 ENLACE - Internet\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Internet\",\"EsAdicional\":\"false\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlUMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"Plan One\",\"CotPlanServicoName\":\"CSER35439\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 ENLACE - Telefonia\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Wifi Extender\",\"numeroDns\":\"5\",\"EsAdicional\":\"false\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlUMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"Plan Two\",\"CotPlanServicoName\":\"CSER35439\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 ENLACE - Telefonia\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Telefonia\",\"numeroDns\":\"5\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlVMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35440\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - UPS\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"UPS\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlWMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35441\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Switch 2\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Switch\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlXMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35442\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Switch 1\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Switch\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlYMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35443\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Switch 3\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Switch\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlZMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35444\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Tel IP 1\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Tel IP\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlaMAE\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35445\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Tel IP 2\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Tel IP\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlbMAE\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35446\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Tel IP 3\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Tel IP\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKlcMAE\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35447\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Tel IP 4\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Tel IP\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKldMAE\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35448\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Tel IP 5\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Tel IP\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKleMAE\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35449\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - Tel IP 6\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Tel IP\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]},{\"IdCotPlanServicio\":\"a0IQ0000008dKmIMAU\",\"CotSitioName\":\"CS21063\",\"CotSitioPlanName\":\"CSP21693\",\"DpPlanName\":\"CONEXIÓN SIP 30 PBX V5 ENLACE\",\"CotPlanServicoName\":\"CSER35489\",\"CotPlanServicoNServicio\":\"CONEXIÓN SIP 30 PBX V5 Enlace - WIFI\",\"DpPlanEtiquetaBRM\":\"PLAN - CONEXIÓN SIP 30 PBX V5 ENLACE\",\"Tipo\":\"Wifi Extender\",\"EsAdicional\":\"true\",\"models\":[{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"},{\"idmodel\":\"Model Value Id\",\"description\":\"Model Equipment\"}]}]}";
                break;
            case WS.SPLITTERS:
                json = "{\"result\":\"0\",\"resultDescription\":\"Operacion exitosa.\",\"empalmes\":{\"empalme\":[{\"id\":\"226028555\",\"nombre\":\"PED-217\",\"latitud\":\"19.3077068\",\"longitud\":\"-99.2052810\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"},{\"id\":\"226042600\",\"nombre\":\"VIRTUAL\",\"latitud\":\"19.3066835\",\"longitud\":\"-99.2045588\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"},{\"id\":\"226036159\",\"nombre\":\"PED-256\",\"latitud\":\"19.3066835\",\"longitud\":\"-99.2045588\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"},{\"id\":\"226028533\",\"nombre\":\"PED-216\",\"latitud\":\"19.3072217\",\"longitud\":\"-99.2060284\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"},{\"id\":\"226036098\",\"nombre\":\"VIRTUAL\",\"latitud\":\"19.3069794\",\"longitud\":\"-99.2054944\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"},{\"id\":\"226028451\",\"nombre\":\"PED-255\",\"latitud\":\"19.3069794\",\"longitud\":\"-99.2054944\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"},{\"id\":\"230968586\",\"nombre\":\"PED-275\",\"latitud\":\"19.3056468\",\"longitud\":\"-99.2043088\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"},{\"id\":\"236950206\",\"nombre\":\"null\",\"latitud\":\"19.3035358\",\"longitud\":\"-99.2069730\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"},{\"id\":\"231264296\",\"nombre\":\"null\",\"latitud\":\"19.3055941\",\"longitud\":\"-99.2094887\",\"PuertosTotales\":\"null\",\"PuestosOcupados\":\"null\",\"Saturado\":\"1\"}]}}";
                break;
            case WS.AUTO_FIND:
                json = "{\n" +
                        "    \"Result\": \"0\",\n" +
                        "    \"ResultDescription\": \"Peticion con exito\",\n" +
                        "    \"OLT\": \"TEPEPAN_DF\",\n" +
                        "    \"IdOLT\": \"46\",\n" +
                        "    \"IpOlt\": \"10.180.230.10\",\n" +
                        "    \"Port\": \"0\",\n" +
                        "    \"Slot\": \"8\",\n" +
                        "    \"Frame\": \"0\",\n" +
                        "    \"SerialNumber\": \"48575443CB6E1623\",\n" +
                        "    \"IdModel\": \"0\",\n" +
                        "    \"Model\": null,\n" +
                        "    \"Version\": null\n" +
                        "}";
                break;
            case WS.GET_EQUIPMENTS:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultDescripcion\": \"Operacion exitosa\",\n" +
                        "    \"InfoEquipos\": [\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epvVMAQ\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000BodpMMAR\",\n" +
                        "          \"NombreEquipo\": \"Smart 1500\",\n" +
                        "          \"Dispositivo\": \"UPS\",\n" +
                        "          \"modelos\": [\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000005CHLtMAO\",\n" +
                        "                \"descripcion_modelo\": \"Smart 1500\"\n" +
                        "             },\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000008LWSzMAO\",\n" +
                        "                \"descripcion_modelo\": \"Smart 1500\"\n" +
                        "             }\n" +
                        "          ]\n" +
                        "       },\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epvWMAQ\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000BodpPMAR\",\n" +
                        "          \"NombreEquipo\": \"TL-SF1024\",\n" +
                        "          \"Dispositivo\": \"Switch\",\n" +
                        "          \"modelos\": [\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000005CHLuMAO\",\n" +
                        "                \"descripcion_modelo\": \"TL-SF1024\"\n" +
                        "             },\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000008LWT1MAO\",\n" +
                        "                \"descripcion_modelo\": \"TL-SF1024\"\n" +
                        "             }\n" +
                        "          ]\n" +
                        "       },\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epvWMAQ\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000BodpPMAR\",\n" +
                        "          \"NombreEquipo\": \"TL-SF1024\",\n" +
                        "          \"Dispositivo\": \"WIFI\",\n" +
                        "          \"modelos\": [\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000005CHLuMAO\",\n" +
                        "                \"descripcion_modelo\": \"TL-SF1024\"\n" +
                        "             },\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000008LWT1MAO\",\n" +
                        "                \"descripcion_modelo\": \"TL-SF1024\"\n" +
                        "             }\n" +
                        "          ]\n" +
                        "       },\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epvXMAQ\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000BodpNMAR\",\n" +
                        "          \"NombreEquipo\": \"GXP1610\",\n" +
                        "          \"Dispositivo\": \"Tel IP\",\n" +
                        "          \"modelos\": [\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000005CHLvMAO\",\n" +
                        "                \"descripcion_modelo\": \"GXP1610\"\n" +
                        "             },\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000008LWT2MAO\",\n" +
                        "                \"descripcion_modelo\": \"GXP1610\"\n" +
                        "             }\n" +
                        "          ]\n" +
                        "       },\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epvYMAQ\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000BodpNMAR\",\n" +
                        "          \"NombreEquipo\": \"GXP1610\",\n" +
                        "          \"Dispositivo\": \"Tel IP\",\n" +
                        "          \"modelos\": [\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000005CHLwMAO\",\n" +
                        "                \"descripcion_modelo\": \"GXP1610\"\n" +
                        "             },\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000008LWT3MAO\",\n" +
                        "                \"descripcion_modelo\": \"GXP1610\"\n" +
                        "             }\n" +
                        "          ]\n" +
                        "       },\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epvZMAQ\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000BodpNMAR\",\n" +
                        "          \"NombreEquipo\": \"GXP1610\",\n" +
                        "          \"Dispositivo\": \"Tel IP\",\n" +
                        "          \"modelos\": [\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000005CHLxMAO\",\n" +
                        "                \"descripcion_modelo\": \"GXP1610\"\n" +
                        "             },\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000008LWT4MAO\",\n" +
                        "                \"descripcion_modelo\": \"GXP1610\"\n" +
                        "             }\n" +
                        "          ]\n" +
                        "       },\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epvmMAA\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000BodpNMAR\",\n" +
                        "          \"NombreEquipo\": \"GXP1610\",\n" +
                        "          \"Dispositivo\": \"Tel IP\",\n" +
                        "          \"modelos\": [\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000005CHMAMA4\",\n" +
                        "                \"descripcion_modelo\": \"GXP1610\"\n" +
                        "             },\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000008LWTHMA4\",\n" +
                        "                \"descripcion_modelo\": \"GXP1610\"\n" +
                        "             }\n" +
                        "          ]\n" +
                        "       },\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epvmMAA\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000BodpNMAR\",\n" +
                        "          \"NombreEquipo\": \"GXP1610\",\n" +
                        "          \"Dispositivo\": \"ONT\",\n" +
                        "          \"modelos\": [\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000005CHMAMA4\",\n" +
                        "                \"descripcion_modelo\": \"ONT GXP1610\"\n" +
                        "             },\n" +
                        "             {\n" +
                        "                \"id_modelo\": \"a0SQ0000008LWTHMA4\",\n" +
                        "                \"descripcion_modelo\": \"ONT GXP1610\"\n" +
                        "             }\n" +
                        "          ]\n" +
                        "       },\n" +
                        "       {\n" +
                        "          \"IdCotPlanServicios\": \"a0IQ0000008epw1MAA\",\n" +
                        "          \"IdEquipo\": \"a0EQ000000B1w40MAB\",\n" +
                        "          \"NombreEquipo\": \"PBX Enlace\",\n" +
                        "          \"Dispositivo\": \"PBX\"\n" +
                        "       }\n" +
                        "    ]\n" +
                        "}";
                break;
            case WS.VALIDATE_SERIAL_NUMBER:
                json = "{\n" +
                        "    \"Result\": {\n" +
                        "        \"Result\": \"0\",\n" +
                        "        \"IdResult\": \"0\",\n" +
                        "        \"Description\": \"SN, DN or MAC does not exist\"\n" +
                        "    }\n" +
                        "}";
                break;
            case WS.GET_MAC:
                json = "{\n" +
                        "    \"Result\": \"0\",\n" +
                        "    \"ResultDescription\": \"Peticion con exito\",\n" +
                        "    \"OLT\": \"TEPEPAN_DF\",\n" +
                        "    \"IdOLT\": \"46\",\n" +
                        "    \"IpOlt\": \"10.180.230.10\",\n" +
                        "    \"Port\": \"0\",\n" +
                        "    \"Slot\": \"8\",\n" +
                        "    \"Frame\": \"0\",\n" +
                        "    \"SerialNumber\": \"48575443CB6E1623\",\n" +
                        "    \"IdModel\": \"3\",\n" +
                        "    \"Model\": null,\n" +
                        "    \"Version\": \"V1R006C00S127\",\n" +
                        "    \"mac\": \"48:57:54:43:CB:6E\"\n" +
                        "}";
                break;
            case WS.GENERATE_DNS:
                json = "{\n" +
                        "    \"Result\": \"0\",\n" +
                        "    \"ResultDescription\": \"Peticion con exito\",\n" +
                        "    \"ArrDn\": [\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976730\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976731\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976732\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976733\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976734\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976735\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976736\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976737\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976738\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976739\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Dn\": \"5575976740\",\n" +
                        "            \"IdStatus\": \"3\",\n" +
                        "            \"IdTransaccion\": \"710419\"\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}";
                break;
            case WS.VALIDATE_WORK_ORDER:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultDescripcion\": \"Operacion exitosa.\",\n" +
                        "    \"ID_Estado\": \"14\",\n" +
                        "    \"Estado\": \"EN ESPERA\",\n" +
                        "    \"ID_Motivo\": \"125\",\n" +
                        "    \"Motivo\": \"DOMICILIO NO COINCIDE\",\n" +
                        "    \"Comentario\": \"PI : OT : DOMICILIO NO COINCIDE ::: Test\"\n" +
                        "}";
                break;
            case WS.UPDATE_DN:
            case WS.UPDATE_EQUIPMENT:
                json = "{\n" +
                        "    \"Result\": {\n" +
                        "        \"result\": \"0\",\n" +
                        "        \"resultDescripcion\": \"Equipos configurados con exito\"\n" +
                        "    }\n" +
                        "}";
                break;
            case WS.GET_MATERIALS:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultDescription\": \"Operacion exitosa.\",\n" +
                        "    \"Materiales\": [\n" +
                        "        {\n" +
                        "            \"Material\": \"16851\",\n" +
                        "            \"Descripcion\": \"CABLE TELEF 2 VIAS REDONDO BLANCO T-100\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16164\",\n" +
                        "            \"Descripcion\": \"CABLE TELEF 4 VIAS MARFIL MO4T-305\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16011\",\n" +
                        "            \"Descripcion\": \"FIBRA OPTICA 12 HILOS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16443\",\n" +
                        "            \"Descripcion\": \"FIBRA OPTICA SAMSUNG PARA DROP\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16762\",\n" +
                        "            \"Descripcion\": \"UNIFIBRA OPTICA CROSSOP\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80026\",\n" +
                        "            \"Descripcion\": \"UNIFIBRA OPTICA LS G657  B3\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"17948\",\n" +
                        "            \"Descripcion\": \"UNIFIBRA OPTICA WAVEOPTICS B3\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"17847\",\n" +
                        "            \"Descripcion\": \"UNIFIBRA TELDOR\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80153\",\n" +
                        "            \"Descripcion\": \"UNIFIBRA OPTICA STAR LIGHT G657 B3\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16058\",\n" +
                        "            \"Descripcion\": \"CAB.UTP CAT 5 BOB 305 MTS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"17886\",\n" +
                        "            \"Descripcion\": \"CAB.UTP GRIS CAT 5 BOB 305 MTS OUTDOOR\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"Mt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16383\",\n" +
                        "            \"Descripcion\": \"CONECTOR PREPULIDO 250UM CROSSOP\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80193\",\n" +
                        "            \"Descripcion\": \"CONECTOR PREPULIDO GT NETWORKS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"84501\",\n" +
                        "            \"Descripcion\": \"JUMPER DE FIBRA OPTICA AMARILLO 3M\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16068\",\n" +
                        "            \"Descripcion\": \"ALCOHOL ISOPROPILICO 96% PUREZA 500 ML\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16635\",\n" +
                        "            \"Descripcion\": \"ARMELLA CERRADA 20 X 70\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16113\",\n" +
                        "            \"Descripcion\": \"CINCHO NEGRO 0.36X15 CM C/100 PZAS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16114\",\n" +
                        "            \"Descripcion\": \"CINCHO NEGRO 1.2X20 CM VELCRO C/10 PZAS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16048\",\n" +
                        "            \"Descripcion\": \"CINTA DE AISLAR NEGRA -PIEZA-\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16101\",\n" +
                        "            \"Descripcion\": \"CINTA DE MONTAJE MOD. 12541 MCA.TRUPER\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16089\",\n" +
                        "            \"Descripcion\": \"CONECTOR RJ-11 PLUG\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16014\",\n" +
                        "            \"Descripcion\": \"JUMPERS OPTICOS SC/APC 3 METROS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16016\",\n" +
                        "            \"Descripcion\": \"CONECTOR RJ45 -IPTV-\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16126\",\n" +
                        "            \"Descripcion\": \"CUBRE BOTAS (MEDIDA STANDAR)\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"17997\",\n" +
                        "            \"Descripcion\": \"ETIQUETAS PARA CONTRASENAS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"17996\",\n" +
                        "            \"Descripcion\": \"ETIQUETAS PARA ONT\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16109\",\n" +
                        "            \"Descripcion\": \"FLEJE DE ACERO 5/8\\\" C/30 MTS WAVEOPTICS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80046\",\n" +
                        "            \"Descripcion\": \"FORMATOS UNIVERSALES DE SERVICIO\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80047\",\n" +
                        "            \"Descripcion\": \"FORMATOS UNIVERSALES DE SOPORTE\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16166\",\n" +
                        "            \"Descripcion\": \"GRAPA P/CABLE COAX BCA CLAVO 3/4\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16165\",\n" +
                        "            \"Descripcion\": \"GRAPA P/CABLE TELEFONICO REDONDO BCO\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16216\",\n" +
                        "            \"Descripcion\": \"GUANTE DE LATEX DESECHABLE\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16637\",\n" +
                        "            \"Descripcion\": \"HEBILLA ACERO PARA FLEJE 5/8\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16036\",\n" +
                        "            \"Descripcion\": \"HERRAJE TIPO \\\" D \\\"\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16638\",\n" +
                        "            \"Descripcion\": \"PIJA CABEZA/ CRUZ DE 10 x 1\\\"\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16124\",\n" +
                        "            \"Descripcion\": \"ROLLO DE NEOPRENO NEGRO\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16862\",\n" +
                        "            \"Descripcion\": \"ROSETA TELEFONICA\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80186\",\n" +
                        "            \"Descripcion\": \"SELLO TIPO CANDADO MODELO 8001\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16639\",\n" +
                        "            \"Descripcion\": \"TAQUETE PLASTICO 1/4\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"70095\",\n" +
                        "            \"Descripcion\": \"TOALLAS ANTIBACTERIALES  PAQ C/80\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16067\",\n" +
                        "            \"Descripcion\": \"TOALLITAS SECAS KIMWIPE C/280 TOALLAS\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16102\",\n" +
                        "            \"Descripcion\": \"TUBO UNIREPID(PEGAMENTO) MCA. COMEX\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"17986\",\n" +
                        "            \"Descripcion\": \"WELCOME KITS FOLDER\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16024\",\n" +
                        "            \"Descripcion\": \"TEL ALAMBRICO C/MANOS LIB E IDEN TEL-220\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16111\",\n" +
                        "            \"Descripcion\": \"GRAPA P/CABLE PLANO DE F.OPTICA DE .250\\\"\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16175\",\n" +
                        "            \"Descripcion\": \"PASACABLE PLASTICO BEIGE P/MURO\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16013\",\n" +
                        "            \"Descripcion\": \"ROSETA EMPRESARIAL\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16445\",\n" +
                        "            \"Descripcion\": \"CONTROL REMOTO NEGRO NETGEM\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16904\",\n" +
                        "            \"Descripcion\": \"CONTROL REMOTO SMK\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80228\",\n" +
                        "            \"Descripcion\": \"Control Remoto Temporal SMK\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80183\",\n" +
                        "            \"Descripcion\": \"CONTROL REMOTO MAMBO NETFLIX\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80229\",\n" +
                        "            \"Descripcion\": \"CONTROL LOGITECH F310 PC ALAMBRICO\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80227\",\n" +
                        "            \"Descripcion\": \"CONTROL REMOTO TPSMK_BASE\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80226\",\n" +
                        "            \"Descripcion\": \"CONTROL REMOTO TPSMK_NFX\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16012\",\n" +
                        "            \"Descripcion\": \"ROSETA RESIDENCIAL CASTEL PLAST\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16355\",\n" +
                        "            \"Descripcion\": \"ROSETA RESIDENCIAL SAMSUNG\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"80178\",\n" +
                        "            \"Descripcion\": \"ROSETA RESIDENCIAL TP TII\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"17972\",\n" +
                        "            \"Descripcion\": \"TENSOR CASTEL PLAST\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"Material\": \"16968\",\n" +
                        "            \"Descripcion\": \"TENSOR LOOP CROSSOPT\",\n" +
                        "            \"Lot\": null,\n" +
                        "            \"Quantity\": null,\n" +
                        "            \"Price\": null,\n" +
                        "            \"Funit\": \"PZA\"\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}";
                break;
            case WS.CONFIG_SMD:
                json = "{\n" +
                        "    \"Result\": {\n" +
                        "        \"IdResult\": \"45124798\",\n" +
                        "        \"result\": \"0\",\n" +
                        "        \"Description\": \"GetConfigElement exitosa en SDM\"\n" +
                        "    },\n" +
                        "    \"AtributeList\": [\n" +
                        "        {\n" +
                        "            \"name\": \"minAnswerSeizureRatio\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"localresponseMap\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"tcp-keepalive\",\n" +
                        "            \"value\": \"none\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"loadBalanceDnsQuery\",\n" +
                        "            \"value\": \"hunt\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"noResponseTo\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"pingMethod\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"redirectAct\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxOutbBurstRate\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"inManipulationId\",\n" +
                        "            \"value\": \"add_iptel\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"sessionRecordingServerTarget\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"allowNextHop\",\n" +
                        "            \"value\": \"enabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"inServicePeriod\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxInbBurstRate\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"description\",\n" +
                        "            \"value\": \"FUTURO EN FARMACIAS SA DE CV\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"referCallTransfer\",\n" +
                        "            \"value\": \"disabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"inclCarrierAs\",\n" +
                        "            \"value\": \"None\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"appType\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"realmID\",\n" +
                        "            \"value\": \"GPON_TK\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"looseRouter\",\n" +
                        "            \"value\": \"enabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"hmrString\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"appProtocol\",\n" +
                        "            \"value\": \"SIP\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"kpmlInterworking\",\n" +
                        "            \"value\": \"inherit\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxInbSustainedRate\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"splOptions\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"hostname\",\n" +
                        "            \"value\": \"10.1.191.135\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"pingSendMode\",\n" +
                        "            \"value\": \"keep-alive\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"sustainedWindow\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"minSeizure\",\n" +
                        "            \"value\": \"5\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxOutbSustainedRate\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"pingToUserPart\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxSustainedRate\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"lastModifiedBy\",\n" +
                        "            \"value\": \"NNC_ITprovisioning_10.216.16.234\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"outServiceCodes\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"trustMeForLI\",\n" +
                        "            \"value\": \"disabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"sipRecursionPolicy\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"rfc2833-payload\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"port\",\n" +
                        "            \"value\": \"5060\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"cacTrapThreshold\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"sipProfile\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"manipPattern\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"rfc2833-mode\",\n" +
                        "            \"value\": \"none\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"precedence\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"trustMe\",\n" +
                        "            \"value\": \"disabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"state\",\n" +
                        "            \"value\": \"enabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"sessionRecordingRequired\",\n" +
                        "            \"value\": \"disabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxRegisterSustainedRate\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"egressRealmID\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"sessionMaxLifeLimit\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"inTranslationId\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"codecPolicy\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"registerBurstWindow\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"dataVersion\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"pingAllAddresses\",\n" +
                        "            \"value\": \"disabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxNumSessions\",\n" +
                        "            \"value\": \"8\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"sipIsupProfile\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxOutbSessions\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"pingInterval\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"reuse-connections\",\n" +
                        "            \"value\": \"NONE\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"options\",\n" +
                        "            \"value\": \"preserve-user-info-sa\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"ipAddress\",\n" +
                        "            \"value\": \"10.1.191.135\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxInbSessions\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"stopRecurse\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"pingFromUserPart\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxRegisterBurstRate\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"responseMap\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"burstWindow\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"allocMedia\",\n" +
                        "            \"value\": \"enabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"useConstraints\",\n" +
                        "            \"value\": \"enabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"invalidateRegistrations\",\n" +
                        "            \"value\": \"disabled\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"inServiceCodes\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"outManipulationId\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"proxyMode\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"transMethod\",\n" +
                        "            \"value\": \"UDP\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"maxBurstRate\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"referNotifyProvisional\",\n" +
                        "            \"value\": \"none\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"enforcementProfile\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"outTranslationId\",\n" +
                        "            \"value\": \"remove8\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"timeToResume\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"assertedID\",\n" +
                        "            \"value\": null\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"lastModifiedDate\",\n" +
                        "            \"value\": \"2017-04-01 06:00:02\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"tcp-reconn-interval\",\n" +
                        "            \"value\": \"0\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"earlyMediaAllow\",\n" +
                        "            \"value\": null\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"GetProxyOutput\": {\n" +
                        "        \"ESP_SBCNAME\": \"Querétaro\",\n" +
                        "        \"ESP_SBCPROXY\": \"10.187.128.19\"\n" +
                        "    },\n" +
                        "    \"InfoClienteSDM\": {\n" +
                        "        \"CuentaCliente\": \"0200103846\",\n" +
                        "        \"NombreCliente\": \"FUTURO EN FARMACIAS SA DE CV\",\n" +
                        "        \"TargetName\": \"SBCQRO1_SBCQRO2\",\n" +
                        "        \"IPdeCliente\": \"10.1.191.135\",\n" +
                        "        \"Gateway\": \"10.1.160.1\",\n" +
                        "        \"Mask\": \"255.255.224.0\",\n" +
                        "        \"IPreference\": null\n" +
                        "    }\n" +
                        "}";
                break;
            case WS.SETTING_EQUIPMENT:
                json = "{\n" +
                        "    \"Result\": {\n" +
                        "        \"result\": \"0\",\n" +
                        "        \"resultDescripcion\": \"Equipos configurados con exito\"\n" +
                        "    }\n" +
                        "}";
                break;
            case WS.VALIDATE_APP:
                json = "{\n" +
                        "    \"Result\": \"1\",\n" +
                        "    \"DescriptionResult\": \"La cuenta no se encuentra facturando\",\n" +
                        "    \"DnAsignado\": null\n" +
                        "}";
                break;
            case WS.CASH_PAYMENT:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultDescripcion\": \"Exito insercion correcta\"\n" +
                        "}";
                break;
            case WS.CHANGE_STATUS:
            case WS.SAVE_ARRIVE_TIME:
            case WS.UPDATE_TIME:
            case WS.ACTIVATION:
            default:
                json = "{\n" +
                        "    \"result\": \"0\",\n" +
                        "    \"resultDescripcion\": \"Operacion exitosa\"\n" +
                        "}";
                break;
        }
        return json;
    }

}
