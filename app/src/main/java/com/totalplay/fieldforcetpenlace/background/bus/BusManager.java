package com.totalplay.fieldforcetpenlace.background.bus;


import org.greenrobot.eventbus.EventBus;

/**
 * Created by totalplay on 5/24/17.
 * FMPServiceProvider
 */

public class BusManager {

    private static EventBus eventBus;

    public BusManager() {
        eventBus = EventBus.getDefault();
    }

    public static void init() {
        eventBus = new EventBus();
    }

    public static void register(Object o) {
        checkIfValid();
        eventBus.register(o);
    }

    private static void checkIfValid() {
        if (eventBus == null) throw new IllegalStateException("Call BusManager.init() first");
    }

    public static void unregister(Object o) {
        checkIfValid();
        eventBus.unregister(o);
    }

    public static void post(Object o) {
        eventBus.post(o);
    }
}
