package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by totalplay on 11/24/16.
 */
public class UpdateDNsRequest extends BaseRequest {
    @SerializedName("IdPlanServicio")
    public String servicePlanId;
    @SerializedName("DNPrincipal")
    public String mainDN;
    @SerializedName("DNS")
    public ArrayList<DNRequest> dns = new ArrayList<>();
    @SerializedName("SRVMode")
    public String srvMode = "Router";
}
