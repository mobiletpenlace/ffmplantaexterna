package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;

/**
 * Created by reype on 18/10/2017.
 */

public class ValidaCuentaReponse implements Serializable, WSBaseResponseInterface {

    @SerializedName("Result")
    public String result;
    @SerializedName("DescriptionResult")
    public String descriptionResult;
    @SerializedName("DnAsignado")
    public String dnAsignado;
}
