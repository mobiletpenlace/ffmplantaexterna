package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by tlion93 on 21/07/17.
 */

public class SaveImageBDResponse extends BaseResponse {
    @SerializedName("ArrImage")
    public ArrImagens arrImagens;

    public class ArrImagens{
        @SerializedName("Image_info")
        public ArrayList<InfoImage> imagesInfo;

        public class InfoImage {
            @SerializedName("FSI_IDOT")
            public String idOt;
            @SerializedName("FSI_IDTECHNICAL")
            public String idTechical;
            @SerializedName("FSI_URL")
            public String urls;
            @SerializedName("FSI_REASON")
            public String motive;
            @SerializedName("FSI_LENGTH")
            public String longi;
            @SerializedName("FSI_LATITUDE")
            public String latitu;
            @SerializedName("FTI_TYPE")
            public String type;
        }

    }

}
