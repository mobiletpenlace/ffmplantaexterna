package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Flag;

import java.util.List;

import io.realm.RealmList;

/**
 * Created by NS-257 on 07/08/2017.
 * Enlace
 */

public class GetFlagResponse extends BaseResponse {

    @SerializedName("Flags")
    public List<FlagContent> flags;

    public class FlagContent {
        @SerializedName("Flag")
        public RealmList<Flag> flags;
    }

}
