package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseRequest;

import java.io.Serializable;

/**
 * Created by totalplay on 11/24/16.
 * Enlace
 */
public class BaseRequest extends WSBaseRequest implements Serializable {

    @SerializedName("Login")
    public AuthenticationRequest authenticationRequest = new AuthenticationRequest();

}
