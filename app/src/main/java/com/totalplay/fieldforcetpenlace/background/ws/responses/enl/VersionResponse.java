package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;


import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;

/**
 * Created by totalplay on 12/28/16.
 * FieldForceManagement-Android
 */
public class VersionResponse implements Serializable, WSBaseResponseInterface {

    public String version;

}
