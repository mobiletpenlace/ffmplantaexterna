package com.totalplay.fieldforcetpenlace.background.ws.definitions;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by totalplay on 12/28/16.
 * FieldForceManagement-Android
 */
public interface WebServicesAppDefinition {

    @GET("/api/app-version/")
    Call<ResponseBody> appVersion(@Query("app_code") String versionName);

    @FormUrlEncoded
    @POST("/rest_framework/log/")
    Call<ResponseBody> crash(@Field("code") String appCode, @Field("error") String crashDescription);

}
