package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.AutoFindResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InternetModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WifiModel;

/**
 * Created by totalplay on 11/24/16.
 * Enlace
 */
public class UpdateEquipmentRequest extends BaseRequest {

    @SerializedName("IdPlanServicio")
    public String servicePlanId = "";
    @SerializedName("Frame")
    public String frame = "";
    @SerializedName("MAC")
    public String mac = "";
    @SerializedName("NoSerie")
    public String serialNumber = "";
    @SerializedName("OLT")
    public String olt = "";
    @SerializedName("Puerto")
    public String port = "";
    @SerializedName("Slot")
    public String slot = "";
    @SerializedName("IdOLT")
    public String oltId = "";
    @SerializedName("SRV_Mode")
    public String srvMode = "Router";
    @SerializedName("Modelo_equipo")
    public String equipmentModel = "";

    public UpdateEquipmentRequest() {
    }

    public UpdateEquipmentRequest(AutoFindResponse autoFindResponse) {
        this.serialNumber = autoFindResponse.serialNumber;
        this.frame = autoFindResponse.frame;
        this.olt = autoFindResponse.olt;
        this.port = autoFindResponse.port;
        this.slot = autoFindResponse.slot;
        this.oltId = autoFindResponse.oltId;
    }

    public UpdateEquipmentRequest(InternetModel internetModel) {
        this.servicePlanId = internetModel.servicePlanId;
        this.frame = internetModel.frame;
        this.mac = internetModel.mac;
        this.serialNumber = internetModel.serialNumber;
        this.olt = internetModel.olt;
        this.port = internetModel.port;
        this.slot = internetModel.slot;
        this.oltId = internetModel.oltId;
        this.srvMode = internetModel.srvMode;
        this.equipmentModel = internetModel.equipmentModel.modelID;
    }

    public UpdateEquipmentRequest(WifiModel wifiModel) {
        this.mac = wifiModel.mac;
        this.servicePlanId = wifiModel.servicePlanId;
        this.serialNumber = wifiModel.serialNumber;
        this.equipmentModel = wifiModel.equipmentModel.modelID;
        this.srvMode = wifiModel.srvMode;
    }
}
