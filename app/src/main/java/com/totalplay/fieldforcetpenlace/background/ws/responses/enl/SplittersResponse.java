package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;

import java.util.ArrayList;

/**
 * Created by totalplay on 1/6/17.
 * FieldForceManagement-Android
 */
public class SplittersResponse extends BaseResponse {

    @SerializedName("empalmes")
    public SplitterObject splitter;

    public static class SplitterObject {
        @SerializedName("empalme")
        public ArrayList<Splitter> splitters;
    }
}
