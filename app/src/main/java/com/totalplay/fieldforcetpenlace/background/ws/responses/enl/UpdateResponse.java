package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;

/**
 * Created by NS-126 on 25/11/2016.
 * Enlace
 */
public class UpdateResponse implements Serializable, WSBaseResponseInterface {

    @SerializedName("Result")
    public Result result;

    public class Result {
        @SerializedName("result")
        public String result;
        @SerializedName(value = "resultDescripcion", alternate = {"resultDescription"})
        public String resultDescription;
    }
}