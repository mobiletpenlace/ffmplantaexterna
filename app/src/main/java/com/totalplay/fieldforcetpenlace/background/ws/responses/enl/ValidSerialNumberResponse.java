package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;

/**
 * Created by totalplay on 11/25/16.
 * Enlace
 */
public class ValidSerialNumberResponse implements Serializable, WSBaseResponseInterface {

    @SerializedName("Result")
    public Result result;

    public class Result {
        @SerializedName("Result")
        public String result;
        @SerializedName(value = "Description", alternate = {"resultDescription"})
        public String resultDescription;
    }

}
