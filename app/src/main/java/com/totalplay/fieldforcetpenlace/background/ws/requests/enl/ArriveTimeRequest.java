package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jorgehdezvilla on 20/09/17.
 * FFM
 */

public class ArriveTimeRequest extends BaseRequest {

    @SerializedName("IdOT")
    public String id;
    @SerializedName("Puntualidad")
    public String punctuality;

}
