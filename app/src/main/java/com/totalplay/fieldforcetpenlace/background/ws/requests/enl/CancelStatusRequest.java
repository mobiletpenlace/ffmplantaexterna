package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 11/16/16.
 * Enlace
 */

public class CancelStatusRequest extends BaseRequest {

    @SerializedName("Id_Ot")
    public String workOrderId = "";
    @SerializedName("Id_Operario")
    public String operatorId = "";
    @SerializedName("Estado")
    public String status = "";
    @SerializedName("Motivo")
    public String reason = "";
    @SerializedName("Latitud")
    public String latitude = "";
    @SerializedName("Longitud")
    public String longitude = "";
    @SerializedName("Origen")
    public String origin = "";
    @SerializedName("Comentarios")
    public String comment = "";

}
