package com.totalplay.fieldforcetpenlace.background.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetMacRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidSerialNumberRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMacResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ValidSerialNumberResponse;
import com.totalplay.fieldforcetpenlace.library.enums.SerialNumberErrorReason;

public class SerialNumberEquipmentTask extends AsyncTask<Object, Object, GetMacResponse> {

    private String serialNumber;
    private SerialNumberTaskListener serialNumberTaskListener;
    private SerialNumberErrorReason errorReason;
    private Context mContext;

    public SerialNumberEquipmentTask(Context context, String serialNumber, SerialNumberTaskListener serialNumberTaskListener) {
        this.serialNumber = serialNumber;
        this.serialNumberTaskListener = serialNumberTaskListener;
        errorReason = null;
        mContext = context;
    }

    @Override
    protected GetMacResponse doInBackground(Object... voids) {
        try {
            ValidSerialNumberResponse validSerialNumberResponse;
            GetMacResponse getMacResponse;

            BaseWSManager retrofitManager = WSManager.init().settings(mContext);

            ValidSerialNumberRequest validSerialNumberRequest = new ValidSerialNumberRequest(serialNumber);
            validSerialNumberResponse = (ValidSerialNumberResponse) retrofitManager.requestWsSync(ValidSerialNumberResponse.class, WSManager.WS.VALIDATE_SERIAL_NUMBER, WebServices.servicesEnlace().validSerialNumber(validSerialNumberRequest));

            if (validSerialNumberResponse != null) {
                if (validSerialNumberResponse.result == null) {
                    errorReason = SerialNumberErrorReason.COULD_NOT_VALID_SERIAL_NUMBER;
                    return null;
                } else if (validSerialNumberResponse.result.result.equals("1")) {
                    errorReason = SerialNumberErrorReason.SERIAL_NUMBER_ALREDY_EXISTS;
                    return null;
                }
            } else {
                errorReason = SerialNumberErrorReason.COULD_NOT_VALID_SERIAL_NUMBER;
                return null;
            }

            GetMacRequest getMacRequest = new GetMacRequest(serialNumber);
            getMacResponse = (GetMacResponse) retrofitManager.requestWsSync(GetMacResponse.class, WSManager.WS.GET_MAC, WebServices.servicesEnlace().getMac(getMacRequest));

            if (getMacResponse != null) {
                if (getMacResponse.mac == null) {
                    errorReason = SerialNumberErrorReason.COULD_NOT_GET_MAC;
                    return null;
                } else {
                    return getMacResponse;
                }
            } else {
                errorReason = SerialNumberErrorReason.COULD_NOT_GET_MAC;
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(GetMacResponse mac) {
        super.onPostExecute(mac);
        if (mac != null) {
            serialNumberTaskListener.onSuccessLoadedMac(mac);
        } else {
            serialNumberTaskListener.onErrorLoadedMac(errorReason);
        }
    }

    public interface SerialNumberTaskListener {
        void onSuccessLoadedMac(GetMacResponse mac);

        void onErrorLoadedMac(SerialNumberErrorReason errorReason);
    }
}
