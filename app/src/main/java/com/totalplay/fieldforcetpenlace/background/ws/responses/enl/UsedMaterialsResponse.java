package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Material;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by totalplay on 11/17/16.
 */
public class UsedMaterialsResponse {

    @SerializedName("Response")
    public BaseResponse response;
    @SerializedName("MaterialCatalgo")
    public MaterialCatalog materialCatalog;

    public class MaterialCatalog implements Serializable {
        @SerializedName("Material")
        public ArrayList<Material> materials;
    }

}
