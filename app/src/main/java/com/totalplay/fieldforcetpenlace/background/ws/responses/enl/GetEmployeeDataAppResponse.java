package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by NS-TOTALPLAY on 28/03/2017.
 */

public class GetEmployeeDataAppResponse  {

    @SerializedName("Result")
    public BaseResponse2 result;
    @SerializedName("Operador")
    public Operador operador;
    @SerializedName("Vehiculo")
    public Vehiculo vehiculo;

    public static class Operador implements Serializable {
        @SerializedName("Nombre")
        public String name;
        @SerializedName("Foto")
        public String foto;
        @SerializedName("Telefono")
        public String telehone;
        @SerializedName("Turno")
        public String turned;
        @SerializedName("Calificacion")
        public String calification;
    }

   public static class Vehiculo implements Serializable {
        @SerializedName("Modelo")
        public String models;
        @SerializedName("Color")
        public String colore;
        @SerializedName("Placas")
        public String placas;
    }

}
