package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 11/24/16.
 * Enlace
 */
public class DNRequest {

    @SerializedName("DN")
    public String dn;

    public DNRequest(String dn) {
        this.dn = dn;
    }
}
