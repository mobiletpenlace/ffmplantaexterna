package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;

/**
 * Created by NS-TOTALPLAY on 14/11/2016.
 * Enlace
 */

public class AutoFindResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("Result")
    public String result;
    @SerializedName("ResultDescription")
    public String resultDescription;
    @SerializedName("OLT")
    public String olt;
    @SerializedName("IdOLT")
    public String oltId;
    @SerializedName("IpOLT")
    public String oltIp;
    @SerializedName("Port")
    public String port;
    @SerializedName("Slot")
    public String slot;
    @SerializedName("Frame")
    public String frame;
    @SerializedName("SerialNumber")
    public String serialNumber;
    @SerializedName("IdModel")
    public String modelId;
    @SerializedName("Model")
    public String model;
    @SerializedName("Version")
    public String version;
    @Expose(serialize = false, deserialize = false)
    public String mac;
}
