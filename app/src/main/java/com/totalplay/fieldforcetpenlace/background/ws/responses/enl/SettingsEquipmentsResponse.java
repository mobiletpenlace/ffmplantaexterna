package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;

import java.util.List;

/**
 * Created by jorgehdezvilla on 19/12/17.
 * FFM Enlace
 */

public class SettingsEquipmentsResponse extends BaseResponse {

    @SerializedName("informacionEquipo")
    public List<InfoEquipment> dispositivesList;

}
