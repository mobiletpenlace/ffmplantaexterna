package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by charlssalazar on 27/06/17.
 * Enlace
 */

public class GetMaterials extends BaseResponse {

    @SerializedName("Materiales")
    public ArrayList<Materials> listMaterials = new ArrayList<>();

    public static class Materials {
        @SerializedName("Material")
        public String material;
        @SerializedName("Descripcion")
        public String description;
        @SerializedName("Lot")
        public String lot;
        @SerializedName("Quantity")
        public String quantity = "0";
        @SerializedName("Price")
        public String price;
        @SerializedName("Funit")
        public String funit;

    }
}
