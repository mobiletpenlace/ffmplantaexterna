package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 11/24/16.
 * FMM
 */
public class SaveWorkAssistantTeamRequest extends BaseRequest {

    @SerializedName("ID_Crew")
    public String crewId = "";
    @SerializedName("ID_Tecnico")
    public String operatorId = "";
    @SerializedName("ID_Vehicle")
    public String vehicleId = "";
    @SerializedName("ID_Auxiliar1")
    public String assistantOne = "";
    @SerializedName("ID_Auxiliar2")
    public String assistantTwo = "";
    @SerializedName("ID_Auxiliar3")
    public String assistantThree = "";
    @SerializedName("ID_Auxiliar4")
    public String assistantFour = "";

}
