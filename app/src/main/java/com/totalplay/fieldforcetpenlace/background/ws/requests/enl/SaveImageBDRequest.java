package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Image;

import java.util.List;

/**
 * Created by tlion93 on 21/07/17.
 * Enlace
 */

public class SaveImageBDRequest extends BaseRequest {
    @SerializedName("ArrImage")
    public ArrImagens arrImagens;

    public static class ArrImagens {
        @SerializedName("Image_info")
        public List<Image> imageInfo;

    }
}