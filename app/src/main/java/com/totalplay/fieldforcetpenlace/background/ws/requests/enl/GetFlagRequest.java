package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-257 on 07/08/2017.
 * Enlace
 */

public class GetFlagRequest extends BaseRequest {

    @SerializedName("ID_OT")
    public String wOId;

    public GetFlagRequest() {
    }

    public GetFlagRequest(String wOId) {
        this.wOId = wOId;
    }

}
