package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 12/19/16.
 */
public class ReferenceUploadResponse {


    private ArrImageBean ArrImage;

    public ArrImageBean getArrImage() {
        return ArrImage;
    }

    public void setArrImage(ArrImageBean ArrImage) {
        this.ArrImage = ArrImage;
    }

    private static class ArrImageBean {
        @SerializedName("Image_info")
        private List<ImageInfoBean> imageInfo;

        public List<ImageInfoBean> getImageInfo() {
            return imageInfo;
        }

        public void setImageInfo(List<ImageInfoBean> imageInfo) {
            this.imageInfo = imageInfo;
        }

        static class ImageInfoBean {


            private String name;
            private String url;
            @SerializedName("ot_id")
            private String otId;
            @SerializedName("status_id")
            private String statusId;
            private String description;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getOtId() {
                return otId;
            }

            public void setOtId(String otId) {
                this.otId = otId;
            }

            public String getStatusId() {
                return statusId;
            }

            public void setStatusId(String statusId) {
                this.statusId = statusId;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
        }
    }
}
