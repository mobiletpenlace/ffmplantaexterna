package com.totalplay.fieldforcetpenlace.background.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

/**
 * Created by app on 1/11/17.
 * EMP
 */
public class NotificationUtils {

    private static NotificationCompat.Builder createDefaultBuilder(Context context,
                                                                   @DrawableRes int icon,
                                                                   String title,
                                                                   String message,
                                                                   boolean sound,
                                                                   PendingIntent pendingIntent) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(icon)
                        .setContentTitle(title)
                        .setContentText(message);

        if (pendingIntent != null) {
            builder.setContentIntent(pendingIntent);
        }
        builder.setLights(Color.RED, 3000, 3000);
        if (sound) {
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setSound(alarmSound);
        }
        builder.setAutoCancel(true);
        return builder;
    }

    public static void simpleNotification(Context context,
                                          @DrawableRes int icon,
                                          @NonNull String title,
                                          @NonNull String body) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(context.getPackageName());
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder builder = createDefaultBuilder(context, icon, title, body, true, pendingIntent);

        builder.setAutoCancel(true);
        notificationManager.notify(1, builder.build());
    }

}