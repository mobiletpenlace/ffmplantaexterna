package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Assistant;

import java.io.Serializable;

import io.realm.RealmList;

/**
 * Created by totalplay on 11/22/16.
 * FFM
 */
public class AssistantResponse implements Serializable, WSBaseResponseInterface {

    public String result;
    @SerializedName("resultdescription")
    public String resultDescription;
    @SerializedName("Nombre_Operario")
    public String operatorName;
    @SerializedName("Auxiliares")
    public RealmList<Assistant> assistants;

}
