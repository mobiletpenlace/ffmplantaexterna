package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

import java.util.StringTokenizer;

/**
 * Created by NS-TOTALPLAY on 17/04/2017.
 * FFM
 */

public class FindSplitterAppRequest extends BaseRequest {

    @SerializedName("nombreEmpalme")
    public String id = "";
    @SerializedName("puertos_totales")
    public String totalPorts = "";
    @SerializedName("puertos_ocupados")
    public String occupedPorts = "";
    @SerializedName("puerto_asignado")
    public String assignedPort = "";
    @SerializedName("latitud")
    public String latitude = "";
    @SerializedName("longitud")
    public String longitude = "";
    @SerializedName("candado")
    public String candado = "";
    @SerializedName("numero_cuenta")
    public String num_Acount = "";
    @SerializedName("usuario")
    public String user = "";
    @SerializedName("StatusSplitter")
    public String status = "";

    public FindSplitterAppRequest() {
    }

    public FindSplitterAppRequest(String id, String totalPorts, String occupedPorts,
                                  String latitude, String longitude, String user, String status) {
        this.id = id;
        this.totalPorts = totalPorts;
        this.occupedPorts = occupedPorts;
        this.assignedPort = "100";
        this.latitude = latitude;
        this.longitude = longitude;
        this.candado = " ";
        this.num_Acount = "0";
        this.user = user;
        this.status = status;
    }

    public FindSplitterAppRequest(String id, String totalPorts, String occupiedPorts,
                                  String assignedPort, Location location, String padLock,
                                  String accountNumber, String operatorId, String status) {

        String latitude = String.valueOf(location.getLatitude());
        String longitude = String.valueOf(location.getLongitude());
        StringTokenizer token = new StringTokenizer(latitude, ".");
        if (latitude.substring(latitude.indexOf(".")).length() > 11) {
            latitude = token.nextToken() +
                    latitude.substring(latitude.indexOf(".")).substring(0, 12);
        }
        token = new StringTokenizer(longitude, ".");
        if (longitude.substring(longitude.indexOf(".")).length() > 11) {
            longitude = token.nextToken() +
                    longitude.substring(longitude.indexOf(".")).substring(0, 12);
        }

        this.id = id;
        this.totalPorts = totalPorts;
        this.occupedPorts = occupiedPorts;
        this.assignedPort = assignedPort;
        this.latitude = latitude;
        this.longitude = longitude;
        this.candado = padLock;
        this.num_Acount = accountNumber;
        this.user = operatorId;
        this.status = status;
    }
}
