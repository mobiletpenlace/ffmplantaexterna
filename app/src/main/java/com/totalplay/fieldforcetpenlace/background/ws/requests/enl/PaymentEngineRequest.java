package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.library.enums.BusssinesType;

/**
 * Created by totalplay on 11/28/16.
 * Total
 */
@SuppressWarnings("WeakerAccess")
public class PaymentEngineRequest extends BaseRequest {

    @SerializedName("NewCard")
    public NewCard newCard;
    @SerializedName("ChargeModel")
    public ChargeModel chargeModel;

    public PaymentEngineRequest() {
    }

    public PaymentEngineRequest(String cardNumber, String cvv2, String expirationMonth,
                                String expirationYear, String paymentType, String amount, String accountNum) {
        this.newCard = new NewCard(cardNumber, cvv2, expirationMonth, expirationYear);
        this.chargeModel = new ChargeModel(paymentType, amount, accountNum);
    }

    public class NewCard {

        public NewCard(String cardNumber, String cvv2, String expirationMonth,
                       String expirationYear) {
            this.saveCard = "false";
            this.cardNumber = cardNumber;
            this.cvv2 = cvv2;
            this.expirationMonth = expirationMonth;
            this.expirationYear = expirationYear;
        }

        @SerializedName("blSaveCard")
        public String saveCard;
        @SerializedName("cardNumber")
        public String cardNumber;
        @SerializedName("cvv2")
        public String cvv2;
        @SerializedName("expirationMonth")
        public String expirationMonth;
        @SerializedName("expirationYear")
        public String expirationYear;
    }

    public class ChargeModel {

        public ChargeModel(String paymentType, String amount, String accountNum) {
            this.accountNum = accountNum;
            this.paymentType = paymentType;
            this.clientIp = "127.0.0.1";
            this.bussinesId = BusssinesType.TOTAL_PLAY;
            this.amount = amount;
            this.monthsNoInterest = "0";
        }

        @SerializedName("accountNo")
        public String accountNum;
        @SerializedName("amount")
        public String amount;
        @SerializedName("monthsNoInterest")
        public String monthsNoInterest;
        @SerializedName("bussinesId")
        public String bussinesId;
        @SerializedName("clientIp")
        public String clientIp;
        @SerializedName("paymentType")
        public String paymentType;
    }

}
