package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jorgehdezvilla on 02/10/17.
 * Enlace
 */

public class InfoEquipmentRequest extends BaseRequest {

    @SerializedName("numeroCuenta")
    public String accountMNumber;

    public InfoEquipmentRequest(String accountMNumber) {
        this.accountMNumber = accountMNumber;
    }

}
