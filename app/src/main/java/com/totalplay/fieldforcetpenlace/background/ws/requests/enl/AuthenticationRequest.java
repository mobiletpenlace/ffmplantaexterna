package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseRequest;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.utils.Prefs;

import java.io.Serializable;

/**
 * Created by NS-TOTALPLAY on 14/11/2016.
 * Enlace
 */
@SuppressWarnings("WeakerAccess")
public class AuthenticationRequest extends WSBaseRequest implements Serializable {

    public String usuario;
    @SerializedName("pass")
    public String validation;
    public String userId;
    public String user;
    @SerializedName("password")
    public String validation2;
    public String ip;
    public String deviceId;

    @SerializedName("User")
    public String user_2;
    @SerializedName("UserId")
    public String user_3;
    @SerializedName("Password")
    public String validation3;
    @SerializedName("Ip")
    public String ip_2;
    @SerializedName("IP")
    public String ip_4;

    public AuthenticationRequest() {
        String userId = "25631";
        String validation = "Middle100$";
        String ip = "10.216.48.43";
        String deviceId = Prefs.instance().string(Keys.FCM_TOKEN);
        this.userId = userId;
        this.user = userId;
        this.validation2 = validation;
        this.ip = ip;
        this.user_2 = userId;
        this.user_3 = userId;
        this.validation3 = validation;
        this.ip_2 = ip;
        this.deviceId = deviceId;
        this.usuario = userId;
        this.validation = validation;
        this.ip_4 = ip;
    }

    public AuthenticationRequest(String userId, String validation, String ip, String deviceId) {

        this.userId = userId;
        this.user = userId;
        this.usuario = userId;
        this.validation2 = validation;
        this.validation = validation;
        this.ip = ip;
        this.user_2 = userId;
        this.user_3 = userId;
        this.validation3 = validation;
        this.ip_2 = ip;
        this.deviceId = deviceId;
        this.ip_4 = ip;
    }
}
