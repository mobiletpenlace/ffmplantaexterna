package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 7/10/17.
 * FieldForceManagement
 */

public class ValidWorkOrderResponse extends BaseResponse {

    @SerializedName("ID_Estado")
    public String statusId;
    @SerializedName("Estado")
    public String status;
    @SerializedName("ID_Motivo")
    public String reasonId;
    @SerializedName("Motivo")
    public String reason;
    @SerializedName("Comentario")
    public String comment;

}
