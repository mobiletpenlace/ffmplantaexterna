package com.totalplay.fieldforcetpenlace.background.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AutoFindRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetMacRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidSerialNumberRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.AutoFindResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMacResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ValidSerialNumberResponse;

/**
 * Created by totalplay on 11/25/16.
 * Enlace
 */

public class AutoFindTask extends AsyncTask<Void, Void, AutoFindResponse> {

    private final String serialNumber;
    private final AutoFindListener autoFindListener;
    public String errorReason;
    public Context mContext;
    private BaseWSManager retrofitManager;

    public AutoFindTask(Context context, String serialNumber, AutoFindListener autoFindListener) {
        this.serialNumber = serialNumber;
        this.autoFindListener = autoFindListener;
        this.mContext = context;
        retrofitManager = WSManager.init().settings(mContext);
        errorReason = null;
    }

    @Override
    protected AutoFindResponse doInBackground(Void... voids) {
        try {
            AutoFindResponse autoFindResponse;
            ValidSerialNumberResponse validSerialNumberResponse;
            GetMacResponse getMacResponse;

            AutoFindRequest autoFindRequest = new AutoFindRequest(serialNumber);
            autoFindResponse = (AutoFindResponse) retrofitManager.requestWsSync(AutoFindResponse.class, WSManager.WS.AUTO_FIND, WebServices.servicesEnlace().autoFind(autoFindRequest));

            if (autoFindResponse != null) {
                if (!autoFindResponse.result.equals("0")) {
                    errorReason = autoFindResponse.resultDescription;
                    return null;
                }
            } else return null;

            ValidSerialNumberRequest validSerialNumberRequest = new ValidSerialNumberRequest(serialNumber);
            validSerialNumberResponse = (ValidSerialNumberResponse) retrofitManager.requestWsSync(ValidSerialNumberResponse.class, WSManager.WS.VALIDATE_SERIAL_NUMBER, WebServices.servicesEnlace().validSerialNumber(validSerialNumberRequest));
            if (validSerialNumberResponse != null) {
                if (validSerialNumberResponse.result == null) {
                    return null;
                } else if (validSerialNumberResponse.result.result.equals("1")) {
                    errorReason = validSerialNumberResponse.result.resultDescription;
                    return null;
                }
            } else return null;


            GetMacRequest getMacRequest = new GetMacRequest(serialNumber);
            getMacResponse = (GetMacResponse) retrofitManager.requestWsSync(GetMacResponse.class, WSManager.WS.GET_MAC, WebServices.servicesEnlace().getMac(getMacRequest));
            if (getMacResponse != null) {
                if (getMacResponse.mac == null) {
//                    if (getMacResponse.message != null) {
//                        errorReason = getMacResponse.message;
//                    } else {
//                        errorReason = "No se pudo recuperar la mac, valide la información";
//                    }
//                    return null;
                } else {
                    autoFindResponse.mac = getMacResponse.mac;
                }
            }

            return autoFindResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(AutoFindResponse autoFindResponse) {
        super.onPostExecute(autoFindResponse);
        if (autoFindResponse != null) {
            autoFindListener.onSuccessAutoFind(autoFindResponse);
        } else {
            autoFindListener.onErrorAutoFind(errorReason);
        }
    }

    public interface AutoFindListener {
        void onSuccessAutoFind(AutoFindResponse autoFindResponse);

        void onErrorAutoFind(String errorReason);
    }
}
