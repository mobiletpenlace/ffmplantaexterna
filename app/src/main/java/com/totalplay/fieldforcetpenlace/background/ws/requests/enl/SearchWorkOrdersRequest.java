package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-262 on 17/11/2016.
 * Enlace
 */

public class SearchWorkOrdersRequest extends BaseRequest {

    @SerializedName("numeroEmpleado")
    public String employeeUser;

    public SearchWorkOrdersRequest() {
    }

    public SearchWorkOrdersRequest(String employeeUser) {
        this.employeeUser = employeeUser;
    }
}
