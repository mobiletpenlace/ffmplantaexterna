package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by totalplay on 11/24/16.
 * Enlace
 */

public class EquipmentRequest extends BaseRequest {

    @SerializedName("id_propietario")
    public String ownerId;
    @SerializedName("id_cots_planes")
    public ServicePlanId servicesPlansIds;

    public EquipmentRequest() {
    }

    public EquipmentRequest(String ownerId, ServicePlanId servicesPlansIds) {
        this.ownerId = ownerId;
        this.servicesPlansIds = servicesPlansIds;
    }

    public static class ServicePlanId {
        @SerializedName("IdCotPlanServicio")
        public String[] id;

        public ServicePlanId(List<String> servicePlanId) {
            this.id = servicePlanId.toArray(new String[0]);
        }
    }

}
