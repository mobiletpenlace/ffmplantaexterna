package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 11/22/16.
 * FFM
 */
public class AssistantsRequest extends BaseRequest {

    @SerializedName("ID_Operario")
    public String operatorId;

}
