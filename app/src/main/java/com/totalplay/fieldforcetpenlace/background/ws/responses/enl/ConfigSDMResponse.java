package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jorgehdezvilla on 05/10/17.
 * FMP
 */

public class ConfigSDMResponse implements Serializable, WSBaseResponseInterface {

    @SerializedName("Result")
    public BaseResponse response;
    @SerializedName("AtributeList")
    public ArrayList<AttributeList> attributeList = new ArrayList<>();
    @SerializedName("GetProxyOutput")
    public GetProxyOutput getProxyOutput;
    @SerializedName("InfoClienteSDM")
    public InfoClienteSDM infoClienteSDM;

    public class AttributeList implements Serializable {
        @SerializedName("name")
        public String name;
        @SerializedName("value")
        public String value;
    }

    public class GetProxyOutput {
        @SerializedName("ESP_SBCNAME")
        public String espSbcname;
        @SerializedName("ESP_SBCPROXY")
        public String espSbcproxy;
    }

    public class InfoClienteSDM {
        @SerializedName("CuentaCliente")
        public String cuentaCliente;
        @SerializedName("NombreCliente")
        public String nombreCliente;
        @SerializedName("TargetName")
        public String targetName;
        @SerializedName("IPdeCliente")
        public String ipdeCliente;
        @SerializedName("Gateway")
        public String gateway;
        @SerializedName("Mask")
        public String mask;
        @SerializedName("IPreference")
        public String iPreference;
    }

}
