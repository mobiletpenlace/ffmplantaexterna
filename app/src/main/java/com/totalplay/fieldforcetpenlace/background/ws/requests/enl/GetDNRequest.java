package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-262 on 09/11/2016.
 */

public class GetDNRequest extends BaseRequest {

    @SerializedName("CantidadDn")
    public String amount;
    @SerializedName("CodigoPostal")
    public String zipCode;

    public GetDNRequest() {
    }

    public GetDNRequest(String amount, String zipCode) {
        this.amount = amount;
        this.zipCode = zipCode;
    }
}
