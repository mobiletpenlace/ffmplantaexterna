package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.library.utils.DateUtils;

/**
 * Created by totalplay on 1/19/17.
 * FieldForceManagement-Android
 */
public class CashPaymentEngineRequest extends BaseRequest {

    @SerializedName("idOT")
    public String workOrderId;
    @SerializedName("idOperario")
    public String operatorId;
    @SerializedName("idOrigen")
    public String origin;
    @SerializedName("monto")
    public String amount;
    @SerializedName("fecha_captura")
    public String captureDate;

    public CashPaymentEngineRequest() {
    }

    public CashPaymentEngineRequest(String workOrderId, String amount, String origin, String operatorId) {
        this.workOrderId = workOrderId;
        this.amount = amount;
        this.origin = origin;
        this.operatorId = operatorId;
        captureDate = DateUtils.dateNow();
    }
}
