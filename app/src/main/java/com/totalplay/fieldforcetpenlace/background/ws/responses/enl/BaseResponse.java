package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;

/**
 * Created by NS-TOTALPLAY on 14/11/2016.
 * FFM
 */

@SuppressWarnings("unused")
public class BaseResponse implements Serializable, WSBaseResponseInterface {

    public String result;
    @SerializedName(value = "resultDescription", alternate = {"resultdescription", "Description", "resultDescripcion"})
    public String resultDescription;
    public String id;
    public String idResult;
    public String code;
    public String description;
    @SerializedName("id_result")
    public String resultId;
    @SerializedName("ress")
    public String result2;
    @SerializedName("resDescription")
    public String resultDescription2;

    // time arrival
    public String idresult;

    @SerializedName("direccionOrigen")
    public String directionSource;
    @SerializedName("direccionDestino")
    public String directionDestination;
    @SerializedName("tiempoTrasladoTrafico")
    public String timeTransferTrafic;

    // cash payment
    @SerializedName("idrest")
    public String resultId2;
    @SerializedName("rest")
    public String result3;


}
