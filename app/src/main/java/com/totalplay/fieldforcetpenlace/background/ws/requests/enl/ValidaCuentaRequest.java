package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by reype on 18/10/2017.
 * Enlace
 */

public class ValidaCuentaRequest extends BaseRequest {

    @SerializedName("Cuenta")
    public String account;

    public ValidaCuentaRequest() {
    }

    public ValidaCuentaRequest(String account) {this.account = account;}
}
