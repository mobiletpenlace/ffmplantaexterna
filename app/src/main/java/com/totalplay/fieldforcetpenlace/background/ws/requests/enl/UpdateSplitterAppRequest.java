package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

import java.util.StringTokenizer;

/**
 * Created by NS-TOTALPLAY on 17/04/2017.
 * Enlace
 */

public class UpdateSplitterAppRequest extends BaseRequest {

    public String id;
    @SerializedName("puertos_totales")
    public String totalPorts;
    @SerializedName("puertos_ocupados")
    public String occupiedPorts;
    @SerializedName("puerto_asignado")
    public String assignedPort;
    @SerializedName("latitud")
    public String latitude;
    @SerializedName("longitud")
    public String longitude;
    @SerializedName("candado")
    public String lock;
    @SerializedName("numero_cuenta")
    public String accountNumber;
    @SerializedName("usuario")
    public String user;

    public UpdateSplitterAppRequest() {
    }

    public UpdateSplitterAppRequest(String id, String totalPorts, String occupiedPorts, String assignedPort, Location location, String padLock, String accountNumber, String operatorId) {
        String latitude = String.valueOf(location.getLatitude());
        String longitude = String.valueOf(location.getLongitude());
        StringTokenizer token = new StringTokenizer(latitude, ".");
        if (latitude.substring(latitude.indexOf(".")).length() > 11) {
            latitude = token.nextToken() +
                    latitude.substring(latitude.indexOf(".")).substring(0, 12);
        }
        token = new StringTokenizer(longitude, ".");
        if (longitude.substring(longitude.indexOf(".")).length() > 11) {
            longitude = token.nextToken() +
                    longitude.substring(longitude.indexOf(".")).substring(0, 12);
        }

        this.id = id;
        this.totalPorts = totalPorts;
        this.occupiedPorts = occupiedPorts;
        this.assignedPort = assignedPort;
        this.latitude = latitude;
        this.longitude = longitude;
        this.lock = padLock;
        this.accountNumber = accountNumber;
        this.user = operatorId;
    }
}
