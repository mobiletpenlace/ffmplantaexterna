package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 27/06/17.
 * Enlace
 */

public class GetMaterialRequest extends BaseRequest {

    @SerializedName("idOperator")
    public String idOperator;

    public GetMaterialRequest() {
    }

    public GetMaterialRequest(String idOperator) {
        this.idOperator = idOperator;
    }

}
