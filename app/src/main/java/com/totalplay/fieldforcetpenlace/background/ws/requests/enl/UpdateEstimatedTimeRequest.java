package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-TOTALPLAY on 02/02/2017.
 */

public class UpdateEstimatedTimeRequest extends BaseRequest {

    @SerializedName("idOt")
    public String workOrderId;
    @SerializedName("idOperario")
    public String userId;
    @SerializedName("tiempo")
    public String minutes;

}
