package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 11/24/16.
 * Enlace
 */
public class ActivationRequest extends BaseRequest {

    @SerializedName("id_ot")
    public String idOt;
    @SerializedName("comentarios")
    public String comments;

    public ActivationRequest() {
    }

    public ActivationRequest(String idOt, String comments) {
        this.idOt = idOt;
        this.comments = comments;
    }

}
