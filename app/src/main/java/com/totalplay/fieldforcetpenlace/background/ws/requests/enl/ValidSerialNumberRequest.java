package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 11/25/16.
 */
public class ValidSerialNumberRequest extends BaseRequest {

    @SerializedName("SN")
    public String serialNumber;

    public ValidSerialNumberRequest() {
    }

    public ValidSerialNumberRequest(String serialNumber) {

        this.serialNumber = serialNumber;
    }
}
