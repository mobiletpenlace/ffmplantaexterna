package com.totalplay.fieldforcetpenlace.background.bus.events;

import android.location.Location;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

public class LocationUpdatedEvent {
    public Location location;

    public LocationUpdatedEvent(Location location) {
        this.location = location;
    }
}
