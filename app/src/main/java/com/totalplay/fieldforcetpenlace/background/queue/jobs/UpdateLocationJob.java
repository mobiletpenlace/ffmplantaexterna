package com.totalplay.fieldforcetpenlace.background.queue.jobs;

import android.location.Location;

import com.birbit.android.jobqueue.Params;
import com.totalplay.fieldforcetpenlace.background.queue.utils.JobPriority;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateLocationRequest;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.utils.Prefs;

import java.io.IOException;

/**
 * Created by totalplay on 6/9/17.
 * FMPServiceProvider
 */

public class UpdateLocationJob extends DataJob {

    public UpdateLocationJob() {
        super(new Params(JobPriority.HIGH).persist());
    }

    @Override
    public void onRun() throws Throwable {
        Location location = TotalPlayLocationService.getLastLocation();
        User user = (User) Prefs.instance().object(Keys.EXTRA_USER, User.class);
        try {
            if (location != null) {
                if (user != null && user.operatorId != null) {
                    WebServices.servicesEnlace().updateLocation(new UpdateLocationRequest(user.operatorId,
                            String.valueOf(location.getLatitude())
                            , String.valueOf(location.getLongitude()))).execute().body();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
