package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 7/10/17.
 * FieldForceManagement
 */

public class ValidWorkOrderRequest extends BaseRequest {

    @SerializedName("IDOT")
    public String id;

    public ValidWorkOrderRequest() {
    }

    public ValidWorkOrderRequest(String id) {
        this.id = id;
    }

}
