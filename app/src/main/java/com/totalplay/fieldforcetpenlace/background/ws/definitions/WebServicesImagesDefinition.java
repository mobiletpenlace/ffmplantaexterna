package com.totalplay.fieldforcetpenlace.background.ws.definitions;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by totalplay on 7/18/17.
 * FieldForceManagement
 */

public interface WebServicesImagesDefinition {

    @Multipart
    @POST("/FFMTpe/WriteFile")
    Call<ResponseBody> writeFile(@Part MultipartBody.Part filePart, @Part("FilePath") RequestBody path);

    @Multipart
    @POST("/FFM/UploadFile")
    Call<ResponseBody> uploadFile(@Part MultipartBody.Part filePart, @Part("FilePath") RequestBody path);

    @GET("/TFE/DownloadFiles/dGZldXNlcjp0ZjNwNHNzdzByZA==/{url}") //DownloadFiles para desarrollo y DownloadFile para producción
    Call<ResponseBody> downloadFiles(@Path("url") String url);

    @GET("/TFE/DownloadFile/dGZldXNlcjp0ZjNwNHNzdzByZA==/{url}") //DownloadFiles para desarrollo y DownloadFile para producción
    Call<ResponseBody> downloadFile(@Path("url") String url);
}
