package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.library.enums.OriginAppEnum;

/**
 * Created by jorgehdezvilla on 12/11/17.
 * Enlace
 */

public class ChangeOperatorRequest extends BaseRequest {

    @SerializedName("idOperario")
    public String operatorId;
    @SerializedName("idEstado")
    public String stateId;
    @SerializedName("Comentarios")
    public String comments;
    @SerializedName("ID_Origen")
    public String origin = OriginAppEnum.ORIGIN_APP;
    @SerializedName("ID_UserModification")
    public String userIdUpdate;

}
