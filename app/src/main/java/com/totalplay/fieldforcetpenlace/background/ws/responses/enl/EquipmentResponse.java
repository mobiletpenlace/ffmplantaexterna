package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 11/24/16.
 * Enlace
 */
public class EquipmentResponse implements WSBaseResponseInterface, Serializable {

    @PrimaryKey
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("result")
    public String result;
    @SerializedName("resultDescripcion")
    public String resultDescription;


    @SerializedName("InfoEquipos")
    public RealmList<Equipment> equipments;

}
