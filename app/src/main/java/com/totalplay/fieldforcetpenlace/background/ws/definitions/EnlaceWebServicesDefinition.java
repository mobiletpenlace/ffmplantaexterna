package com.totalplay.fieldforcetpenlace.background.ws.definitions;

import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ActivationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ArriveTimeRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AssistantsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AuthenticationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AutoFindRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.CancelStatusRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.CashPaymentEngineRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ChangeOperatorRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ChangeStatusRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.CompletedWorkOrderRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ConfigSDMRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.DiscountMaterialsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.EquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.FindSplitterAppRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetDNRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetFlagRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetMacRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetMaterialRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.InfoEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.PaymentEngineRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.QueryWorkTeamRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.QuotationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SaveImageBDRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SaveWorkAssistantTeamRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SearchWorkOrdersRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SetFlagRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SettingEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SplittersRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.TestRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateDNsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateEstimatedTimeRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateLocationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateSplitterAppRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidSerialNumberRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidWorkOrderRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidaCuentaRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.SaveImageBDResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by NS-262 on 09/11/2016.
 * FFM
 */

public interface EnlaceWebServicesDefinition {

    @POST("/FFMTpeEnlace/LoginUserApp_ENL")
    Call<ResponseBody> login(@Body AuthenticationRequest authenticationRequest);

    @POST("/FFMTpeEnlace/ConsultaOtApp_Enlace")
    Call<ResponseBody> workOrders(@Body SearchWorkOrdersRequest searchWorkOrdersRequest);

    @POST("/FFMTpeEnlace/SplittersConsulta_ENL")
    Call<ResponseBody> splitters(@Body SplittersRequest splittersRequest);

    @POST("/ENL/AsignaOtAPP")
    Call<ResponseBody> changeStatus(@Body ChangeStatusRequest changeStatusRequest);

    @POST("/ENL/CancelaOtAPP")
    Call<ResponseBody> cancelWorkOrder(@Body CancelStatusRequest changeStatusRequest);

    @POST("/ENL/TerminaOtAPP")
    Call<ResponseBody> finishStatus(@Body CompletedWorkOrderRequest changeStatusRequest);

    @POST("/ENL/CatalogoAuxiliares_ENL")
    Call<ResponseBody> assistants(@Body AssistantsRequest assistantsRequest);

    @POST("/FFMTpeEnlace/ConsultaWorkTeam")
    Call<ResponseBody> workAssistantTeam(@Body QueryWorkTeamRequest queryWorkTeamRequest);

    @POST("/FFMTpeEnlace/SaveWorkTeam_ENL")
    Call<ResponseBody> saveWorkAssistantTeam(@Body SaveWorkAssistantTeamRequest saveWorkAssistantTeamRequest);

    @POST("/FFMTpeEnlace/SplittersInserta_ENL")
    Call<ResponseBody> findSplitter(@Body FindSplitterAppRequest retrofitBaseRequest);

    @POST("/FFMTpeEnlace/SplittersActualiza_ENL")
    Call<ResponseBody> updateSplitterApp(@Body UpdateSplitterAppRequest updateSplitterAppRequest);

    @POST("/FFMTpeEnlace/SaveArriveTimeAPP")
    Call<ResponseBody> arriveTime(@Body ArriveTimeRequest retrofitBaseRequest);

    @POST("/ENL/ConsultaEstatusEsperaAPP")
    Call<ResponseBody> validWorkOrder(@Body ValidWorkOrderRequest request);

    @POST("/FFMTpeEnlace/GetInfoCotizacion_Enlace")
    Call<ResponseBody> quotation(@Body QuotationRequest quotationRequest);

    @POST("/ENL/SaveImageURL")
    Call<SaveImageBDResponse> saveImageBD(@Body SaveImageBDRequest saveImageBDRequest);

    @POST("/FFMTpeEnlace/ActualizaTiempoEstimado_ENL")
    Call<ResponseBody> updateTime(@Body UpdateEstimatedTimeRequest retrofitBaseRequest);

    @POST("/ENL/GuardaEncuestaENL")
    Call<ResponseBody> testapp(@Body TestRequest retrofitBaseRequest);

    @POST("/ENL/Consulta_Equipos_ENL_App")
    Call<ResponseBody> equipments(@Body EquipmentRequest equipmentRequest);

    @POST("/ENL/GeneraDnsAPP")
    Call<ResponseBody> generateDN(@Body GetDNRequest generateDNRequest);

    @POST("/ENL/PaymentsEngineAPP")
    Call<ResponseBody> payment(@Body PaymentEngineRequest mPaymentEngineRequest);

    @POST("/FFMTpeEnlace/InsertaMonto_ENL")
    Call<ResponseBody> cashPayment(@Body CashPaymentEngineRequest cashPaymentEngineRequest);

    @POST("/FFMTpeEnlace/ConsultaBandera_ENL")
    Call<ResponseBody> getFlag(@Body GetFlagRequest getFlagRequest);

    @POST("/FFMTpeEnlace/InsertaBandera_ENL")
    Call<ResponseBody> setFlag(@Body SetFlagRequest setFlagRequest);

    @POST("/ENL/ActivacionMovilAPP")
    Call<ResponseBody> activate(@Body ActivationRequest activationRequest);

    //// WITHOUT MANAGER
    @POST("/ENL/AutoFindApp")
    Call<ResponseBody> autoFind(@Body AutoFindRequest autoFindRequest);

    @POST("/ENL/SearchSN")
    Call<ResponseBody> validSerialNumber(@Body ValidSerialNumberRequest validSerialNumberRequest);

    @POST("/ENL/GetMacENL")
    Call<ResponseBody> getMac(@Body GetMacRequest getMacRequest);

    @POST("/ENL/ConfiguraEquiposENL_APP")
    Call<ResponseBody> updateEquipment(@Body UpdateEquipmentRequest updateEquipmentRequestInternet);

    @POST("/ENL/ConfiguraDNS_APP")
    Call<ResponseBody> updateDNs(@Body UpdateDNsRequest updateDNsRequest);

    @POST("/ENL/ConsultaMateriales")
    Call<ResponseBody> consultingMaterials(@Body GetMaterialRequest getMaterialRequest);

    @POST("/ENL/DiscountStorage_ENL")
    Call<ResponseBody> discountMaterials(@Body DiscountMaterialsRequest discountMaterialsRequest);

    @POST("/ENL/ConfiguraDispositivos_ENL")
    Call<ResponseBody> settingEquipment(@Body SettingEquipmentRequest settingEquipmentRequest);

    @POST("/FFMTpeEnlace/OperatorLocation_ENL")
    Call<ResponseBody> updateLocation(@Body UpdateLocationRequest updateLocationRequest);

    @POST("/ENL/GetConfigSDM")
    Call<ResponseBody> getConfigSDM(@Body ConfigSDMRequest configSDMRequest);

    @POST("/ENL/ValidaCuentaENL")
    Call<ResponseBody> validaCuentaENL(@Body ValidaCuentaRequest validaCuentaRequest);

    @POST("/FFMTpeEnlace/UpdateStatusOperario_ENL")
    Call<ResponseBody> changeOperatorStatus(@Body ChangeOperatorRequest changeOperatorRequest);

    @POST("/ENL/InfoEquiposSFAPP")
    Call<ResponseBody> settingsEquipments(@Body InfoEquipmentRequest settingEquipmentRequest);


}
