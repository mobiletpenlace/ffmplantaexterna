package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-262 on 24/11/2016.
 */
public class GetMacResponse extends BaseResponse {
    @SerializedName("centro")
    public String center;
    @SerializedName("almacen")
    public String storage;
    @SerializedName("tipStock")
    public String stockType;
    @SerializedName("serieDec")
    public String decSerial;
    @SerializedName("serie")
    public String serial;
    public String mac;
    @SerializedName("codRetorno")
    public String returnCode;
    @SerializedName("tipo")
    public String type;
    @SerializedName("msg")
    public String message;
}
