package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by totalplay on 11/18/16.
 * Enlace
 */
public class QuotationRequest extends BaseRequest {

    @SerializedName("NumeroCuentaFactura")
    public String accountTicketNumber;

}
