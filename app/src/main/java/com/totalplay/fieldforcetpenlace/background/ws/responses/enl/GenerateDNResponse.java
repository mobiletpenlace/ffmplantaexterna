package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.DN;

import java.io.Serializable;
import java.util.List;

/**
 * Created by NS-TOTALPLAY on 14/11/2016.
 * Enlace
 */

public class GenerateDNResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("Result")
    public String result;
    @SerializedName("ResultDescription")
    public String resultDescription;
    @SerializedName("ArrDn")
    public List<DN> arrayDn;
}
