package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by charlssalazar on 30/06/17.
 * Enlace
 */

public class DiscountMaterialsRequest extends BaseRequest {

    @SerializedName("idOperator")
    public String idOperario;
    @SerializedName("idOT")
    public String idOT;
    @SerializedName("Materiales")
    public List<UsedMaterials> materials;

    public static class UsedMaterials {

        @SerializedName("Material")
        public String material;
        @SerializedName("Cantidad")
        public String quantity;
        @SerializedName("Serie")
        public String serie;
        @SerializedName("UOM")
        public String uom;
        @SerializedName("Valoracion")
        public String rate;

        public UsedMaterials() {
            this.material = "";
            this.quantity = "";
            this.serie = "";
            this.uom = "";
            this.rate = "";
        }
    }

}
