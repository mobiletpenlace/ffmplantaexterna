package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Question;

import java.util.List;

/**
 * Created by totalplay on 11/24/16.
 * TFFM
 */
public class TestRequest extends BaseRequest{

    @SerializedName("Pregunta")
    public List<Question> questions;

}
