package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.RequestParameter;

import java.util.List;

/**
 * Created by totalplay on 11/16/16.
 * Enlace
 */

public class ChangeStatusRequest extends BaseRequest {

    @SerializedName("Id_Ot")
    public String workOrderId = "";
    @SerializedName("Id_Despacho")
    public String operatorId = "";
    @SerializedName("Id_Estado")
    public String status = "";
    @SerializedName("Id_Motivo")
    public String reason = "";
    @SerializedName("Latitud")
    public String latitude = "";
    @SerializedName("Longitud")
    public String longitude = "";
    @SerializedName("Origen")
    public String origin = "";
    @SerializedName("Comentarios")
    public String comment = "";
    @SerializedName("ParametrosAdicionales")
    public List<RequestParameter> parameterList;

}
