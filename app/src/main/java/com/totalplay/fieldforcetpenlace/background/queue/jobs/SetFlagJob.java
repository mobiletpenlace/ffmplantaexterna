package com.totalplay.fieldforcetpenlace.background.queue.jobs;

import com.birbit.android.jobqueue.Params;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.background.queue.utils.JobPriority;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SetFlagRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.SetFlagResponse;

/**
 * Created by JMora on 17/08/2017
 */

public class SetFlagJob extends DataJob {

    private String flagId;
    private String workOrderId;
    private BaseWSManager retrofitManager;

    public SetFlagJob(String flagId, String workOrderId) {
        super(new Params(JobPriority.HIGH).persist().requireNetwork());
        this.flagId = flagId;
        this.workOrderId = workOrderId;
    }

    @Override
    public void onRun() throws Throwable {
        retrofitManager = WSManager.init().settings(getApplicationContext());
        retrofitManager.requestWsSync(SetFlagResponse.class, WSManager.WS.SEND_FLAG, WebServices.servicesEnlace().setFlag(new SetFlagRequest(workOrderId, flagId)));
        retrofitManager.onDestroy();
    }
}
