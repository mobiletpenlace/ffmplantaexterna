package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by totalplay on 11/18/16.
 * Enlace
 */
@RealmClass
public class Quotation implements WSBaseResponseInterface, RealmModel, Serializable {

    @PrimaryKey
    public String _uuid = UUID.randomUUID().toString();

    @SerializedName("result")
    public String result;
    @SerializedName("resultDescription")
    public String resultDescription;
    @SerializedName("CotizacionName")
    public String quotationName;
    @SerializedName("ProntoPago")
    public String amount;
    @SerializedName("MegasSubida")
    public String uploadSpeed;
    @SerializedName("MegasBajada")
    public String downloadSpeed;
    @SerializedName("DNsTotales")
    public String totalDNs;
    @SerializedName("PrimeraRenta")
    public String firstRent;

    @SerializedName("CotPlanServicios")
    public RealmList<ServicePlan> servicePlans;

}
