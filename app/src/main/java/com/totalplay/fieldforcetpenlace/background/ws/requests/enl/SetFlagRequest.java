package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-257 on 07/08/2017.
 * Enlace
 */

public class SetFlagRequest extends BaseRequest {

    @SerializedName("ID_OT")
    public String wOId;
    @SerializedName("IDBandera")
    public String flagId;

    public SetFlagRequest() {
    }

    public SetFlagRequest(String wOId, String flagId) {
        this.wOId = wOId;
        this.flagId = flagId;
    }
}
