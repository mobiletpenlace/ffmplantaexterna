package com.totalplay.fieldforcetpenlace.background.queue.jobs;

import android.location.Location;
import android.util.Log;

import com.birbit.android.jobqueue.Params;
import com.library.pojos.DataManager;
import com.orhanobut.logger.Logger;
import com.totalplay.fieldforcetpenlace.BuildConfig;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.queue.utils.JobPriority;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.FileRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SaveImageBDRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.SaveImageBDResponse;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.OriginAppEnum;
import com.totalplay.fieldforcetpenlace.library.utils.CryptoUtils;
import com.totalplay.fieldforcetpenlace.library.utils.ResponseUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Image;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by totalplay on 6/9/17.
 * FMPServiceProvider
 */

public class SynchronizeImagesJob extends DataJob {

    private boolean isImageProfileNotEmpty = true;
    private User mUser;
    private DataManager ffmDataManager;
    private ArrayList<PhotoEvidence> mlistPhotos;

    public SynchronizeImagesJob(User user, ArrayList<PhotoEvidence> listPhotos) {
        super(new Params(JobPriority.HIGH).persist().requireNetwork());
        mUser = user;
        mlistPhotos = listPhotos;
    }

    @Override
    public void onRun() throws Throwable {

        ffmDataManager = DataManager.validateConnection(null);
        if (mUser == null) return;

        for (PhotoEvidence photoEvidence : mlistPhotos) {
            if (photoEvidence.workOrder.equals("-1")) {
                ffmDataManager.tx(tx -> tx.delete(PhotoEvidence.class, photoEvidence.id));
                return;
            }
            isImageProfileNotEmpty = true;
            File file = new File(photoEvidence.path);
            if (!file.exists()) {
                ffmDataManager.tx(tx -> {
                    tx.delete(PhotoEvidence.class, photoEvidence.id);
                });
                return;
            }

            FileRequest fileRequest = new FileRequest();
            String pathFile;
            if (photoEvidence.type.equals(ImageTypeEnum.PROFILE)) {
                if (mUser.profilePhoto != null && !mUser.profilePhoto.equals("")) {
                    isImageProfileNotEmpty = false;
                }
                pathFile = String.format("/%s%s", photoEvidence.baseUrl, photoEvidence.serverFolder);
                mUser.profilePhoto = CryptoUtils.crypt(this.getApplicationContext().getString(R.string.secret_key), String.format("%s/%s", pathFile, file.getName()));
                ffmDataManager.tx(tx -> tx.save(mUser));
            } else {
                pathFile = String.format("/%sFFM/OT/%s/%s", photoEvidence.baseUrl, photoEvidence.workOrder, photoEvidence.serverFolder);
            }

            fileRequest.path = CryptoUtils.crypt(this.getApplicationContext().getString(R.string.secret_key), pathFile);

            Logger.d(String.format("%s/%s", pathFile, file.getName()));
            MediaType TYPE_MULTIPART = MediaType.parse("multipart/form-data");
            RequestBody path = RequestBody.create(TYPE_MULTIPART, fileRequest.path);
            RequestBody requestFile = RequestBody.create(TYPE_MULTIPART, file);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("File", file.getName(), requestFile);

            Call<ResponseBody> mFileCall;
            if (BuildConfig.DEBUG) {
                mFileCall = WebServices.servicesImages().uploadFile(filePart, path);
            } else {
                mFileCall = WebServices.servicesImages().writeFile(filePart, path);
            }

            try {
                Response<ResponseBody> response = mFileCall.execute();
                if (response.isSuccessful()) {
                    ffmDataManager.tx(tx -> {
                        photoEvidence.synced = true;
                        tx.save(photoEvidence);
                    });

                    boolean deleted = file.delete();
                    Log.d("TAG", "Archivo borrado: " + deleted);

                    Image infoImage = new Image();
                    Location location = TotalPlayLocationService.getLastLocation();

                    infoImage.idTechical = (mUser.operatorId);
                    infoImage.url = (ResponseUtils.getServerURL(response));
                    if (location != null) {
                        infoImage.longitude = String.valueOf(location.getLongitude());
                        infoImage.latitude = String.valueOf(location.getLatitude());
                    }
                    infoImage.idOt = photoEvidence.workOrder;
                    infoImage.type = photoEvidence.type;
                    infoImage.origin = OriginAppEnum.ORIGIN_APP;
                    infoImage.interventionId = photoEvidence.interventionType;
                    infoImage.name = file.getName();

                    SaveImageBDRequest.ArrImagens imageBean = new SaveImageBDRequest.ArrImagens();
                    imageBean.imageInfo = new ArrayList<>();
                    imageBean.imageInfo.add(infoImage);

                    SaveImageBDRequest request = new SaveImageBDRequest();

                    request.arrImagens = (imageBean);
                    if (isImageProfileNotEmpty) {
                        Call<SaveImageBDResponse> imageBDResponseCall = WebServices.servicesEnlace().saveImageBD(request);
                        try {
                            imageBDResponseCall.execute();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                //errorReason = UploadPhotoErrorReason.COULD_NOT_UPLOAD_FILE;
                // TODO: 7/18/17 Handle error
            }
        }

        ffmDataManager.close();
    }

}
