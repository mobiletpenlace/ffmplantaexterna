package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-TOTALPLAY on 14/11/2016.
 * Enlace
 */

public class AutoFindRequest extends BaseRequest {
    @SerializedName("SerialNumber")
    public String serialNumber;

    public AutoFindRequest() {
    }

    public AutoFindRequest(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
