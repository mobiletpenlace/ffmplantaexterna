package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Dispositive;

import java.util.List;

/**
 * Created by jorgehdezvilla on 02/10/17.
 * Enlace
 */

public class SettingEquipmentRequest extends BaseRequest {

    @SerializedName("Dispositivos")
    public Dispositivos dispositives;

    public static class Dispositivos {
        @SerializedName("dispositivo")
        public List<Dispositive> dispositivesList;
    }

}
