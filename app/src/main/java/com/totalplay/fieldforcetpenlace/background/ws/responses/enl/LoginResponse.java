package com.totalplay.fieldforcetpenlace.background.ws.responses.enl;

import com.google.gson.annotations.SerializedName;
import com.totalplay.background.WSBaseResponseInterface;

import java.io.Serializable;

/**
 * Created by NS-262 on 14/11/2016.
 */

public class LoginResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("IdResult")
    public String resultId;
    @SerializedName("Result")
    public String result;
    @SerializedName("ResultDescription")
    public String resultDescription;
    @SerializedName("IdOperario")
    public String operatorId;
    @SerializedName("Propietarios")
    public String owner;
    @SerializedName("Primer_Acceso")
    public String firstAccess;
    @SerializedName("Foto_Perfil")
    public String profilePhoto;
    @SerializedName("Num_Empleado")
    public String employeNumber;
    @SerializedName("Nombre")
    public String employeName;
    @SerializedName("Apellido_Paterno")
    public String lastName;
    @SerializedName("Apellido_Materno")
    public String lastMotherName;
    @SerializedName("Nombre_Completo")
    public String completeName;
    @SerializedName("Fecha_Ingreso")
    public String entryDate;
    @SerializedName("Compania")
    public String company;
    @SerializedName("Tipo_Operario")
    public String operatorType;
    @SerializedName("Ciudad")
    public String country;
    @SerializedName("Jefe_Inmediato")
    public String  supervisor;

}
