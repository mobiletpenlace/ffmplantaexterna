package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-262 on 24/11/2016.
 * Enlace
 */
public class GetMacRequest extends BaseRequest {

    @SerializedName("SerialNumber")
    public String serialNumber;

    public GetMacRequest() {
    }

    public GetMacRequest(String serialNumber) {
        this.serialNumber = serialNumber;
    }

}
