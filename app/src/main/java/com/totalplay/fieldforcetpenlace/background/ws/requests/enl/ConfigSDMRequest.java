package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jorgehdezvilla on 05/10/17.
 * Enlace
 */

public class ConfigSDMRequest extends BaseRequest {

    @SerializedName("CuentaCliente")
    public String clientAccount;

    public ConfigSDMRequest() {
    }

    public ConfigSDMRequest(String clientAccount) {
        this.clientAccount = clientAccount;
    }

}
