package com.totalplay.fieldforcetpenlace.background.ws.requests.enl;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-TOTALPLAY on 24/03/2017.
 * Enlace
 */

public class UpdateLocationRequest extends BaseRequest {
    @SerializedName("idOperario")
    public String employerId;
    @SerializedName("Latitud")
    public String latitud;
    @SerializedName("Longitud")
    public String longitud;

    public UpdateLocationRequest(String employerId, String latitud, String longitud){
        this.employerId = employerId;
        this.latitud = latitud;
        this.longitud = longitud;
    }
}
