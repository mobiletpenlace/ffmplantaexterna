package com.totalplay.fieldforcetpenlace.background.queue;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.log.CustomLogger;
import com.birbit.android.jobqueue.scheduling.FrameworkJobSchedulerService;
import com.totalplay.fieldforcetpenlace.BuildConfig;
import com.totalplay.fieldforcetpenlace.background.queue.utils.JobService;

import java.util.logging.Logger;


/**
 * Created by totalplay on 3/7/17.
 * EMP
 */

public class QueueManager {
    private static JobManager jobManager;

    public static void init(Context context) {
        setupJobManager(context);
    }

    public static void add(Job job) {
        checkIfValid();
        try {
            jobManager.addJobInBackground(job);
        } catch (Exception e){

        }
    }

    private static void checkIfValid() {
        if (jobManager == null) throw new IllegalStateException("Call QueueManager.init() first");
    }

    private synchronized static void setupJobManager(Context context) {
        Configuration.Builder builder = new Configuration.Builder(context)
                .customLogger(new CustomLogger() {
                    private static final String TAG = "JOBS";

                    @Override
                    public boolean isDebugEnabled() {
                        return BuildConfig.DEBUG;
                    }

                    @Override
                    public void d(String text, Object... args) {
                        Log.d(TAG, String.format(text, args));
                    }

                    @Override
                    public void e(Throwable t, String text, Object... args) {
                        Log.e(TAG, String.format(text, args), t);
                    }

                    @Override
                    public void e(String text, Object... args) {
                        Log.e(TAG, String.format(text, args));
                    }

                    @Override
                    public void v(String text, Object... args) {

                    }
                })
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(3)//up to 3 consumers at a time
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120);//wait 2 minute
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.scheduler(FrameworkJobSchedulerService.createSchedulerFor(context,
                    JobService.class), true);
        }
        jobManager = new JobManager(builder.build());
    }

    public static JobManager instance() {
        return jobManager;
    }
}
