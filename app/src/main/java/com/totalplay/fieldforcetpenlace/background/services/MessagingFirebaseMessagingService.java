package com.totalplay.fieldforcetpenlace.background.services;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;

import java.util.Map;

/**
 * Created by app on 12/11/16.
 * EMP
 */

public class MessagingFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        com.orhanobut.logger.Logger.d(data);
        if (data != null && data.size() > 0) {
            if (data.containsKey(Keys.NOTIFICATION_TYPE)) {
                NotificationsEngine engine = new NotificationsEngine();
                engine.execute();
            } else {
                String title = data.get("title");
                String body = data.get("body");
                NotificationUtils.simpleNotification(this, android.R.mipmap.sym_def_app_icon, title, body);
            }
        } else if (remoteMessage.getNotification() != null) {
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();
            if (title != null && !title.isEmpty() && body != null && !body.isEmpty()) {
                NotificationUtils.simpleNotification(this, R.mipmap.ic_launcher, title, body);
            }
        }
    }
}
