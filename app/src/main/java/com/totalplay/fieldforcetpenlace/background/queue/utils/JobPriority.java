package com.totalplay.fieldforcetpenlace.background.queue.utils;

/**
 * Created by totalplay on 3/7/17.
 * EMP
 */

public class JobPriority {
    public static final int HIGH = 1000;
    public static final int LOW = 0;
}
