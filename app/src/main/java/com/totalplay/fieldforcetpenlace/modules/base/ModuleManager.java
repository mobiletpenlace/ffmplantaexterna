package com.totalplay.fieldforcetpenlace.modules.base;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.modules.definition.FlowDefinition;
import com.totalplay.fieldforcetpenlace.modules.definition.ModuleDefinition;

import java.lang.reflect.Constructor;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

@SuppressWarnings("unchecked")
public class ModuleManager {

    public static ModuleManager instance;
    private String currentFlow = "";
    private String instantModule = "";

    public static ModuleManager getInstance() {
        if (instance == null) {
            instance = new ModuleManager();
        }
        return instance;
    }

    public void startFlow(AppCompatActivity appCompatActivity, String flowKey, ModuleParameter moduleParameter) {
        currentFlow = flowKey;
        startModule(appCompatActivity, FlowDefinition.GET_FLOWS_APP().get(flowKey).get(0), moduleParameter);
    }

    public void startFlow(AppCompatActivity appCompatActivity, String flowKey, String moduleKey, ModuleParameter moduleParameter) {
        currentFlow = flowKey;
        startModule(appCompatActivity, moduleKey, moduleParameter);
    }

    public void openModule(AppCompatActivity appCompatActivity, String moduleName, ModuleParameter moduleParameter) {
        instantModule = moduleName;
        initModule(appCompatActivity, moduleName, moduleParameter);
    }

    private void startModule(AppCompatActivity appCompatActivity, String moduleName, ModuleParameter moduleParameter) {
        initModule(appCompatActivity, moduleName, moduleParameter);
    }

    private void initModule(AppCompatActivity appCompatActivity, String moduleName, ModuleParameter moduleParameter) {
        Class moduleClass = ModuleDefinition.MODULE_COMPOSITION.get(moduleName);
        try {
            Constructor<BaseModule> constructor = moduleClass.getDeclaredConstructor();
            BaseModule baseModule = constructor.newInstance();
            baseModule.setModuleName(moduleName);
            try {
                baseModule.startModule(appCompatActivity, moduleParameter);
            } catch (Exception e) {
                baseModule.startModule(appCompatActivity, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onNextModule(AppCompatActivity appCompatActivity, String moduleName, ModuleParameter parameter) {
        if (!instantModule.equals("")) {
            instantModule = "";
            appCompatActivity.finish();
        } else {
            String name = FlowDefinition.GET_FLOWS_APP().get(currentFlow).get(FlowDefinition.GET_FLOWS_APP().get(currentFlow).indexOf(moduleName) + 1);
            startModule(appCompatActivity, name, parameter);
        }
    }

}
