package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.AutoFindResponse;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.EquipmentModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InternetModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.view.activities.SimpleScannerActivity;
import com.totalplay.fieldforcetpenlace.view.custom.CatalogEditText;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.fieldforcetpenlace.view.utils.MacEditText;
import com.totalplay.fieldforcetpenlace.view.utils.UIUtils;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;
import com.totalplay.utils.validators.TextViewValidator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.CLIPBOARD_SERVICE;
import static com.totalplay.fieldforcetpenlace.R.string.dialog_error_serial_required;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

public class InternetFragment extends BaseFFMFragment implements InternetPresenter.InternetCallback {

    private static final int REQUEST_SERIAL_NUMBER_INTERNET = 0x100;
    private static final int REQUEST_MAC_INTERNET = 0x101;

    @BindView(R.id.act_service_activation_internet)
    public LinearLayout mInternetLayout;
    @BindView(R.id.act_service_settings_equipment_next)
    public Button mSettingButton;
    @BindView(R.id.act_service_activate_equipments)
    public Button mActivationButton;

    @BindView(R.id.act_service_activation_internet_pack)
    TextView mPackTextView;
    @BindView(R.id.act_service_activation_internet_plan_id)
    TextView mInternetPlanIdTextView;
    @BindView(R.id.act_service_activation_internet_serial_number)
    EditText mInternetSerialEditText;
    @BindView(R.id.act_service_activation_internet_mac)
    MacEditText mMacInternetEditText;


    @BindView(R.id.act_service_activation_internet_olt)
    TextView mOltTextView;
    @BindView(R.id.act_service_activation_internet_id_olt)
    TextView mOltIdTextView;
    @BindView(R.id.act_service_activation_internet_frame)
    TextView mFrameTextView;
    @BindView(R.id.act_service_activation_internet_port)
    TextView mPortTextView;
    @BindView(R.id.act_service_activation_internet_slot)
    TextView mSlotTextView;

    @BindView(R.id.act_service_activation_internet_equipment_type)
    CatalogEditText<String> mTypeEquipmentEditText;
    @BindView(R.id.act_service_activation_internet_equipment)
    CatalogEditText<EquipmentModel> mEquipmentEditText;
    @BindView(R.id.act_service_activation_comments)
    EditText mCommentsEditText;

    @BindView(R.id.act_service_activation_status)
    TextView mStatusTextView;

    public FormValidator mFormValidator;

    private String mServicePlanId;

    private InternetPresenter mInternetPresenter;

    private boolean settingsContinueNext = false;

    public static InternetFragment newInstance() {
        return new InternetFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_internet, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mMacInternetEditText.registerAfterMacTextChangedCallback();

        mFormValidator = new FormValidator(getContext(), true);
        mFormValidator.addValidators(
                new EditTextValidator(mTypeEquipmentEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mEquipmentEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mInternetSerialEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mInternetSerialEditText, 12, 50, R.string.dialog_error_length_serial_number),
                new EditTextValidator(mMacInternetEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new TextViewValidator(mOltTextView, Regex.NOT_EMPTY, R.string.empty_data_ont)
        );

        mTypeEquipmentEditText.setCatalogs(new ArrayList<String>() {{
            add("Router");
            add("Bridge");
        }});


        Equipment equipment = mFFMDataManager.queryWhere(Equipment.class).filter("dispositive", EquipmentType.ONT).findFirst();
        if (equipment != null && equipment.equipmentsModels != null) {
            mEquipmentEditText.setCatalogs(equipment.equipmentsModels);
        }

        mInternetSerialEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(mInternetSerialEditText.getWindowToken(), 0);
                searchMac();
                return true;
            }
            return false;
        });

        if (mWorkOrder != null) {
            mPackTextView.setText(mWorkOrder.packageContracted);
            if (mWorkOrder.quotationInfo != null && mWorkOrder.quotationInfo.servicePlans != null) {
                boolean internetFound = false;

                for (ServicePlan servicePlan : mWorkOrder.quotationInfo.servicePlans) {
                    if (servicePlan.servicePlanNameService != null) {
                        String planName = servicePlan.servicePlanNameService;
                        if (planName.contains("Internet") && !planName.contains("WIFI")) {
                            internetFound = true;
                            mServicePlanId = servicePlan.servicePlanId;
                            mInternetPlanIdTextView.setText(String.format("INTERNET - %s", servicePlan.sitePlanName));
                            setup(mWorkOrder.configWorkOrder.internet);
                        }
                    }
                }

                if (!internetFound) UIUtils.disableEnableControls(false, mInternetLayout);
                mInternetLayout.setVisibility(internetFound ? View.VISIBLE : View.GONE);
            }

            if (mWorkOrder.configuredDevices && mWorkOrder.configuredDN) {
                mActivationButton.setVisibility(View.VISIBLE);
                mSettingButton.setVisibility(View.GONE);
            }
            boolean configured = mWorkOrder.configuredDevices && mWorkOrder.configuredDN;
            if (configured) {
                UIUtils.disableEnableControls(false, mInternetLayout);
                mActivationButton.setEnabled(true);
            }
        }

        mStatusTextView.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
            clipboard.setText(mStatusTextView.getText());
            MessageUtils.toast(getActivity(), "");
        });
    }

    @Override
    public BasePresenter getPresenter() {
        return mInternetPresenter = new InternetPresenter((AppCompatActivity) getActivity(), this);
    }

    @OnClick(R.id.act_service_activate_equipments)
    public void onActivateEquipmentsClick() {
        ((ActivationServiceActivity) getActivity()).activateWorkOrder(mCommentsEditText.getText().toString().trim());
    }

    @OnClick(R.id.act_service_settings_equipment_next)
    public void onSettingsEquipmentClick() {
        if (!mInternetSerialEditText.getText().toString().equals("") && mOltTextView.getText().toString().trim().equals("")) {
            settingsContinueNext = true;
            searchMac();
        } else {
            ((ActivationServiceActivity) getActivity()).settingsEquipment();
        }
    }

    @OnClick(R.id.act_service_activation_internet_scan_bar_code)
    void onClickScanBarCode() {
        SimpleScannerActivity.launch((AppCompatActivity) getActivity(), REQUEST_SERIAL_NUMBER_INTERNET);
    }

    @OnClick(R.id.act_service_activation_internet_scan_bar_code_mac)
    void onClickScanBarMacCode() {
        SimpleScannerActivity.launch((AppCompatActivity) getActivity(), REQUEST_MAC_INTERNET);
    }

    public void setMac(String mac) {
        mMacInternetEditText.setText(mac);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            String serialNumber = data.getExtras().getString(Keys.SCAN_RESULT);
            if (requestCode == REQUEST_SERIAL_NUMBER_INTERNET) {
                mInternetSerialEditText.clearFocus();
                mInternetSerialEditText.setText(serialNumber);
                searchMac();
            } else if (requestCode == REQUEST_MAC_INTERNET) {
                mMacInternetEditText.setText(serialNumber);
            }
        }
    }

    private void searchMac() {
        if (!TextUtils.isEmpty(mInternetSerialEditText.getText())) {
            mInternetPresenter.autoFind2(mInternetSerialEditText.getText().toString().trim());
        } else {
            MessageUtils.toast(getContext(), dialog_error_serial_required);
        }
    }


    public InternetModel getInternetModel() {
        return new InternetModel(
                mServicePlanId,
                mTypeEquipmentEditText.getText().toString().trim(),
                mMacInternetEditText.getText().toString().trim(),
                mOltTextView.getText().toString().trim(),
                mOltIdTextView.getText().toString().trim(),
                mPortTextView.getText().toString().trim(),
                mSlotTextView.getText().toString().trim(),
                mFrameTextView.getText().toString(),
                mInternetSerialEditText.getText().toString().replace(":", "").trim(),
                mEquipmentEditText.getSelectedValue()
        );
    }

    public void setup(InternetModel internet) {
        if (internet != null) {
            mMacInternetEditText.setText(internet.mac);
            mInternetSerialEditText.setText(internet.serialNumber);
            mOltTextView.setText(internet.olt);
            mOltIdTextView.setText(internet.oltId);
            mFrameTextView.setText(internet.frame);
            mSlotTextView.setText(internet.slot);
            mPortTextView.setText(internet.port);
            mTypeEquipmentEditText.setText(internet.srvMode);
            mEquipmentEditText.setText(internet.equipmentModel.modelDescription);
        }
    }

    @Override
    public void onSuccessLoadFind(AutoFindResponse autoFindResponse) {
        if (autoFindResponse != null) {
            mOltTextView.setText(autoFindResponse.olt);
            mOltIdTextView.setText(autoFindResponse.oltId);
            mFrameTextView.setText(autoFindResponse.frame);
            mSlotTextView.setText(autoFindResponse.slot);
            mPortTextView.setText(autoFindResponse.port);
            if (autoFindResponse.mac != null && !autoFindResponse.mac.trim().equals(""))
                mMacInternetEditText.setText(autoFindResponse.mac);
            if (settingsContinueNext) {
                ((ActivationServiceActivity) getActivity()).settingsEquipment();
            }
        }
        settingsContinueNext = false;
    }

    public void setStatusWS(String statusWS) {
        mStatusTextView.setText(statusWS);
    }
}
