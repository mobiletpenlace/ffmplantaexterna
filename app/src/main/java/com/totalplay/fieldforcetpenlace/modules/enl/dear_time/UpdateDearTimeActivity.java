package com.totalplay.fieldforcetpenlace.modules.enl.dear_time;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NS-262 on 07/02/2017.
 * FFM
 */

@SuppressWarnings("unchecked")
public class UpdateDearTimeActivity extends BaseFFMActivity implements UpdateDearTimePresenter.Callback {

    String[] mHours = new String[]{"00", "01", "02", "03",
            "04", "05", "06", "07",
            "08", "09", "10", "11", "12"};
    String[] mMinutes = new String[]{"00", "15", "30", "45"};

    @BindView(R.id.act_update_estimated_time_hours_number_picker)
    NumberPicker mHoursNumberPicker;
    @BindView(R.id.act_update_estimated_time_minutes_number_picker)
    NumberPicker mMinutesNumberPicker;
    @BindView(R.id.act_update_estimated_package_name)
    TextView mPackageNameTextView;
    @BindView(R.id.act_update_estimated_time_linear)
    LinearLayout mEstimatedTimeLinearLayout;
    @BindView(R.id.act_update_estimated_time_estimated_time)
    TextView mEstimatedTimeTextTextView;

    UpdateDearTimePresenter mUpdateDearTimePresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isTimeEstimated) {
            Intent intent = new Intent(appCompatActivity, UpdateDearTimeActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_update_estimated_time);
        setTitle("");
        ButterKnife.bind(this);

        int minutes = 0;
        try {
            minutes = Integer.valueOf(mWorkOrder.dearTime);
        } catch (Exception ignored) {
        }

        int hours = minutes / 60;
        int mins = minutes - hours * 60;

        mHoursNumberPicker.setMinValue(0);
        mHoursNumberPicker.setMaxValue(12);
        mHoursNumberPicker.setValue(hours);
        mHoursNumberPicker.setDisplayedValues(mHours);

        mMinutesNumberPicker.setMinValue(0);
        mMinutesNumberPicker.setMaxValue(3);
        mMinutesNumberPicker.setValue(mins);
        mMinutesNumberPicker.setDisplayedValues(mMinutes);

        mPackageNameTextView.setText(mWorkOrder.packageContracted);

        mTrainingManager.updateDearTimeActivity();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mUpdateDearTimePresenter = new UpdateDearTimePresenter(this, this);
    }

    @OnClick(R.id.act_update_estimated_time_save_button)
    public void onSaveClick() {
        if (mHours[mHoursNumberPicker.getValue()].equals("00")
                && mMinutes[mMinutesNumberPicker.getValue()].equals("00")) {
            MessageUtils.toast(this, R.string.error_time_update_activity);
        } else {
            int cMinutes = Integer.parseInt(String.valueOf(mMinutes[mMinutesNumberPicker.getValue()])) +
                    Integer.parseInt(String.valueOf(mHoursNumberPicker.getValue())) * 60;
            String minutes = String.valueOf(cMinutes);
            mUpdateDearTimePresenter.updateTime(minutes);
        }
    }


    @Override
    public void onSuccessSendDearTime(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
        finish();
    }
}
