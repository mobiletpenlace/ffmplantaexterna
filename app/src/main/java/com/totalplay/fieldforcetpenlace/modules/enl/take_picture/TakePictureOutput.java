package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

import java.io.File;
import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 19/01/18.
 * FFM ENlace
 */

public class TakePictureOutput implements ModuleParameter, Serializable {

    public File file;

    public TakePictureOutput(File file) {
        this.file = file;
    }
}
