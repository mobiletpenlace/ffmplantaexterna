package com.totalplay.fieldforcetpenlace.modules.definition;

import com.totalplay.fieldforcetpenlace.modules.enl.acceptance_act.AcceptanceActModule;
import com.totalplay.fieldforcetpenlace.modules.enl.activation.ActivationModule;
import com.totalplay.fieldforcetpenlace.modules.enl.arrive_early.ArriveEarlyModule;
import com.totalplay.fieldforcetpenlace.modules.enl.arrive_evidences.ArriveEvidencesModule;
import com.totalplay.fieldforcetpenlace.modules.enl.auxiliars.AssistantsModule;
import com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives.ConfigureDispositivesModule;
import com.totalplay.fieldforcetpenlace.modules.enl.dear_time.DearTimeModule;
import com.totalplay.fieldforcetpenlace.modules.enl.evidences.EvidencesModule;
import com.totalplay.fieldforcetpenlace.modules.enl.finish_installation.FinishInstallationActivity;
import com.totalplay.fieldforcetpenlace.modules.enl.info.WorkOrderInfoModule;
import com.totalplay.fieldforcetpenlace.modules.enl.info_map.WorkOrderInfoMapModule;
import com.totalplay.fieldforcetpenlace.modules.enl.introducction.IntroducctionModule;
import com.totalplay.fieldforcetpenlace.modules.enl.load_work_order.LoadWorkOrderModule;
import com.totalplay.fieldforcetpenlace.modules.enl.login.LoginModule;
import com.totalplay.fieldforcetpenlace.modules.enl.materials.MaterialsModule;
import com.totalplay.fieldforcetpenlace.modules.enl.operator_status.OperatorStatusModule;
import com.totalplay.fieldforcetpenlace.modules.enl.payment.PaymentModule;
import com.totalplay.fieldforcetpenlace.modules.enl.profile.ProfileModule;
import com.totalplay.fieldforcetpenlace.modules.enl.replace_dispositives.ReplaceDispositivesModule;
import com.totalplay.fieldforcetpenlace.modules.enl.resume.ResumeWorkOrderModule;
import com.totalplay.fieldforcetpenlace.modules.enl.selfie_photo.SelfiePhotoModule;
import com.totalplay.fieldforcetpenlace.modules.enl.splitters.SplittersModule;
import com.totalplay.fieldforcetpenlace.modules.enl.survey.SurveyModule;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureModule;
import com.totalplay.fieldforcetpenlace.modules.enl.travel.WorkOrderTravelModule;
import com.totalplay.fieldforcetpenlace.modules.enl.work_order_complete.WorkOrderCompletedModule;

import java.util.HashMap;

/**
 * Created by jorgehdezvilla on 04/01/18.
 * Modules
 */

public class ModuleDefinition {

    public static final HashMap<String, Class> MODULE_COMPOSITION = ModuleDefinition.createMap();

    private static HashMap<String, Class> createMap() {
        HashMap<String, Class> myMap = new HashMap<>();

        /// ENLACE
        myMap.put(ModuleKey.LOGIN, LoginModule.class);
        myMap.put(ModuleKey.SELFIE_PHOTO, SelfiePhotoModule.class);
        myMap.put(ModuleKey.INTRODUCCTION_APP, IntroducctionModule.class);
        myMap.put(ModuleKey.ASSISTANTS, AssistantsModule.class);
        myMap.put(ModuleKey.LOAD_WORK_ORDER, LoadWorkOrderModule.class);
        myMap.put(ModuleKey.WORK_ORDER_MAP_INFO, WorkOrderInfoMapModule.class);
        myMap.put(ModuleKey.WORK_ORDER_TRAVEL, WorkOrderTravelModule.class);
        myMap.put(ModuleKey.WORK_ORDER_ARRIVE_EARLY, ArriveEarlyModule.class);
        myMap.put(ModuleKey.WORK_ORDER_ARRIVE_EVIDENCE, ArriveEvidencesModule.class);
        myMap.put(ModuleKey.WORK_ORDER_INFO, WorkOrderInfoModule.class);
        myMap.put(ModuleKey.WORK_ORDER_DEAR_TIME, DearTimeModule.class);
        myMap.put(ModuleKey.WORK_ORDER_SPLITTERS, SplittersModule.class);
        myMap.put(ModuleKey.WORK_ORDER_ACTIVATION, ActivationModule.class);
        myMap.put(ModuleKey.WORK_ORDER_CONFIGURE_DISPOSITIVES, ConfigureDispositivesModule.class);
        myMap.put(ModuleKey.WORK_ORDER_REPLACE_DISPOSITIVES, ReplaceDispositivesModule.class);
        myMap.put(ModuleKey.WORK_ORDER_RESUME, ResumeWorkOrderModule.class);
        myMap.put(ModuleKey.WORK_ORDER_PAYMENT, PaymentModule.class);
        myMap.put(ModuleKey.WORK_ORDER_SURVEY, SurveyModule.class);
        myMap.put(ModuleKey.WORK_ORDER_FINISH_INSTALLATION, FinishInstallationActivity.class);
        myMap.put(ModuleKey.WORK_ORDER_ACCEPTANCE_ACT, AcceptanceActModule.class);
        myMap.put(ModuleKey.WORK_ORDER_EVIDENCES, EvidencesModule.class);
        myMap.put(ModuleKey.WORK_ORDER_MATERIALS, MaterialsModule.class);
        myMap.put(ModuleKey.WORK_ORDER_COMPLETED, WorkOrderCompletedModule.class);

        myMap.put(ModuleKey.MY_PROFILE, ProfileModule.class);
        myMap.put(ModuleKey.OPERATOR_STATUS, OperatorStatusModule.class);

        myMap.put(ModuleKey.TAKE_PICTURE, TakePictureModule.class);

        /// PLANTA EXTERNA

        return myMap;
    }

}
