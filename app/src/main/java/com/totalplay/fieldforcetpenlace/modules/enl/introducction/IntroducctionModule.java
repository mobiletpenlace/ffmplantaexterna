package com.totalplay.fieldforcetpenlace.modules.enl.introducction;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public class IntroducctionModule extends BaseModule<User, ModuleParameter> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, User user) {
        PreviewActivity.launch(this, appCompatActivity, user);
    }
}
