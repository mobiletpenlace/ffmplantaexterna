package com.totalplay.fieldforcetpenlace.modules.enl.materials;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.fieldforcetpenlace.background.queue.jobs.SetFlagJob;
import com.totalplay.fieldforcetpenlace.library.enums.FlagType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

/**
 * Created by jorgehdezvilla on 16/01/18.
 * FFM Enlace
 */

public class MaterialsModule extends BaseModule<WorkOrder, WorkOrder> {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        UsedMaterialActivity.launch(this, appCompatActivity, workOrder);
    }

    @Override
    public void finishModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.SEND_MATERIALS, workOrder.id));
        super.finishModule(appCompatActivity, workOrder);
    }
}
