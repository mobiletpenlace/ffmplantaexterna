package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.CashOrigin;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.library.utils.StringUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by NS-TOTALPLAY on 25/09/2017.
 * Enlace
 */

@SuppressWarnings("unchecked")
public class PaymentOtherFragment extends BaseFFMFragment implements PaymentOtherPresenter.Callback {

    @BindView(R.id.fr_payment_other_pack)
    TextView mPackTextView;
    @BindView(R.id.fr_payment_other_amount)
    TextView mAmountTextView;
    @BindView(R.id.fr_payment_other_photo_evidences)
    ViewGroup mEvidencesContent;

    private ImageView mSelectedImageView;
    private PaymentOtherPresenter mPaymentOtherPresenter;

    public static PaymentOtherFragment newInstance(String cashOrigin) {
        Bundle bundle = new Bundle();
        bundle.putString(Keys.CASH_ORIGIN, cashOrigin);
        PaymentOtherFragment fragment = new PaymentOtherFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_payment_other, container, false);
    }

    @Override
    public BasePresenter getPresenter() {
        return mPaymentOtherPresenter = new PaymentOtherPresenter((AppCompatActivity) getActivity(), this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mPaymentOtherPresenter.loadArguments(this);

        if (mWorkOrder.quotationInfo != null && mWorkOrder.quotationInfo.amount != null)
            mAmountTextView.setText(String.format("$ %s", StringUtils.formattedAmount(mWorkOrder.quotationInfo.amount)));
        mPackTextView.setText(mWorkOrder.packageContracted);


        if (getArguments().getString(Keys.CASH_ORIGIN).equals(CashOrigin.NONE)) {
            mAmountTextView.setText("SIN COBRO");
            mEvidencesContent.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.fr_payment_other_photo_one, R.id.fr_payment_other_photo_two})
    public void onImageClick(View view) {
        mSelectedImageView = (ImageView) view;
        mPaymentOtherPresenter.imageViewClick(view.getId());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mPaymentOtherPresenter != null && resultCode == Activity.RESULT_OK)
            mPaymentOtherPresenter.onActivityResult(requestCode, resultCode);
    }

    @OnClick(R.id.fr_payment_other_pay)
    public void onPayClick() {
        if (mAmountTextView.getText().toString().trim().length() > 0) {
            if (getArguments().getString(Keys.CASH_ORIGIN).equals(CashOrigin.NONE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("¿Estás seguro de realizar la instalación sin cobro?");
                builder.setPositiveButton("Aceptar", (dialog, which) -> mPaymentOtherPresenter.withoutPayment());
                builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.cancel());
                builder.show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("¿Estás seguro de realizar el pago?");
                builder.setPositiveButton("Aceptar", (dialog, which) -> mPaymentOtherPresenter.payment(mAmountTextView.getText().toString().trim()));
                builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.cancel());
                builder.show();
            }
        } else {
            MessageUtils.toast(getContext(), R.string.dialog_error_validate_edit_text);
        }
    }

    @Override
    public void onSuccessLoadImage(int requestCode, File mRequestedFile) {
        if (mSelectedImageView != null) {
            mSelectedImageView.setPadding(0, 0, 0, 0);
            Glide.with(this)
                    .load(mRequestedFile)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(mSelectedImageView);
            mPaymentOtherPresenter.saveImageFile(mRequestedFile.getName(), mRequestedFile.getPath());
        }
    }

    @Override
    public void onSuccessPayment(WorkOrder workOrder) {
        ((BaseFFMActivity) getActivity()).baseModule.finishModule((AppCompatActivity) getActivity(), workOrder);
    }

}
