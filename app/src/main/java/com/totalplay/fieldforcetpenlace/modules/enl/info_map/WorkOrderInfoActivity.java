package com.totalplay.fieldforcetpenlace.modules.enl.info_map;


import android.app.ActivityManager;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.travel.WorkOrderInfoInRouteActivity;
import com.totalplay.fieldforcetpenlace.view.activities.OptionsMenuActivity;
import com.totalplay.fieldforcetpenlace.modules.definition.FlowKey;
import com.totalplay.fieldforcetpenlace.modules.definition.ModuleKey;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorkOrderInfoActivity extends OptionsMenuActivity implements WorkOrderInfoPresenter.Callback {

    @BindView(R.id.act_installation_detail)
    ViewGroup mDetailInstallationViewGroup;


    @BindView(R.id.act_installation_start_trip)
    Button mStartTripButton;

    @BindView(R.id.act_installation_client_name)
    TextView mOrderClientNameTextView;
    @BindView(R.id.act_installation_address)
    TextView mOrderAddress;
    @BindView(R.id.act_installation_compromise_time)
    TextView mCompromiseTimeTextView;
    @BindView(R.id.act_installation_package)
    TextView mPackageTextView;
    @BindView(R.id.act_installation_type_delayed_hours_icon)
    ImageView mDelayedHoursIconImageView;

    @BindView(R.id.act_installation_type)
    TextView typeInstallationText;

    private StaticMapFragment.MapPoint mWorkOrderMapPoint;
    private WorkOrderInfoPresenter mWorkOrderInfoPresenter;
    private GetFlagPresenter mGetFlagPresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, WorkOrderInfoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    public static void launch(AppCompatActivity appCompatActivity, String installationEnabled) {
        Intent intent = new Intent(appCompatActivity, WorkOrderInfoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(Keys.EXTRA_WORK_ORDER_INSTALLATION_ENABLED, installationEnabled);
        appCompatActivity.startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_workorder_info);
        ButterKnife.bind(this);
        setTitle("");
        showResumeOT();
        mTrainingManager.workOrderInfoActivity();
        mGetFlagPresenter.getFlags();
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mWorkOrderInfoPresenter = new WorkOrderInfoPresenter(this, this),
                mGetFlagPresenter = new GetFlagPresenter(this)
        };
    }

    private void showResumeOT() {
        if (mWorkOrder != null) {
            String addressString = String.format(Locale.getDefault(), "%s %s, %s %s, %s, %s",
                    mWorkOrder.street, mWorkOrder.exteriorNumber,
                    mWorkOrder.town, mWorkOrder.colony,
                    mWorkOrder.zipCode, mWorkOrder.city);

            mPackageTextView.setText(mWorkOrder.packageContracted);
            mOrderClientNameTextView.setText(mWorkOrder.clientName);
            mCompromiseTimeTextView.setText(mWorkOrder.compromiseTime);
            mOrderAddress.setText(addressString);

            Location location = TotalPlayLocationService.getLastLocation();
            if (location != null) {
                if (mWorkOrder.latitude != null && mWorkOrder.latitude.isEmpty()
                        && mWorkOrder.longitude != null && mWorkOrder.longitude.isEmpty()) {
                    mWorkOrderMapPoint = new StaticMapFragment.MapPoint(Double.valueOf(mWorkOrder.latitude), Double.valueOf(mWorkOrder.longitude));
                    ArrayList<StaticMapFragment.MapPoint> points = new ArrayList<StaticMapFragment.MapPoint>() {{
                        add(new StaticMapFragment.MapPoint(location.getLatitude(), location.getLongitude()));
                        add(mWorkOrderMapPoint);
                    }};
                    StaticMapFragment mMapFragment = (StaticMapFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.act_installation_map_fragment);
                    mMapFragment.setMapInitialData(new StaticMapFragment.MapInitialData(points));
                    StaticMapFragment.mWorkOrders = mWorkOrder;
                }
            }
        } else {
            finish();
            MessageUtils.toast(this, "Ha ocurrido un error, intenten nuevamente");
        }
    }

    @OnClick(R.id.act_installation_start_trip)
    public void onStartInstallationClick() {
        mWorkOrder = mFFMDataManager.queryWhere(WorkOrder.class).findFirst();
        mWorkOrderInfoPresenter.startInstallation();
    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage("¿Está seguro de salir de la OT y regresar a la pantalla principal?")
                    .setPositiveButton("Aceptar", (dialog, which) -> {
                        mFFMDataManager.tx(tx -> tx.delete(WorkOrder.class));
                        WorkOrderInfoActivity.this.finish();
                        baseModule.startFlow(WorkOrderInfoActivity.this, FlowKey.MAIN_FLOW, ModuleKey.LOAD_WORK_ORDER, null);
                    })
                    .setNegativeButton("Cancelar", null);
            builder.create().show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSuccessChangeStatus(WorkOrder workOrder) {
        WorkOrderInfoInRouteActivity.launch(baseModule, this, workOrder);
    }
}
