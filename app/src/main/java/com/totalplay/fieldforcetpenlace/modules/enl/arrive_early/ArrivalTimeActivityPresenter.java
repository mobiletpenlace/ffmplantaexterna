package com.totalplay.fieldforcetpenlace.modules.enl.arrive_early;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ArriveTimeRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.SaveArriveTimeResponse;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

/**
 * Created by NS-TOTALPLAY on 18/09/2017.
 * FFM
 */

public class ArrivalTimeActivityPresenter extends FFMPresenter implements WSCallback {
    private final String serverFolder = ServerFolders.ARRIVAL;

    public ArrivalTimeActivityPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void registerArrivalTime(String arrivalStatus) {
        ArriveTimeRequest arriveTimeRequest = new ArriveTimeRequest();
        arriveTimeRequest.id = mWorkOrder.id;
        arriveTimeRequest.punctuality = arrivalStatus;
        mWSManager.requestWs(SaveArriveTimeResponse.class, WSManager.WS.SAVE_ARRIVE_TIME, getDefinition().arriveTime(arriveTimeRequest));
    }

    private void successSaveArriveTime(String requestUrl, SaveArriveTimeResponse timeArrival) {
        if (timeArrival.result.equals("0")) {
            mWorkOrder.isRegisterTimeArrival = true;
            mFFMDataManager.tx(tx -> tx.save(mWorkOrder));
        } else {
            onErrorLoadResponse(requestUrl, timeArrival.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_info);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.SAVE_ARRIVE_TIME)) {
            successSaveArriveTime(requestUrl, (SaveArriveTimeResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        if (requestUrl.equals(WSManager.WS.SAVE_ARRIVE_TIME)) {
            super.onErrorLoadResponse(requestUrl, messageError);
            mAppCompatActivity.finish();
        }
    }
}
