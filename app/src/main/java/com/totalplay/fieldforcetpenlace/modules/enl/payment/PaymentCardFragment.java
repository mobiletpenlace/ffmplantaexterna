package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.PaymentType;
import com.totalplay.fieldforcetpenlace.library.utils.StringUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PaymentCard;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.RadioGroupValidator;
import com.totalplay.utils.validators.Regex;
import com.totalplay.utils.validators.TextViewValidator;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 * Created by totalplay on 6/26/17.
 * FieldForceManagement
 */

@SuppressWarnings("unchecked")
public class PaymentCardFragment extends BaseFFMFragment implements PaymentPresenter.Callback {

    @BindView(R.id.fr_payment_card_disclaimer)
    CheckBox mDisclaimerCheckBox;
    @BindView(R.id.fr_payment_card_name)
    EditText nameEditText;
    @BindView(R.id.fr_payment_card_last_name)
    EditText lastNameEditText;
    @BindView(R.id.fr_payment_card_mother_last_name)
    EditText motherLastNameEditText;
    @BindView(R.id.fr_payment_card_scanner)
    ImageView creditCardReader;
    @BindView(R.id.fr_payment_card_card)
    TextView mCardTextView;
    @BindView(R.id.fr_payment_card_cvv)
    EditText mCvvEditText;
    @BindView(R.id.fr_payment_card_expiration_date)
    EditText mExpirationDateTextView;
    @BindView(R.id.fr_payment_card_amount)
    TextView mAmountTextView;
    @BindView(R.id.fr_payment_card_pack)
    TextView mPackTextView;
    @BindView(R.id.act_pay_out_type_card)
    RadioGroup mTypeCardRadioGroup;

    private FormValidator mFormValidator;
    private int MY_SCAN_REQUEST_CODE = 100;
    private String creditCardNumber;
    private String lastDigits;
    private String expireMonth;
    private String getExpireYearFull;
    private String expireYear;

    private PaymentPresenter mPaymentPresenter;

    public static PaymentCardFragment newInstance() {
        return new PaymentCardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_payment_card, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mFormValidator = new FormValidator(getContext(), true);
        mFormValidator.addValidators(
                new EditTextValidator(nameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(lastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(motherLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new TextViewValidator(mCardTextView, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mCvvEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mExpirationDateTextView, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new RadioGroupValidator(mTypeCardRadioGroup, R.string.dialog_error_select_type_card)
        );

        if (mWorkOrder.quotationInfo != null && mWorkOrder.quotationInfo.amount != null)
            mAmountTextView.setText(String.format("$ %s", StringUtils.formattedAmount(mWorkOrder.quotationInfo.amount)));
        mPackTextView.setText(mWorkOrder.packageContracted);
    }


    @OnClick({R.id.fr_payment_card_scanner, R.id.fr_payment_card_card})
    public void onClickCardReader() {
        Intent scanIntent = new Intent(getContext(), CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false); // default: false
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                if (scanResult.isExpiryValid()) {
                    creditCardNumber = scanResult.getFormattedCardNumber().replace(" ", "");
                    lastDigits = scanResult.getLastFourDigitsOfCardNumber();
                    expireMonth = String.format(Locale.getDefault(), "%s", scanResult.expiryMonth);
                    getExpireYearFull = String.format(Locale.getDefault(), "%s", scanResult.expiryYear);
                    expireYear = String.valueOf(scanResult.expiryYear).substring(2, 4);
                    mCardTextView.setText(lastDigits);

                    try {
                        Integer.parseInt(expireMonth);
                        mExpirationDateTextView.setText(String.format("%s/%s", scanResult.expiryMonth,
                                String.valueOf(scanResult.expiryYear).substring(2, 4)));
                    } catch (Exception ignored) {
                        MessageUtils.toast(getContext(), "La fecha es incorrecta");
                    }

                }
            }
        }
    }


    @OnClick(R.id.fr_payment_card_pay)
    public void onPayClick() {
        if (mFormValidator.isValid()) {
            String amount = mWorkOrder.quotationInfo.amount;
            String typeCard = "";
            if (mTypeCardRadioGroup.getCheckedRadioButtonId() == R.id.fr_payment_card_credit_card) {
                typeCard = PaymentType.CREDIT_CARD;
            } else if (mTypeCardRadioGroup.getCheckedRadioButtonId() == R.id.fr_payment_card_debit_card) {
                typeCard = PaymentType.DEBIT_CARD;
            }


            if (amount != null && !amount.equals("null") && !amount.isEmpty()) {
                PaymentCard paymentCard = new PaymentCard();
                paymentCard.numbber = creditCardNumber;
                paymentCard.cvv = mCvvEditText.getText().toString();
                paymentCard.expireMonth = expireMonth;
                paymentCard.expireYear = getExpireYearFull;
                paymentCard.type = typeCard;

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("¿Estás seguro de realizar el pago?");
                builder.setPositiveButton("Aceptar", (dialog, which) -> mPaymentPresenter.payment(paymentCard, amount));
                builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.cancel());
                builder.show();
            }
        }
    }

    @Override
    public BasePresenter getPresenter() {
        return mPaymentPresenter = new PaymentPresenter((AppCompatActivity) getActivity(), this);
    }

    @Override
    public void onSuccessPayment(WorkOrder workOrder) {
        ((BaseFFMActivity) getActivity()).baseModule.finishModule((AppCompatActivity) getActivity(), workOrder);
    }
}
