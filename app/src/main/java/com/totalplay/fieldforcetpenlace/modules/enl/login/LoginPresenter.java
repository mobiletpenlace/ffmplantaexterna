package com.totalplay.fieldforcetpenlace.modules.enl.login;

import android.Manifest;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.BuildConfig;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AuthenticationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.LoginResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.VersionResponse;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;
import com.totalplay.utils.Prefs;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by jorgehdezvilla on 06/09/17.
 * FFM
 */

public class LoginPresenter extends FFMPresenter implements WSCallback {

    public interface LoginCallback {
        void onSuccessLoginEvent(User user);

        void onVersionEventStatus(boolean status);
    }

    private LoginCallback mLoginCallback;
    private String mUsername;

    public LoginPresenter(AppCompatActivity appCompatActivity, LoginCallback loginCallback) {
        super(appCompatActivity);
        mLoginCallback = loginCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void loadVersion() {
        mWSManager.requestWs(VersionResponse.class, WSManager.WS.VERSION_APP, getAppDefinition().appVersion("FFMEnlace"));
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void loginClick(String username, String validation) {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                login(username, validation);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        };

        new TedPermission(mContext)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("Favor de aceptar todos los permisos para continuar")
                .setPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                )
                .check();
    }

    public void login(String username, String validation) {
        mUsername = username;
        AuthenticationRequest authenticationRequest = new AuthenticationRequest(username, validation, "10.10.10.10", Prefs.instance().string(Keys.FCM_TOKEN));
        if (Prefs.instance().string(Keys.FCM_TOKEN).equals("")) {
            MessageUtils.toast(mContext, "Error Token Google");
            return;
        }
        mWSManager.requestWs(LoginResponse.class, WSManager.WS.LOGIN, getDefinition().login(authenticationRequest));
    }

    private void successLoadVersion(VersionResponse versionResponse) {
        if (versionResponse.version != null && !versionResponse.version.equals(BuildConfig.VERSION_NAME)) {
            AlertDialog builder = new AlertDialog.Builder(mContext)
                    .setMessage(mContext.getString(R.string.dialog_info_new_version, versionResponse.version))
                    .create();
            builder.show();
            mLoginCallback.onVersionEventStatus(false);
        } else {
            mLoginCallback.onVersionEventStatus(true);
        }
    }

    private void successLogin(String requestUrl, LoginResponse loginResponse) {
        if (loginResponse.result != null && loginResponse.result.equals("0")) {
            User user = new User(mUsername, mUsername, loginResponse.operatorId, loginResponse.owner, loginResponse.firstAccess, loginResponse.profilePhoto,
                    loginResponse.employeNumber, loginResponse.employeName, loginResponse.lastName, loginResponse.lastMotherName, loginResponse.completeName,
                    loginResponse.entryDate, loginResponse.company, loginResponse.operatorType, loginResponse.country, loginResponse.supervisor);
            Prefs.instance().putObject(Keys.EXTRA_USER, user, User.class);
            mFFMDataManager.deleteAll();
            mFFMDataManager.tx(tx -> tx.save(user));

            Prefs.instance().putLong(Keys.PREF_LAST_TIME_LOGGED, DateTime.now().getMillis());
            mLoginCallback.onSuccessLoginEvent(user);
        } else {
            onErrorLoadResponse(requestUrl, loginResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.VERSION_APP)) {
            MessageUtils.progress(mAppCompatActivity, "Validando versión");
        } else if (requestUrl.equals(WSManager.WS.LOGIN)) {
            MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_login);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.LOGIN)) {
            successLogin(requestUrl, (LoginResponse) baseResponse);
        } else if (requestUrl.equals(WSManager.WS.VERSION_APP)) {
            successLoadVersion((VersionResponse) baseResponse);
        }
    }


}
