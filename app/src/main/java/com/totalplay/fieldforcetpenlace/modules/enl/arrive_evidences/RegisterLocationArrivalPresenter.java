package com.totalplay.fieldforcetpenlace.modules.enl.arrive_evidences;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WrapperPhotosEvidence;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charlssalazar on 29/06/17.
 * FFM
 */

public class RegisterLocationArrivalPresenter extends FFMPresenter {


    private String serverFolder;
    private WrapperPhotosEvidence mFileWrapper;
    private RegisterLocationArrivalCallback mRegisterLocationArrivalCallback;

    public interface RegisterLocationArrivalCallback {
        void onSuccessLoadEvidences(List<PhotoEvidence> photoEvidences);
        void onSuccessUploadPhotos(WorkOrder workOrder);
    }

    public RegisterLocationArrivalPresenter(AppCompatActivity appCompatActivity, RegisterLocationArrivalCallback registerLocationArrivalCallback) {
        super(appCompatActivity);
        mRegisterLocationArrivalCallback = registerLocationArrivalCallback;
        this.serverFolder = ServerFolders.ARRIVAL;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String savedInstance;
        if (mWorkOrder != null) {
            mFileWrapper = mFFMDataManager.queryWhere(WrapperPhotosEvidence.class)
                    .filter("workOrder", mWorkOrder.id)
                    .filter("serverFolder", serverFolder)
                    .findFirst();
            savedInstance = mWorkOrder.id;
        } else {
            savedInstance = "default";
        }
        if (mFileWrapper == null) {
            mFileWrapper = new WrapperPhotosEvidence(savedInstance, serverFolder);
        }

    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void savePhoto(String name, String path) {
        mFFMDataManager.tx(tx -> {
            PhotoEvidence photo = new PhotoEvidence();
            photo.name = name;
            photo.path = path;

            photo.workOrder = mWorkOrder.id;
            photo.serverFolder = serverFolder;
            photo.baseUrl = mWorkOrder.baseUrl;
            photo.interventionType = mWorkOrder.interventionType;

            if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
                photo.type = ImageTypeEnum.InternalPlant.LOCATION;
            } else if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.EXTERNAL_PLANT)) {
                photo.type = ImageTypeEnum.ExternalPlant.LOCATION;
            } else if (WorkOrderOwnerType.INTEGRATOR.contains(mWorkOrder.ownerId)) {
                photo.type = ImageTypeEnum.Integrator.LOCATION;
            }

            photo = tx.save(photo);
            mFileWrapper.listPhotos.add(photo);
            tx.save(mFileWrapper);
        });
    }

    public void uploadPhotos() {
        mWorkOrder.isRegisterLocationArrival = true;
        mFFMDataManager.tx(tx -> tx.save(mWorkOrder));
        mRegisterLocationArrivalCallback.onSuccessUploadPhotos(mWorkOrder);
    }

    public void getPhotosIfPossible() {
        if (mFileWrapper != null && mFileWrapper.isValid()) {
            mRegisterLocationArrivalCallback.onSuccessLoadEvidences(mFileWrapper.listPhotos);
        }
        mRegisterLocationArrivalCallback.onSuccessLoadEvidences(new ArrayList<>());

    }
}