package com.totalplay.fieldforcetpenlace.modules.enl.info_map;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public class WorkOrderInfoMapModule extends BaseModule<ModuleParameter, WorkOrder> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, ModuleParameter moduleParameter) {
        WorkOrderInfoActivity.launch(this, appCompatActivity);
    }

}
