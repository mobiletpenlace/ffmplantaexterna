package com.totalplay.fieldforcetpenlace.modules.enl.work_order_complete;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.newpresenter.WorkOrderCompletedPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;


/**
 * Created by jorgehdezvilla on 03/10/17.
 * Enlacez
 */

@SuppressWarnings("unchecked")
public class WorkOrderCompletedActivity extends BaseFFMActivity implements WorkOrderCompletedPresenter.Callback {

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, WorkOrderCompletedActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.enl_activity_splash);
    }

    @Override
    protected BasePresenter getPresenter() {
        return new WorkOrderCompletedPresenter(this, this);
    }

    @Override
    public void onSuccessChangeStatus() {
        baseModule.finishModule(this, null);
        finish();
    }
}
