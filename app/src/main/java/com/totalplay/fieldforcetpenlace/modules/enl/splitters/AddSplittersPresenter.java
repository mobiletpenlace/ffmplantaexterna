package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.app.Activity;
import android.location.Location;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.FindSplitterAppRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.FindSplitterAppResponse;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WrapperPhotosEvidence;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.BusinessFiles;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.FileUtils;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureUtils;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by jorgehdezvilla on 11/09/17.
 * FFM
 */

public class AddSplittersPresenter extends FFMPresenter implements WSCallback {

    private static final int REQUEST_CODE_PICTURE = 0x0019;

    private boolean mNotProblem;
    private final String serverFolder = ServerFolders.SPLITTERS;
    private WrapperPhotosEvidence mFileWrapper;
    private File mRequestedFile;
    private List<Integer> mPhotosValidator = new ArrayList<>();
    private Reason reason;

    private Splitter mSplitter;
    private AddSplittersCallback mAddSplittersCallback;

    private boolean mStopWorkOrder = false;
    private boolean mSearchOtherSplitter = false;

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public interface AddSplittersCallback {
        void onSuccessLoadImage(int requestCode, File file);

        void onSuccessSaveSplitterInfo(WorkOrder mWorkOrder, Splitter mSplitter, boolean mSearchOtherSplitter, Reason reason, boolean mStopWorkOrder);
    }

    public AddSplittersPresenter(AppCompatActivity appCompatActivity, AddSplittersCallback addSplittersCallback) {
        super(appCompatActivity);
        mAddSplittersCallback = addSplittersCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mWorkOrder != null) {
            mFileWrapper = mFFMDataManager.queryWhere(WrapperPhotosEvidence.class)
                    .filter("workOrder", mWorkOrder.id)
                    .filter("serverFolder", serverFolder)
                    .findFirst();
            mFileWrapper = new WrapperPhotosEvidence(mWorkOrder.id, serverFolder);
        } else {
            mFileWrapper = new WrapperPhotosEvidence("default", serverFolder);
        }
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    private void findSplitter(String id, String portT, String puertosOc, boolean notProblem) {
        mNotProblem = notProblem;
        Location location = TotalPlayLocationService.getLastLocation();
        User user = mFFMDataManager.queryWhere(User.class).findFirst();
        String latitude = String.valueOf(location.getLatitude());
        String longitude = String.valueOf(location.getLongitude());
        StringTokenizer token = new StringTokenizer(latitude, ".");
        if (latitude.substring(latitude.indexOf(".")).length() > 11) {
            latitude = token.nextToken() + latitude.substring(latitude.indexOf(".")).substring(0, 12);
        }
        token = new StringTokenizer(longitude, ".");
        if (longitude.substring(longitude.indexOf(".")).length() > 11) {
            longitude = token.nextToken() + longitude.substring(longitude.indexOf(".")).substring(0, 12);
        }
        String reasonId = reason == null ? "" : reason.id;
        mSplitter = new Splitter();
        mSplitter.latitude = latitude;
        mSplitter.longitude = longitude;
        FindSplitterAppRequest splittersRequest = new FindSplitterAppRequest(id, portT, puertosOc, latitude, longitude, user.operatorId, reasonId);
        if (reason != null && !mSearchOtherSplitter) {
            showAlertStopWorkOrder(splittersRequest);
        } else {
            mWSManager.requestWs(FindSplitterAppResponse.class, WSManager.WS.ADD_SPLITERS, getDefinition().findSplitter(splittersRequest));
        }
    }

    private void showAlertStopWorkOrder(FindSplitterAppRequest splittersRequest) {
        AlertDialog dialog = new AlertDialog.Builder(mContext)
                .setTitle(reason.name)
                .setMessage("¿Desea detener la orden de trabajo?")
                .setCancelable(false)
                .setPositiveButton("Detener orden", (dialog12, which)
                        -> {
                    mStopWorkOrder = true;
                    mSearchOtherSplitter = false;
                    mWSManager.requestWs(FindSplitterAppResponse.class, WSManager.WS.ADD_SPLITERS, getDefinition().findSplitter(splittersRequest));
                })
                .setNegativeButton("Buscar otro splitter ", (dialog1, which) -> {
                    mStopWorkOrder = false;
                    mSearchOtherSplitter = true;
                    mWSManager.requestWs(FindSplitterAppResponse.class, WSManager.WS.ADD_SPLITERS, getDefinition().findSplitter(splittersRequest));
                })
                .setIcon(R.mipmap.ic_launcher)
                .create();
        dialog.show();
    }


    public void imageViewClick(int imageViewId) {
        String photoName = "";
        if (!mPhotosValidator.contains(imageViewId)) mPhotosValidator.add(imageViewId);
        switch (imageViewId) {
            case R.id.act_edit_splitter_evidence_panoramic:
                photoName = "Panoramic";
                break;
            case R.id.act_edit_splitter_evidence_power:
                photoName = "Power";
                break;
            case R.id.act_edit_splitter_evidence_box:
                photoName = "Box";
                break;
        }
        mRequestedFile = BusinessFiles.newDocument(photoName);
        mRequestedFile = FileUtils.validFile(mRequestedFile);
        if (mRequestedFile != null) {
//            TakePictureUtils.startImportPicture(mAppCompatActivity, mRequestedFile.getAbsolutePath(), REQUEST_CODE_PICTURE);
        }
    }

    public void continueSplitterClick(int portNumberCatalog, int portBussy, String status, String closeLabel) {
        if (mPhotosValidator.size() > 2) {
            if (portNumberCatalog < portBussy) {
                MessageUtils.toast(mContext, R.string.act_register_splitter_max_total_ports);
            } else {
                boolean notProblem = false;
                switch (status) {
                    case "Saturado":
                        reason = new Reason("70", "SPLITTER SATURADO");
                        break;
                    case "No Iluminado":
                        reason = new Reason("112", "SPLITTER NO ILUMINADA");
                        break;
                    case "Dañado":
                        reason = new Reason("118", "SPLITTER PUERTOS DAÑADOS");
                        break;
                    case "Atenuado":
                        reason = new Reason("69", "SPLITTER ATENUADO");
                        break;
                    case "Sin Problemas":
                        notProblem = true;
                        break;
                }
                findSplitter(closeLabel, String.valueOf(portNumberCatalog), String.valueOf(portBussy), notProblem);
            }
        } else {
            MessageUtils.toast(mContext, "Es necesario tomar las 3 fotografias");
        }
    }

    public void savePhoto(String name, String path) {
        mFFMDataManager.tx(tx -> {
            PhotoEvidence photo = new PhotoEvidence();
            photo.name = name;
            photo.path = path;
            if (mWorkOrder != null) {
                photo.workOrder = mWorkOrder.id;
                photo.baseUrl = mWorkOrder.baseUrl;
                photo.interventionType = mWorkOrder.interventionType;
            } else {
                photo.baseUrl = "Ventas/Enlace/";
            }
            photo.serverFolder = serverFolder;
            photo.type = ImageTypeEnum.FIND_SPLITTER;

            photo = tx.save(photo);
            mFileWrapper.listPhotos.add(photo);
            tx.save(mFileWrapper);
        });
    }

    private void successFindSplitter(String requestUrl, FindSplitterAppResponse findSplitterAppResponse) {
        if (findSplitterAppResponse.result.equals("0")) {
            mAddSplittersCallback.onSuccessSaveSplitterInfo(mWorkOrder, mSplitter, mSearchOtherSplitter, reason, mStopWorkOrder);
        } else {
            onErrorLoadResponse(requestUrl, findSplitterAppResponse.resultDescription);
        }
    }

    public void onActivityResult(int requestCode, int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_PICTURE) {
                mAddSplittersCallback.onSuccessLoadImage(requestCode, mRequestedFile);
            }
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_splitter);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.ADD_SPLITERS)) {
            successFindSplitter(requestUrl, (FindSplitterAppResponse) baseResponse);
        }
    }


}
