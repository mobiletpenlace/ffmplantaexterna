package com.totalplay.fieldforcetpenlace.modules.enl.ws_tester;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponse;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;

import java.util.List;

/**
 * Created by jorgehdezvilla on 03/11/17.
 * Enlace
 */

public class WSTesterPresenter extends FFMPresenter implements WSCallback {

    public interface WSTesterCallback {
        void onRequestWs(String wsService);

        void onSuccessResponseWS(String wsService, boolean status);
    }

    private List<WSTesterActivity.TesterWS> mWsServices;
    private int mWsRequestCount;
    private WSTesterCallback mWsTesterCallback;

    public WSTesterPresenter(AppCompatActivity appCompatActivity, WSTesterCallback wsTesterCallback) {
        super(appCompatActivity);
        this.mWsTesterCallback = wsTesterCallback;
    }

    public void testServices(List<WSTesterActivity.TesterWS> wsServices) {
        mWsRequestCount = 0;
        mWsServices = wsServices;
        requestWs();
    }

    public void stopTest() {
        mWSManager.onDestroy();
    }

    private void requestWs() {
        if (mWsRequestCount < mWsServices.size()) {
            WSTesterActivity.TesterWS testerWS = mWsServices.get(mWsRequestCount);
            mWsRequestCount++;
            mWSManager.requestWs(WSBaseResponse.class, testerWS.wsName, testerWS.call);
        }
    }

    private void onSuccessTestWS(String requestUrl, boolean status) {
        requestWs();
        mWsTesterCallback.onSuccessResponseWS(requestUrl, status);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        mWsTesterCallback.onRequestWs(requestUrl);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        onSuccessTestWS(requestUrl, true);
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        onSuccessTestWS(requestUrl, false);
    }
}
