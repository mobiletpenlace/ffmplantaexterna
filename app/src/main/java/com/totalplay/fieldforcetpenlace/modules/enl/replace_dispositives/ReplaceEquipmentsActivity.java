package com.totalplay.fieldforcetpenlace.modules.enl.replace_dispositives;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives.ConfigureDispositivesPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.adapters.base.BaseTab;
import com.totalplay.fieldforcetpenlace.view.adapters.base.SimpleBaseTabAdapter;
import com.totalplay.mvp.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by totalplay on 6/26/17.
 * FieldForceManagement
 */

@SuppressWarnings("unchecked")
public class ReplaceEquipmentsActivity extends BaseFFMActivity implements GetSettingsEquipmentsPresenter.Callback,
        SettingsEquipmentsListFragment.Callback, ConfigureDispositivesPresenter.Callback {

    @BindView(R.id.act_replace_equipments_view_pager)
    ViewPager pager;
    @BindView(R.id.act_replace_equipments_view_tabs)
    TabLayout tabLayout;

    private GetSettingsEquipmentsPresenter mGetSettingsEquipmentsPresenter;
    private ConfigureDispositivesPresenter mConfigureDispositivesPresenter;

    private List<BaseTab> mBaseTabs = new ArrayList<>();

    private SettingsEquipmentsListFragment mSettingsEquipmentsListFragment;
    private ReplaceEquipmentsListFragment mReplaceEquipmentsListFragment;

    private boolean mSaveCaptureInfo = true;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, ReplaceEquipmentsActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_replace_equipments);
        setTitle("");
        ButterKnife.bind(this);

        mGetSettingsEquipmentsPresenter.loadSettingsEquipments();
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mGetSettingsEquipmentsPresenter = new GetSettingsEquipmentsPresenter(this, this),
                mConfigureDispositivesPresenter = new ConfigureDispositivesPresenter(this, this)
        };
    }

    @OnClick(R.id.act_replace_equipments_continue)
    public void onContinueClick() {
        if (mReplaceEquipmentsListFragment.isValid()) {
            List<Equipment> equipmentList = mReplaceEquipmentsListFragment.getEquipments();
            if (equipmentList.size() > 0)
                mConfigureDispositivesPresenter.sendEquipments(mReplaceEquipmentsListFragment.getEquipments());
            else
                onSuccessSettingsEquipments();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mReplaceEquipmentsListFragment != null && mSaveCaptureInfo)
            mFFMDataManager.tx(tx -> tx.save(mReplaceEquipmentsListFragment.getEquipments()));
    }

    @Override
    public void onSuccessLoadSettingsEquipments() {
        mBaseTabs.add(new BaseTab("Equipos configurados", mSettingsEquipmentsListFragment = SettingsEquipmentsListFragment.newInstance()));
        mBaseTabs.add(new BaseTab("Equipos a Reemplazar", mReplaceEquipmentsListFragment = ReplaceEquipmentsListFragment.newInstance()));
        mSettingsEquipmentsListFragment.mCallback = this;
        pager.setAdapter(new SimpleBaseTabAdapter(getSupportFragmentManager(), mBaseTabs));
        tabLayout.setupWithViewPager(pager);
    }

    @Override
    public void onReplaceDispositive(InfoEquipment infoEquipment) {
        pager.setCurrentItem(1);
        mReplaceEquipmentsListFragment.addDispositive(infoEquipment);
    }

    @Override
    public void onSuccessSettingsEquipments() {
        mSaveCaptureInfo = false;
        baseModule.finishModule(this, mWorkOrder);
    }
}
