package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;

import java.util.ArrayList;

/**
 * Created by totalplay on 6/26/17.
 * FieldForceManagement
 */

public class PaymentPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<BaseFFMFragment> mPaymentFragments;

    public PaymentPagerAdapter(FragmentManager supportFragmentManager, ArrayList<BaseFFMFragment> paymentFragments) {
        super(supportFragmentManager);
        mPaymentFragments = paymentFragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mPaymentFragments.get(position);
    }

    @Override
    public int getCount() {
        return mPaymentFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Tarjeta";
        } else if (position == 1) {
            return "Efectivo";
        } else if (position == 2) {
            return "Cheque";
        } else if (position == 3) {
            return "Transferencia";
        } else if (position == 4) {
            return "Sin Cobro";
        }
        return null;
    }
}
