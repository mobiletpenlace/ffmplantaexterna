package com.totalplay.fieldforcetpenlace.modules.enl.survey;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Question;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.utils.SignatureView;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by totalplay on 26/06/17.
 * FMM
 */

@SuppressWarnings("unchecked")
public class SurveyActivity extends BaseFFMActivity implements SurveyPresenter.Callback {

    @BindView(R.id.act_survey_one_question)
    RatingBar mOneQuestionRatinBar;
    @BindView(R.id.act_survey_two_question)
    RatingBar mTwoQuestionRatinBar;
    @BindView(R.id.act_survey_three_question)
    RatingBar mThreeQuestionRatinBar;
    @BindView(R.id.act_survey_four_question)
    RatingBar mFourQuestionRatinBar;
    @BindView(R.id.act_survey_five_question)
    RatingBar mFiveQuestionRatinBar;

    @BindView(R.id.content_survey_installation)
    LinearLayout contentSurveyInstallation;

    @BindView(R.id.act_content_signature)
    ViewGroup mContent;
    @BindView(R.id.act_signature_accept)
    CheckBox mAcceptCheckBox;

    private SurveyPresenter mSurveyPresenter;
    private List<Question> questions;

    private SignatureView mSignature;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isSurveyCompleted) {
            Intent intent = new Intent(appCompatActivity, SurveyActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        return mSurveyPresenter = new SurveyPresenter(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.enl_activity_survey);
        ButterKnife.bind(this);
        questions = new ArrayList<>();
        questions.add(new Question("1", getString(R.string.question_1), "0"));
        questions.add(new Question("2", getString(R.string.question_2), "0"));
        questions.add(new Question("3", getString(R.string.question_3), "0"));
        questions.add(new Question("4", getString(R.string.question_4), "0"));
        questions.add(new Question("5", getString(R.string.question_5), "0"));

        mSignature = new SignatureView(getApplicationContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        mSignature.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
        ));

        mContent.addView(mSignature);
        mTrainingManager.surveyActivity();
    }

    @OnClick({R.id.act_signature_dialog_save, R.id.act_signature_dialog_clear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.act_signature_dialog_clear:
                mSignature.clear();
                break;
            case R.id.act_signature_dialog_save:
                if (mAcceptCheckBox.isChecked()) {
                    File signatureFile = SignatureView.generateFile(mContent);
                    savePhoto(signatureFile.getName(), signatureFile.getPath());

                    float numberStarsOne = mOneQuestionRatinBar.getRating() * 2;
                    float numberStarsTwo = mTwoQuestionRatinBar.getRating() * 2;
                    float numberStarsThree = mThreeQuestionRatinBar.getRating() * 2;
                    float numberStarsFour = mFourQuestionRatinBar.getRating() * 2;
                    float numberStarsFive = mFiveQuestionRatinBar.getRating() * 2;
                    if (numberStarsOne > 0 && numberStarsTwo > 0 && numberStarsThree > 0 && numberStarsFour > 0) {
                        questions.get(0).idResponse = String.valueOf(numberStarsOne);
                        questions.get(1).idResponse = String.valueOf(numberStarsTwo);
                        questions.get(2).idResponse = String.valueOf(numberStarsThree);
                        questions.get(3).idResponse = String.valueOf(numberStarsFour);
                        questions.get(4).idResponse = String.valueOf(numberStarsFive);
                        mSurveyPresenter.sendQuestions(questions);
                    } else {
                        MessageUtils.toast(this, R.string.dialog_error_question_empty);
                    }
                } else {
                    MessageUtils.toast(getApplicationContext(), R.string.act_signature_accept_privacity);
                }
                break;
        }
    }

    public void savePhoto(String name, String path) {
        mFFMDataManager.tx(tx -> {
            PhotoEvidence photo = new PhotoEvidence();
            photo.name = name;
            photo.path = path;

            photo.workOrder = mWorkOrder.id;
            photo.serverFolder = ServerFolders.EVIDENCES;
            photo.baseUrl = mWorkOrder.baseUrl;
            photo.interventionType = mWorkOrder.interventionType;
            photo.type = ImageTypeEnum.SIGNATURE;

            tx.save(photo);
        });
    }

    @Override
    public void onSuccessSurvey(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
    }
}
