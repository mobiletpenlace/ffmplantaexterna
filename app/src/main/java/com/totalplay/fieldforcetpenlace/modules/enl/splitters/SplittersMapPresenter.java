package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.bus.BusManager;
import com.totalplay.fieldforcetpenlace.background.bus.events.LocationUpdatedEvent;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SplittersRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.SplittersResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;

/**
 * Created by jorgehdezvilla on 06/09/17.
 * FFM
 */

public class SplittersMapPresenter extends FFMPresenter implements WSCallback {

    public Boolean mWasFirstLoadSplitters = false;
    private SplittersMapCallback mSplittersMapCallback;

    public interface SplittersMapCallback {
        void onSuccessLoadSplitters(ArrayList<Splitter> splitters);

        void onSuccessUpdateLocation(Location location);
    }

    public SplittersMapPresenter(AppCompatActivity appCompatActivity, SplittersMapCallback splittersMapCallback) {
        super(appCompatActivity);
        mSplittersMapCallback = splittersMapCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void loadSplitters() {
        Location location = TotalPlayLocationService.getLastLocation();
        mSplittersMapCallback.onSuccessUpdateLocation(location);
        if (location != null) {
            User user = mFFMDataManager.queryWhere(User.class).findFirst();
            SplittersRequest splittersRequest = new SplittersRequest();
            splittersRequest.radio = "200";
            splittersRequest.name = user.operatorId;//user.employeeId;
            splittersRequest.latitude = String.valueOf(location.getLatitude());
            splittersRequest.longitude = String.valueOf(location.getLongitude());
            mWSManager.requestWs(SplittersResponse.class, WSManager.WS.SPLITTERS, getDefinition().splitters(splittersRequest));
        } else {
            MessageUtils.stopProgress();
            MessageUtils.toast(mContext, "Tratanto de obtener ubicación");
        }
    }

    private void successLoadedSplitters(String requestUrl, SplittersResponse splittersResponse) {
        if (splittersResponse.result != null && splittersResponse.result.equals("0")
                && splittersResponse.splitter != null) {
            mSplittersMapCallback.onSuccessLoadSplitters(splittersResponse.splitter.splitters);
        } else {
            onErrorLoadResponse(requestUrl, splittersResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (!mWasFirstLoadSplitters) {
            mWasFirstLoadSplitters = true;
            MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_splitters);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.SPLITTERS)) {
            successLoadedSplitters(requestUrl, (SplittersResponse) baseResponse);
        }
    }

    @Override
    public void onLocationUpdatedEvent(LocationUpdatedEvent event) {
        loadSplitters();
        mSplittersMapCallback.onSuccessUpdateLocation(event.location);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusManager.unregister(this);
    }

}
