package com.totalplay.fieldforcetpenlace.modules.enl.load_work_order;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.SearchWorkOrdersResponse;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.library.utils.Training.TrainingManager;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.UserStatus;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ConfigWorkOrder;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.DNPhones;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.auxiliars.AssistantsParameter;
import com.totalplay.fieldforcetpenlace.modules.enl.operator_status.ChangeOperatorStatusActivity;
import com.totalplay.fieldforcetpenlace.modules.enl.operator_status.ChangeOperatorStatusPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.modules.definition.FlowKey;
import com.totalplay.fieldforcetpenlace.modules.definition.ModuleKey;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.totalplay.fieldforcetpenlace.library.enums.OperatorStatus.OUT_SERVICE;

public class LoadWorkOrdersActivity extends BaseFFMActivity implements ChangeOperatorStatusPresenter.ChangeOperatorStatusCallback {

    @BindView(R.id.act_main_change_status)
    Button mChangeStatusButton;
    @BindView(R.id.act_main_change_status_color)
    View mChngeStatusColorView;
    @BindView(R.id.act_main_map_fragment_ot_assigned_test_content)
    ViewGroup mOtAssignedImageview;

    @BindView(R.id.act_main_content_actions)
    ViewGroup mActionsViewGroup;

    private WorkOrdersFragment mWorkOrdersFragment;
    private ChangeOperatorStatusPresenter mChangeOperatorStatusPresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, LoadWorkOrdersActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        WSManager.DEBUG_ENABLED = false;
        TrainingManager.TRAINING_ENABLED = false;
        try {
            mFFMDataManager.deleteWorkOrder();
        } catch (Exception ignored) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.enl_activity_load_work_orders);
        ButterKnife.bind(this);

        Intent msgIntent = new Intent(this, TotalPlayLocationService.class);
        startService(msgIntent);
        mWorkOrdersFragment = (WorkOrdersFragment) getSupportFragmentManager().findFragmentById(R.id.act_main_map_fragment);
        if (mUser.firstAccess.equals("true")) {
            if (!mUser.isCompleteTraningMain) {
                mTrainingManager.mainActivity(() -> mWorkOrdersFragment.onSuccessUpdateLocation(TotalPlayLocationService.getLastLocation()));
                mFFMDataManager.tx(tx -> {
                    mUser = mFFMDataManager.queryWhere(User.class).findFirst();
                    mUser.isCompleteTraningMain = true;
                    tx.save(mUser);
                });
            }
        }
        if (mUser.owner != null && !mUser.owner.contains("1")) {
            mActionsViewGroup.setVisibility(View.GONE);
        }
        onSuccessChangeOperatorStatus(mUser);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mChangeOperatorStatusPresenter = new ChangeOperatorStatusPresenter(this, this);
    }

    @OnClick({R.id.act_main_materials, R.id.act_main_profile, R.id.act_main_history, R.id.act_main_splitters})
    public void onClick(View view) {
        switch (view.getId()) {
           /*case R.id.act_main_materials:
                RoutingApp.OnButtonMaterialsEvent(this);
                break;*/
            case R.id.act_main_splitters:
//                RoutingApp.OnButtonSplitterEvent(this);
                break;
            case R.id.act_main_profile:
                baseModule.openModule(this, ModuleKey.MY_PROFILE, null);
                break;
        }
    }

    @OnClick(R.id.act_main_open_menu)
    public void onCloseSession(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.getMenuInflater().inflate(R.menu.main_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.main_menu_close_session) {
                mFFMDataManager.deleteAll();
                mChangeOperatorStatusPresenter.sendOperatorStatus(new UserStatus("7", OUT_SERVICE, R.color.red_splitter));
                baseModule.startFlow(this, FlowKey.MAIN_FLOW, null);
                finish();
            } else if (item.getItemId() == R.id.main_menu_change_assistants) {
                baseModule.openModule(this, ModuleKey.ASSISTANTS, new AssistantsParameter(true));
            } else if (item.getItemId() == R.id.main_menu_start_training) {
                startTrainingOT();
            } else if (item.getItemId() == R.id.main_menu_start_intro) {
                mUser.isCompletePreview = false;
                mUser.firstAccess = "true";
                baseModule.openModule(this, ModuleKey.INTRODUCCTION_APP, mUser);
            }
            return true;
        });
        popupMenu.show();
    }

    @OnClick(R.id.act_main_change_status)
    public void onChangeStauts() {
        baseModule.openModule(this, ModuleKey.OPERATOR_STATUS, null);
    }

    public void startTrainingOT() {
        WSManager.DEBUG_ENABLED = true;
        TrainingManager.TRAINING_ENABLED = true;
        Gson gson = new Gson();
        String json = "{\n" +
                "    \"result\": \"0\",\n" +
                "    \"resultDescripcion\": \"Operacion exitosa\",\n" +
                "    \"Ot\": [\n" +
                "        {\n" +
                "            \"Id_Operario\": \"TOTALDEMO\",\n" +
                "            \"NumeroOT\": \"-1\",\n" +
                "            \"NumeroTicket\": \" \",\n" +
                "            \"NumeroOS\": \"OS-01964\",\n" +
                "            \"NumeroCuenta\": \"0210001063\",\n" +
                "            \"NombreCliente\": \"TEST ENLACE- OPORTUNIDAD  ENLACE\",\n" +
                "            \"NombreContacto\": \"TEST MIDDLEWARE DIRZO BATMAN\",\n" +
                "            \"CanalVenta\": \"Página web\",\n" +
                "            \"Calle\": \"periferico sur\",\n" +
                "            \"NInterior\": \"null\",\n" +
                "            \"NExterior\": \"4119\",\n" +
                "            \"Colonia\": \"Fuentes del Pedregal\",\n" +
                "            \"Municipio\": \"Tlalpan\",\n" +
                "            \"Ciudad\": \"Ciudad de Mexico\",\n" +
                "            \"CodigoPostal\": \"14141\",\n" +
                "            \"Referencia\": \"na\",\n" +
                "            \"entreCalles\": \"null\",\n" +
                "            \"Lat\": \"19.37224624655047\",\n" +
                "            \"Long1\": \"-99.15061789849244\",\n" +
                "            \"TelefonoContacto\": \"5548945646\",\n" +
                "            \"TelefonoEmpresa\": \"5556456456\",\n" +
                "            \"Extencion\": null,\n" +
                "            \"PaqueteContratado\": \"CONEXION 4 Analógico D\",\n" +
                "            \"Cluser\": \"CIUDAD DE MEXICO\",\n" +
                "            \"IdEstatusOT\": \"2\",\n" +
                "            \"DescEstatusOT\": \"ASIGNADA\",\n" +
                "            \"TipoIntervencion\": \"48\",\n" +
                "            \"SubTipoIntervencion\": \"49\",\n" +
                "            \"idEstadoOT\": \"9\",\n" +
                "            \"descEstadoOT\": \"PROGRAMADA PERO NO INICIADA\",\n" +
                "            \"idMotivoOT\": \"23\",\n" +
                "            \"DescMotivoOT\": \"ASIGNADO POR DESPACHO\",\n" +
                "            \"UnidadNegocio\": \"ENLACE\",\n" +
                "            \"DiasRetraso\": \"-1\",\n" +
                "            \"VISITAS\": \"0\",\n" +
                "            \"idPropietario\": \"1\",\n" +
                "            \"hora_compromiso\": \"20:00:00\",\n" +
                "            \"url_imagenes\": \"Ventas/Enlace/0210001063/\",\n" +
                "            \"url_checkList\": \"https://msstest.totalplay.com.mx/TFE/DownloadFiles/dGZldXNlcjp0ZjNwNHNzdzByZA==/L1ZlbnRhcy9FbmxhY2UvMDIxMDAwMTA2My9Eb2N1bWVudG9zL2NoZWNrTGlzdC54bHN4\",\n" +
                "            \"tiempoEstimado\": \"120\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        SearchWorkOrdersResponse searchWorkOrdersResponse = gson.fromJson(json, SearchWorkOrdersResponse.class);
        WorkOrder workOrder = searchWorkOrdersResponse.workOrders.get(0);
        Location latLng = TotalPlayLocationService.getLastLocation();
        if (latLng != null) {
            workOrder.latitude = Double.toString(latLng.getLatitude());
            workOrder.longitude = Double.toString(latLng.getLongitude());
            if (workOrder.configWorkOrder == null) {
                workOrder.configWorkOrder = new ConfigWorkOrder();
                workOrder.configWorkOrder.dnPhones = new DNPhones();
            }
            mFFMDataManager.tx(tx -> tx.save(workOrder));
//            WorkOrderInfoActivity.launch(this);
        } else {
            MessageUtils.toast(this, "No se ha podido obtener tu ubicación. Intente nuevamente");
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChangeOperatorStatusActivity.REQUEST_CODE_CHANGE_STATUS) {
            if (resultCode == RESULT_OK) {
                User user = (User) data.getSerializableExtra(Keys.EXTRA_USER);
                onSuccessChangeOperatorStatus(user);
            }
        }
    }

    @OnClick(R.id.act_main_content_actions_training)
    public void onTrainingClick() {
        startTrainingOT();
    }

    @Override
    public void onSuccessChangeOperatorStatus(User user) {
        if (user != null && user.userStatus != null) {
            mChangeStatusButton.setText(String.format("Estatus: %s", user.userStatus.name));
            Drawable circle = getResources().getDrawable(R.drawable.circle);
            circle.setColorFilter(ContextCompat.getColor(this, user.userStatus.color), PorterDuff.Mode.MULTIPLY);
            mChngeStatusColorView.setBackground(circle);
        }
    }
}
