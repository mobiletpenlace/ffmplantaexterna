package com.totalplay.fieldforcetpenlace.modules.enl.replace_dispositives;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.fieldforcetpenlace.view.adapters.CategorySettingsEquipmentsAdapter;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleRecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * Created by jorgehdezvilla on 19/12/17.
 * FFM Enlace
 */

public class SettingsEquipmentsListFragment extends BaseFFMFragment {

    private BaseSimpleRecyclerView mBaseSimpleRecyclerView;
    private Disposable mDispositiveDisposable;

    public Callback mCallback;

    public interface Callback {
        void onReplaceDispositive(InfoEquipment infoEquipment);
    }

    private View.OnClickListener mOnClickListener = view -> {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setMessage("¿Está seguro de reemplazar el equipo seleccionado?")
                .setPositiveButton("Aceptar", (dialog, which) -> {
                    InfoEquipment infoEquipment = (InfoEquipment) view.getTag();
                    infoEquipment.wasReplace = true;
                    mFFMDataManager.tx(tx -> tx.save(infoEquipment));
                    mCallback.onReplaceDispositive(infoEquipment);
                })
                .setNegativeButton("Cancelar", null);
        builder.create().show();
    };

    public static SettingsEquipmentsListFragment newInstance() {
        SettingsEquipmentsListFragment settingsEquipmentsListFragment = new SettingsEquipmentsListFragment();
        Bundle bundle = new Bundle();
        settingsEquipmentsListFragment.setArguments(bundle);
        return settingsEquipmentsListFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_base_recycler, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBaseSimpleRecyclerView = new BaseSimpleRecyclerView(view, R.id.fr_reclycler)
                .setAdapter(new CategorySettingsEquipmentsAdapter(mOnClickListener));

        mDispositiveDisposable = mFFMDataManager.listObservable(InfoEquipment.class, "wasReplace", false).subscribe(this::onSuccessLoadDispositives);
    }

    private void onSuccessLoadDispositives(List<InfoEquipment> infoEquipments) {
        HashMap<String, List<InfoEquipment>> dispositiveTypes = new HashMap<>();
        for (InfoEquipment dispositive : infoEquipments) {
            if (!dispositiveTypes.containsKey(dispositive.type)) {
                dispositiveTypes.put(dispositive.type, new ArrayList<>());
            }
            dispositiveTypes.get(dispositive.type).add(dispositive);
        }

        List<List<InfoEquipment>> dispositives = new ArrayList<>();
        for (String key : dispositiveTypes.keySet()) {
            dispositives.add(dispositiveTypes.get(key));
        }
        mBaseSimpleRecyclerView.update(dispositives);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mDispositiveDisposable.dispose();
    }
}
