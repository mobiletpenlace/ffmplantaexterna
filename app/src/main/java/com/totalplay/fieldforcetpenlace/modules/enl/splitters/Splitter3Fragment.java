package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.mvp.BaseFragment;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by icortes on 07/07/17.
 * FFM Enlace
 */

public class Splitter3Fragment extends BaseFragment {

    @BindView(R.id.port1_izq)
    LinearLayout fr3Port1Izp;
    @BindView(R.id.port2_izq)
    LinearLayout fr3Port2Izp;
    @BindView(R.id.port3_izq)
    LinearLayout fr3Port3Izp;
    @BindView(R.id.port4_izq)
    LinearLayout fr3Port4Izp;
    @BindView(R.id.port5_izq)
    LinearLayout fr3Port5Izp;
    @BindView(R.id.port6_izq)
    LinearLayout fr3Port6Izp;
    @BindView(R.id.port7_izq)
    LinearLayout fr3Port7Izp;
    @BindView(R.id.port8_izq)
    LinearLayout fr3Port8Izp;
    @BindView(R.id.port1_der)
    LinearLayout fr3Port1Der;
    @BindView(R.id.port2_der)
    LinearLayout fr3Port2Der;
    @BindView(R.id.port3_der)
    LinearLayout fr3Port3Der;
    @BindView(R.id.port4_der)
    LinearLayout fr3Port4Der;
    @BindView(R.id.port5_der)
    LinearLayout fr3Port5Der;
    @BindView(R.id.port6_der)
    LinearLayout fr3Port6Der;
    @BindView(R.id.port7_der)
    LinearLayout fr3Port7Der;
    @BindView(R.id.port8_der)
    LinearLayout fr3Port8Der;

    Boolean[] array = new Boolean[16];
    LinearLayout[] imagenes;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_splitter_3, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        Arrays.fill(array, Boolean.FALSE);
        imagenes = new LinearLayout[]{fr3Port1Izp, fr3Port2Izp, fr3Port3Izp, fr3Port4Izp, fr3Port5Izp, fr3Port6Izp, fr3Port7Izp, fr3Port8Izp, fr3Port1Der,
                fr3Port2Der, fr3Port3Der, fr3Port4Der, fr3Port5Der, fr3Port6Der, fr3Port7Der, fr3Port8Der};
    }

    @OnClick({R.id.port1_izq, R.id.port2_izq, R.id.port3_izq, R.id.port4_izq, R.id.port5_izq, R.id.port6_izq, R.id.port7_izq, R.id.port8_izq, R.id.port1_der, R.id.port2_der, R.id.port3_der, R.id.port4_der, R.id.port5_der, R.id.port6_der, R.id.port7_der, R.id.port8_der})
    public void onPortSelected(View view) {
        getActivity();
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(100);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getActivity(), notification);
        r.play();
        switch (view.getId()) {
            case R.id.port1_izq:
                if (array[0]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[0] = true;
                }
                getPort("1");
                break;
            case R.id.port2_izq:
                if (array[1]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[1] = true;
                }
                getPort("2");
                break;
            case R.id.port3_izq:
                if (array[2]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[2] = true;
                }
                getPort("3");
                break;
            case R.id.port4_izq:
                if (array[3]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[3] = true;
                }
                getPort("4");
                break;
            case R.id.port5_izq:
                if (array[4]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[4] = true;
                }
                getPort("5");
                break;
            case R.id.port6_izq:
                if (array[5]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[5] = true;
                }
                getPort("6");
                break;
            case R.id.port7_izq:
                if (array[6]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[6] = true;
                }
                getPort("7");
                break;
            case R.id.port8_izq:
                if (array[7]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[7] = true;
                }
                getPort("8");
                break;
            case R.id.port1_der:
                if (array[8]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[8] = true;
                }
                getPort("1");
                break;
            case R.id.port2_der:
                if (array[9]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[9] = true;
                }
                getPort("2");
                break;
            case R.id.port3_der:
                if (array[10]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[10] = true;
                }
                getPort("3");
                break;
            case R.id.port4_der:
                if (array[11]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[11] = true;
                }
                getPort("4");
                break;
            case R.id.port5_der:
                if (array[12]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[12] = true;
                }
                getPort("5");
                break;
            case R.id.port6_der:
                if (array[13]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[13] = true;
                }
                getPort("6");
                break;
            case R.id.port7_der:
                if (array[14]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[14] = true;
                }
                getPort("7");
                break;
            case R.id.port8_der:
                if (array[15]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[15] = true;
                }
                getPort("8");
                break;
        }
    }

    private void getPort(String port) {
        SplitterSelectedActivity.numberPort = port;
        for (int i = 0; i < array.length; i++) {
            if (i < 8) {
                if (array[i]) {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_asignado_izq));
                } else {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_disponible_izq));
                }
            } else {
                if (array[i]) {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_asignado_der));
                } else {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_disponible_der));
                }
            }
        }
    }


}
