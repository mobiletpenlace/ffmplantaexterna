package com.totalplay.fieldforcetpenlace.modules.enl.materials;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetMaterialRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMaterials;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;

/**
 * Created by NS-TOTALPLAY on 14/09/2017.
 * FFM
 */

public class MaterialsPresenter extends FFMPresenter implements WSCallback {

    private ArrayList<GetMaterials.Materials> mMaterialList = new ArrayList<>();
    private MaterialsCallback mMaterialsCallback;

    public interface MaterialsCallback {

        void onSuccessLoadMaterials(ArrayList<GetMaterials.Materials> mMaterialLists);

    }

    public MaterialsPresenter(AppCompatActivity appCompatActivity, MaterialsCallback materialsCallback) {
        super(appCompatActivity);
        mMaterialsCallback = materialsCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void loadMaterials() {
        GetMaterialRequest getMaterialRequest = new GetMaterialRequest(mUser.operatorId);
        mWSManager.requestWs(GetMaterials.class, WSManager.WS.GET_MATERIALS, getDefinition().consultingMaterials(getMaterialRequest));
    }

    private void successConsultingMaterials(String requestUrl, GetMaterials getMaterials) {
        if (getMaterials.result.equals("0")) {
            mMaterialList.clear();
            if (getMaterials.listMaterials != null && !getMaterials.listMaterials.isEmpty()) {
                for (GetMaterials.Materials item : getMaterials.listMaterials) {
                    mMaterialList.add(item);
                }
            }
            MessageUtils.stopProgress();
            MessageUtils.toast(mContext, getMaterials.resultDescription);
            mMaterialsCallback.onSuccessLoadMaterials(mMaterialList);
        } else {
            onErrorLoadResponse(requestUrl, getMaterials.result);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_data);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.GET_MATERIALS)) {
            successConsultingMaterials(requestUrl, (GetMaterials) baseResponse);
        }
    }

}

