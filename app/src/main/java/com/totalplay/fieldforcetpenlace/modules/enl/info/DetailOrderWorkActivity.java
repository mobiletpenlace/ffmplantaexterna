package com.totalplay.fieldforcetpenlace.modules.enl.info;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.Quotation;
import com.totalplay.fieldforcetpenlace.library.enums.ReasonDefinition;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.OptionsMenuActivity;
import com.totalplay.mvp.BasePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressWarnings("unchecked")
public class DetailOrderWorkActivity extends OptionsMenuActivity
        implements QuotationPresenter.ActivationServiceCallback, DetailWorkOrderPresenter.Callback {

    @BindView(R.id.act_detail_work_order_name)
    TextView mNameTextView;
    @BindView(R.id.act_detail_contact_name)
    TextView mContactNameTextView;
    @BindView(R.id.act_detail_work_account_phone)
    TextView mContactPhoneTextView;
    @BindView(R.id.act_detail_work_order_extension)
    TextView mContactExtPhoneTextView;
    @BindView(R.id.act_detail_work_order_pack)
    TextView mPackTextView;
    @BindView(R.id.act_detail_work_order_pack_price)
    TextView mPackPriceTextView;
    @BindView(R.id.act_detail_work_account_number)
    TextView mAccountNumberTextView;
    @BindView(R.id.act_detail_work_order_sale_channel)
    TextView mSaleChannelTextView;
    @BindView(R.id.act_detail_work_order_type)
    TextView mWorkOrderTypeTextView;
    @BindView(R.id.act_detail_work_order_addons)
    TextView mAddonsTextView;

    @BindView(R.id.act_work_order_detail_next)
    Button mWorkOrderDetailNext;

    QuotationPresenter mQuotationPresenter;
    DetailWorkOrderPresenter mDetailWorkOrderPresenter;


    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, DetailOrderWorkActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_detail_order_work);
        setTitle("");
        ButterKnife.bind(this);

        initReport(getString(R.string.dialog_error_work_order), ReasonDefinition.getReasonsWorkOrder(mWorkOrder));

        mQuotationPresenter.loadQuotation();
        onLoadedWorkOrder();

        mTrainingManager.detailOrderWorkActivity();
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mDetailWorkOrderPresenter = new DetailWorkOrderPresenter(this, this),
                mQuotationPresenter = new QuotationPresenter(this, this)
        };
    }

    public void onLoadedWorkOrder() {
        mNameTextView.setText(mWorkOrder.clientName);
        mContactNameTextView.setText(mWorkOrder.contactName);
        mContactPhoneTextView.setText(mWorkOrder.contactPhone);
        mContactExtPhoneTextView.setText(mWorkOrder.phoneExt);
        mWorkOrderTypeTextView.setText(R.string.act_detail_work_order_type_installation);
        mWorkOrderDetailNext.setText("Iniciar Instalacion");
        mPackTextView.setText(mWorkOrder.packageContracted);
        mAccountNumberTextView.setText(mWorkOrder.account);
        mSaleChannelTextView.setText(mWorkOrder.saleChannel);
    }

    @OnClick(R.id.act_work_order_detail_next)
    public void onClick(View view) {
        mDetailWorkOrderPresenter.onContinueClick();
    }

    @Override
    public void onSuccessLoadQuotationInfo(Quotation quotationInfo) {
        if (quotationInfo != null) {
            String addons = "";
            for (ServicePlan servicePlan : quotationInfo.servicePlans) {
                if (servicePlan.wasAdditional.equals("true")) {
                    addons += servicePlan.planLabel + "\n";
                }
            }
            mAddonsTextView.setText(addons);
            if (quotationInfo.firstRent.equals("true")) {
                mPackPriceTextView.setText(quotationInfo.amount);
            } else {
                mPackPriceTextView.setText("No realizar cobro");
            }
        }
    }

    @Override
    public void onSuccessChangeStatus(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
    }
}





