package com.totalplay.fieldforcetpenlace.modules.enl.selfie_photo;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.fieldforcetpenlace.background.queue.jobs.SynchronizeImagesJob;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.definition.FlowKey;
import com.totalplay.fieldforcetpenlace.modules.definition.ModuleKey;
import com.totalplay.fieldforcetpenlace.modules.enl.profile.MyProfilePresenter;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureInput;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureModule;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureOutput;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by reype on 15/11/2017.
 * Enlace
 */

@SuppressWarnings("unchecked")
public class UserProfileActivity extends BaseFFMActivity {

    private static final int REQUEST_CODE_PROFILE = 0x0021;

    @BindView(R.id.act_my_profile_photo_image)
    CircleImageView mProfilePhotoImageView;
    @BindView(R.id.employe_name)
    TextView mEmployeName;
    @BindView(R.id.company)
    TextView mCompany;
    @BindView(R.id.relative_profile)
    RelativeLayout mRelativeProfile;

    public Boolean takePicture = false;
    private MyProfilePresenter mMyProfilePresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, User user) {
        if (user.profilePhoto == null || user.profilePhoto.equals("")) {
            Intent intent = new Intent(appCompatActivity, UserProfileActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, user);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_user_profile);
        setTitle("");
        ButterKnife.bind(this);
        User user = mFFMDataManager.queryWhere(User.class).findFirst();
        if (user != null) {
            mEmployeName.setText(user.completeName != null ? user.completeName : "");
            mCompany.setText(user.company != null ? user.company : "");
        }

    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mMyProfilePresenter = new MyProfilePresenter(this),
        };
    }

    @OnClick({R.id.act_my_profile_photo_image, R.id.act_img_profile,})
    public void onImageClick(View view) {
        baseModule.openModule(this, ModuleKey.TAKE_PICTURE, new TakePictureInput(mUser.employeeId, 33));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            TakePictureOutput takePictureOutput = (TakePictureOutput) data.getSerializableExtra(TakePictureModule.TAKE_PICTURE_OUTPUT_KEY);
            if (mProfilePhotoImageView != null && takePictureOutput.file != null) {
                mProfilePhotoImageView.setPadding(0, 0, 0, 0);
                Glide.with(this)
                        .load(takePictureOutput.file)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(mProfilePhotoImageView);
                mMyProfilePresenter.savePhoto(takePictureOutput.file.getName(), takePictureOutput.file.getAbsolutePath());
                takePicture = true;
            }
        }
    }


    @OnClick(R.id.act_arrival_next_profile)
    public void onNextClick(View view) {
        if (!takePicture) {
            MessageUtils.toast(this, "Debe tomarse una foto");
        } else {
            QueueManager.add(new SynchronizeImagesJob(mUser, mFFMDataManager.queryWhere(PhotoEvidence.class).filter("synced", false).list()));
            baseModule.finishModule(this, mUser);
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage("¿Está seguro de cerrar sesión y regresar a la pantalla de inicio de sesión?")
                    .setPositiveButton("Aceptar", (dialog, which) -> {
                        mFFMDataManager.deleteAll();
                        UserProfileActivity.this.finish();
                        baseModule.startFlow(UserProfileActivity.this, FlowKey.MAIN_FLOW, null);
                    })
                    .setNegativeButton("Cancelar", null);
            builder.create().show();
        } else {
            super.onBackPressed();
        }
    }


}
