package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhoneModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.view.adapters.PhoneServiceAdapter;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.fieldforcetpenlace.view.utils.UIUtils;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

public class PhoneFragment extends BaseFFMFragment implements PhonePresenter.PhoneCallback {

    @BindView(R.id.act_service_activation_phone)
    ViewGroup mPhoneLayout;
    @BindView(R.id.act_service_activation_phone_zip_code)
    EditText mZipCodeEditText;
    @BindView(R.id.act_service_activation_phone_quantity)
    EditText mQuantityEditText;
    @BindView(R.id.act_service_activation_phone_pack)
    TextView mPackTextView;

    private PhonePresenter mPhonePresenter;

    private PhoneServiceAdapter mPhoneServiceAdapter;


    public static PhoneFragment newInstance() {
        PhoneFragment phoneFragment = new PhoneFragment();
        Bundle bundle = new Bundle();
        phoneFragment.setArguments(bundle);
        return phoneFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_phone, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        if (mWorkOrder != null) {
            mPackTextView.setText(mWorkOrder.packageContracted);

            new BaseSimpleRecyclerView(view, R.id.act_service_activation_phone_services)
                    .setAdapter(mPhoneServiceAdapter = new PhoneServiceAdapter())
                    .addBottomOffsetDecoration(200);

            setUpWithWorkOrder(mWorkOrder);
        }
    }

    @Override
    public BasePresenter getPresenter() {
        return mPhonePresenter = new PhonePresenter((AppCompatActivity) getActivity(), this);
    }

    public void setUpWithWorkOrder(WorkOrder workOrder) {
        mZipCodeEditText.setText(workOrder.zipCode);
        mZipCodeEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mPhonePresenter.obtainDN();
                return true;
            }
            return false;
        });
        mQuantityEditText.setText(workOrder.quotationInfo.totalDNs);
        mPhonePresenter.obtainDN();
    }

    public List<PhoneModel> getPhoneModels() {
        return mPhonePresenter.getPhoneModels();
    }

    @Override
    public void onSuccessLoadPhonePlans(List<ServicePlan> mPhoneServicesPlans) {
        mPhoneServiceAdapter.update(mPhoneServicesPlans);
    }

    @Override
    public void onNotFindPhonePlanEvent(boolean phoneFound) {
        if (!phoneFound) UIUtils.disableEnableControls(false, mPhoneLayout);
        mPhoneLayout.setVisibility(phoneFound ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onSuccessConfiguredDevicesDnEvent() {
        UIUtils.disableEnableControls(false, mPhoneLayout);
    }

}
