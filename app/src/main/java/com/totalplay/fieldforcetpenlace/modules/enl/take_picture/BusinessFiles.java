package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import android.os.Environment;

import java.io.File;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

public class BusinessFiles {

    private static final String FFM_FILES_FOLDER = "FFM/";

    public static File newDocument(String name) {
        if (SystemUtils.isStorageAvailable()) {
            return new File(
                    Environment.getExternalStorageDirectory(), FFM_FILES_FOLDER + "documents/" + name + ".jpg"
            );
        }
        return null;
    }
}
