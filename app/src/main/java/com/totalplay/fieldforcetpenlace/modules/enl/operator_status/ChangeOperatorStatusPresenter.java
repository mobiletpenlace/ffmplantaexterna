package com.totalplay.fieldforcetpenlace.modules.enl.operator_status;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ChangeOperatorRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.BaseResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.UserStatus;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

/**
 * Created by jorgehdezvilla on 12/11/17.
 * Enlace
 */

public class ChangeOperatorStatusPresenter extends FFMPresenter implements WSCallback {

    private UserStatus mUserStatus;
    private ChangeOperatorStatusCallback mChangeOperatorStatusCallback;

    public interface ChangeOperatorStatusCallback {
        void onSuccessChangeOperatorStatus(User user);
    }

    public ChangeOperatorStatusPresenter(AppCompatActivity appCompatActivity, ChangeOperatorStatusCallback changeOperatorStatusCallback) {
        super(appCompatActivity);
        mChangeOperatorStatusCallback = changeOperatorStatusCallback;
    }

    public void reloadUser() {
        mUser = mFFMDataManager.queryWhere(User.class).findFirst();
    }

    public void sendOperatorStatus(UserStatus userStatus) {
        this.mUserStatus = userStatus;
        ChangeOperatorRequest changeOperatorRequest = new ChangeOperatorRequest();
        changeOperatorRequest.operatorId = mUser.operatorId;
        changeOperatorRequest.userIdUpdate = mUser.operatorId;
        changeOperatorRequest.stateId = mUserStatus.id;
        mWSManager.requestWs(BaseResponse.class, WSManager.WS.CHANGE_OPERATOR_STATUS, getDefinition().changeOperatorStatus(changeOperatorRequest));
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, "Enviando información...");
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.CHANGE_OPERATOR_STATUS)) {
            BaseResponse changeOperatorStatusResponse = (BaseResponse) baseResponse;
            if (changeOperatorStatusResponse.result.equals("0")) {
                mUser = mFFMDataManager.queryWhere(User.class).findFirst();
                if (mUser != null) {
                    mUser.userStatus = mUserStatus;
                    mFFMDataManager.tx(tx -> tx.save(mUser));
                    MessageUtils.toast(mContext, changeOperatorStatusResponse.resultDescription);
                    mChangeOperatorStatusCallback.onSuccessChangeOperatorStatus(mUser);
                }
            } else {
                onErrorLoadResponse(requestUrl, changeOperatorStatusResponse.resultDescription);
                mChangeOperatorStatusCallback.onSuccessChangeOperatorStatus(mUser);
            }
        }
    }
}
