package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.fieldforcetpenlace.background.queue.jobs.SetFlagJob;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidaCuentaRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ValidaCuentaReponse;
import com.totalplay.fieldforcetpenlace.library.enums.FlagType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

/**
 * Created by reype on 18/10/2017.
 * Enlace
 */

public class ValidaAppPresenter extends FFMPresenter implements WSCallback {
    private ValidaAppCallback mValidaCallback;

    public ValidaAppPresenter(AppCompatActivity appCompatActivity, ValidaAppCallback validaAppCallback) {
        super(appCompatActivity);
        mValidaCallback = validaAppCallback;
    }

    public interface ValidaAppCallback {

        void onRequiredActivationsProcess();

        void onActivatedAccountStatus(WorkOrder workOrder);

    }

    public void validateApp() {
        ValidaCuentaRequest validaCuenta = new ValidaCuentaRequest(mWorkOrder.account);
        mWSManager.requestWs(ValidaCuentaReponse.class, WSManager.WS.VALIDATE_APP, getDefinition().validaCuentaENL(validaCuenta));
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }


    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_validate_account);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.VALIDATE_APP)) {
            ValidaCuentaReponse valida = (ValidaCuentaReponse) baseResponse;
            //// SI ES 1 LA CUENTA NO SE ENCUENTRA FACTURANDO
            if (valida.result.equals("0")) {
                Answers.getInstance().logCustom(
                        new CustomEvent("Activación")
                                .putCustomAttribute("Usuario", mUser.employeeId)
                                .putCustomAttribute("OT", mWorkOrder.id)
                                .putCustomAttribute("Cuenta", mWorkOrder.account)
                );
                mFFMDataManager.tx(tx -> {
                    mWorkOrder.isActivate = true;
                    tx.save(mWorkOrder);
                });
                MessageUtils.toast(mContext, R.string.dialog_success_activated);
                QueueManager.add(new SetFlagJob(FlagType.ACTIVATED_ACCOUNT, mWorkOrder.id));
                mValidaCallback.onActivatedAccountStatus(mWorkOrder);
            } else {
                mValidaCallback.onRequiredActivationsProcess();
            }

        }
    }

}
