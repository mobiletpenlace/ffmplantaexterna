package com.totalplay.fieldforcetpenlace.modules.enl.auxiliars;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AssistantsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.QueryWorkTeamRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SaveWorkAssistantTeamRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.AssistantResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.BaseResponse;
import com.totalplay.fieldforcetpenlace.library.utils.DateUtils;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Assistant;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.Collections;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by jorgehdezvilla on 06/09/17.
 * FFM
 */


public class AssistantsPresenter extends FFMPresenter implements WSCallback {

    private AssistantsCallback mAssistantsCallback;

    public interface AssistantsCallback {

        void onSuccessLoadAssistants(List<Assistant> assistantList);

        void onSuccessSaveAssistants();

    }

    public AssistantsPresenter(AppCompatActivity appCompatActivity, AssistantsCallback assistantsCallback) {
        super(appCompatActivity);
        mAssistantsCallback = assistantsCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void checkAssistantsWorkTeam() {
        if (!mAppCompatActivity.getIntent().getBooleanExtra(Keys.ASSISTANT_IS_CHANGING, false)) {
            List<Assistant> assistants = mFFMDataManager.queryWhere(Assistant.class)
                    .filter("date", DateUtils.dateNow()).list();
            if (assistants.size() == 0) {
                QueryWorkTeamRequest queryWorkTeamRequest = new QueryWorkTeamRequest();
                queryWorkTeamRequest.operatorId = mUser.operatorId;
                queryWorkTeamRequest.queryDate = DateUtils.dateNow();
                mWSManager.requestWs(AssistantResponse.class, WSManager.WS.WORK_ASSISTANTS_TEAM, getDefinition().workAssistantTeam(queryWorkTeamRequest));
            } else {
                mAssistantsCallback.onSuccessSaveAssistants();
            }
        } else {
            loadAssistants();
        }
    }

    public void loadAssistants() {
        AssistantsRequest assistantsRequest = new AssistantsRequest();
        assistantsRequest.operatorId = mUser.operatorId;
        mWSManager.requestWs(AssistantResponse.class, WSManager.WS.ASSISTANTS, getDefinition().assistants(assistantsRequest));
    }

    private void successWorkAssistantsTeam(String requestUrl, AssistantResponse assistantResponse) {
        if (assistantResponse.result != null && (assistantResponse.result.equals("0"))) {
            RealmList<Assistant> assistants = assistantResponse.assistants;
            if (assistants.size() > 0) {
                mFFMDataManager.tx(tx -> tx.delete(Assistant.class));
                for (Assistant assistant : assistants) assistant.date = DateUtils.dateNow();
                mFFMDataManager.tx(tx -> tx.save(assistants));
                mAssistantsCallback.onSuccessSaveAssistants();
            } else {
                loadAssistants();
            }
        } else {
            onErrorLoadResponse(requestUrl, assistantResponse.resultDescription);
        }
    }

    private void successLoadAssistants(String requestUrl, AssistantResponse assistantResponse) {
        if (assistantResponse.result != null && assistantResponse.result.equals("0")) {
            RealmList<Assistant> assistants = assistantResponse.assistants;
            for (Assistant assistant : assistants) assistant.date = DateUtils.dateNow();
            Assistant withoutAssistant = assistants.get(0);
            assistants.remove(0);
            Collections.sort(assistants, (obj1, obj2) -> obj1.name.compareToIgnoreCase(obj2.name));
            assistants.add(0, withoutAssistant);
            mAssistantsCallback.onSuccessLoadAssistants(assistants);
        } else {
            onErrorLoadResponse(requestUrl, assistantResponse.resultDescription);
        }
    }

    private void successSaveAssistants(String requestUrl, BaseResponse baseResponse) {
        mAssistantsCallback.onSuccessSaveAssistants();
    }

    public void continueClick(List<Assistant> selectAssistantList) {
        if (selectAssistantList.size() == 0) {
            MessageUtils.toast(mContext, R.string.dialog_error_empty_assistants);
            return;
        }
        if (selectAssistantList.size() > 4) {
            MessageUtils.toast(mContext, R.string.dialog_error_max_assistants);
            return;
        }
        saveAssistants(selectAssistantList);
    }

    private void saveAssistants(List<Assistant> selectAssistantList) {
        for (Assistant assistant : selectAssistantList) assistant.date = DateUtils.dateNow();
        mFFMDataManager.tx(tx -> {
            tx.delete(Assistant.class);
            tx.save(selectAssistantList);
        });

        SaveWorkAssistantTeamRequest saveWorkAssistantTeamRequest = new SaveWorkAssistantTeamRequest();
        for (Assistant assistant : selectAssistantList) {
            if (saveWorkAssistantTeamRequest.assistantOne.equals("")) {
                saveWorkAssistantTeamRequest.assistantOne = assistant.auxId;
            } else if (saveWorkAssistantTeamRequest.assistantTwo.equals("")) {
                saveWorkAssistantTeamRequest.assistantTwo = assistant.auxId;
            } else if (saveWorkAssistantTeamRequest.assistantThree.equals("")) {
                saveWorkAssistantTeamRequest.assistantThree = assistant.auxId;
            } else if (saveWorkAssistantTeamRequest.assistantFour.equals("")) {
                saveWorkAssistantTeamRequest.assistantFour = assistant.auxId;
            }
        }
        saveWorkAssistantTeamRequest.operatorId = mUser.operatorId;
        mWSManager.requestWs(BaseResponse.class, WSManager.WS.SAVE_WORK_ASSISTANTS_TEAM, getDefinition().saveWorkAssistantTeam(saveWorkAssistantTeamRequest));
    }

    @Override
    public void onRequestWS(String requestUrl) {
        switch (requestUrl) {
            case WSManager.WS.ASSISTANTS:
                MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_assitants);
                break;
            case WSManager.WS.SAVE_WORK_ASSISTANTS_TEAM:
                MessageUtils.progress(mAppCompatActivity, R.string.dialog_uploading_assistants);
                break;
            case WSManager.WS.WORK_ASSISTANTS_TEAM:
                MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_assitants);
                break;
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        switch (requestUrl) {
            case WSManager.WS.WORK_ASSISTANTS_TEAM:
                successWorkAssistantsTeam(requestUrl, (AssistantResponse) baseResponse);
                break;
            case WSManager.WS.ASSISTANTS:
                successLoadAssistants(requestUrl, (AssistantResponse) baseResponse);
                break;
            case WSManager.WS.SAVE_WORK_ASSISTANTS_TEAM:
                successSaveAssistants(requestUrl, (BaseResponse) baseResponse);
                break;
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        switch (requestUrl) {
            case WSManager.WS.ASSISTANTS:
                super.onErrorLoadResponse(requestUrl, messageError);
                break;
            case WSManager.WS.WORK_ASSISTANTS_TEAM:
                loadAssistants();
                break;
            case WSManager.WS.SAVE_WORK_ASSISTANTS_TEAM:
                super.onErrorLoadResponse(requestUrl, messageError);
                break;
        }
    }

}
