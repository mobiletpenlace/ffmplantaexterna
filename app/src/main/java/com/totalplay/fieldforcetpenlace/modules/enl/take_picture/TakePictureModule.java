package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jorgehdezvilla on 18/01/18.
 * FFM Enlace
 */

public class TakePictureModule extends BaseModule<TakePictureInput, TakePictureOutput> {

    public static final String TAKE_PICTURE_OUTPUT_KEY = "takePictureOutputKey";
    private File mRequestedFile;
    private SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

    @Override
    public void startModule(AppCompatActivity appCompatActivity, TakePictureInput takePictureInput) {
        mRequestedFile = BusinessFiles.newDocument(takePictureInput.fileName + timeStampFormat.format(new Date()));
        mRequestedFile = FileUtils.validFile(mRequestedFile);
        if (mRequestedFile != null) {
            TakePictureUtils.startImportPicture(this, appCompatActivity, mRequestedFile.getAbsolutePath(), takePictureInput.requestCode);
        }
    }

    @Override
    public void finishModule(AppCompatActivity appCompatActivity, TakePictureOutput takePictureOutput) {
        Intent intent = new Intent();
        intent.putExtra(TAKE_PICTURE_OUTPUT_KEY, takePictureOutput);
        appCompatActivity.setResult(Activity.RESULT_OK, intent);
        appCompatActivity.finish();
    }
}
