package com.totalplay.fieldforcetpenlace.modules.enl.travel;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.info_map.StaticMapFragment;
import com.totalplay.fieldforcetpenlace.newpresenter.WorkOrderInfoInSitePresenter;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressWarnings("unchecked")
public class WorkOrderInfoInSiteActivity extends BaseFFMActivity implements WorkOrderInfoInSitePresenter.Callback {

    @BindView(R.id.act_installation_arrival)
    ViewGroup mArrivalInstallationViewGroup;

    @BindView(R.id.act_installation_client_name)
    TextView mOrderClientNameTextView;
    @BindView(R.id.act_installation_address)
    TextView mOrderAddress;
    @BindView(R.id.act_installation_compromise_time)
    TextView mCompromiseTimeTextView;
    @BindView(R.id.act_installation_package)
    TextView mPackageTextView;

    private StaticMapFragment.MapPoint mWorkOrderMapPoint;

    private WorkOrderInfoInSitePresenter mWorkOrderInfoInSitePresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, WorkOrderInfoInSiteActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_workorder_info_in_site);
        ButterKnife.bind(this);
        setTitle("");

        initView();

        mTrainingManager.workOrderInfoInSiteActivity();
    }

    private void initView() {
        String addressString = String.format(Locale.getDefault(), "%s %s, %s %s, %s, %s",
                mWorkOrder.street, mWorkOrder.exteriorNumber,
                mWorkOrder.town, mWorkOrder.colony,
                mWorkOrder.zipCode, mWorkOrder.city);

        mPackageTextView.setText(mWorkOrder.packageContracted);
        mOrderClientNameTextView.setText(mWorkOrder.clientName);
        mCompromiseTimeTextView.setText(mWorkOrder.compromiseTime);
        mOrderAddress.setText(addressString);

        Location location = TotalPlayLocationService.getLastLocation();
        if (location != null) {
            if (!mWorkOrder.latitude.isEmpty() && !mWorkOrder.longitude.isEmpty()) {
                mWorkOrderMapPoint = new StaticMapFragment.MapPoint(Double.valueOf(mWorkOrder.latitude), Double.valueOf(mWorkOrder.longitude));
                ArrayList<StaticMapFragment.MapPoint> points = new ArrayList<StaticMapFragment.MapPoint>() {{
                    add(new StaticMapFragment.MapPoint(location.getLatitude(), location.getLongitude()));
                    add(mWorkOrderMapPoint);
                }};
                StaticMapFragment mMapFragment = (StaticMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.act_installation_map_fragment);
                mMapFragment.setMapInitialData(new StaticMapFragment.MapInitialData(points));
                StaticMapFragment.mWorkOrders = mWorkOrder;
            }
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        return mWorkOrderInfoInSitePresenter = new WorkOrderInfoInSitePresenter(this, this);
    }

    @OnClick(R.id.act_installation_confirm_location)
    public void onConfirmArrivalClick() {
        mWorkOrderInfoInSitePresenter.confirmArrivalClick();
    }


    @Override
    public void onSuccessInSite(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
        finish();
    }
}
