package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.tasks.SerialNumberEquipmentTask;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMacResponse;
import com.totalplay.fieldforcetpenlace.library.enums.SerialNumberErrorReason;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

/**
 * Created by jorgehdezvilla on 27/09/17.
 * Enlace
 */

public class WifiPresenter extends FFMPresenter {

    private SerialNumberEquipmentTask mSerialNumberEquipmentTask;
    private WifiCallback mWifiCallback;

    public interface WifiCallback {
        void onSuccessLoadMac(GetMacResponse mac);

        void onErrorLoadMac();
    }

    public WifiPresenter(AppCompatActivity appCompatActivity, WifiCallback wifiCallback) {
        super(appCompatActivity);
        onCreate();
        mWifiCallback = wifiCallback;
    }

    public void validWifiExtenderSerialNumber(final String serialNumber) {
        boolean flag = false;
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_data);
        for (int i = 0; i < mWorkOrder.quotationInfo.servicePlans.size(); i++) {
            if (mWorkOrder.quotationInfo.servicePlans.get(i).type.equals(("Wifi Extender"))) {
                flag = true;
                break;
            }
        }
        String serialNum;
        if (flag) {
            if (serialNumber.length() >= 20) {
                serialNum = serialNumber.substring(2, serialNumber.length());
            } else if (serialNumber.length() == 19) {
                serialNum = serialNumber.substring(1, serialNumber.length());
            } else {
                serialNum = serialNumber;
            }
        } else {
            serialNum = serialNumber;
        }


        mSerialNumberEquipmentTask = new SerialNumberEquipmentTask(mContext, serialNum, new SerialNumberEquipmentTask.SerialNumberTaskListener() {
            @Override
            public void onSuccessLoadedMac(GetMacResponse mac) {
                MessageUtils.stopProgress();
                mWifiCallback.onSuccessLoadMac(mac);
            }

            @Override
            public void onErrorLoadedMac(SerialNumberErrorReason errorReason) {
                MessageUtils.stopProgress();
                if (errorReason != null) {
                    switch (errorReason) {
                        case COULD_NOT_GET_MAC:
                            MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_loaded_mac);
                            break;
                        case COULD_NOT_VALID_SERIAL_NUMBER:
                            MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_invalid_serial_number);
                            break;
                        case SERIAL_NUMBER_ALREDY_EXISTS:
                            MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_already_exist_serial_number);
                            break;
                        default:
                            MessageUtils.toast(mAppCompatActivity, R.string.dialog_error);
                            break;
                    }
                    mWifiCallback.onErrorLoadMac();
                } else {
                    MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_connection);
                }
            }
        });
        mSerialNumberEquipmentTask.execute();
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSerialNumberEquipmentTask != null) mSerialNumberEquipmentTask.cancel(true);
    }
}
