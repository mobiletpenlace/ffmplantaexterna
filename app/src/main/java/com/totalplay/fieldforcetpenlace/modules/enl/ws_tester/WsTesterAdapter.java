package com.totalplay.fieldforcetpenlace.modules.enl.ws_tester;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleAdapter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by charlssalazar on 30/06/17.
 * FFM
 */

public class WsTesterAdapter extends BaseSimpleAdapter<WSTesterActivity.TesterWS, WsTesterAdapter.AdapterViewHolder> {

    private Context mContext;

    public WsTesterAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getItemLayout() {
        return R.layout.enl_item_ws_tester;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new AdapterViewHolder(view);
    }

    class AdapterViewHolder extends BaseViewHolder<WSTesterActivity.TesterWS> {

        @BindView(R.id.item_ws_tester_name)
        public TextView mNameTextView;
        @BindView(R.id.item_ws_tester_status)
        public ViewGroup mStatusView;
        @BindView(R.id.item_ws_tester_progress)
        ProgressBar mProgressBar;

        public AdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            GradientDrawable gradientDrawable = (GradientDrawable) mStatusView.getBackground();
            gradientDrawable.setColor(ContextCompat
                    .getColor(mContext, R.color.gray));
        }

        @Override
        public void populate(BaseViewHolder holder, int position, WSTesterActivity.TesterWS testerWS) {
            mNameTextView.setText(testerWS.wsName);
            GradientDrawable gradientDrawable = (GradientDrawable) mStatusView.getBackground();
            if (testerWS.status == WSTesterActivity.TestWsStatus.OK) {
                gradientDrawable.setColor(ContextCompat
                        .getColor(mContext, R.color.LightGreen));
            } else if (testerWS.status == WSTesterActivity.TestWsStatus.ERROR) {
                gradientDrawable.setColor(ContextCompat
                        .getColor(mContext, R.color.red_splitter));
            } else {
                gradientDrawable.setColor(ContextCompat
                        .getColor(mContext, R.color.gray_splitter));
            }
            mProgressBar.setVisibility(testerWS.inProgress ? View.VISIBLE : View.GONE);
        }
    }

}
