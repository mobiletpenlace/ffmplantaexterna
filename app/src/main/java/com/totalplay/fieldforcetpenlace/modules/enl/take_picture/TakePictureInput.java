package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

/**
 * Created by jorgehdezvilla on 18/01/18.
 * FFN ENlace
 */

public class TakePictureInput implements ModuleParameter {

    public String fileName;
    public int requestCode = 0x100;
    public boolean withDate = false;

    public TakePictureInput(String fileName, int requestCode) {
        this.fileName = fileName;
        this.requestCode = requestCode;
    }

    public TakePictureInput(String fileName, boolean withDate) {
        this.fileName = fileName;
        this.withDate = withDate;
    }

    public TakePictureInput(String fileName, int requestCode, boolean withDate) {
        this.fileName = fileName;
        this.requestCode = requestCode;
        this.withDate = withDate;
    }
}
