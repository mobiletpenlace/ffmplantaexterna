package com.totalplay.fieldforcetpenlace.modules.enl.info_map;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.library.utils.MapAnimator;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.mvp.BaseFragment;
import com.totalplay.mvp.BasePresenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by totalplay on 11/18/16.
 * :v
 */

@SuppressWarnings("unused")
public class StaticMapFragment extends BaseFragment {

    MapView mapView;
    private MapInitialData mMapInitialData;
    private GoogleMap mGoogleMap;
    private List<LatLng> bangaloreRoute = new ArrayList<>();
    public static WorkOrder mWorkOrders;

    public static StaticMapFragment newInstance() {
        return new StaticMapFragment();
    }

    @Override
    public BasePresenter getPresenter() {
        return null;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.enl_fragment_map, container, false);
        ImageView mReportImageView = view.findViewById(R.id.toolbar_report);
        /*if(mWorkOrders.type.equals(WorkOrderType.EXTERNAL)){
            mReportImageView.setVisibility(View.VISIBLE);
        }else{
            mReportImageView.setVisibility(View.INVISIBLE);
        }*/
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mapView = getActivity().findViewById(R.id.fr_map_view);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(googleMap -> {
            if (googleMap != null) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                googleMap.setMyLocationEnabled(false);
                setUpMap(googleMap);
            }
        });
    }

    public void setMapInitialData(MapInitialData mapInitialData) {
        this.mMapInitialData = mapInitialData;
    }

    public void disableGestureMap() {
        mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
    }

    public void removeAllMarkers() {
        mGoogleMap.clear();
    }

    public void addMarkers(ArrayList<MapPoint> points) {
        this.mMapInitialData.points = points;
        paintMarkers();
    }


    private void setUpMap(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.clear();
        final float scale = getResources().getDisplayMetrics().density;
        int dpBottomMargin = (int) (200 * scale);
        int dpTopMargin = (int) (100 * scale);
        googleMap.setPadding(0, dpTopMargin, 0, dpBottomMargin);
        googleMap.setBuildingsEnabled(false);
        googleMap.setIndoorEnabled(false);
        googleMap.setTrafficEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);

        googleMap.getUiSettings().setIndoorLevelPickerEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        MapsInitializer.initialize(this.getActivity());

        try {
            boolean success = mGoogleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.style_json));
            if (!success) {
                Log.e(Keys.DEBUG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(Keys.DEBUG, "Can't find style. Error: ", e);
        }

        mGoogleMap.setOnMapLoadedCallback(() -> {
            if (isAdded()) {
                Location mLocation = TotalPlayLocationService.getLastLocation();
                if (mLocation != null) {
                    if (mMapInitialData != null) {
                        if (mMapInitialData.points.size() > 1) {
                            paintMarkers();
                        } else if (mMapInitialData.points.size() > 0) {
                            MapPoint mapPoint = mMapInitialData.points.get(0);
                            LatLng latLng = new LatLng(mapPoint.latitude, mapPoint.longitude);
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16.0f);
                            mGoogleMap.moveCamera(cameraUpdate);
                        }
                    }
                }
            }
        });
    }

    private void paintMarkers() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (MapPoint mapPoint : mMapInitialData.points) {
            bangaloreRoute.add(new LatLng(mapPoint.latitude, mapPoint.longitude));
            LatLng latLng = new LatLng(mapPoint.latitude, mapPoint.longitude);
            if (mapPoint == mMapInitialData.points.get(0)) {
                mGoogleMap.addMarker(new MarkerOptions().position(latLng).draggable(false)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin)));
            } else {
                mGoogleMap.addMarker(new MarkerOptions().position(latLng).draggable(false)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin_blue)));
            }
            builder.include(latLng);
        }
        if (isAdded()) {
            int width = getResources().getDisplayMetrics().widthPixels;
            int padding = (int) (width * 0.15);
            LatLngBounds bounds = builder.build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mGoogleMap.moveCamera(cameraUpdate);
            startAnim();
        }
    }

    private void startAnim() {
        if (mGoogleMap != null) {
            MapAnimator.getInstance().animateRoute(mGoogleMap, bangaloreRoute, true);
        } else {
            Toast.makeText(getContext(), "Map not ready", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public static class MapPoint implements Serializable {
        public double latitude;
        public double longitude;

        public MapPoint(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    public static class MapInitialData implements Serializable {
        ArrayList<MapPoint> points;

        public MapInitialData(ArrayList<MapPoint> points) {
            this.points = points;
        }
    }
}
