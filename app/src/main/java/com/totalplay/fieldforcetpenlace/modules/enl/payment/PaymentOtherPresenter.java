package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.CashPaymentEngineRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.CashPaymentEngineResponse;
import com.totalplay.fieldforcetpenlace.library.enums.CashOrigin;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.BusinessFiles;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.FileUtils;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WrapperPhotosEvidence;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorgehdezvilla on 28/09/17.
 * Enlace
 */

public class PaymentOtherPresenter extends FFMPresenter implements WSCallback {

    private static final int REQUEST_CODE_PICTURE_CHECK = 0x0019;
    private static final int REQUEST_CODE_PICTURE_TRANSFER = 0x0019;

    private List<Integer> mPhotosValidator = new ArrayList<>();
    private File mRequestedFile;
    private final String serverFolder = ServerFolders.EVIDENCES;
    private WrapperPhotosEvidence mFileWrapper;
    private Callback mCallback;

    private String mOriginCash;

    public interface Callback {
        void onSuccessLoadImage(int requestCode, File mRequestedFile);

        void onSuccessPayment(WorkOrder workOrder);
    }

    public PaymentOtherPresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mWorkOrder != null) {
            mFileWrapper = mFFMDataManager.queryWhere(WrapperPhotosEvidence.class)
                    .filter("workOrder", mWorkOrder.id)
                    .filter("serverFolder", serverFolder)
                    .findFirst();
            mFileWrapper = new WrapperPhotosEvidence(mWorkOrder.id, serverFolder);
        } else {
            mFileWrapper = new WrapperPhotosEvidence("default", serverFolder);
        }
    }

    public void loadArguments(Fragment fragment) {
        mOriginCash = fragment.getArguments().getString(Keys.CASH_ORIGIN);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void payment(String amount) {
        if (mOriginCash.equals(CashOrigin.CHECK)) {
            if (mPhotosValidator.size() < 2) {
                MessageUtils.toast(mContext, "Es necesario tomar las 2 fotografias");
                return;
            }
        }
        CashPaymentEngineRequest cashPaymentEngineRequest = new CashPaymentEngineRequest(
                mWorkOrder.id, amount, mOriginCash, mUser.operatorId);
        mWSManager.requestWs(CashPaymentEngineResponse.class, WSManager.WS.CASH_PAYMENT, getDefinition().cashPayment(cashPaymentEngineRequest));
    }

    public void withoutPayment() {
        mFFMDataManager.tx(tx -> {
            mWorkOrder.isPaid = true;
            tx.save(mWorkOrder);
        });
        mCallback.onSuccessPayment(mWorkOrder);
    }

    private void successCashPayment(String requestUrl, CashPaymentEngineResponse cashPaymentEngineResponse) {
        if (cashPaymentEngineResponse.result.equals("0")) {
            MessageUtils.toast(mContext, R.string.dialog_successful_cash_payment);
            mFFMDataManager.tx(tx -> {
                mWorkOrder.isPaid = true;
                tx.save(mWorkOrder);
            });
            mCallback.onSuccessPayment(mWorkOrder);
        } else {
            onErrorLoadResponse(requestUrl, mContext.getString(R.string.dialog_error_payment));
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_do_payment);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.CASH_PAYMENT)) {
            successCashPayment(requestUrl, (CashPaymentEngineResponse) baseResponse);
        }
    }

    public void imageViewClick(int imageViewId) {
        String photoName = "";
        if (!mPhotosValidator.contains(imageViewId)) mPhotosValidator.add(imageViewId);
        switch (imageViewId) {
            case R.id.fr_payment_other_photo_one:
                photoName = "Evidence One";
                break;
            case R.id.fr_payment_other_photo_two:
                photoName = "Evidence Two";
                break;
        }
        mRequestedFile = BusinessFiles.newDocument(photoName);
        mRequestedFile = FileUtils.validFile(mRequestedFile);
        if (mRequestedFile != null) {
            if (mOriginCash.equals(CashOrigin.CHECK)) {
//                TakePictureUtils.startImportPicture(mAppCompatActivity, mRequestedFile.getAbsolutePath(), REQUEST_CODE_PICTURE_CHECK);
            } else if (mOriginCash.equals(CashOrigin.TRANSFER)) {
//                TakePictureUtils.startImportPicture(mAppCompatActivity, mRequestedFile.getAbsolutePath(), REQUEST_CODE_PICTURE_TRANSFER);
            }
        }
    }

    public void saveImageFile(String name, String path) {
        mFFMDataManager.tx(tx -> {
            PhotoEvidence photo = new PhotoEvidence();
            photo.name = name;
            photo.path = path;

            photo.workOrder = mWorkOrder.id;
            photo.serverFolder = serverFolder;
            photo.baseUrl = mWorkOrder.baseUrl;
            photo.interventionType = mWorkOrder.interventionType;

            if (mOriginCash.equals(CashOrigin.CHECK)) {
                photo.type = ImageTypeEnum.CHECK_PAYMENT;
            } else if (mOriginCash.equals(CashOrigin.TRANSFER)) {
                photo.type = ImageTypeEnum.TRANSFER_PAYMENT;
            }

            photo = tx.save(photo);
            mFileWrapper.listPhotos.add(photo);
            tx.save(mFileWrapper);
        });
    }

    public void onActivityResult(int requestCode, int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            if (mOriginCash.equals(CashOrigin.CHECK)) {
                if (requestCode == REQUEST_CODE_PICTURE_CHECK) {
                    if (mCallback != null)
                        mCallback.onSuccessLoadImage(requestCode, mRequestedFile);
                }
            } else if (mOriginCash.equals(CashOrigin.TRANSFER)) {
                if (requestCode == REQUEST_CODE_PICTURE_TRANSFER) {
                    if (mCallback != null)
                        mCallback.onSuccessLoadImage(requestCode, mRequestedFile);
                }
            }

        }
    }

}
