package com.totalplay.fieldforcetpenlace.modules.enl.arrive_early;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by armando on 21/06/17.
 * FFM
 */

@SuppressWarnings("unchecked")
public class ArrivalTimeActivity extends BaseFFMActivity {

    @BindView(R.id.act_arrival_time_image_date)
    ImageView mLateImageView;
    @BindView(R.id.act_arrival_time_image_congratulation)
    ImageView mImageView;
    @BindView(R.id.act_arrival_time_congratulations)
    TextView mTextView;

    ArrivalTimeActivityPresenter mArrivalTimeActivityPresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isRegisterTimeArrival) {
            Intent intent = new Intent(appCompatActivity, ArrivalTimeActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_arrival_time);
        ButterKnife.bind(this);
        setTitle("");

        String arrivalStatus = "-1";
        if (mWorkOrder.compromiseTime != null) {
            String compromiseTime = mWorkOrder.compromiseTime;
            DateTime now = DateTime.now();
            DateTime compromise = DateTime.now().withTime(
                    Integer.parseInt(compromiseTime.split(":")[0]),
                    Integer.parseInt(compromiseTime.split(":")[1]), 0, 0);
            if (compromise.isAfterNow() || compromise.plusMinutes(15).isAfterNow()) {
                mLateImageView.setVisibility(View.GONE);
                mImageView.setVisibility(View.VISIBLE);
                arrivalStatus = "0";
            } else {
                mLateImageView.setVisibility(View.VISIBLE);
                mImageView.setVisibility(View.GONE);
                Duration duration = new Duration(compromise, now);
                long exceed = duration.getStandardMinutes();
                mTextView.setText(getString(R.string.arrival_late, String.valueOf(exceed)));
                arrivalStatus = "1";
            }
        } else {
            mLateImageView.setVisibility(View.GONE);
            mImageView.setVisibility(View.VISIBLE);
            arrivalStatus = "0";
        }
        mArrivalTimeActivityPresenter.registerArrivalTime(arrivalStatus);

        mTrainingManager.arrivalTimeActivity();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mArrivalTimeActivityPresenter = new ArrivalTimeActivityPresenter(this);
    }


    @OnClick(R.id.act_good_arrival_next)
    void onArrivalNextClick() {
        finish();
        baseModule.finishModule(this, mWorkOrder);
    }

}
