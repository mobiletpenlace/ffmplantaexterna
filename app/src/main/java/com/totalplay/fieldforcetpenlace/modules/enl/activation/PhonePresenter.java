package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetDNRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GenerateDNResponse;
import com.totalplay.fieldforcetpenlace.library.enums.ServicePlanType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.DN;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhoneModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorgehdezvilla on 26/09/17.
 * Enlace
 */

public class PhonePresenter extends FFMPresenter implements WSCallback {

    private PhoneCallback mPhoneCallback;
    private List<DN> mDNList = new ArrayList<>();
    private ServicePlan mServicePlanMain;
    private List<ServicePlan> mPhoneServicesPlans = new ArrayList<>();

    public interface PhoneCallback {
        void onSuccessLoadPhonePlans(List<ServicePlan> mPhoneServicesPlans);

        void onNotFindPhonePlanEvent(boolean phoneFound);

        void onSuccessConfiguredDevicesDnEvent();
    }

    public PhonePresenter(AppCompatActivity appCompatActivity, PhoneCallback phoneCallback) {
        super(appCompatActivity);
        mPhoneCallback = phoneCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void loadPhonePlans() {
        if (mWorkOrder.quotationInfo != null && mWorkOrder.quotationInfo.servicePlans != null) {
            boolean phoneFound = false;
            ArrayList<ServicePlan> servicePlans = new ArrayList<>();
            for (ServicePlan servicePlan : mWorkOrder.quotationInfo.servicePlans) {
                if (servicePlan.servicePlanNameService != null && servicePlan.type.equals(ServicePlanType.PHONE)) {
                    phoneFound = true;
                    if (servicePlan.wasAdditional.equals("false")) {
                        mServicePlanMain = servicePlan;
                    } else {
                        servicePlans.add(servicePlan);
                    }
                }
            }
            if (mServicePlanMain != null) {
                mPhoneServicesPlans.add(mServicePlanMain);
            }
            mPhoneServicesPlans.addAll(servicePlans);

            int dNAux = 0;
            for (ServicePlan servicePlan : mPhoneServicesPlans) {
                int totalDn = Integer.parseInt(servicePlan.dnAmount);
                for (int i = 0; i < totalDn; i++) {
                    servicePlan.dns.add(mDNList.get(dNAux));
                    dNAux++;
                }
            }

            mPhoneCallback.onNotFindPhonePlanEvent(phoneFound);
            mPhoneCallback.onSuccessLoadPhonePlans(mPhoneServicesPlans);
        }
        if (mWorkOrder.configuredDevices && mWorkOrder.configuredDN) {
            mPhoneCallback.onSuccessConfiguredDevicesDnEvent();
        }
    }

    public void obtainDN() {
        int quantity = (int) Float.parseFloat(mWorkOrder.quotationInfo.totalDNs);
        if (mWorkOrder.zipCode.trim().isEmpty() || mWorkOrder.zipCode.trim().length() != 5) {
            MessageUtils.toast(mContext, R.string.dialog_error_obtain_dns);
            return;
        }
        if (mWorkOrder.configWorkOrder != null && mWorkOrder.configWorkOrder.dnPhones != null)
            mDNList = mWorkOrder.configWorkOrder.dnPhones.dns;
        if (mDNList.isEmpty()) {
            if (quantity > 0) {
                GetDNRequest generateDNRequest = new GetDNRequest(mWorkOrder.quotationInfo.totalDNs, mWorkOrder.zipCode);
                mWSManager.requestWs(GenerateDNResponse.class, WSManager.WS.GENERATE_DNS, getDefinition().generateDN(generateDNRequest));
            }
        } else {
            loadPhonePlans();
        }
    }

    private void onSuccessLoadDNS(String requestUrl, GenerateDNResponse generateDNResponse) {
        if (generateDNResponse.result.equals("0")) {
            mFFMDataManager.tx(tx -> {
                mFFMDataManager.queryWhere(DN.class).remove();
                mDNList = generateDNResponse.arrayDn;
                for (DN dn : generateDNResponse.arrayDn) {
                    tx.save(dn);
                    mWorkOrder.configWorkOrder.dnPhones.dns.add(dn);
                }
                tx.save(mWorkOrder.configWorkOrder);
                tx.save(mWorkOrder.configWorkOrder.dnPhones);
                tx.save(mWorkOrder);
            });
            loadPhonePlans();
        } else {
            onErrorLoadResponse(requestUrl, generateDNResponse.resultDescription);
        }
    }

    public List<PhoneModel> getPhoneModels() {
        List<PhoneModel> phoneModels = new ArrayList<>();
        String mainDn = mServicePlanMain != null && mServicePlanMain.dns.size() > 0 ? mServicePlanMain.dns.get(0).dn : "";
        for (ServicePlan servicePlan : mPhoneServicesPlans) {
            phoneModels.add(new PhoneModel(servicePlan.servicePlanId, servicePlan.dns, mainDn));
        }
        return phoneModels;
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_obtaining_dn);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.GENERATE_DNS)) {
            onSuccessLoadDNS(requestUrl, (GenerateDNResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        mAppCompatActivity.finish();
    }
}
