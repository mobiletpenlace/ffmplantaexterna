package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.mvp.BaseActivity;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by mahegots on 28/07/17.
 */

@SuppressWarnings("unchecked")
public class ImportGalleryActivity extends BaseActivity {

    private final int PICTURE_TAKEN_FROM_GALLERY = 2;
    ImageView imageView;
    private String mImagePath;
    private Bitmap backgroundImage;

    public static void launch(BaseModule baseModule, AppCompatActivity activity, String path, int requestCode) {
        Intent intent = new Intent(activity, ImportGalleryActivity.class);
        intent.putExtra(Keys.EXTRA_IMAGE_PATH, path);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.enl_activity_resume);
        mImagePath = getIntent().getStringExtra(Keys.EXTRA_IMAGE_PATH);

        imageView = findViewById(R.id.imgView);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Seleccionar"), PICTURE_TAKEN_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICTURE_TAKEN_FROM_GALLERY) {
            if (data != null) {
                Uri image = data.getData();
                Glide.with(this).load(image).asBitmap()
                        .into(new SimpleTarget<Bitmap>(720, 1080) {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                                setBackgroundImage(resource);
                            }
                        });
            } else {
                finish();
            }
        } else {
            finish();
        }
    }

    public void copy(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }


    public void setBackgroundImage(Bitmap bitmap) {
        try {
            File file = new File(mImagePath);
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 65, os);
            os.close();
            ((BaseModule) getIntent().getExtras().get(Keys.EXTRA_BASE_MODULE)).finishModule(this, new TakePictureOutput(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
