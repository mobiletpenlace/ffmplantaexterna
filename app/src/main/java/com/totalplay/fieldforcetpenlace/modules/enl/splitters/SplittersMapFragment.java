package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.library.utils.BitmapUtils;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.adapters.SplitterWindowAdapter;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;
import com.totalplay.utils.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by NS-TOTALPLAY on 21/04/2017.
 * :v
 */

public class SplittersMapFragment extends BaseFFMFragment implements OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener, SplittersMapPresenter.SplittersMapCallback {

    public WorkOrder mWorkOrder;
    public Map<Marker, SplitterWindowAdapter.InfoWindowSplitterContent> markersContent = new HashMap<>();
    public GoogleMap mGoogleMap;
    private MarkerOptions mUserMarker;

    private BitmapDescriptor markerIconGreen;
    private BitmapDescriptor markerIconYellow;
    private BitmapDescriptor markerIconRed;
    private BitmapDescriptor markerIconGray;

    private Boolean mWasFirstMovedCamera = false;
    private SplittersMapPresenter mSplittersMapPresenter;
    private ArrayList<Splitter> mSplitters = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.enl_activity_map_splitters, container, false);

        SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.act_splitters_map));
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createResources();
    }

    @Override
    public BasePresenter getPresenter() {
        return mSplittersMapPresenter = new SplittersMapPresenter((AppCompatActivity) getActivity(), this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        try {
            boolean success = mGoogleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.style_json));
            if (!success) {
                Log.e(Keys.DEBUG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(Keys.DEBUG, "Can't find style. Error: ", e);
        }

        mGoogleMap.setOnMarkerClickListener(marker -> {
            marker.showInfoWindow();
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), mGoogleMap.getCameraPosition().zoom));
            return true;
        });
        mGoogleMap.setOnInfoWindowClickListener(this);
        mGoogleMap.setInfoWindowAdapter(new SplitterWindowAdapter(getContext(), markersContent));
        mSplittersMapPresenter.loadSplitters();
    }


    public Marker createMarker(Splitter splitter) {
        Marker marker = null;
        if (splitter.latitude != null && !TextUtils.isEmpty(splitter.latitude)
                && splitter.longitude != null && !TextUtils.isEmpty(splitter.longitude)) {
            LatLng latLng = new LatLng(Double.valueOf(splitter.latitude), Double.valueOf(splitter.longitude));
            if (!splitter.totalPorts.equals("") && !splitter.occupedPorts.equals("") && !splitter.totalPorts.equals("null") && !splitter.occupedPorts.equals("null") && (splitter.totalPorts != null) && (splitter.occupedPorts != null)) {
                int portsAvaliables = Integer.parseInt(splitter.totalPorts) - Integer.parseInt(splitter.occupedPorts);
                if (portsAvaliables >= 3) {
                    splitter.colorSplitter = ContextCompat.getColor(getContext(), R.color.green_splitter);
                    marker = mGoogleMap.addMarker(new MarkerOptions().title(splitter.name).position(latLng).icon(markerIconGreen));
                } else if (portsAvaliables < 3 && portsAvaliables > 0) {
                    splitter.colorSplitter = ContextCompat.getColor(getContext(), R.color.yellow_splitter);
                    marker = mGoogleMap.addMarker(new MarkerOptions().title(splitter.name).position(latLng).icon(markerIconYellow));
                } else if (portsAvaliables <= 0) {
                    splitter.colorSplitter = ContextCompat.getColor(getContext(), R.color.red_splitter);
                    marker = mGoogleMap.addMarker(new MarkerOptions().title(splitter.name).position(latLng).icon(markerIconRed));
                }
            } else {
                if (getContext() != null) {
                    splitter.colorSplitter = ContextCompat.getColor(getContext(), R.color.gray_splitter);
                    marker = mGoogleMap.addMarker(new MarkerOptions().title(splitter.name).position(latLng).icon(markerIconGray));
                }
            }
        }
        return marker;
    }

    public void createInfoMarker(Splitter splitter, Marker marker) {
        SplitterWindowAdapter.InfoWindowSplitterContent markerContent = new SplitterWindowAdapter.InfoWindowSplitterContent();
        markerContent.splitter = splitter;
        markerContent.label = splitter.name;
        markerContent.number = splitter.id;
        markerContent.color = splitter.colorSplitter;
        markersContent.put(marker, markerContent);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        SplitterWindowAdapter.InfoWindowSplitterContent infoWindowSplitterContent = markersContent.get(marker);
        if (infoWindowSplitterContent != null && infoWindowSplitterContent.splitter != null) {
            if (Prefs.instance().bool(Keys.EXTRA_SPLITTERS_CAN_ADD)) {
                UpdateSplitterActivity.launch(((BaseFFMActivity) getActivity()).baseModule, (AppCompatActivity) getActivity(), infoWindowSplitterContent.splitter);
            }
        }
    }

    private void createResources() {
        markerIconGreen = BitmapDescriptorFactory.fromBitmap(BitmapUtils.GetBitmapFromVectorDrawable(getContext(), R.drawable.ic_splitter_green));
        markerIconYellow = BitmapDescriptorFactory.fromBitmap(BitmapUtils.GetBitmapFromVectorDrawable(getContext(), R.drawable.ic_splitter_yellow));
        markerIconRed = BitmapDescriptorFactory.fromBitmap(BitmapUtils.GetBitmapFromVectorDrawable(getContext(), R.drawable.ic_splitter_red));
        markerIconGray = BitmapDescriptorFactory.fromBitmap(BitmapUtils.GetBitmapFromVectorDrawable(getContext(), R.drawable.ic_splitter_gray));

        Location location = TotalPlayLocationService.getLastLocation();
        if (location != null) {
            LatLng positionActually = new LatLng(location.getLatitude(), location.getLongitude());
            mUserMarker = new MarkerOptions().position(positionActually).draggable(false)
                    .icon(BitmapDescriptorFactory.fromBitmap(BitmapUtils.GetBitmapFromVectorDrawable(getContext(), R.drawable.ic_cvv)));
        }
    }

    @Override
    public void onSuccessLoadSplitters(ArrayList<Splitter> splitters) {
        mGoogleMap.clear();
        mSplitters = splitters;
        for (Splitter splitter : splitters) {
            Marker marker = createMarker(splitter);
            createInfoMarker(splitter, marker);
        }
        mGoogleMap.addMarker(mUserMarker);
    }

    @Override
    public void onSuccessUpdateLocation(Location location) {
        updatePosition(location);
    }

    private void updatePosition(Location location) {
        if (getContext() != null) {
            if (location != null) {
                LatLng positionActually = new LatLng(location.getLatitude(), location.getLongitude());
                mUserMarker.position(positionActually);
                onSuccessLoadSplitters(mSplitters);
                if (!mWasFirstMovedCamera) {
                    mWasFirstMovedCamera = true;
                    mGoogleMap.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()),
                                    18));
                }
            } else {
                MessageUtils.toast(getContext(), "No se pudo obtener su ubicación");
            }

        }
    }

    public void refreshSplitters() {
        mSplittersMapPresenter.mWasFirstLoadSplitters = false;
        mSplittersMapPresenter.loadSplitters();
    }

}