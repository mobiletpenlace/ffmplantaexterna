package com.totalplay.fieldforcetpenlace.modules.enl.finish_installation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by TotalPlay on 27/06/17.
 * Enlace
 */

@SuppressWarnings("unchecked")
public class FinishInstallationActivity extends BaseFFMActivity {

    @BindView(R.id.act_finish_instalation_button)
    Button mFinishButton;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, FinishInstallationActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.enl_activity_finish_instalation);
        ButterKnife.bind(this);

        mTrainingManager.finishInstallationActivity();
    }

    @OnClick({R.id.act_finish_instalation_button})
    public void onImageClick(View view) {
        baseModule.finishModule(this, mWorkOrder);
    }

}

