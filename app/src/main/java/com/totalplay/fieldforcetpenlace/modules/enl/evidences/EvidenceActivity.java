package com.totalplay.fieldforcetpenlace.modules.enl.evidences;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.definition.ModuleKey;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureInput;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureModule;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureOutput;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by total on 26/06/17.
 * Enlace
 */

@SuppressWarnings("unchecked")
public class EvidenceActivity extends BaseFFMActivity implements EvidencePresenter.EvidenceCallback {

    @BindView(R.id.act_evidence_selfie)
    ImageView mSelfieImageView;
    @BindView(R.id.act_evidence_photo_2)
    ImageView mPhoto2ImageView;
    @BindView(R.id.act_evidence_photo_3)
    ImageView mPhoto3ImageView;
    @BindView(R.id.act_evidence_photo_4)
    ImageView mPhoto4ImageView;
    @BindView(R.id.act_evidence_photo_5)
    ImageView mPhoto5ImageView;
    @BindView(R.id.act_evidence_photo_6)
    ImageView mPhoto6ImageView;

    private EvidencePresenter mEvidencePresenter;
    private ImageView mSelectedImageView;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isEvidencesCaptured) {
            Intent intent = new Intent(appCompatActivity, EvidenceActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.enl_activity_evidence);
        ButterKnife.bind(this);

        mEvidencePresenter.checkEvidences();

        mTrainingManager.evidenceActivity();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mEvidencePresenter = new EvidencePresenter(this, this);
    }

    @OnClick({R.id.act_evidence_selfie, R.id.act_evidence_photo_2, R.id.act_evidence_photo_3, R.id.act_evidence_photo_4,
            R.id.act_evidence_photo_5, R.id.act_evidence_photo_6})
    public void onImageClick(View view) {
        mSelectedImageView = (ImageView) view;
        switch (view.getId()) {
            case R.id.act_evidence_selfie:
                baseModule.openModule(this, ModuleKey.TAKE_PICTURE, new TakePictureInput("Profile1", true));
                break;
            case R.id.act_evidence_photo_2:
                baseModule.openModule(this, ModuleKey.TAKE_PICTURE, new TakePictureInput("Profile2", true));
                break;
            case R.id.act_evidence_photo_3:
                baseModule.openModule(this, ModuleKey.TAKE_PICTURE, new TakePictureInput("Profile3", true));
                break;
            case R.id.act_evidence_photo_4:
                baseModule.openModule(this, ModuleKey.TAKE_PICTURE, new TakePictureInput("Profile4", true));
                break;
            case R.id.act_evidence_photo_5:
                baseModule.openModule(this, ModuleKey.TAKE_PICTURE, new TakePictureInput("Profile5", true));
                break;
            case R.id.act_evidence_photo_6:
                baseModule.openModule(this, ModuleKey.TAKE_PICTURE, new TakePictureInput("Profile6", true));
                break;
        }
    }

    @OnClick(R.id.continueButton)
    public void onNextClick() {
        mEvidencePresenter.onContinueClick();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mEvidencePresenter.mWasTakeEvidence = true;
        mSelectedImageView.setPadding(0, 0, 0, 0);
        TakePictureOutput takePictureOutput = (TakePictureOutput) data.getSerializableExtra(TakePictureModule.TAKE_PICTURE_OUTPUT_KEY);
        Glide.with(this)
                .load(takePictureOutput.file)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(mSelectedImageView);
        mEvidencePresenter.saveImageFile(takePictureOutput.file.getName(), takePictureOutput.file.getPath());
    }

    @Override
    public void onSuccessLoadEvidences(List<PhotoEvidence> photoEvidences) {
        if (photoEvidences.size() > 0) {
            HashMap<String, ImageView> imageViewHashMap = new HashMap<String, ImageView>() {{
                put("Profile1.jpg", mSelfieImageView);
                put("Profile2.jpg", mPhoto2ImageView);
                put("Profile3.jpg", mPhoto3ImageView);
                put("Profile4.jpg", mPhoto4ImageView);
                put("Profile5.jpg", mPhoto5ImageView);
                put("Profile6.jpg", mPhoto6ImageView);
            }};
            for (PhotoEvidence photoEvidence : photoEvidences) {
                if (photoEvidence.name == null) continue;
                if (imageViewHashMap.containsKey(photoEvidence.name)) {
                    Glide.with(this)
                            .load(photoEvidence.path)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(imageViewHashMap.get(photoEvidence.name));
                }
            }
        }
    }

    @Override
    public void onSuccessContinue(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
        finish();
    }

}
