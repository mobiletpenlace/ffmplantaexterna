package com.totalplay.fieldforcetpenlace.modules.enl.work_order_complete;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

/**
 * Created by jorgehdezvilla on 16/01/18.
 * FFM Enlace
 */

public class WorkOrderCompletedModule extends BaseModule<WorkOrder, ModuleParameter> {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        WorkOrderCompletedActivity.launch(this, appCompatActivity);
    }
}
