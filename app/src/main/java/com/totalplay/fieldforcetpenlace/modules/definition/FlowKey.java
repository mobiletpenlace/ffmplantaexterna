package com.totalplay.fieldforcetpenlace.modules.definition;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public class FlowKey {

    public static final String MAIN_FLOW = "mainFlow";
    public static final String INTERNAL_PLANT_FLOW = "internalPlantFlow";
    public static final String INTEGRATOR_FLOW = "integratorFlow";

}
