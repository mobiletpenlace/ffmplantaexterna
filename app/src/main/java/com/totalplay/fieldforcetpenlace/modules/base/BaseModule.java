package com.totalplay.fieldforcetpenlace.modules.base;

import android.support.v7.app.AppCompatActivity;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public abstract class BaseModule<Input extends ModuleParameter, Output extends ModuleParameter> implements Serializable {

    private String moduleName;
    private HashMap<String, Object> extras = new HashMap<>();

    public BaseModule() {
    }

    public abstract void startModule(AppCompatActivity appCompatActivity, Input request);

    public void finishModule(AppCompatActivity appCompatActivity, Output response) {
        ModuleManager.getInstance().onNextModule(appCompatActivity, moduleName, response);
    }

    public void openModule(AppCompatActivity appCompatActivity, String moduleName, ModuleParameter moduleParameter) {
        ModuleManager.getInstance().openModule(appCompatActivity, moduleName, moduleParameter);
    }

    public void startFlow(AppCompatActivity appCompatActivity, String flowName, ModuleParameter moduleParameter) {
        ModuleManager.getInstance().startFlow(appCompatActivity, flowName, moduleParameter);
    }

    public void startFlow(AppCompatActivity appCompatActivity, String flowName, String moduleName, ModuleParameter moduleParameter) {
        ModuleManager.getInstance().startFlow(appCompatActivity, flowName, moduleName, moduleParameter);
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public HashMap<String, Object> getExtras() {
        return extras;
    }

    public void putExtra(String key, Object object) {
        extras.put(key, object);
    }
}
