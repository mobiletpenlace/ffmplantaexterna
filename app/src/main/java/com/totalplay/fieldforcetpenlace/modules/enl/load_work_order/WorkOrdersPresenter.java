package com.totalplay.fieldforcetpenlace.modules.enl.load_work_order;

import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.BuildConfig;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.bus.events.LocationUpdatedEvent;
import com.totalplay.fieldforcetpenlace.background.bus.events.NotificationArrivedEvent;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SearchWorkOrdersRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.SearchWorkOrdersResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ConfigWorkOrder;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.DNPhones;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorgehdezvilla on 06/09/17.
 * FFM
 */

public class WorkOrdersPresenter extends FFMPresenter implements WSCallback {

    private WorkOrdersCallback mWorkOrdersCallback;
    private Runnable mRunnable;
    private Handler mainHandler = new Handler(Looper.getMainLooper());
    private boolean loadWorkOrders = false;
    private boolean onPause = false;

    public interface WorkOrdersCallback {

        void onSuccessUpdateLocation(Location location);

        void onSuccessLoadWorkOrderList(List<WorkOrder> workOrderList);

        void onSuccessSaveWorkOrder(WorkOrder workOrder);

    }

    public WorkOrdersPresenter(AppCompatActivity appCompatActivity, WorkOrdersCallback workOrdersCallback) {
        super(appCompatActivity);
        this.mWorkOrdersCallback = workOrdersCallback;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onPause) {
            onPause = false;
            loadWorkOrders();
        }
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void markerClick() {
        if (mWorkOrder != null) {
            mFFMDataManager.deleteWorkOrder();
            mFFMDataManager.tx(tx -> {
                if (BuildConfig.DEBUG) {
//                    Location location = TotalPlayLocationService.getLastLocation();
//                    mWorkOrder.latitude = String.format(Locale.getDefault(), "%f", location.getLatitude());
//                    mWorkOrder.longitude = String.format(Locale.getDefault(), "%f", location.getLongitude());
                }
                tx.save(mWorkOrder);
            });
            mWorkOrdersCallback.onSuccessSaveWorkOrder(mWorkOrder);
        }
    }

    public void loadWorkOrders() {
        loadWorkOrders = true;
        initTimer();
    }

    private void initTimer() {
        mRunnable = () -> {
            mWSManager.requestWs(SearchWorkOrdersResponse.class, WSManager.WS.WORK_ORDERS, getDefinition().workOrders(new SearchWorkOrdersRequest(mUser.employeeUser)));
            if (loadWorkOrders)
                initTimer();
        };
        mainHandler.postDelayed(mRunnable, 10000);
    }

    private void successLoadedWorkOrders(String requestUrl, SearchWorkOrdersResponse searchWorkOrdersResponse) {
        if (searchWorkOrdersResponse.resultId != null && searchWorkOrdersResponse.resultId.equals("0")) {
            if (searchWorkOrdersResponse.workOrders != null) {
                mFFMDataManager.tx(tx -> {
                    tx.delete(WorkOrder.class);
                });
                ArrayList<WorkOrder> workOrders = new ArrayList<>();
                for (WorkOrder workOrder : searchWorkOrdersResponse.workOrders) {
                    if (workOrder.id != null) {
                        if (workOrder.configWorkOrder == null) {
                            workOrder.configWorkOrder = new ConfigWorkOrder();
                            workOrder.configWorkOrder.dnPhones = new DNPhones();
                        }
                        workOrders.add(workOrder);
                    }
                }
                if (workOrders.size() > 0) {
                    mWorkOrder = workOrders.get(0);
                    mWorkOrdersCallback.onSuccessLoadWorkOrderList(workOrders);
                } else {
                    MessageUtils.toast(mContext, R.string.dialog_error_empty_work_orders);
                }
            } else {
                onErrorLoadResponse(requestUrl, "");
            }
        } else if (searchWorkOrdersResponse.resultId != null && searchWorkOrdersResponse.resultId.equals("1")) {
            MessageUtils.stopProgress();
            MessageUtils.toast(mContext, R.string.dialog_error_empty_work_orders);
        } else {
            onErrorLoadResponse(requestUrl, "");
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {

    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.WORK_ORDERS)) {
            successLoadedWorkOrders(requestUrl, (SearchWorkOrdersResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        MessageUtils.toast(mContext, R.string.dialog_error_loaded_work_orders);
    }

    @Override
    public void onLocationUpdatedEvent(LocationUpdatedEvent event) {
        mWorkOrdersCallback.onSuccessUpdateLocation(event.location);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNotificationArrivedEvent(NotificationArrivedEvent event) {
        mWSManager.requestWs(SearchWorkOrdersResponse.class, WSManager.WS.WORK_ORDERS, getDefinition().workOrders(new SearchWorkOrdersRequest(mUser.employeeUser)));
    }

    @Override
    public void onPause() {
        super.onPause();
        mainHandler.removeCallbacks(mRunnable);
        onPause = true;
        loadWorkOrders = false;
    }
}
