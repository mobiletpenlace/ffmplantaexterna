package com.totalplay.fieldforcetpenlace.modules.enl.info;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ChangeStatusResponse;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderStatus;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.ChangeStatusPresenter;
import com.totalplay.utils.MessageUtils;

/**
 * Created by totalplay on 11/18/16.
 * FFM
 */
public class DetailWorkOrderPresenter extends ChangeStatusPresenter implements WSCallback {

    private Callback mCallback;

    public DetailWorkOrderPresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    public interface Callback {
        void onSuccessChangeStatus(WorkOrder workOrder);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mStatus = WorkOrderStatus.WORKING;
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void onContinueClick() {
        if (!mWorkOrder.isInInstallation) {
            changeStatus(mStatus);
        } else {
            mCallback.onSuccessChangeStatus(mWorkOrder);
        }
    }

    private void successChangeStatus(String requestUrl, ChangeStatusResponse changeStatusResponse) {
        if (changeStatusResponse.result.equals("0")) {
            MessageUtils.toast(mContext, R.string.dialog_success_changed_status);
            mWorkOrder = mFFMDataManager.queryWhere(WorkOrder.class).findFirst();
            mWorkOrder.isInInstallation = true;
            mFFMDataManager.tx(tx -> tx.save(mWorkOrder));
            mCallback.onSuccessChangeStatus(mWorkOrder);
        } else {
            onErrorLoadResponse(requestUrl, changeStatusResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.CHANGE_STATUS)) {
            MessageUtils.progress(mAppCompatActivity, R.string.dialog_changing_status);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.CHANGE_STATUS)) {
            successChangeStatus(requestUrl, (ChangeStatusResponse) baseResponse);
        }
    }

}
