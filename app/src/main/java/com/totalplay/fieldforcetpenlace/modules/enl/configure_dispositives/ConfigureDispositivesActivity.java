package com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.newpresenter.WorkOrderPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.adapters.base.BaseTab;
import com.totalplay.fieldforcetpenlace.view.adapters.base.SimpleBaseTabAdapter;
import com.totalplay.fieldforcetpenlace.view.fragments.EquipmentListFragment;
import com.totalplay.mvp.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by totalplay on 6/26/17.
 * FieldForceManagement
 */

@SuppressWarnings("unchecked")
public class ConfigureDispositivesActivity extends BaseFFMActivity implements
        ConfigureDispositivesPresenter.Callback,
        GetEquipmentsPresenter.Callback {

    @BindView(R.id.act_equipments_view_pager)
    ViewPager pager;
    @BindView(R.id.act_equipments_view_tabs)
    TabLayout tabLayout;

    private List<BaseTab> mBaseTabs;
    private ConfigureDispositivesPresenter mConfigureDispositivesPresenter;
    private GetEquipmentsPresenter mGetEquipmentsPresenter;
    private WorkOrderPresenter mWorkOrderPresenter;
    private EquipmentListFragment mEquipmentListFragmentOne;
    private EquipmentListFragment mEquipmentListFragmentTwo;

    private boolean mSaveCaptureInfo = true;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isSettingsDispositive) {
            Intent intent = new Intent(appCompatActivity, ConfigureDispositivesActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_equipments);
        setTitle("");
        ButterKnife.bind(this);
        mGetEquipmentsPresenter.loadEquipments();

        mTrainingManager.settingEquipmentsActivity();
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mConfigureDispositivesPresenter = new ConfigureDispositivesPresenter(this, this),
                mGetEquipmentsPresenter = new GetEquipmentsPresenter(this, this),
                mWorkOrderPresenter = new WorkOrderPresenter(this)
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (BaseTab baseTab : mBaseTabs)
            baseTab.fragment.onActivityResult(requestCode, resultCode, data);
    }

    public void onLoadedWorkOrders() {
        if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
            mBaseTabs = new ArrayList<BaseTab>() {{
                add(new BaseTab("Dispositivos", mEquipmentListFragmentOne = EquipmentListFragment.newInstance(EquipmentType.OTHERS)));
                add(new BaseTab("Teléfonos", mEquipmentListFragmentTwo = EquipmentListFragment.newInstance(EquipmentType.PHONES)));
            }};
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(mWorkOrder.ownerId)) {
            tabLayout.setVisibility(View.GONE);
            mBaseTabs = new ArrayList<BaseTab>() {{
                add(new BaseTab("Teléfonos", mEquipmentListFragmentOne = EquipmentListFragment.newInstance(EquipmentType.ALL)));
            }};
        }

        pager.setAdapter(new SimpleBaseTabAdapter(getSupportFragmentManager(), mBaseTabs));
        tabLayout.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(pager));
    }

    @OnClick(R.id.act_equipments_continue)
    public void onContinueClick() {
        if (mBaseTabs != null) {
            List<Equipment> equipmentList = new ArrayList<>();
            if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
                if (mEquipmentListFragmentOne.isValidInfoDispositives() & mEquipmentListFragmentTwo.isValidInfoDispositives()) {
                    equipmentList.addAll(mEquipmentListFragmentOne.getEquipmentList());
                    equipmentList.addAll(mEquipmentListFragmentTwo.getEquipmentList());
                    mConfigureDispositivesPresenter.sendEquipments(equipmentList);
                }
            } else if (WorkOrderOwnerType.INTEGRATOR.contains(mWorkOrder.ownerId)) {
                if (mEquipmentListFragmentOne.isValidInfoDispositives()) {
                    equipmentList.addAll(((EquipmentListFragment) mBaseTabs.get(0).fragment).getEquipmentList());
                    mConfigureDispositivesPresenter.sendEquipments(equipmentList);
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBaseTabs != null && mSaveCaptureInfo) {
            List<Equipment> equipmentList = new ArrayList<>();
            if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
                equipmentList.addAll(mEquipmentListFragmentOne.getEquipmentList());
                equipmentList.addAll(mEquipmentListFragmentTwo.getEquipmentList());
            } else if (WorkOrderOwnerType.INTEGRATOR.contains(mWorkOrder.ownerId)) {
                equipmentList.addAll(((EquipmentListFragment) mBaseTabs.get(0).fragment).getEquipmentList());
            }
            mConfigureDispositivesPresenter.saveEquipments(equipmentList);
        }
    }

    @Override
    public void onSuccessLoadEquipments(List<Equipment> equipmentList) {
        if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {

        }
        if (equipmentList.size() == 0) {
            mWorkOrderPresenter.saveSuccessSettingsEquipments();
            baseModule.finishModule(this, mWorkOrder);
            finish();
        } else {
            onLoadedWorkOrders();
        }
    }

    @Override
    public void onErrorGetEquipmentsEvent() {
        finish();
    }

    @Override
    public void onErrorModelsInfoEquipments() {

    }

    @Override
    public void onSuccessSettingsEquipments() {
        mSaveCaptureInfo = false;
        baseModule.finishModule(this, mWorkOrder);
        finish();
    }
}
