package com.totalplay.fieldforcetpenlace.modules.enl.acceptance_act;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.BusinessFiles;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.FileUtils;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WrapperPhotosEvidence;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.io.File;
import java.util.List;

/**
 * Created by jorgehdezvilla on 25/09/17.
 * Enlace
 */

public class AcceptanceActEvidencePresenter extends FFMPresenter {

    private static final int TAKE_PICTURE = 0x0022;

    private final String serverFolder = ServerFolders.ACCEPTANCE_CARD;
    private WrapperPhotosEvidence mFileWrapper;
    private File mRequestedFile;
    private boolean mWasTakeEvidence = false;

    private Callback mCallback;

    public interface Callback {
        void onSuccessLoadPicture(File file);

        void onSuccessLoadEvidences(List<PhotoEvidence> photoEvidences);

        void onSuccessContinue(WorkOrder workOrder);
    }

    public AcceptanceActEvidencePresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String savedInstance;
        if (mWorkOrder != null) {
            mFileWrapper = mFFMDataManager.queryWhere(WrapperPhotosEvidence.class)
                    .filter("workOrder", mWorkOrder.id)
                    .filter("serverFolder", serverFolder)
                    .findFirst();
            savedInstance = mWorkOrder.id;
        } else {
            savedInstance = "default";
        }

        if (mFileWrapper == null) {
            mFileWrapper = new WrapperPhotosEvidence(savedInstance, serverFolder);
        }
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void onActivityResult(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            if (mRequestedFile != null) {
                mWasTakeEvidence = true;
                mCallback.onSuccessLoadPicture(mRequestedFile);
            }
        }
    }

    public void checkEvidences() {
        if (mFileWrapper != null && mFileWrapper.isValid()) {
            mCallback.onSuccessLoadEvidences(mFileWrapper.listPhotos);
        }
    }

    public void onContinueClick() {
        if (mWasTakeEvidence) {
            mFFMDataManager.tx(tx -> {
                mWorkOrder.isAcceptanceCardEvidencesCaptured = true;
                tx.save(mWorkOrder);
            });
            mCallback.onSuccessContinue(mWorkOrder);
        } else {
            MessageUtils.toast(mContext, R.string.dialog_info_evidence_activity);
        }
    }

    public void saveImageFile(String name, String path) {
        mFFMDataManager.tx(tx -> {
            PhotoEvidence photo = new PhotoEvidence();
            photo.name = name;
            photo.path = path;

            photo.workOrder = mWorkOrder.id;
            photo.serverFolder = serverFolder;
            photo.baseUrl = mWorkOrder.baseUrl;
            photo.interventionType = mWorkOrder.interventionType;
            if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
                photo.type = ImageTypeEnum.InternalPlant.ACCEPTANCE_ACT;
            } else if (WorkOrderOwnerType.INTEGRATOR.contains(mWorkOrder.ownerId)) {
                photo.type = ImageTypeEnum.Integrator.ACCEPTANCE_ACT;
            }


            photo = tx.save(photo);
            mFileWrapper.listPhotos.add(photo);
            tx.save(mFileWrapper);
        });
    }

    public void onImageClick(String name) {
        mRequestedFile = BusinessFiles.newDocument(name);
        mRequestedFile = FileUtils.validFile(mRequestedFile);
        if (mRequestedFile != null) {
//            TakePictureUtils.startImportPicture(mAppCompatActivity, mRequestedFile.getAbsolutePath(), TAKE_PICTURE);
        }
    }
}
