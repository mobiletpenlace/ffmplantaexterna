package com.totalplay.fieldforcetpenlace.modules.enl.login;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.BuildConfig;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.UserStatus;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.operator_status.ChangeOperatorStatusPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.modules.enl.ws_tester.WSTesterActivity;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.Prefs;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.OnTouch;

import static com.totalplay.fieldforcetpenlace.library.enums.OperatorStatus.AVAILABLE;

@SuppressWarnings("unchecked")
public class LoginActivity extends BaseFFMActivity implements LoginPresenter.LoginCallback, ChangeOperatorStatusPresenter.ChangeOperatorStatusCallback {

    @BindView(R.id.act_login_signin_flow_type)
    RadioGroup mFlowTypeRadioGroup;
    @BindView(R.id.act_login_signin_flow_pi_training)
    TextView mTrainingTextView;

    @BindView(R.id.act_login_username)
    public EditText mUsernameEditText;
    @BindView(R.id.act_login_validation)
    public EditText mValidationEditText;
    @BindView(R.id.act_login_version)
    public TextView mVersionTextView;
    @BindView(R.id.preview_password)
    public ImageView mPreviewPassword;
    @BindView(R.id.act_login_signin)
    public Button mLoginButton;

    private LoginPresenter mLoginPresenter;
    private ChangeOperatorStatusPresenter mChangeOperatorStatusPresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, LoginActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    public static Intent createIntent(Context applicationContext) {
        return new Intent(applicationContext, LoginActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.enl_activity_login);
        ButterKnife.bind(this);

        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mUsernameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mValidationEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );

        ((TextInputLayout) findViewById(R.id.act_login_username_content)).setHintEnabled(false);
        ((TextInputLayout) findViewById(R.id.act_login_password_content)).setHintEnabled(false);

        mLoginPresenter.loadVersion();

        mVersionTextView.setText(getString(R.string.version, BuildConfig.VERSION_NAME));
        if (BuildConfig.DEBUG) {
            mUsernameEditText.setText(BuildConfig.USER_TEST);
            mValidationEditText.setText(BuildConfig.VALIDATION_TEST);
        }

        if (WSManager.init().getDebugEnabled()) {
            mFlowTypeRadioGroup.setVisibility(View.VISIBLE);
            mTrainingTextView.setVisibility(View.VISIBLE);
            Prefs.instance().putString(Keys.FLOW_TYPE, WorkOrderOwnerType.INTERNAL_PLANT);
            mFlowTypeRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                if (checkedId == R.id.act_login_signin_flow_pi) {
                    Prefs.instance().putString(Keys.FLOW_TYPE, WorkOrderOwnerType.INTERNAL_PLANT);
                } else {
                    Prefs.instance().putString(Keys.FLOW_TYPE, WorkOrderOwnerType.INTEGRATOR.get(0));
                }
            });
        }
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mLoginPresenter = new LoginPresenter(this, this),
                mChangeOperatorStatusPresenter = new ChangeOperatorStatusPresenter(this, this)
        };
    }

    @OnTextChanged(R.id.act_login_validation)
    public void textChangedPassword(CharSequence text) {
        if (text.length() > 5) {
            mPreviewPassword.setVisibility(View.VISIBLE);
        } else {
            mPreviewPassword.setVisibility(View.GONE);
        }
    }

    @OnTouch(R.id.preview_password)
    public boolean onTouch(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mValidationEditText.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            case MotionEvent.ACTION_UP:
                mValidationEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
        }
        return true;
    }

    @OnClick(R.id.act_login_signin)
    public void onClickLogin(View v) {
        if (mFormValidator.isValid()) {
            String username = mUsernameEditText.getText().toString().trim();
            String validation = mValidationEditText.getText().toString().trim();
            mLoginPresenter.loginClick(username, validation);
        }
    }

    @Override
    public void onSuccessLoginEvent(User user) {
        mChangeOperatorStatusPresenter.reloadUser();
        mChangeOperatorStatusPresenter.sendOperatorStatus(new UserStatus("1", AVAILABLE, R.color.green_splitter));
    }

    @Override
    public void onSuccessChangeOperatorStatus(User user) {
        baseModule.finishModule(this, user);
        finish();
    }

    @Override
    public void onVersionEventStatus(boolean status) {
        if (!status) mLoginButton.setEnabled(false);
    }

    int countVersion = 0;

    @OnClick(R.id.act_login_version)
    public void onVersionClick() {
        countVersion++;
        if (countVersion > 10) {
            countVersion = 0;
            WSTesterActivity.launch(this);
        }
    }

}