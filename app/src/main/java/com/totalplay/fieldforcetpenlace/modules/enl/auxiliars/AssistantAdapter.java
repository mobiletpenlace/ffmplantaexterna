package com.totalplay.fieldforcetpenlace.modules.enl.auxiliars;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Assistant;
import com.totalplay.utils.MessageUtils;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleAdapter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseViewHolder;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.RealmList;

/**
 * Created by jorgeHdez on 1/5/17.
 * FMP
 */
@SuppressWarnings({"unchecked"})
public class AssistantAdapter extends BaseSimpleAdapter<Assistant, AssistantAdapter.AdapterViewHolder> {

    private Context mContext;
    private String[] colors = new String[]{"#ff4950E7", "#ff04CF67", "#ff0089D4"};
    private HashMap<String, Integer> lettersAlphabet;
    private Assistant mWithoutAssistantsElement;
    public List<Assistant> mSelectAssistant = new RealmList<>();

    public AssistantAdapter(Context context) {
        mContext = context;
        lettersAlphabet = new HashMap<>();
    }

    @Override
    public int getItemLayout() {
        return R.layout.enl_item_assistant;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new AdapterViewHolder(view);
    }

    class AdapterViewHolder extends BaseViewHolder<Assistant> {

        @BindView(R.id.item_assistant_letter)
        public TextView mLetterAssistantTextView;
        @BindView(R.id.item_assistant_letter_icon)
        public TextView mLetterIconAssistantTextView;
        @BindView(R.id.item_assistant_name)
        public TextView mNameAssistantTextView;
        @BindView(R.id.item_assistant_background_icon)
        public CircleImageView cr1;

        @BindView(R.id.item_assistant_frame_select_icon)
        public ViewGroup mSelectIconViewGroup;
        @BindView(R.id.item_assistant_frame_select_check_icon)
        public ImageView mSelectImageIconViewGroup;

        AdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(mOnItemClickListener);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, Assistant assistant) {
            String firstLetter = "";
            if (assistant.name.length() > 0) {
                firstLetter = String.valueOf(assistant.name.toCharArray()[0]);
            }
            if (!lettersAlphabet.containsKey(firstLetter)) {
                lettersAlphabet.put(firstLetter, position);
            }
            if (position % 2 == 0) {
                cr1.setBorderColor(Color.parseColor(colors[2]));
            } else if (position % 3 == 0) {
                cr1.setBorderColor(Color.parseColor(colors[1]));
            } else {
                cr1.setBorderColor(Color.parseColor(colors[0]));
            }

            int letterPosition = lettersAlphabet.get(firstLetter);
            if (letterPosition == position) {
                mLetterAssistantTextView.setVisibility(View.VISIBLE);
            } else {
                mLetterAssistantTextView.setVisibility(View.INVISIBLE);
            }

            GradientDrawable gradientDrawable = (GradientDrawable) mSelectIconViewGroup.getBackground();
            if (mSelectAssistant.contains(assistant)) {
                mSelectImageIconViewGroup.setVisibility(View.VISIBLE);


                gradientDrawable.setColor(ContextCompat
                        .getColor(mLetterAssistantTextView.getContext(), R.color.LightGreen));
            } else {
                mSelectImageIconViewGroup.setVisibility(View.GONE);
                gradientDrawable.setColor(ContextCompat
                        .getColor(mLetterAssistantTextView.getContext(), R.color.gray2));
            }

            mLetterAssistantTextView.setText(firstLetter);
            mLetterIconAssistantTextView.setText(firstLetter);
            mNameAssistantTextView.setText(assistant.name);
            itemView.setTag(assistant);
        }
    }

    private View.OnClickListener mOnItemClickListener = v1 -> {
        Assistant model = (Assistant) v1.getTag();
        if (mWithoutAssistantsElement.auxId.equals(model.auxId)) {
            if (mSelectAssistant.contains(model)) {
                mSelectAssistant.remove(model);
            } else {
                mSelectAssistant.clear();
                mSelectAssistant.add(model);
            }
        } else {
            if (!mSelectAssistant.contains(mWithoutAssistantsElement)) {
                if (mSelectAssistant.contains(model)) {
                    mSelectAssistant.remove(model);
                } else {
                    if (mSelectAssistant.size() < 4) {
                        mSelectAssistant.add(model);
                    } else {
                        MessageUtils.toast(mContext, "Solo puede seleccionar 4 auxliares");
                    }
                }
            }
        }
        notifyDataSetChanged();
    };

    @Override
    public void update(List<Assistant> assistants) {
        mSelectAssistant.clear();
        mWithoutAssistantsElement = assistants.get(0);
        super.update(assistants);
    }

}
