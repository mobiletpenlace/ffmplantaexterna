package com.totalplay.fieldforcetpenlace.modules.enl.survey;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.TestRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.BaseResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Question;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.List;

/**
 * Created by NS-TOTALPLAY on 25/09/2017.
 * Enlace
 */

public class SurveyPresenter extends FFMPresenter implements WSCallback {

    private Callback mCallback;

    public SurveyPresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    public interface Callback {
        void onSuccessSurvey(WorkOrder workOrder);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void sendQuestions(List<Question> questions) {
        TestRequest testRequest = new TestRequest();
        for (Question question : questions) {
            question.idOTFFM = mWorkOrder.id;
            question.idoperator = mUser.operatorId;
        }
        testRequest.questions = questions;
        mWSManager.requestWs(BaseResponse.class, WSManager.WS.SURVEY, getDefinition().testapp(testRequest));
    }

    private void successUploadSurvey(String requestUrl, BaseResponse baseResponse) {
        if (baseResponse.result.equals("0")) {
            mFFMDataManager.tx(tx -> {
                mWorkOrder.isSigned = true;
                mWorkOrder.isSurveyCompleted = true;
                tx.save(mWorkOrder);
            });
            mCallback.onSuccessSurvey(mWorkOrder);
        } else {
            onErrorLoadResponse(requestUrl, baseResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, "Enviando encuesta");
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.SURVEY)) {
            successUploadSurvey(requestUrl, (BaseResponse) baseResponse);
        }
    }

}
