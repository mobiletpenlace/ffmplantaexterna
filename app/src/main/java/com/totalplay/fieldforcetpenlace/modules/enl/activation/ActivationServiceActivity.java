package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.totalplay.fieldforcetpenlace.BuildConfig;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WifiModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives.ConfigureDispositivesPresenter;
import com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives.GetEquipmentsPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.adapters.ActivationServiceFragmentAdapter;
import com.totalplay.fieldforcetpenlace.view.custom.WifiView;
import com.totalplay.fieldforcetpenlace.view.fragments.EquipmentListFragment;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


@SuppressWarnings("unchecked")
public class ActivationServiceActivity extends BaseFFMActivity implements
        ActivationServiceUpdatePresenter.ActivationServiceUpdateCallback, ValidaAppPresenter.ValidaAppCallback,
        GetEquipmentsPresenter.Callback, ConfigureDispositivesPresenter.Callback {

    @BindView(android.R.id.content)
    View mRootView;

    @BindView(R.id.item_activation_extender)
    ImageView mExtenderImageView;
    @BindView(R.id.item_activation_dispositives)
    ImageView mDispositivesImageView;


    @BindView(R.id.act_service_settings_equipment_next)
    FloatingActionButton mNextButton;
    @BindView(R.id.act_service_activation_done)
    FloatingActionButton mDoneButton;
    @BindView(R.id.act_service_activation_previous)
    FloatingActionButton mPreviousButton;
    @BindView(R.id.act_service_activation_view_pager)
    ViewPager mViewPager;

    private ActivationServiceUpdatePresenter mActivationServiceUpdatePresenter;
    private ValidaAppPresenter mValidaAppPresenter;
    private GetEquipmentsPresenter mGetEquipmentsPresenter;
    private ConfigureDispositivesPresenter mConfigureEquipmentsPresenter;

    private InternetFragment mInternetFragment;
    private PhoneFragment mPhoneFragment;
    private WifiFragment mWifiFragment;
    private EquipmentListFragment mEquipmentListFragment;

    public String mComments;


    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isActivate) {
            Intent intent = new Intent(appCompatActivity, ActivationServiceActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
//            RoutingApp.OnSuccessActivationEvent(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_service_activation);
        setTitle(R.string.title_activity_activation_service);
        ButterKnife.bind(this);

        mGetEquipmentsPresenter.loadEquipments();
        mTrainingManager.activationServiceActivity();
        if (BuildConfig.DEBUG) {
            mDoneButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mActivationServiceUpdatePresenter = new ActivationServiceUpdatePresenter(this, this),
                mValidaAppPresenter = new ValidaAppPresenter(this, this),
                mGetEquipmentsPresenter = new GetEquipmentsPresenter(this, this),
                mConfigureEquipmentsPresenter = new ConfigureDispositivesPresenter(this, this)
        };
    }

    @Override
    public void onSuccessSettingsEquipments() {
        ArrayList<WifiModel> wifiModels = new ArrayList<>();
        for (WifiView wifiView : mWifiFragment.mWifiViews) {
            wifiModels.add(wifiView.getTvModel());
        }

        mActivationServiceUpdatePresenter.configureEquipments(
                mInternetFragment.getInternetModel(),
                mPhoneFragment.getPhoneModels(), wifiModels);
    }

    public void settingsEquipment() {
        boolean allFormsValid = mInternetFragment.mFormValidator.isValid();
        for (WifiView wifiView : mWifiFragment.mWifiViews) {
            allFormsValid &= wifiView.mFormValidator.isValid();
        }

        allFormsValid &= mEquipmentListFragment.isValidInfoDispositives();

        if (allFormsValid) {
            mConfigureEquipmentsPresenter.sendEquipments(mEquipmentListFragment.getEquipmentList());
        } else {
            MessageUtils.toast(this, R.string.dialog_error_validate_edit_text);
        }

    }

    public void activateWorkOrder(String comments) {
        mComments = comments;
        mValidaAppPresenter.validateApp();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mInternetFragment.onActivityResult(requestCode, resultCode, data);
        mPhoneFragment.onActivityResult(requestCode, resultCode, data);
        mWifiFragment.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.act_service_settings_equipment_next, R.id.act_service_activation_previous})
    public void onNavigationClick(View view) {
        if (view.getId() == R.id.act_service_settings_equipment_next) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
        } else {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
        }
    }

    @OnClick(R.id.act_service_activation_done)
    public void onDoneActivation() {
        baseModule.finishModule(this, mWorkOrder);
        finish();
    }

    public void onLoadedWorkOrder() {
        if (mWorkOrder != null) {
            if (mWorkOrder.quotationInfo != null && mWorkOrder.quotationInfo.servicePlans != null) {

                if (getSupportFragmentManager().getFragments().size() == 0) {
                    mInternetFragment = InternetFragment.newInstance();
                    mPhoneFragment = PhoneFragment.newInstance();
                    mWifiFragment = WifiFragment.newInstance();
                    mEquipmentListFragment = EquipmentListFragment.newInstance(EquipmentType.OTHERS);
                } else {
//                    mPhoneFragment = (PhoneFragment) getSupportFragmentManager().getFragments().get(0);
//                    mWifiFragment = (WifiFragment) getSupportFragmentManager().getFragments().get(1);
//                    mInternetFragment = (InternetFragment) getSupportFragmentManager().getFragments().get(2);
                }

                List<Equipment> equipmentList = mFFMDataManager.queryWhere(Equipment.class)
                        .filterWhereNot("dispositive", EquipmentType.WIFI_EXTENDER)
                        .filterWhereNot("dispositive", EquipmentType.ONT)
                        .filterWhereNot("dispositive", EquipmentType.PBX)
                        .list();

                List<Equipment> equipmentWifiList = mFFMDataManager.queryWhere(Equipment.class).filter("dispositive", EquipmentType.WIFI_EXTENDER).list();

                if (equipmentWifiList.size() == 0) mExtenderImageView.setVisibility(View.GONE);
                if (equipmentList.size() == 0) mDispositivesImageView.setVisibility(View.GONE);

                getSupportFragmentManager().getFragments().clear();
                ActivationServiceFragmentAdapter adapter = new ActivationServiceFragmentAdapter(getSupportFragmentManager());
                adapter.addFragment(mPhoneFragment);
                if (equipmentWifiList.size() != 0)
                    adapter.addFragment(mWifiFragment);
                if (equipmentList.size() > 0) {
                    adapter.addFragment(mEquipmentListFragment);
                }
                adapter.addFragment(mInternetFragment);

                mViewPager.setOffscreenPageLimit(5);
                mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        if (position == 0) {
                            mPreviousButton.setVisibility(View.GONE);
                            mNextButton.setVisibility(View.VISIBLE);
                        } else if (position == mViewPager.getAdapter().getCount() - 1) {
                            mNextButton.setVisibility(View.GONE);
                            mPreviousButton.setVisibility(View.VISIBLE);
                        } else {
                            mPreviousButton.setVisibility(View.VISIBLE);
                            mNextButton.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
                mViewPager.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onSuccessLoadStatusWS(String statusWS) {
        mInternetFragment.setStatusWS(statusWS);
    }

    @Override
    public void onSuccessSettingsEquipment() {
        mInternetFragment.mActivationButton.setVisibility(View.VISIBLE);
        mInternetFragment.mSettingButton.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessActivateWorkOrder(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
        finish();
    }

    @Override
    public void onRequiredActivationsProcess() {
        mActivationServiceUpdatePresenter.activate(mComments);
    }

    @Override
    public void onActivatedAccountStatus(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
        finish();
    }

    @Override
    public void onSuccessLoadEquipments(List<Equipment> equipmentList) {
        onLoadedWorkOrder();
    }

    @Override
    public void onErrorGetEquipmentsEvent() {
        finish();
    }

    @Override
    public void onErrorModelsInfoEquipments() {
        MessageUtils.toast(this, "Ha ocurrido un error. Valide la información con soporte (Modelos)");
        finish();
    }

}
