package com.totalplay.fieldforcetpenlace.modules.enl.profile;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

/**
 * Created by jorgehdezvilla on 15/01/18.
 * FFM Enlace
 */

public class ProfileModule extends BaseModule<ModuleParameter, ModuleParameter> {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, ModuleParameter moduleParameter) {
        MyProfileActivity.launch(this, appCompatActivity);
    }
}
