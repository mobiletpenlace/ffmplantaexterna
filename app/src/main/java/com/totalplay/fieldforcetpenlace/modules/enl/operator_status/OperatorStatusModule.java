package com.totalplay.fieldforcetpenlace.modules.enl.operator_status;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

/**
 * Created by jorgehdezvilla on 15/01/18.
 * FFM Enlace
 */

public class OperatorStatusModule extends BaseModule {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, ModuleParameter moduleParameter) {
        ChangeOperatorStatusActivity.launch(appCompatActivity);
    }
}
