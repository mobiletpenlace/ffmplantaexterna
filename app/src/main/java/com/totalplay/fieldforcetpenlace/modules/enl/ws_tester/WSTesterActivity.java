package com.totalplay.fieldforcetpenlace.modules.enl.ws_tester;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.background.ws.definitions.EnlaceWebServicesDefinition;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ActivationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ArriveTimeRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AssistantsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AuthenticationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.AutoFindRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.CancelStatusRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.CashPaymentEngineRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ChangeStatusRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.CompletedWorkOrderRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ConfigSDMRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.DiscountMaterialsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.EquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.FindSplitterAppRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetDNRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetFlagRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetMacRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetMaterialRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.PaymentEngineRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.QueryWorkTeamRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.QuotationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SaveWorkAssistantTeamRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SearchWorkOrdersRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SetFlagRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SettingEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SplittersRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.TestRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateDNsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateEstimatedTimeRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateSplitterAppRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidSerialNumberRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidWorkOrderRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidaCuentaRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.BaseResponse;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleRecyclerView;
import com.totalplay.view.BaseSimpleRecyclerView.RefreshBaseRecyclerCallback;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by jorgehdezvilla on 03/11/17.
 * Enlace
 */

public class WSTesterActivity extends BaseFFMActivity implements WSTesterPresenter.WSTesterCallback, RefreshBaseRecyclerCallback {

    private List<TesterWS> mWsServices;
    private WSTesterPresenter mWsTesterPresenter;
    private BaseSimpleRecyclerView mBaseSimpleRecyclerView;

    public static void launch(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, WSTesterActivity.class);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_ws_tester);

        EnlaceWebServicesDefinition definition = WebServices.servicesEnlace();

        mWsServices = new ArrayList<TesterWS>() {{
            add(new TesterWS(WSManager.WS.LOGIN, definition.login(new AuthenticationRequest())));
//            add(new TesterWS(WSManager.WS.SEARCH_EMPLOYEE, new SearchEmployeeRequest()));
            add(new TesterWS(WSManager.WS.ASSISTANTS, definition.assistants(new AssistantsRequest())));
            add(new TesterWS(WSManager.WS.WORK_ASSISTANTS_TEAM, definition.workAssistantTeam(new QueryWorkTeamRequest())));
            add(new TesterWS(WSManager.WS.SAVE_WORK_ASSISTANTS_TEAM, definition.saveWorkAssistantTeam(new SaveWorkAssistantTeamRequest())));
            add(new TesterWS(WSManager.WS.CHANGE_STATUS, definition.changeStatus(new ChangeStatusRequest())));
            add(new TesterWS(WSManager.WS.CANCEL_WORK_ORDER, definition.cancelWorkOrder(new CancelStatusRequest())));
            add(new TesterWS(WSManager.WS.WORK_ORDERS, definition.workOrders(new SearchWorkOrdersRequest())));
            add(new TesterWS(WSManager.WS.SPLITTERS, definition.splitters(new SplittersRequest())));
            add(new TesterWS(WSManager.WS.ADD_SPLITERS, definition.findSplitter(new FindSplitterAppRequest())));
            add(new TesterWS(WSManager.WS.UPDATE_SPLITTER, definition.updateSplitterApp(new UpdateSplitterAppRequest())));
            add(new TesterWS(WSManager.WS.GET_MATERIALS, definition.consultingMaterials(new GetMaterialRequest())));
            add(new TesterWS(WSManager.WS.DISCOUNT_MATERIALS, definition.discountMaterials(new DiscountMaterialsRequest())));
            add(new TesterWS(WSManager.WS.SAVE_ARRIVE_TIME, definition.arriveTime(new ArriveTimeRequest())));
            add(new TesterWS(WSManager.WS.VALIDATE_WORK_ORDER, definition.validWorkOrder(new ValidWorkOrderRequest())));
            add(new TesterWS(WSManager.WS.UPDATE_TIME, definition.updateTime(new UpdateEstimatedTimeRequest())));
            add(new TesterWS(WSManager.WS.SURVEY, definition.testapp(new TestRequest())));
            add(new TesterWS(WSManager.WS.QUOTATION, definition.quotation(new QuotationRequest())));
            add(new TesterWS(WSManager.WS.GET_EQUIPMENTS, definition.equipments(new EquipmentRequest())));
            add(new TesterWS(WSManager.WS.GENERATE_DNS, definition.generateDN(new GetDNRequest())));
            add(new TesterWS(WSManager.WS.PAYMENT, definition.payment(new PaymentEngineRequest())));
            add(new TesterWS(WSManager.WS.CASH_PAYMENT, definition.cashPayment(new CashPaymentEngineRequest())));
            add(new TesterWS(WSManager.WS.GET_FLAGS, definition.getFlag(new GetFlagRequest())));
            add(new TesterWS(WSManager.WS.ACTIVATION, definition.activate(new ActivationRequest())));
            add(new TesterWS(WSManager.WS.AUTO_FIND, definition.autoFind(new AutoFindRequest())));
            add(new TesterWS(WSManager.WS.VALIDATE_SERIAL_NUMBER, definition.validSerialNumber(new ValidSerialNumberRequest())));
            add(new TesterWS(WSManager.WS.GET_MAC, definition.getMac(new GetMacRequest())));
            add(new TesterWS(WSManager.WS.UPDATE_EQUIPMENT, definition.updateEquipment(new UpdateEquipmentRequest())));
            add(new TesterWS(WSManager.WS.SETTING_EQUIPMENT, definition.settingEquipment(new SettingEquipmentRequest())));
            add(new TesterWS(WSManager.WS.UPDATE_DN, definition.updateDNs(new UpdateDNsRequest())));
            add(new TesterWS(WSManager.WS.SEND_FLAG, definition.setFlag(new SetFlagRequest())));
            add(new TesterWS(WSManager.WS.FINISH_STATUS, definition.finishStatus(new CompletedWorkOrderRequest())));
            add(new TesterWS(WSManager.WS.CONFIG_SMD, definition.getConfigSDM(new ConfigSDMRequest())));
            add(new TesterWS(WSManager.WS.VALIDATE_APP, definition.validaCuentaENL(new ValidaCuentaRequest())));
        }};

        mBaseSimpleRecyclerView = new BaseSimpleRecyclerView(this, R.id.act_ws_tester_list, R.id.act_ws_tester_refresh)
                .setRefreshBaseRecycler(this)
                .setAdapter(new WsTesterAdapter(this))
                .addBottomOffsetDecoration(200);

        mBaseSimpleRecyclerView.update(mWsServices);
        mWsTesterPresenter.testServices(mWsServices);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mWsTesterPresenter = new WSTesterPresenter(this, this);
    }

    @Override
    public void onRequestWs(String wsService) {
        for (TesterWS testerWS : mWsServices) {
            if (wsService.equals(testerWS.wsName)) {
                testerWS.inProgress = true;
            }
        }
    }

    @Override
    public void onSuccessResponseWS(String wsService, boolean status) {
        for (TesterWS testerWS : mWsServices) {
            if (wsService.equals(testerWS.wsName)) {
                testerWS.inProgress = false;
                testerWS.status = status ? TestWsStatus.OK : TestWsStatus.ERROR;
            }
        }
        mBaseSimpleRecyclerView.update(mWsServices);
    }

    @Override
    public void onRefreshItems() {
        mWsTesterPresenter.stopTest();
        for (TesterWS testerWS : mWsServices) {
            testerWS.status = TestWsStatus.WAIT;
            testerWS.inProgress = false;
        }
        mBaseSimpleRecyclerView.stopRefresh();
        mWsTesterPresenter.testServices(mWsServices);
        mBaseSimpleRecyclerView.update(mWsServices);
    }

    public class TestWsStatus {
        public static final int WAIT = 1;
        public static final int OK = 2;
        public static final int ERROR = 3;
    }

    public class TesterWS<T> {
        public String wsName;
        public int status;
        public Class<BaseResponse> tClass;
        public Call<ResponseBody> call;
        public boolean inProgress = false;

        public TesterWS(String wsName, Call<ResponseBody> call) {
            this.wsName = wsName;
            this.status = TestWsStatus.WAIT;
            this.tClass = BaseResponse.class;
            this.call = call;
        }
    }

}
