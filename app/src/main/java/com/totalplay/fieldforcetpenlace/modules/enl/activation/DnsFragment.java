package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.routing.RoutingApp;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by charlssalazar on 10/10/17.
 * Enlace
 */

public class DnsFragment extends BaseFFMFragment {
    @BindView(R.id.fr_rv_dns)
    RecyclerView fr_rv_dns;

    private ArrayList<Item_Dns> list_dns = new ArrayList<>();
    private DnsAdapter dnsAdapter;

    public static DnsFragment newInstance() {
        Bundle bundle = new Bundle();
        DnsFragment fragment = new DnsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_dns, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        populateArray();

        fr_rv_dns.setLayoutManager(new LinearLayoutManager(getActivity()));
        dnsAdapter = new DnsAdapter();
        fr_rv_dns.setAdapter(dnsAdapter);
        dnsAdapter.update(list_dns);
    }

    public void populateArray() {
        Item_Dns obj1 = new Item_Dns();
        obj1.data1 = "DNS 1";
        list_dns.add(obj1);

        Item_Dns obj2 = new Item_Dns();
        obj2.data1 = "DNS 2";
        list_dns.add(obj2);


        Item_Dns obj3 = new Item_Dns();
        obj3.data1 = "DNS 3";
        list_dns.add(obj3);

        Item_Dns obj4 = new Item_Dns();
        obj4.data1 = "DNS 4";
        list_dns.add(obj4);

        Item_Dns obj5 = new Item_Dns();
        obj5.data1 = "DNS 5";
        list_dns.add(obj5);

        Item_Dns obj6 = new Item_Dns();
        obj6.data1 = "DNS 6";
        list_dns.add(obj6);

        Item_Dns obj7 = new Item_Dns();
        obj7.data1 = "DNS 7";
        list_dns.add(obj7);
    }

    @OnClick(R.id.fr_rv_dns_continue)
    public void onContinue() {
        if (mWorkOrder.isSetttingPXB) {
            RoutingApp.OnSuccessActivatePBXEvent((AppCompatActivity) getActivity(), mWorkOrder);
        } else {
            MessageUtils.toast(getContext(), "Debe configurar el equipo");
        }
    }

    @Override
    public BasePresenter getPresenter() {
        return null;
    }


    public class Item_Dns {
        public String data1;
        public String data2;
        public String data3;
        public String data4;
    }
}


