package com.totalplay.fieldforcetpenlace.modules.enl.acceptance_act;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by total on 26/06/17.
 * Enlace
 */

@SuppressWarnings("unchecked")
public class AcceptanceActActivity extends BaseFFMActivity implements AcceptanceActEvidencePresenter.Callback {

    @BindView(R.id.act_card_evidence_evidence_one)
    ImageView mOneImageView;
    @BindView(R.id.act_card_evidence_evidence_two)
    ImageView mTwoImageView;
    @BindView(R.id.act_card_evidence_evidence_three)
    ImageView mThreeImageView;

    private AcceptanceActEvidencePresenter mEvidencePresenter;
    private ImageView mSelectedImageView;
    private SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isAcceptanceCardEvidencesCaptured) {
            Intent intent = new Intent(appCompatActivity, AcceptanceActActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.enl_activity_acceptance_act_evidences);
        ButterKnife.bind(this);

        mEvidencePresenter.checkEvidences();
        mTrainingManager.acceptanceActivity();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mEvidencePresenter = new AcceptanceActEvidencePresenter(this, this);
    }

    @OnClick({R.id.act_card_evidence_evidence_one, R.id.act_card_evidence_evidence_two, R.id.act_card_evidence_evidence_three})
    public void onImageClick(View view) {
        mSelectedImageView = (ImageView) view;
        switch (view.getId()) {
            case R.id.act_card_evidence_evidence_one:
                mEvidencePresenter.onImageClick("PageOne" + timeStampFormat.format(new Date()));
                break;
            case R.id.act_card_evidence_evidence_two:
                mEvidencePresenter.onImageClick("PageTwo" + timeStampFormat.format(new Date()));
                break;
            case R.id.act_card_evidence_evidence_three:
                mEvidencePresenter.onImageClick("PageThree" + timeStampFormat.format(new Date()));
                break;
        }
    }

    @OnClick(R.id.continueButton)
    public void onNextClick() {
        mEvidencePresenter.onContinueClick();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mEvidencePresenter.onActivityResult(resultCode);
    }

    @Override
    public void onSuccessLoadPicture(File file) {
        mSelectedImageView.setPadding(0, 0, 0, 0);
        Glide.with(this)
                .load(file)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(mSelectedImageView);
        mEvidencePresenter.saveImageFile(file.getName(), file.getPath());
    }

    @Override
    public void onSuccessLoadEvidences(List<PhotoEvidence> photoEvidences) {
        if (photoEvidences.size() > 0) {
            HashMap<String, ImageView> imageViewHashMap = new HashMap<String, ImageView>() {{
                put("AcceptancePageOne.jpg", mOneImageView);
                put("AcceptancePageTwo.jpg", mTwoImageView);
                put("AcceptancePageThree.jpg", mThreeImageView);
            }};
            for (PhotoEvidence photoEvidence : photoEvidences) {
                if (photoEvidence.name == null) continue;
                if (imageViewHashMap.containsKey(photoEvidence.name)) {
                    Glide.with(this)
                            .load(photoEvidence.path)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(imageViewHashMap.get(photoEvidence.name));
                }
            }
        }
    }

    @Override
    public void onSuccessContinue(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
        finish();
    }

}
