package com.totalplay.fieldforcetpenlace.modules.enl.replace_dispositives;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.InfoEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.SettingsEquipmentsResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;

/**
 * Created by jorgehdezvilla on 19/12/17.
 * FFM Enlace
 */

public class GetSettingsEquipmentsPresenter extends FFMPresenter implements WSCallback {

    private Callback mCallback;

    public interface Callback {
        void onSuccessLoadSettingsEquipments();
    }

    public GetSettingsEquipmentsPresenter(AppCompatActivity appCompatActivity, GetSettingsEquipmentsPresenter.Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    public void loadSettingsEquipments() {
        if (mFFMDataManager.queryWhere(InfoEquipment.class).list().size() > 0) {
            mCallback.onSuccessLoadSettingsEquipments();
        } else {
            InfoEquipmentRequest infoEquipmentRequest = new InfoEquipmentRequest(mWorkOrder.account);
            mWSManager.requestWs(SettingsEquipmentsResponse.class, WSManager.WS.GET_SETTINGS_EQUIPMENTS, getDefinition().settingsEquipments(infoEquipmentRequest));
        }
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
        if (requestUrl.equals(WSManager.WS.GET_SETTINGS_EQUIPMENTS)) {
            SettingsEquipmentsResponse settingsEquipmentsResponse = (SettingsEquipmentsResponse) baseResponse;
            if (settingsEquipmentsResponse.result.equals("0")) {
                mFFMDataManager.tx(tx -> tx.save(settingsEquipmentsResponse.dispositivesList));
                mCallback.onSuccessLoadSettingsEquipments();
            } else {
                onErrorLoadResponse(requestUrl, settingsEquipmentsResponse.resultDescription);
            }
        }
    }
}
