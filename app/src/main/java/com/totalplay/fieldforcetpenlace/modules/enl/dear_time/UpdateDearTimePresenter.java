package com.totalplay.fieldforcetpenlace.modules.enl.dear_time;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateEstimatedTimeRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.UpdateEstimatedTimeResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

/**
 * Created by NS-262 on 07/02/2017.
 * FFM
 */

public class UpdateDearTimePresenter extends FFMPresenter implements WSCallback {

    private Callback mCallback;

    public UpdateDearTimePresenter(AppCompatActivity appCompatActivity, Callback updateDearTimeCallback) {
        super(appCompatActivity);
        mCallback = updateDearTimeCallback;
    }

    public interface Callback {
        void onSuccessSendDearTime(WorkOrder workOrder);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void updateTime(String minutes) {
        UpdateEstimatedTimeRequest updateTime = new UpdateEstimatedTimeRequest();
        updateTime.workOrderId = mWorkOrder.id;
        updateTime.userId = mUser.operatorId;
        updateTime.minutes = minutes;
        mWSManager.requestWs(UpdateEstimatedTimeResponse.class, WSManager.WS.UPDATE_TIME, getDefinition().updateTime(updateTime));
    }

    public void successUpdateEstimatedTime(String requestUrl, UpdateEstimatedTimeResponse updateEstimatedTimeResponse) {
        if (updateEstimatedTimeResponse.result != null && updateEstimatedTimeResponse.result.equals("0")) {
            mWorkOrder.isTimeEstimated = true;
            mFFMDataManager.tx(tx -> tx.save(mWorkOrder));
            mCallback.onSuccessSendDearTime(mWorkOrder);
        } else {
            onErrorLoadResponse(requestUrl, updateEstimatedTimeResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, "Registrando hora");
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.UPDATE_TIME)) {
            successUpdateEstimatedTime(requestUrl, (UpdateEstimatedTimeResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.toast(mContext, "Error al actualizar la hora");
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.toast(mContext, R.string.dialog_error_connection);
    }
}
