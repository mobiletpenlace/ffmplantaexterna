package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.CashOrigin;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by totalplay on 6/26/17.
 * FieldForceManagement
 */

@SuppressWarnings("unchecked")
public class PaymentActivity extends BaseFFMActivity {

    @BindView(R.id.act_payment_view_pager)
    ViewPager pager;
    @BindView(R.id.act_payment_view_tabs)
    TabLayout tabLayout;

    private ArrayList<BaseFFMFragment> mPaymentFragments;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isPaid && workOrder.quotationInfo.firstRent.equals("true")) {
            Intent intent = new Intent(appCompatActivity, PaymentActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_payment);
        setTitle("");
        ButterKnife.bind(this);
        onLoadedWorkOrders();
        mTrainingManager.paymentActivity();
    }

    public void onLoadedWorkOrders() {

        mPaymentFragments = new ArrayList<BaseFFMFragment>() {{
            add(PaymentCardFragment.newInstance());
            add(PaymentCashFragment.newInstance());
            add(PaymentOtherFragment.newInstance(CashOrigin.CHECK));
            add(PaymentOtherFragment.newInstance(CashOrigin.TRANSFER));
            add(PaymentOtherFragment.newInstance(CashOrigin.NONE));
        }};

        pager.setAdapter(new PaymentPagerAdapter(getSupportFragmentManager(), mPaymentFragments));
        tabLayout.setupWithViewPager(pager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(pager));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (BaseFFMFragment fragment : mPaymentFragments) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
