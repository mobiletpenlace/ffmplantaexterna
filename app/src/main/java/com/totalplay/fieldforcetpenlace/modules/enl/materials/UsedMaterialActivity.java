package com.totalplay.fieldforcetpenlace.modules.enl.materials;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMaterials;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.adapters.UsedMaterialsAdapter;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleRecyclerView;
import com.totalplay.view.BaseSimpleRecyclerView.RefreshBaseRecyclerCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by charlssalazar on 30/06/17.
 * FFM
 */

@SuppressWarnings("unchecked")
public class UsedMaterialActivity extends BaseFFMActivity implements MaterialsPresenter.MaterialsCallback,
        RefreshBaseRecyclerCallback, UsedMaterialPresenter.Callback {

    @BindView(R.id.act_materials_recycler)
    RecyclerView mRecyclerView;

    MaterialsPresenter mMaterialsPresenter;
    UsedMaterialPresenter mUsedMaterialPresenter;

    UsedMaterialsAdapter mConsultingMaterialsAdapter;

    private BaseSimpleRecyclerView mBaseSimpleRecyclerView;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isCapturedMaterials) {
            Intent intent = new Intent(appCompatActivity, UsedMaterialActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_used_materials_v2);
        ButterKnife.bind(this);

        mBaseSimpleRecyclerView = new BaseSimpleRecyclerView(this, R.id.act_materials_recycler, R.id.act_materials_recycler_refresh)
                .setRefreshBaseRecycler(this)
                .setAdapter(mConsultingMaterialsAdapter = new UsedMaterialsAdapter(this))
                .setEmptyView(R.id.view_layout_empty_recycler)
                .addBottomOffsetDecoration(200);
        mMaterialsPresenter.loadMaterials();

        mTrainingManager.usedMAterialActivity();
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mMaterialsPresenter = new MaterialsPresenter(this, this),
                mUsedMaterialPresenter = new UsedMaterialPresenter(this, this)
        };
    }

    @OnClick(R.id.act_used_materials_finish_button)
    public void onClickFinish() {
        if (mConsultingMaterialsAdapter.getUsedMaterials().size() > 0) {
            mUsedMaterialPresenter.discount(mConsultingMaterialsAdapter.getUsedMaterials());
        } else {
            MessageUtils.toast(this, "Agrega materiales para poder continuar");
        }
    }

    @Override
    public void onSuccessLoadMaterials(ArrayList<GetMaterials.Materials> mMaterialLists) {
        mBaseSimpleRecyclerView.mSwipeRefreshLayout.setEnabled(false);
        mBaseSimpleRecyclerView.update(mMaterialLists);
    }

    @Override
    public void onRefreshItems() {
        mMaterialsPresenter.loadMaterials();
    }

    @Override
    public void onSuccessSendMaterials(WorkOrder workOrder) {
        baseModule.finishModule(this, workOrder);
        finish();
    }
}
