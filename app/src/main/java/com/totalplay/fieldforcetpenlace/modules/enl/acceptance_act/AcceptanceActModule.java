package com.totalplay.fieldforcetpenlace.modules.enl.acceptance_act;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

/**
 * Created by jorgehdezvilla on 15/01/18.
 * FFM Enlace
 */

public class AcceptanceActModule extends BaseModule<WorkOrder, WorkOrder> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        AcceptanceActActivity.launch(this, appCompatActivity, workOrder);
    }

}
