package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.mvp.BaseFragment;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by icortes on 07/07/17.
 * FFM Enlace
 */

public class Splitter1Fragment extends BaseFragment {

    @BindView(R.id.fr1_port1_izq)
    LinearLayout fr1PortIzq;
    @BindView(R.id.fr1_port2_izq)
    LinearLayout fr2PortIzq;
    @BindView(R.id.fr1_port3_izq)
    LinearLayout fr3PortIzq;
    @BindView(R.id.fr1_port4_izq)
    LinearLayout fr4PortIzq;
    @BindView(R.id.fr1_port5_der)
    LinearLayout fr5PortDer;
    @BindView(R.id.fr1_port6_der)
    LinearLayout fr6PortDer;
    @BindView(R.id.fr1_port7_der)
    LinearLayout fr7PortDer;
    @BindView(R.id.fr1_port8_der)
    LinearLayout fr8PortDer;

    Boolean[] array = new Boolean[8];
    LinearLayout[] imagenes;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_splitter_1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        Arrays.fill(array, Boolean.FALSE);
        imagenes = new LinearLayout[]{fr1PortIzq, fr2PortIzq, fr3PortIzq, fr4PortIzq, fr5PortDer, fr6PortDer, fr7PortDer, fr8PortDer};
    }


    @OnClick({R.id.fr1_port1_izq, R.id.fr1_port2_izq, R.id.fr1_port3_izq, R.id.fr1_port4_izq, R.id.fr1_port5_der, R.id.fr1_port6_der, R.id.fr1_port7_der, R.id.fr1_port8_der})
    public void onPortSelected(View view) {
        getActivity();
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(100);
        switch (view.getId()) {
            case R.id.fr1_port1_izq:
                if (array[0]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[0] = true;
                }
                getPort("1");
                break;
            case R.id.fr1_port2_izq:
                if (array[1]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[1] = true;
                }
                getPort("2");
                break;
            case R.id.fr1_port3_izq:
                if (array[2]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[2] = true;
                }
                getPort("3");
                break;
            case R.id.fr1_port4_izq:
                if (array[3]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[3] = true;
                }
                getPort("4");
                break;
            case R.id.fr1_port5_der:
                if (array[4]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[4] = true;
                }
                getPort("5");
                break;
            case R.id.fr1_port6_der:
                if (array[5]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[5] = true;
                }
                getPort("6");
                break;
            case R.id.fr1_port7_der:
                if (array[6]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[6] = true;
                }
                getPort("7");
                break;
            case R.id.fr1_port8_der:
                if (array[7]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[7] = true;
                }
                getPort("8");
                break;

        }
    }

    private void getPort(String port) {
        SplitterSelectedActivity.numberPort = port;
        for (int i = 0; i < array.length; i++) {
            if (i < 4) {
                if (array[i]) {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_asignado_izq));
                } else {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_disponible_izq));
                }
            } else {
                if (array[i]) {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_asignado_der));
                } else {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_disponible_der));
                }
            }
        }
    }

}
