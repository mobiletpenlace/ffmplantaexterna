package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.tasks.AutoFindTask;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.AutoFindResponse;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

/**
 * Created by jorgehdezvilla on 26/09/17.
 * Enlace
 */

public class InternetPresenter extends FFMPresenter {

    private InternetCallback mInternetCallback;

    public interface InternetCallback {
        void onSuccessLoadFind(AutoFindResponse autoFindResponse);
    }

    public InternetPresenter(AppCompatActivity appCompatActivity, InternetCallback internetCallback) {
        super(appCompatActivity);
        mInternetCallback = internetCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void autoFind2(String serialNumber) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_data);
        if (serialNumber.isEmpty()) {
            MessageUtils.toast(mContext, "Número de serie obligatorio");
            return;
        }
        AutoFindTask mAutoFindTask = new AutoFindTask(mContext, serialNumber, new AutoFindTask.AutoFindListener() {
            @Override
            public void onSuccessAutoFind(AutoFindResponse autoFindResponse) {
                MessageUtils.stopProgress();
                mInternetCallback.onSuccessLoadFind(autoFindResponse);
            }

            @Override
            public void onErrorAutoFind(String errorReason) {
                MessageUtils.stopProgress();
                if (errorReason != null) {
                    MessageUtils.toast(mAppCompatActivity, errorReason);
                } else {
                    MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_connection);
                }
            }
        });
        mAutoFindTask.execute();
    }

}

