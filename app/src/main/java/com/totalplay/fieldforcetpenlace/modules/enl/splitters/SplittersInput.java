package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 11/01/18.
 * FFM ENLACE
 */

public class SplittersInput implements Serializable, ModuleParameter {

    public WorkOrder workOrder;
    public boolean canAdd;

}
