package com.totalplay.fieldforcetpenlace.modules.enl.info_map;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.GetFlagRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetFlagResponse;
import com.totalplay.fieldforcetpenlace.library.enums.FlagType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Flag;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

/**
 * Created by NS-257 on 07/08/2017.
 * Enlace
 */

public class GetFlagPresenter extends FFMPresenter implements WSCallback {

    public GetFlagPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void getFlags() {
        if (mWorkOrder.id != null) {
            mWSManager.requestWs(GetFlagResponse.class, WSManager.WS.GET_FLAGS, getDefinition().getFlag(new GetFlagRequest(mWorkOrder.id)));
        }
    }

    private void successLoadFlags(String requestUrl, GetFlagResponse getFlagResponse) {
        if (getFlagResponse.result.equals("0")) {
            if (getFlagResponse.flags != null) {
                mFFMDataManager.tx(tx -> {
                    for (Flag flag : getFlagResponse.flags.get(0).flags) {
                        if (flag.onOff.equals("TRUE")) {
                            if (flag.flagId.equals(FlagType.CONFIGURED_EQUIPMENT))
                                mWorkOrder.configuredDevices = true;
                            if (flag.flagId.equals(FlagType.CONFIGURED_DNS))
                                mWorkOrder.configuredDN = true;
                            if (flag.flagId.equals(FlagType.ACTIVATED_ACCOUNT))
                                mWorkOrder.isActivate = true;
                            if (flag.flagId.equals(FlagType.PAID_ACCOUNT))
                                mWorkOrder.isPaid = true;
                            if (flag.flagId.equals(FlagType.SENT_EVIDENCE))
                                mWorkOrder.isEvidencesCaptured = true;
                            if (flag.flagId.equals(FlagType.ASSIGNED_SPLITTER))
                                mWorkOrder.isAssignedSplitter = true;
                            if (flag.flagId.equals(FlagType.ESTIMATED_TIME))
                                mWorkOrder.isTimeEstimated = true;
                            if (flag.flagId.equals(FlagType.SEND_MATERIALS))
                                mWorkOrder.isCapturedMaterials = true;
                            if (flag.flagId.equals(FlagType.URBAN_REFERENCES)) {
                                mWorkOrder.isTransit = true;
                                mWorkOrder.isInSite = true;
                                mWorkOrder.isRegisterTimeArrival = true;
                                mWorkOrder.isRegisterLocationArrival = true;
                            }
                            if (flag.flagId.equals(FlagType.END_SURVEY))
                                mWorkOrder.isSurveyCompleted = true;
                        }
                    }
                    tx.save(mWorkOrder);
                });
            } else {
                onErrorLoadResponse(requestUrl, getFlagResponse.resultDescription);
            }
        } else {
            onErrorLoadResponse(requestUrl, getFlagResponse.resultDescription);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.GET_FLAGS)) {
            successLoadFlags(requestUrl, (GetFlagResponse) baseResponse);
        }
    }

}
