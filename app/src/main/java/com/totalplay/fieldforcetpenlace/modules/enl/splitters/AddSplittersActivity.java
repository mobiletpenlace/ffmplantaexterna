package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.custom.CatalogEditText;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by FFM on 20/06/17.
 * FFM
 */

@SuppressWarnings("unchecked")
public class AddSplittersActivity extends BaseFFMActivity implements AddSplittersPresenter.AddSplittersCallback {

    @BindView(R.id.act_add_splitter_label)
    EditText mSplitterLabelTextView;
    @BindView(R.id.act_add_splitter_ports_number)
    CatalogEditText<String> mPortNumberCatalog;
    @BindView(R.id.act_add_splitter_porter_bussy)
    EditText mPortBusyTextView;
    @BindView(R.id.act_add_splitter_status_spinner)
    CatalogEditText<String> mStatusSpinner;
    @BindView(R.id.act_add_splitter_panoramic_photo)
    ImageView mPanoramicPhotoImageView;
    @BindView(R.id.act_add_splitter_power_photo)
    ImageView mPowerPhotoImageView;
    @BindView(R.id.act_add_splitter_box_photo)
    ImageView mBoxPhotoImageView;

    private FormValidator mFormValidator;
    private AddSplittersPresenter mAddSplittersPresenter;
    private ImageView mSelectedImageView = null;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, AddSplittersActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.enl_activity_add_splitter);
        ButterKnife.bind(this);


        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(mPortBusyTextView, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStatusSpinner, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mSplitterLabelTextView, Regex.SPECIAL_ALPHA_NUMERIC, R.string.dialog_error_invalid_characters)
        );

        initCatalogs();
    }

    private void initCatalogs() {
        mPortNumberCatalog.setCatalogs(new ArrayList<String>() {{
            add("8");
            add("16");
            add("24");
            add("32");
            add("48");
        }});

        mStatusSpinner.setCatalogs(new ArrayList<String>() {{
            add("Saturado");
            add("No Iluminado");
            add("Dañado");
            add("Atenuado");
            add("Sin Problemas");
        }});
    }

    @Override
    protected BasePresenter getPresenter() {
        return mAddSplittersPresenter = new AddSplittersPresenter(this, this);
    }

    @OnClick({R.id.act_add_splitter_panoramic_photo, R.id.act_add_splitter_power_photo, R.id.act_add_splitter_box_photo})
    public void onImageClick(View view) {
        mSelectedImageView = (ImageView) view;
        mAddSplittersPresenter.imageViewClick(view.getId());
    }

    @OnClick(R.id.button_continue_splitter)
    public void onContinueSplitterClick() {
        if (mFormValidator.isValid()) {
            int portNumberCatalog = Integer.parseInt(mPortNumberCatalog.getText().toString());
            int portBussy = Integer.parseInt(mPortBusyTextView.getText().toString());
            String status = mStatusSpinner.getText().toString();
            String closeLabel = mSplitterLabelTextView.getText().toString().equals("") ?
                    "SIN NOMBRE" : mSplitterLabelTextView.getText().toString();
            mAddSplittersPresenter.continueSplitterClick(portNumberCatalog, portBussy, status, closeLabel);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAddSplittersPresenter.onActivityResult(requestCode, resultCode);
    }

    @Override
    public void onSuccessLoadImage(int requestCode, File mRequestedFile) {
        if (mSelectedImageView != null) {
            mSelectedImageView.setPadding(0, 0, 0, 0);
            Glide.with(this)
                    .load(mRequestedFile)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(mSelectedImageView);
            mAddSplittersPresenter.savePhoto(mRequestedFile.getName(), mRequestedFile.getAbsolutePath());
        }
    }

    @Override
    public void onSuccessSaveSplitterInfo(WorkOrder mWorkOrder, Splitter splitter, boolean searchOtherSplitter, Reason reason, boolean mStopWorkOrder) {
        baseModule.finishModule(this, new SplittersOutput(mWorkOrder, splitter, reason, searchOtherSplitter, mStopWorkOrder));
    }

}