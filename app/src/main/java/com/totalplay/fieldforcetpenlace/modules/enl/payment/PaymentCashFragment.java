package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.CashOrigin;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.library.utils.StringUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by totalplay on 6/26/17.
 * FieldForceManagement
 */

@SuppressWarnings("unchecked")
public class PaymentCashFragment extends BaseFFMFragment implements PaymentOtherPresenter.Callback {

    @BindView(R.id.fr_payment_cash_pack)
    TextView mPackTextView;
    @BindView(R.id.fr_payment_cash_amount)
    TextView mAmountTextView;

    private PaymentOtherPresenter mPaymentOtherPresenter;

    public static PaymentCashFragment newInstance() {
        Bundle bundle = new Bundle();
        bundle.putString(Keys.CASH_ORIGIN, CashOrigin.CASH);
        PaymentCashFragment fragment = new PaymentCashFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_payment_cash, container, false);
    }

    @Override
    public BasePresenter getPresenter() {
        return mPaymentOtherPresenter = new PaymentOtherPresenter((AppCompatActivity) getActivity(), this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mPaymentOtherPresenter.loadArguments(this);

        if (mWorkOrder.quotationInfo != null && mWorkOrder.quotationInfo.amount != null)
            mAmountTextView.setText(String.format("$ %s", StringUtils.formattedAmount(mWorkOrder.quotationInfo.amount)));
        mPackTextView.setText(mWorkOrder.packageContracted);
    }

    @OnClick(R.id.fr_payment_cash_pay)
    public void onPayClick() {
        if (mAmountTextView.getText().toString().trim().length() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("¿Estás seguro de realizar el pago?");
            builder.setPositiveButton("Aceptar", (dialog, which) -> mPaymentOtherPresenter.payment(mAmountTextView.getText().toString().trim()));
            builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.cancel());
            builder.show();
        } else {
            MessageUtils.toast(getContext(), R.string.dialog_error_validate_edit_text);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccessLoadImage(int requestCode, File mRequestedFile) {

    }

    @Override
    public void onSuccessPayment(WorkOrder workOrder) {
        ((BaseFFMActivity) getActivity()).baseModule.finishModule((AppCompatActivity) getActivity(), workOrder);
    }
}
