package com.totalplay.fieldforcetpenlace.modules.enl.finish_installation;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

/**
 * Created by jorgehdezvilla on 16/01/18.
 * FFM Enlace
 */

public class FinishInstallationModule extends BaseModule {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, ModuleParameter moduleParameter) {
        FinishInstallationActivity.launch(this, appCompatActivity);
    }
}
