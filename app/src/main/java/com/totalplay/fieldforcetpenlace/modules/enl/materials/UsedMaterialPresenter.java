package com.totalplay.fieldforcetpenlace.modules.enl.materials;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.DiscountMaterialsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.DiscountMaterialsResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorgehdezvilla on 16/09/17.
 * FFM
 */

public class UsedMaterialPresenter extends FFMPresenter implements WSCallback {

    private Callback mCallback;

    public UsedMaterialPresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    public interface Callback {
        void onSuccessSendMaterials(WorkOrder workOrder);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void discount(List<DiscountMaterialsRequest.UsedMaterials> usedMaterials) {
        List<DiscountMaterialsRequest.UsedMaterials> usedMaterialRequest = new ArrayList<>();
        for (DiscountMaterialsRequest.UsedMaterials used : usedMaterials) {
            if (used.quantity != null && !used.quantity.equals("0")) {
                usedMaterialRequest.add(used);
            }
        }
        DiscountMaterialsRequest discountMaterialsRequest = new DiscountMaterialsRequest();
        discountMaterialsRequest.idOT = mWorkOrder.id;
        discountMaterialsRequest.idOperario = mUser.operatorId;
        discountMaterialsRequest.materials = usedMaterialRequest;
        mWSManager.requestWs(DiscountMaterialsResponse.class, WSManager.WS.DISCOUNT_MATERIALS, getDefinition().discountMaterials(discountMaterialsRequest));
    }

    private void successDiscountMaterials(String requestUrl, DiscountMaterialsResponse discountMaterialsResponse) {
        if (discountMaterialsResponse.result.equals("0")) {
            MessageUtils.stopProgress();
            MessageUtils.toast(mContext, discountMaterialsResponse.resultDescription);
            mCallback.onSuccessSendMaterials(mWorkOrder);
        } else {
            onErrorLoadResponse(requestUrl, discountMaterialsResponse.resultDescription);
        }
    }


    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.DISCOUNT_MATERIALS)) {
            MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_data);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.DISCOUNT_MATERIALS)) {
            mWorkOrder.isCapturedMaterials = true;
            mFFMDataManager.tx(tx -> tx.save(mWorkOrder));
            successDiscountMaterials(requestUrl, (DiscountMaterialsResponse) baseResponse);
        }
    }

}
