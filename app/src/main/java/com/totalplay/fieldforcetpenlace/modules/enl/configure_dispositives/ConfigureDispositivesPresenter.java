package com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SettingEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.UpdateResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Dispositive;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorgehdezvilla on 02/10/17.
 * Enlace
 */

public class ConfigureDispositivesPresenter extends FFMPresenter implements WSCallback {

    private Callback mCallback;

    public ConfigureDispositivesPresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    public interface Callback {
        void onSuccessSettingsEquipments();
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void sendEquipments(List<Equipment> equipmentList) {
        if (equipmentList.size() > 0){
            List<Dispositive> dispositiveList = new ArrayList<>();
            for (Equipment equipment : equipmentList) {
                String modelId = equipment.equipmentModel != null ? equipment.equipmentModel.modelID : "";
                dispositiveList.add(new Dispositive(equipment.servicePlan, equipment.serialNumber, equipment.mac, modelId));
            }
            SettingEquipmentRequest settingEquipmentRequest = new SettingEquipmentRequest();
            settingEquipmentRequest.dispositives = new SettingEquipmentRequest.Dispositivos();
            settingEquipmentRequest.dispositives.dispositivesList = dispositiveList;
            mWSManager.requestWs(UpdateResponse.class, WSManager.WS.SETTING_EQUIPMENT, getDefinition().settingEquipment(settingEquipmentRequest));
        } else {
            mCallback.onSuccessSettingsEquipments();
        }
    }

    public void saveEquipments(List<Equipment> equipmentList) {
        mFFMDataManager.tx(tx -> tx.save(equipmentList));
    }

    private void successSettingEquipments(String requestUrl, UpdateResponse updateResponse) {
        if (updateResponse.result != null && updateResponse.result.result.equals("0")) {
            MessageUtils.toast(mContext, updateResponse.result.resultDescription);
            mFFMDataManager.tx(tx -> {
                mWorkOrder.isSettingsDispositive = true;
                tx.save(mWorkOrder);
                tx.delete(Equipment.class);
                tx.delete(InfoEquipment.class);
            });
            mCallback.onSuccessSettingsEquipments();
        } else {
            if (updateResponse.result != null)
                onErrorLoadResponse(requestUrl, updateResponse.result.resultDescription);
            else onErrorLoadResponse(requestUrl, "");
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_settings_equipments);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        successSettingEquipments(requestUrl, (UpdateResponse) baseResponse);
    }

}
