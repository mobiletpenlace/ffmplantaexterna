package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by charlssalazar on 10/10/17.
 */

public class DnsAdapter extends RecyclerView.Adapter<DnsAdapter.DnsViewHolder> {
    private Activity activity;
    private ArrayList<DnsFragment.Item_Dns> list_dns= new ArrayList<>();

    @Override
    public DnsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.enl_item_dns, parent, false);
        return new DnsAdapter.DnsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DnsViewHolder holder, int position) {
        holder.dns_textField.setText(list_dns.get(position).data1);
    }

    @Override
    public int getItemCount() {
        return list_dns.size();
    }

    public void update(ArrayList<DnsFragment.Item_Dns> list_dns) {
        this.list_dns = list_dns;
        notifyDataSetChanged();
    }

    public class DnsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.dns_textField)
        TextView dns_textField;

        public DnsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
