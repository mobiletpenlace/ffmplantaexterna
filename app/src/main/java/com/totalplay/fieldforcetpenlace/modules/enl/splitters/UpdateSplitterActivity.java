package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.ReasonDefinition;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.BusinessFiles;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.FileUtils;
import com.totalplay.fieldforcetpenlace.view.activities.OptionsMenuActivity;
import com.totalplay.fieldforcetpenlace.view.custom.CatalogEditText;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;


@SuppressWarnings("unchecked")
public class UpdateSplitterActivity extends OptionsMenuActivity
        implements UpdateSplitterPresenter.UpdateSplitterCallback {

    public static final int REQUEST_CODE_UPDATE_SPLITTER = 0x321;

    @BindView(R.id.act_edit_splitter_content)
    ViewGroup mButtonsEditSplitterViewGroup;
    @BindView(R.id.act_find_splitter_content)
    ViewGroup mButtonsFindSplitterViewGroup;

    @BindView(R.id.act_edit_splitter_title)
    TextView mLayoutName;
    @BindView(R.id.act_register_splitter_close_label)
    EditText mCloseLabelEditText;
    @BindView(R.id.act_register_splitter_account_num)
    EditText mAccountNumEditText;
    @BindView(R.id.act_register_splitter_total_ports)
    CatalogEditText<String> mTotalPortsCatalog;
    @BindView(R.id.act_register_splitter_ports_occupied)
    EditText mOccupiedPortsEditText;
    @BindView(R.id.act_register_splitter_port_assigned)
    EditText mAssignedPortEditText;
    @BindView(R.id.act_register_splitter_padlock)
    EditText mPadlockEditText;
    @BindView(R.id.act_register_splitter_status)
    CatalogEditText<String> mStatusCatalogEditText;
    @BindView(R.id.act_edit_splitter_evidence_panoramic)
    ImageView mPanoramicPhotoImageView;
    @BindView(R.id.act_edit_splitter_evidence_power)
    ImageView mPowerPhotoImageView;
    @BindView(R.id.act_edit_splitter_evidence_box)
    ImageView mBoxPhotoImageView;
    @BindView(R.id.setPort)
    LinearLayout linearSetPort;

    private ImageView mSelectedImageView;
    private FormValidator mAssignedFormValidator;
    private FormValidator mUpdateFormValidator;

    private UpdateSplitterPresenter mUpdateSplitterPresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, Splitter splitter) {
        Intent intent = new Intent(appCompatActivity, UpdateSplitterActivity.class);
        intent.putExtra(Keys.EXTRA_SPLITTER, splitter);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivityForResult(intent, REQUEST_CODE_UPDATE_SPLITTER);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.enl_activity_register_splitter);
        ButterKnife.bind(this);

        initCatalogs();

        mAssignedFormValidator = new FormValidator(this, true);
        mAssignedFormValidator.addValidators(
                new EditTextValidator(mAccountNumEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mTotalPortsCatalog, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mOccupiedPortsEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mAssignedPortEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStatusCatalogEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );

        mUpdateFormValidator = new FormValidator(this, true);
        mUpdateFormValidator.addValidators(
                new EditTextValidator(mAccountNumEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mTotalPortsCatalog, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mOccupiedPortsEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mStatusCatalogEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );

        initReport(getString(R.string.dialog_error_splitter), ReasonDefinition.reasonsRegister);

        mUpdateSplitterPresenter.loadSplitter();
        mTrainingManager.updateSplitterActivity();
    }

    private void initCatalogs() {
        mStatusCatalogEditText.setCatalogs(new ArrayList<String>() {{
            add("Saturado");
            add("No iluminado");
            add("Dañado");
            add("Atenuado");
            add("Sin Problemas");
        }});
        mTotalPortsCatalog.setCatalogs(new ArrayList<String>() {{
            add("8");
            add("16");
            add("24");
            add("32");
            add("48");
        }});

    }

    @Override
    protected BasePresenter getPresenter() {
        return mUpdateSplitterPresenter = new UpdateSplitterPresenter(this, this);
    }

    @OnTextChanged(value = R.id.act_register_splitter_status, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void onStatusSplitterTextChanged(Editable editable) {
        Reason reason = null;
        switch (mStatusCatalogEditText.getText().toString()) {
            case "Saturado":
                reason = new Reason("70", "SPLITTER SATURADO");
                break;
            case "No iluminado":
                reason = new Reason("112", "SPLITTER NO ILUMINADA");
                break;
            case "Dañado":
                reason = new Reason("118", "SPLITTER PUERTOS DAÑADOS");
                break;
            case "Atenuado":
                reason = new Reason("69", "SPLITTER ATENUADO");
                break;
            case "Sin Problemas":
                reason = null;
                break;
        }
        mUpdateSplitterPresenter.setReason(reason);
    }

    @OnClick({R.id.act_edit_splitter_evidence_panoramic, R.id.act_edit_splitter_evidence_power, R.id.act_edit_splitter_evidence_box})
    public void onImageClick(View view) {
//        mSelectedImageView = (ImageView) view;
//
//        String photoName = "";
//        if (!mUpdateSplitterPresenter.mPhotosValidator.contains(imageViewId)) mPhotosValidator.add(imageViewId);
//        switch (imageViewId) {
//            case R.id.act_edit_splitter_evidence_panoramic:
//                photoName = "Panoramic";
//                break;
//            case R.id.act_edit_splitter_evidence_power:
//                photoName = "Power";
//                break;
//            case R.id.act_edit_splitter_evidence_box:
//                photoName = "Box";
//                break;
//        }
//
//        mRequestedFile = BusinessFiles.newDocument(photoName);
//        mRequestedFile = FileUtils.validFile(mRequestedFile);
//        if (mRequestedFile != null) {
////            TakePictureUtils.startImportPicture(mAppCompatActivity, mRequestedFile.getAbsolutePath(), REQUEST_CODE_PICTURE);
//        }
    }

    @OnClick(R.id.act_edit_splitter_search_other)
    public void onUpdateSplitterAndSearchOtherRegister(View view) {
        mUpdateSplitterPresenter.mSearchOtherSplitter = true;
        if (mUpdateFormValidator.isValid()) {
            String totalPorts = mTotalPortsCatalog.getText().toString().trim();
            String occupiedPorts = mOccupiedPortsEditText.getText().toString().trim();
            String assignedPort = mAssignedPortEditText.getText().toString().trim();
            String padLock = mPadlockEditText.getText().toString().trim();
            mUpdateSplitterPresenter.updateSplitter(totalPorts, occupiedPorts, padLock, assignedPort);
        }
    }

    @OnClick(R.id.act_edit_splitter_register)
    public void onUpdateSplitterRegister(View view) {
        mUpdateSplitterPresenter.mSearchOtherSplitter = false;
        if (mAssignedFormValidator.isValid()) {
            String totalPorts = mTotalPortsCatalog.getText().toString().trim();
            String occupiedPorts = mOccupiedPortsEditText.getText().toString().trim();
            String assignedPort = mAssignedPortEditText.getText().toString().trim();
            String padLock = mPadlockEditText.getText().toString().trim();
            mUpdateSplitterPresenter.updateSplitter(totalPorts, occupiedPorts, padLock, assignedPort);
        }
    }

    @OnClick(R.id.act_find_splitter_search_other)
    public void onFindSplitterAndSearchOtherRegister(View view) {
        mUpdateSplitterPresenter.mSearchOtherSplitter = true;
        if (mUpdateFormValidator.isValid()) {
            String totalPorts = mTotalPortsCatalog.getText().toString().trim();
            String occupiedPorts = mOccupiedPortsEditText.getText().toString().trim();
            String assignedPort = mAssignedPortEditText.getText().toString().trim();
            String padLock = mPadlockEditText.getText().toString().trim();
            mUpdateSplitterPresenter.findSplitter(totalPorts, occupiedPorts, padLock, assignedPort);
        }
    }

    @OnClick(R.id.act_find_splitter_register)
    public void onFindSplitterRegister(View view) {
        mUpdateSplitterPresenter.mSearchOtherSplitter = false;
        if (mAssignedFormValidator.isValid()) {
            String totalPorts = mTotalPortsCatalog.getText().toString().trim();
            String occupiedPorts = mOccupiedPortsEditText.getText().toString().trim();
            String assignedPort = mAssignedPortEditText.getText().toString().trim();
            String padLock = mPadlockEditText.getText().toString().trim();
            mUpdateSplitterPresenter.findSplitter(totalPorts, occupiedPorts, padLock, assignedPort);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mUpdateSplitterPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.setPort, R.id.act_register_splitter_port_assigned})
    public void onSetPortSelected() {
        mUpdateSplitterPresenter.onSelectPortClick();
    }

    @Override
    public void onSuccessLoadSplitter(Splitter splitter) {
        if (splitter.name.equals("")) {
            mButtonsFindSplitterViewGroup.setVisibility(View.VISIBLE);
            mButtonsEditSplitterViewGroup.setVisibility(View.GONE);
            mLayoutName.setText("Hallazgo");
        }
        mCloseLabelEditText.setText(splitter.name);
        mAccountNumEditText.setText(mWorkOrder.account);
        mTotalPortsCatalog.setText(splitter.totalPorts != null ? splitter.totalPorts : "");
        mOccupiedPortsEditText.setText(splitter.occupedPorts != null ? splitter.occupedPorts : "");
    }

    @Override
    public void onSuccessLoadImage(int requestCode, File requestedFile) {
        if (mSelectedImageView != null) {
            mSelectedImageView.setPadding(0, 0, 0, 0);
            Glide.with(this)
                    .load(requestedFile)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(mSelectedImageView);
            mUpdateSplitterPresenter.saveImageFile(requestedFile.getName(), requestedFile.getPath());
        }
    }

    @Override
    public void onSuccessLoadAssignedPort(String assignedPort) {
        mAssignedPortEditText.setText(assignedPort);
    }

    @Override
    public void onSuccessSaveInfoSplitter(WorkOrder workOrder, Splitter splitter, boolean searchOtherSplitter, Reason mReason, boolean mStopWorkOrder) {
        baseModule.finishModule(this, new SplittersOutput(mWorkOrder, splitter, mReason, searchOtherSplitter, mStopWorkOrder));
    }

}