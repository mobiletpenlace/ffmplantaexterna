package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.fieldforcetpenlace.background.queue.jobs.SetFlagJob;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ActivationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.DNRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateDNsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ActivationResponse;
import com.totalplay.fieldforcetpenlace.library.enums.FlagType;
import com.totalplay.fieldforcetpenlace.library.enums.UpdateActivationErrorReason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.DN;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InternetModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhoneModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WifiModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by totalplay on 11/24/16.
 * Total
 */
public class ActivationServiceUpdatePresenter extends FFMPresenter implements WSCallback,
        ActivationTask.ActivationTaskCallback {

    private ActivationTask mActivationTask;
    private ActivationServiceUpdateCallback mActivationServiceUpdateCallback;

    public interface ActivationServiceUpdateCallback {
        void onSuccessLoadStatusWS(String statusWS);

        void onSuccessSettingsEquipment();

        void onSuccessActivateWorkOrder(WorkOrder workOrder);
    }

    public ActivationServiceUpdatePresenter(AppCompatActivity appCompatActivity, ActivationServiceUpdateCallback activationServiceUpdateCallback) {
        super(appCompatActivity);
        mActivationServiceUpdateCallback = activationServiceUpdateCallback;
    }

    public void configureEquipments(InternetModel internetModel, List<PhoneModel> phoneModels, ArrayList<WifiModel> wifiModels) {
        mFFMDataManager.tx(tx -> {
            tx.delete(WifiModel.class);
            tx.delete(InternetModel.class);
            mWorkOrder.configWorkOrder.internet = internetModel;
            mWorkOrder.configWorkOrder.wifiModels.addAll(wifiModels);
            tx.save(mWorkOrder.configWorkOrder);
        });


        UpdateEquipmentRequest updateEquipmentRequestInternet = new UpdateEquipmentRequest(internetModel);
        ArrayList<UpdateEquipmentRequest> updateEquipmentRequests = new ArrayList<>();
        for (WifiModel wifiModel : wifiModels) {
            UpdateEquipmentRequest updateEquipmentRequestTv = new UpdateEquipmentRequest(wifiModel);
            updateEquipmentRequests.add(updateEquipmentRequestTv);
        }

        List<UpdateDNsRequest> updateDNResponseList = new ArrayList<>();
        for (PhoneModel phoneModel : phoneModels) {
            if (phoneModel != null) {
                UpdateDNsRequest updateDNRequest = new UpdateDNsRequest();
                updateDNRequest.servicePlanId = phoneModel.servicePlanId;
                updateDNRequest.mainDN = phoneModel.mainDN;
                for (DN dn : phoneModel.dns) {
                    updateDNRequest.dns.add(new DNRequest(dn.dn));
                }
                updateDNResponseList.add(updateDNRequest);
            }
        }


        MessageUtils.progress(mAppCompatActivity, R.string.dialog_configuring_equipment);
        mActivationTask = new ActivationTask(mContext, this, updateEquipmentRequestInternet, updateDNResponseList, updateEquipmentRequests);
        mActivationTask.execute();
    }

    public void activate(String comments) {
        String requestMessage = "Usuario: " + mUser.employeeId + " Comentario: " + comments;
        ActivationRequest activationRequest = new ActivationRequest(mWorkOrder.id, requestMessage);
        mWSManager.requestWs(ActivationResponse.class, WSManager.WS.ACTIVATION, getDefinition().activate(activationRequest));
    }

    private void onSuccessActivation(String requestUrl, ActivationResponse activationResponse) {
        if (activationResponse.result.equals("0")) {
            Answers.getInstance().logCustom(
                    new CustomEvent("Activación")
                            .putCustomAttribute("Usuario", mUser.employeeId)
                            .putCustomAttribute("OT", mWorkOrder.id)
                            .putCustomAttribute("Cuenta", mWorkOrder.account)
            );
            mFFMDataManager.tx(tx -> {
                mWorkOrder.isActivate = true;
                tx.save(mWorkOrder);
            });
            mActivationServiceUpdateCallback.onSuccessLoadStatusWS(mContext.getString(R.string.dialog_success_activated));
            MessageUtils.toast(mContext, R.string.dialog_success_activated);
            QueueManager.add(new SetFlagJob(FlagType.ACTIVATED_ACCOUNT, mWorkOrder.id));
            mFFMDataManager.tx(tx -> tx.delete(Equipment.class));
            mActivationServiceUpdateCallback.onSuccessActivateWorkOrder(mWorkOrder);
        } else {
            mActivationServiceUpdateCallback.onSuccessLoadStatusWS(activationResponse.resultDescription);
            onErrorLoadResponse(requestUrl, activationResponse.resultDescription);
        }
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mActivationTask != null) mActivationTask.cancel(true);
    }

    @Override
    public void onSuccessUpdate() {
        MessageUtils.stopProgress();
        MessageUtils.toast(mContext, R.string.dialog_configured_equipment);
        mActivationServiceUpdateCallback.onSuccessLoadStatusWS(mContext.getString(R.string.dialog_configured_equipment));
        QueueManager.add(new SetFlagJob(FlagType.CONFIGURED_EQUIPMENT, mWorkOrder.id));
        QueueManager.add(new SetFlagJob(FlagType.CONFIGURED_DNS, mWorkOrder.id));
        mActivationServiceUpdateCallback.onSuccessSettingsEquipment();
    }

    @Override
    public void onErrorUpdate(UpdateActivationErrorReason errorReason, String errorMessage) {
        MessageUtils.stopProgress();
        if (errorReason != null) {
            switch (errorReason) {
                case COULD_NOT_ATIVATE:
                    MessageUtils.toast(mContext, errorMessage);
                    mActivationServiceUpdateCallback.onSuccessLoadStatusWS(errorMessage);
                    break;
                case COULD_NOT_UPDATE_DNS:
                    MessageUtils.toast(mContext, errorMessage);
                    mActivationServiceUpdateCallback.onSuccessLoadStatusWS(errorMessage);
                    break;
                case COULD_NOT_UPDATE_EQUIPMENT:
                    MessageUtils.toast(mContext, errorMessage);
                    mActivationServiceUpdateCallback.onSuccessLoadStatusWS(errorMessage);
                    break;
                case COULD_NOT_UPDATE_EQUIPMENT_TV:
                    MessageUtils.toast(mContext, errorMessage);
                    mActivationServiceUpdateCallback.onSuccessLoadStatusWS(errorMessage);
                    break;
                default:
                    mActivationServiceUpdateCallback.onSuccessLoadStatusWS(mContext.getString(R.string.dialog_error));
                    MessageUtils.toast(mContext, R.string.dialog_error);
                    break;
            }
        } else {
            mActivationServiceUpdateCallback.onSuccessLoadStatusWS(mContext.getString(R.string.dialog_error_connection));
            MessageUtils.toast(mContext, R.string.dialog_error_connection);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_activation);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.ACTIVATION)) {
            onSuccessActivation(requestUrl, (ActivationResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        mActivationServiceUpdateCallback.onSuccessLoadStatusWS(messageError);
    }
}
