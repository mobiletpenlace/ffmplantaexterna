package com.totalplay.fieldforcetpenlace.modules.enl.travel;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.background.bus.events.LocationUpdatedEvent;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.library.utils.LocationUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;

/**
 * Created by jorgehdezvilla on 12/09/17.
 * FFM
 */

public class WorkOrderInfoInRoutePresenter extends FFMPresenter {

    private float meters = 0.0f;
    private String latitude = "0.0";
    private String longitude = "0.0";
    private float requiredDistance = 2000.0f;

    private WorkOrderInfoInRouteCallback mWorkOrderInfoInRouteCallback;

    public interface WorkOrderInfoInRouteCallback {

        void onUpdateLocation(Location location);

        void onLoadWorkOrderAddress(Location userLocation, String workOrderlatitude, String workOrderlongitude);

        void onLoadWorkOrder(WorkOrder workOrder);

        void onCompletedMetersRequited();

    }

    public WorkOrderInfoInRoutePresenter(AppCompatActivity appCompatActivity, WorkOrderInfoInRouteCallback workOrderInfoInRouteCallback) {
        super(appCompatActivity);
        mWorkOrderInfoInRouteCallback = workOrderInfoInRouteCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void loadLocation() {
        final Location location = TotalPlayLocationService.getLastLocation();
        if (location != null) {
            latitude = mWorkOrder.latitude;
            longitude = mWorkOrder.longitude;
            if (latitude != null && !latitude.isEmpty() && longitude != null && !longitude.isEmpty()) {
                mWorkOrderInfoInRouteCallback.onLoadWorkOrderAddress(location, latitude, longitude);
            }
        }
        mWorkOrderInfoInRouteCallback.onLoadWorkOrder(mWorkOrder);
    }

    public void travelProblemsClick() {
//        RoutingApp.OnTravelProblemsEvent(mAppCompatActivity);
    }

    @Override
    public void onLocationUpdatedEvent(LocationUpdatedEvent event) {
        super.onLocationUpdatedEvent(event);
        mWorkOrderInfoInRouteCallback.onUpdateLocation(event.location);
        meters = LocationUtils.distance(event.location.getLatitude(), event.location.getLongitude(),
                Double.valueOf(latitude), Double.valueOf(longitude));
//        if (meters < requiredDistance) {
//            mWorkOrderInfoInRouteCallback.onCompletedMetersRequited();
//        }
        mWorkOrderInfoInRouteCallback.onCompletedMetersRequited();
    }

}
