package com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.EquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.EquipmentResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorgehdezvilla on 02/10/17.
 * Enlace
 */

public class GetEquipmentsPresenter extends FFMPresenter implements WSCallback {

    private Callback mSettingEquipmentsCallback;

    public GetEquipmentsPresenter(AppCompatActivity appCompatActivity, Callback settingEquipmentsCallback) {
        super(appCompatActivity);
        mSettingEquipmentsCallback = settingEquipmentsCallback;
    }

    public interface Callback {
        void onSuccessLoadEquipments(List<Equipment> equipmentList);

        void onErrorGetEquipmentsEvent();

        void onErrorModelsInfoEquipments();
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void loadEquipments() {
        if (mWorkOrder.quotationInfo != null && mWorkOrder.quotationInfo.servicePlans != null
                && mWorkOrder.quotationInfo.servicePlans.size() >= 0) {
            ArrayList<String> servicePlanIds = new ArrayList<>();
            for (ServicePlan servicePlan : mWorkOrder.quotationInfo.servicePlans) {
                servicePlanIds.add(servicePlan.servicePlanId);
            }
            EquipmentRequest equipmentRequest = new EquipmentRequest(mWorkOrder.ownerId, new EquipmentRequest.ServicePlanId(servicePlanIds));
            mWSManager.requestWs(EquipmentResponse.class, WSManager.WS.GET_EQUIPMENTS, getDefinition().equipments(equipmentRequest));
        } else {
            MessageUtils.toast(mContext, "No hay planes, intente nuevamente");
        }
    }

    private void successLoadEquipments(String requestUrl, EquipmentResponse equipmentResponse) {
        if (equipmentResponse.result.equals("0")) {
            if (equipmentResponse.result.equals("0") && equipmentResponse.equipments != null) {
                boolean validInfo = true;
                for (Equipment equipment : equipmentResponse.equipments) {
                    if (equipment.equipmentsModels == null || equipment.equipmentsModels.isEmpty()) {
                        validInfo = false;
                        break;
                    }
                }
                if (validInfo) {
                    mFFMDataManager.tx(tx -> {
                        for (Equipment equipment : equipmentResponse.equipments) {
                            equipment.serialNumber = "";
                            equipment.mac = "";
                        }
                        tx.delete(Equipment.class);
                        tx.save(equipmentResponse.equipments);
                    });
                    mSettingEquipmentsCallback.onSuccessLoadEquipments(equipmentResponse.equipments);
                } else {
                    mSettingEquipmentsCallback.onErrorModelsInfoEquipments();
                }
            } else if (equipmentResponse.result.equals("0") && equipmentResponse.equipments == null) {
                mSettingEquipmentsCallback.onSuccessLoadEquipments(new ArrayList<>());
            } else {
                onErrorLoadResponse(requestUrl, equipmentResponse.resultDescription);
            }
        } else {
            onErrorLoadResponse(requestUrl, equipmentResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_equipments);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        successLoadEquipments(requestUrl, (EquipmentResponse) baseResponse);
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        mSettingEquipmentsCallback.onErrorGetEquipmentsEvent();
    }
}
