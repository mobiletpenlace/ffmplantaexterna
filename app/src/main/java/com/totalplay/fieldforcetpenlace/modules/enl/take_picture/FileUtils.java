package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by totalplay on 11/16/16.
 */
public class FileUtils {
    public static boolean writeToFile(File file, String content) {
        try {
            if (!file.exists()) {
                boolean created = file.createNewFile();
                Log.d("TAG", "Request deleted: " + created);
            }
            PrintWriter pr = new PrintWriter(new FileOutputStream(file));
            pr.println(content);
            pr.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static File validFile(File file) {
        try {
            if (file.exists())
                if (!file.delete()) return null;
            file.getParentFile().mkdirs();
            if (!file.createNewFile()) return null;
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
