package com.totalplay.fieldforcetpenlace.modules.enl.auxiliars;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public class AssistantsModule extends BaseModule<AssistantsParameter, ModuleParameter> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, AssistantsParameter assistantsParameter) {
        AssistantsActivity.launch(this, appCompatActivity, assistantsParameter);
    }

}
