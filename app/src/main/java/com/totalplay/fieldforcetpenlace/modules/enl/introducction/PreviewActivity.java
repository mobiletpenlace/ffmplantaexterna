package com.totalplay.fieldforcetpenlace.modules.enl.introducction;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PreviewView;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.adapters.PreviewAdapter;
import com.totalplay.fieldforcetpenlace.modules.definition.FlowKey;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by reype on 25/10/2017.
 * Enlace
 */

@SuppressWarnings("unchecked")
public class PreviewActivity extends BaseFFMActivity {

    @BindView(R.id.act_preview_pager)
    ViewPager mViewPager;

    @BindView(R.id.act_preview_next)
    FloatingActionButton mNextButton;
    @BindView(R.id.act_preview_prev)
    FloatingActionButton mPrevButton;
    @BindView(R.id.act_preview_done)
    FloatingActionButton mDoneButton;

    List<PreviewView> slides;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
        }
    };

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (position == 0) {
                mPrevButton.setVisibility(View.GONE);
            } else if (position == slides.size() - 1) {
                mNextButton.setVisibility(View.GONE);
                mPrevButton.setVisibility(View.GONE);
                mDoneButton.setVisibility(View.VISIBLE);
            } else {
                mNextButton.setVisibility(View.VISIBLE);
                mPrevButton.setVisibility(View.VISIBLE);
                mDoneButton.setVisibility(View.GONE);
            }
        }

        @Override
        public void onPageSelected(int position) {
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, User user) {
        if (user.firstAccess.equals("true")) {
            if (!user.isCompletePreview) {
                Intent intent = new Intent(appCompatActivity, PreviewActivity.class);
                intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
                appCompatActivity.startActivity(intent);
            } else {
                baseModule.finishModule(appCompatActivity, user);
            }
        } else {
            baseModule.finishModule(appCompatActivity, user);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.enl_activity_preview);
        ButterKnife.bind(this);

        slides = new ArrayList<PreviewView>() {{
            add(new PreviewView("FFM Empresarial", "Introducción a la aplicación móvil", R.drawable.logo_ffmtpe));
            add(new PreviewView("Carga de OT´s", "Aquí podras visualizar tus OT asignadas", R.drawable.preview_carga_ot));
            add(new PreviewView("Información de la OT", "Te permitirá ver la información detallada de la OT", R.drawable.preview_info_ot));
            add(new PreviewView("Traslado", "Podrás navegar al domicilio de la OT con la ayuda de estas dos aplicaciones", R.drawable.preview_translado));
            add(new PreviewView("En sitio", "Notifica que haz llegado a domicilio indicado", R.drawable.preview_continue_translado));
            add(new PreviewView("En sitio", "Te mostrará la información nuevamente para validar", R.drawable.preview_fin_translado));
            add(new PreviewView("Arribo", "Te indicará que has llegado a tiempo ", R.drawable.preview_arrival));
            add(new PreviewView("Evidencias de arribo", "Tomarás evidencias para registrar tu llegada", R.drawable.preview_register_arrival));
            add(new PreviewView("Detalles de tu Orden", "Te mostrará información más específica del paquete", R.drawable.preview_detalle_ot));
            add(new PreviewView("Estimación de tiempo", "Indicarás el tiempo estimado para tu trabajo", R.drawable.preview_estimate_time));
            add(new PreviewView("Busca un Splitter", "Podrás vizualizar los splitter más cernanos a tu posición", R.drawable.preview_splitters));
            add(new PreviewView("Selección de Splitter", "Selecciona el Splitter con el que trabajarás", R.drawable.preview_select_splitter));
            add(new PreviewView("Actualización de Solitter", "Registrarás el puerto que haz ocupado para la instalación", R.drawable.preview_update_splitter));
            add(new PreviewView("Activación de telefonia", "Te mostrará la información detallada para el plan telefónico", R.drawable.preview_activation_phone));
            add(new PreviewView("Activación de Wifi", "Ingresarás la información solicitada de los dispositivos de Wifi Extender", R.drawable.preview_activation_wifi));
            add(new PreviewView("Activación de Internet", "Ingresarás la información solicitada del dispositivo de Internet y procederás con la configuración y activación", R.drawable.preview_activation_internet));
            add(new PreviewView("Configuración de equipos", "Configurarás los dispositivos restantes de acuerdo al paquete a configurar", R.drawable.preview_configure_addons));
            add(new PreviewView("Resumen de la orden", "Te mostrará detalle de la OT nuevamente", R.drawable.preview_resume_ot));
            add(new PreviewView("Pago", "Procederás con el pago en caso de ser requerido", R.drawable.preview_payment));
            add(new PreviewView("Firma de cliente", "El cliente firmará para garantizar su aprobación", R.drawable.preview_signature));
            add(new PreviewView("OT finalizada", "Te mostrará que has finalizado correctamente la OT", R.drawable.preview_finish_ot));
            add(new PreviewView("Encuesta de satisfacción", "El cliente calificará nuestro trabajo", R.drawable.preview_survey));
            add(new PreviewView("Evidencia de trabajo", "Tomarás nuevamente evidencias del trabajo realizado", R.drawable.preview_evidence));
            add(new PreviewView("Actualización de inventario", "Colocarás los materiales utilizados para el trabajo de la OT", R.drawable.preview_materials));
            add(new PreviewView("Gracias!", "Gracias por tomar este curso de introducción", R.mipmap.ic_done_preview));
        }};

        PreviewAdapter previewAdapter = new PreviewAdapter(getSupportFragmentManager(), slides);
        mViewPager.setAdapter(previewAdapter);
        mViewPager.addOnPageChangeListener(onPageChangeListener);
        mViewPager.setOnClickListener(onClickListener);
    }

    @OnClick(R.id.act_preview_next)
    public void onNextPager() {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
    }

    @OnClick(R.id.act_preview_prev)
    public void onPrevPager() {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
    }

    @OnClick(R.id.act_preview_done)
    public void onDonePager() {
        mFFMDataManager.tx(tx -> {
            mUser = mFFMDataManager.queryWhere(User.class).findFirst();
            mUser.isCompletePreview = true;
            tx.save(mUser);
        });
        baseModule.finishModule(this, null);
        finish();
    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage("¿Está seguro de cerrar sesión y regresar a la pantalla de inicio de sesión?")
                    .setPositiveButton("Aceptar", (dialog, which) -> {
                        mFFMDataManager.deleteAll();
                        baseModule.startFlow(PreviewActivity.this, FlowKey.MAIN_FLOW, null);
                        PreviewActivity.this.finish();
                    })
                    .setNegativeButton("Cancelar", null);
            builder.create().show();
        } else {
            super.onBackPressed();
        }
    }
}
