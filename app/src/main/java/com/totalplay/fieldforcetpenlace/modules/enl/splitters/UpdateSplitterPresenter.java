package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.BaseRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.FindSplitterAppRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateSplitterAppRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.FindSplitterAppResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.UpdateSplitterAppResponse;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.BusinessFiles;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.FileUtils;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureModule;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureOutput;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WrapperPhotosEvidence;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by NS-TOTALPLAY on 17/04/2017.
 * Enlace
 */

public class UpdateSplitterPresenter extends FFMPresenter implements WSCallback {

    private static final int REQUEST_CODE_PICTURE = 0x0019;

    private Reason mReason = null;

    public List<Integer> mPhotosValidator = new ArrayList<>();

    private UpdateSplitterCallback mUpdateSplitterCallback;
    private WrapperPhotosEvidence mFileWrapper;
    private Splitter mSplitter;

    private boolean mStopWorkOrder = false;
    public boolean mSearchOtherSplitter = false;

    public interface UpdateSplitterCallback {
        void onSuccessLoadSplitter(Splitter splitter);

        void onSuccessLoadImage(int requestCode, File mRequestedFile);

        void onSuccessLoadAssignedPort(String assignedPort);

        void onSuccessSaveInfoSplitter(WorkOrder mWorkOrder, Splitter splitter, boolean searchOtherSplitter, Reason mReason, boolean mStopWorkOrder);
    }

    public UpdateSplitterPresenter(AppCompatActivity appCompatActivity, UpdateSplitterCallback updateSplitterCallback) {
        super(appCompatActivity);
        mUpdateSplitterCallback = updateSplitterCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String savedInstance;
        if (mWorkOrder != null) {
            mFileWrapper = mFFMDataManager.queryWhere(WrapperPhotosEvidence.class)
                    .filter("workOrder", mWorkOrder.id)
                    .filter("serverFolder", ServerFolders.SPLITTERS)
                    .findFirst();
            savedInstance = mWorkOrder.id;
        } else {
            savedInstance = "default";
        }
        if (mFileWrapper == null) {
            mFileWrapper = new WrapperPhotosEvidence(savedInstance, ServerFolders.SPLITTERS);
        }
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }


    public void loadSplitter() {
        mSplitter = (Splitter) mAppCompatActivity.getIntent().getSerializableExtra(Keys.EXTRA_SPLITTER);
        mUpdateSplitterCallback.onSuccessLoadSplitter(mSplitter);
    }

    public void findSplitter(String totalPorts, String occupiedPorts, String padLock, String assignedPort) {
        if (isValidForm(totalPorts, occupiedPorts)) {
            if (Integer.parseInt(totalPorts) > Integer.parseInt(occupiedPorts)) {
                String accountNumber = mWorkOrder.account;
                if (!mSearchOtherSplitter) {
                    if (padLock.isEmpty()) {
                        padLock = "Sin Candado";
                    }
                }
                Location location = TotalPlayLocationService.getLastLocation();
                if (location != null) {
                    FindSplitterAppRequest findSplitterAppRequest;
                    String reasonId = mReason == null ? "" : mReason.id;
                    findSplitterAppRequest = new FindSplitterAppRequest("", totalPorts, occupiedPorts,
                            assignedPort, location, padLock, accountNumber, mUser.operatorId, reasonId);

                    if (mReason != null && !mSearchOtherSplitter) {
                        alertStopWorkOrder(findSplitterAppRequest, true);
                    } else {
                        sendAddSplitterInfo(findSplitterAppRequest);
                    }
                } else {
                    MessageUtils.toast(mContext, "No se pudo obtener su ubicación");
                }
            } else {
                MessageUtils.toast(mContext, R.string.act_register_splitter_max_total_ports);
            }
        }
    }

    public void updateSplitter(String totalPorts, String occupiedPorts, String padLock, String assignedPort) {
        if (isValidForm(totalPorts, occupiedPorts)) {
            String accountNumber = mWorkOrder.account;
            if (!mSearchOtherSplitter) {
                if (padLock.isEmpty()) {
                    padLock = "Sin Candado";
                }
                if (assignedPort.isEmpty()) {
                    assignedPort = "100";
                }
            } else {
                assignedPort = "100";
                padLock = " ";
                accountNumber = "0";
            }

            Location location = TotalPlayLocationService.getLastLocation();
            if (location != null) {
                UpdateSplitterAppRequest updateSplitterAppRequest = new UpdateSplitterAppRequest(mSplitter.id,
                        totalPorts, occupiedPorts, assignedPort, location, padLock, accountNumber, mUser.operatorId);

                if (mReason != null && !mSearchOtherSplitter) {
                    alertStopWorkOrder(updateSplitterAppRequest, false);
                } else {
                    sendUpdateSplitterInfo(updateSplitterAppRequest);
                }
            } else {
                MessageUtils.toast(mContext, "No se pudo obtener su ubicación");
            }
        }
    }

    private void alertStopWorkOrder(BaseRequest baseRequest, boolean add) {
        AlertDialog dialog = new AlertDialog.Builder(mContext)
                .setTitle(mReason.name)
                .setMessage("¿Desea detener la orden de trabajo?")
                .setPositiveButton("Detener orden", (dialog12, which)
                        -> {
                    mStopWorkOrder = true;
                    mSearchOtherSplitter = false;
                    if (add)
                        sendAddSplitterInfo((FindSplitterAppRequest) baseRequest);
                    else
                        sendUpdateSplitterInfo((UpdateSplitterAppRequest) baseRequest);
                })
                .setNegativeButton("Buscar otro splitter ", (dialog1, which)
                        -> {
                    mStopWorkOrder = false;
                    mSearchOtherSplitter = true;
                    if (add)
                        sendAddSplitterInfo((FindSplitterAppRequest) baseRequest);
                    else
                        sendUpdateSplitterInfo((UpdateSplitterAppRequest) baseRequest);
                })
                .setIcon(R.mipmap.ic_launcher)
                .create();
        dialog.show();
    }

    private void sendAddSplitterInfo(FindSplitterAppRequest findSplitterAppRequest) {
        mWSManager.requestWs(FindSplitterAppResponse.class, WSManager.WS.ADD_SPLITERS, getDefinition().findSplitter(findSplitterAppRequest));
    }

    private void sendUpdateSplitterInfo(UpdateSplitterAppRequest updateSplitterAppRequest) {
        mWSManager.requestWs(UpdateSplitterAppResponse.class, WSManager.WS.UPDATE_SPLITTER, getDefinition().updateSplitterApp(updateSplitterAppRequest));
    }

    private void successUpdateSplitter(String requestUrl, UpdateSplitterAppResponse updateSplitterAppResponse) {
        if (updateSplitterAppResponse.result != null && updateSplitterAppResponse.result.equals("0")) {
            successSaveSplitter();
        } else {
            onErrorLoadResponse(requestUrl, updateSplitterAppResponse.resultDescription);
        }
    }

    private void successSaveSplitter() {
        MessageUtils.toast(mContext, R.string.dialog_success_sending_data);
        mUpdateSplitterCallback.onSuccessSaveInfoSplitter(mWorkOrder, mSplitter, mSearchOtherSplitter, mReason, mStopWorkOrder);
    }

    private void successFindSplitter(String requestUrl, FindSplitterAppResponse findSplitterAppResponse) {
        if (findSplitterAppResponse.result.equals("0")) {
            successSaveSplitter();
        } else {
            onErrorLoadResponse(requestUrl, findSplitterAppResponse.resultDescription);
        }
    }

    private boolean isValidForm(String totalPorts, String occupiedPorts) {
        if (mPhotosValidator.size() < 3) {
            MessageUtils.toast(mContext, "Es necesario tomar las 3 fotografias");
            return false;
        }
        int totalPortValue;
        int occupiedPortsValue;
        try {
            totalPortValue = Integer.parseInt(totalPorts);
            occupiedPortsValue = Integer.parseInt(occupiedPorts);
        } catch (Exception ignored) {
            MessageUtils.toast(mContext, "Ingrese valores correctos en los puertos");
            return false;
        }
        if (totalPortValue < occupiedPortsValue) {
            MessageUtils.toast(mContext, R.string.act_register_splitter_max_total_ports);
            return false;
        }
        return true;
    }

    public void saveImageFile(String name, String path) {
        mFFMDataManager.tx(tx -> {
            PhotoEvidence photo = new PhotoEvidence();
            photo.name = name;
            photo.path = path;
            photo.workOrder = mWorkOrder.id;
            photo.serverFolder = ServerFolders.SPLITTERS;
            photo.baseUrl = mWorkOrder.baseUrl;
            photo.interventionType = mWorkOrder.interventionType;

            if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
                photo.type = ImageTypeEnum.InternalPlant.SPLITTER;
            } else if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.EXTERNAL_PLANT)) {
                photo.type = ImageTypeEnum.ExternalPlant.SPLITTER;
            } else if (WorkOrderOwnerType.INTEGRATOR.contains(mWorkOrder.ownerId)) {
                photo.type = ImageTypeEnum.Integrator.SPLITTER;
            }

            photo = tx.save(photo);
            mFileWrapper.listPhotos.add(photo);
            tx.save(mFileWrapper);
        });
    }

    public void setReason(Reason reason) {
        this.mReason = reason;
    }

    public void onSelectPortClick() {
        SplitterSelectedActivity.launch(mAppCompatActivity);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_PICTURE) {
                TakePictureOutput takePictureOutput = (TakePictureOutput) data.getSerializableExtra(TakePictureModule.TAKE_PICTURE_OUTPUT_KEY);
                mUpdateSplitterCallback.onSuccessLoadImage(requestCode, takePictureOutput.file);
            } else if (requestCode == SplitterSelectedActivity.REQUEST_CODE_PORT) {
                String numberPort = data.getStringExtra("portNumber");
                mUpdateSplitterCallback.onSuccessLoadAssignedPort(numberPort);
            }
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_sending_register_splitter);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.UPDATE_SPLITTER)) {
            successUpdateSplitter(requestUrl, (UpdateSplitterAppResponse) baseResponse);
        } else if (requestUrl.equals(WSManager.WS.ADD_SPLITERS)) {
            successFindSplitter(requestUrl, (FindSplitterAppResponse) baseResponse);
        }
    }

}
