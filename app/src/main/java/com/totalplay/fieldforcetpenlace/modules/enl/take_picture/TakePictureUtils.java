package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

public class TakePictureUtils {


    private static BottomSheetDialog mBottomSheetDialog;


    public static void startImportPicture(BaseModule baseModule, AppCompatActivity activity, String mRequestedFile, int requestCode) {
        try {

            mBottomSheetDialog = new BottomSheetDialog(activity);
            View sheetView = activity.getLayoutInflater().inflate(R.layout.enl_dialog_import_photo, null);
            sheetView.findViewById(R.id.bottom_sheet_camera).setOnClickListener(view -> {
                CapturePhotoActivity.launch(baseModule, activity, mRequestedFile, requestCode);
                mBottomSheetDialog.dismiss();
            });

            sheetView.findViewById(R.id.bottom_sheet_gallery).setOnClickListener(view -> {
                ImportGalleryActivity.launch(baseModule, activity, mRequestedFile, requestCode);
                mBottomSheetDialog.dismiss();
            });

            mBottomSheetDialog.setContentView(sheetView);
            if (!(activity).isFinishing()) {
                mBottomSheetDialog.show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
