package com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

/**
 * Created by jorgehdezvilla on 16/01/18.
 * FFM Enlace
 */

public class ConfigureDispositivesModule extends BaseModule<WorkOrder, WorkOrder> {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        ConfigureDispositivesActivity.launch(this, appCompatActivity, workOrder);
    }
}
