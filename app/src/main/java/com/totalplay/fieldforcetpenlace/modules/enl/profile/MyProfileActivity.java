package com.totalplay.fieldforcetpenlace.modules.enl.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.fieldforcetpenlace.background.queue.jobs.SynchronizeImagesJob;
import com.totalplay.fieldforcetpenlace.library.utils.CryptoUtils;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.definition.ModuleKey;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureInput;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureModule;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureOutput;
import com.totalplay.fieldforcetpenlace.newpresenter.ConsultingURLImgPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.mvp.BasePresenter;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by totalplay on 11/22/16.
 * FFM
 */

public class MyProfileActivity extends BaseFFMActivity implements ConsultingURLImgPresenter.ConsultingURLImgCallback {

    private static final int REQUEST_CODE_PROFILE = 0x0021;

    @BindView(R.id.act_my_profile_photo_image)
    CircleImageView mProfilePhotoImageView;
    @BindView(R.id.employe_name)
    TextView mEmployeName;
    @BindView(R.id.company)
    TextView mCompany;
    @BindView(R.id.employe_number)
    TextView mEmployeNumber;
    @BindView(R.id.date)
    TextView mDate;
    @BindView(R.id.employe_type)
    TextView mEmployeType;
    @BindView(R.id.country)
    TextView mCountry;
    @BindView(R.id.supervisor)
    TextView mSupervisor;
    @BindView(R.id.imgProfile)
    ImageView iconImage;

    @BindView(R.id.imgprofile_progress)
    ProgressBar mProgressBar;

    private MyProfilePresenter mMyProfilePresenter;
    private ConsultingURLImgPresenter mConsultingURLImgPresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, MyProfileActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_my_profile);
        setTitle("");
        ButterKnife.bind(this);
        mEmployeName.setText(mUser.completeName);
        mCompany.setText(mUser.company);
        mEmployeNumber.setText(mUser.employeNumber);
        mDate.setText(mUser.entryDate);
        mEmployeType.setText(mUser.operatorType);
        mCountry.setText(mUser.country);
        mSupervisor.setText(mUser.supervisor);

        if (mUser.profilePhoto != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            mConsultingURLImgPresenter.fullImage(CryptoUtils.desEncrypt(mUser.profilePhoto), "profile.jpg");
        }
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mMyProfilePresenter = new MyProfilePresenter(this),
                mConsultingURLImgPresenter = new ConsultingURLImgPresenter(this, this)
        };
    }

    @OnClick({R.id.act_my_profile_photo_image, R.id.act_img_profile,})
    public void onImageClick(View view) {
        baseModule.openModule(this, ModuleKey.TAKE_PICTURE, new TakePictureInput(mUser.employeeId, false));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            TakePictureOutput takePictureOutput = (TakePictureOutput) data.getSerializableExtra(TakePictureModule.TAKE_PICTURE_OUTPUT_KEY);
            if (mProfilePhotoImageView != null && takePictureOutput.file != null) {
                mProfilePhotoImageView.setPadding(0, 0, 0, 0);
                Glide.with(this)
                        .load(takePictureOutput.file)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(mProfilePhotoImageView);
                mMyProfilePresenter.savePhoto(takePictureOutput.file.getName(), takePictureOutput.file.getAbsolutePath());
                QueueManager.add(new SynchronizeImagesJob(mUser, mFFMDataManager.queryWhere(PhotoEvidence.class).filter("synced", false).list()));
                iconImage.setVisibility(View.GONE);
            } else {
                iconImage.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onSuccessLoadFile(File file) {
        mProgressBar.setVisibility(View.GONE);
        Glide.with(this)
                .load(file)
                .skipMemoryCache(true)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(mProfilePhotoImageView);
        iconImage.setVisibility(View.GONE);
    }

    @Override
    public void onErrorLoadImage() {
        mProgressBar.setVisibility(View.GONE);
    }

}
