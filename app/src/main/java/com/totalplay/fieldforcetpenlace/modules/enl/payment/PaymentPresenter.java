package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.PaymentEngineRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.PaymentEngineResponse;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PaymentCard;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

import java.util.Locale;

/**
 * Created by totalplay on 11/28/16.
 * Total
 */
public class PaymentPresenter extends FFMPresenter implements WSCallback {

    private Callback mCallback;

    public PaymentPresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    public interface Callback {
        void onSuccessPayment(WorkOrder workOrder);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void payment(PaymentCard paymentCard, String amount) {
        PaymentEngineRequest paymentEngineRequest = new PaymentEngineRequest(
                paymentCard.numbber,
                paymentCard.cvv,
                String.format(Locale.getDefault(), "%02d", Integer.parseInt(paymentCard.expireMonth)),
                paymentCard.expireYear,
                paymentCard.expireYear,
                amount,
                mWorkOrder.account);
        mWSManager.requestWs(PaymentEngineResponse.class, WSManager.WS.PAYMENT, getDefinition().payment(paymentEngineRequest));
    }

    private void successPayment(String requestUrl, PaymentEngineResponse paymentEngineResponse) {
        if (paymentEngineResponse.bankResponse.response.code.equals("0")) {
            MessageUtils.toast(mContext, "Orden correctamente pagada");

            if (paymentEngineResponse.bankResponse != null) {
                final PaymentEngineResponse.BankResponse.BankingAuthorizerResponse bankingAuthorizerResponse =
                        paymentEngineResponse.bankResponse.bankingAuthorizerResponse;
                BankResponseDialogFragment fragment = BankResponseDialogFragment.newInstance(bankingAuthorizerResponse.authorizationCode,
                        bankingAuthorizerResponse.bankResponseCode, bankingAuthorizerResponse.descriptionResponse,
                        bankingAuthorizerResponse.standardResponseCode);
                fragment.setListener(() -> {
                    if (bankingAuthorizerResponse.bankResponseCode != null &&
                            bankingAuthorizerResponse.bankResponseCode.equals("00")) {
                        mCallback.onSuccessPayment(mWorkOrder);
                    }
                });
                fragment.show(mAppCompatActivity.getSupportFragmentManager(), "bank_response_dialog");
            } else {
                MessageUtils.toast(mContext, paymentEngineResponse.brmResult.response.description);
            }
        } else {
            onErrorLoadResponse(requestUrl, paymentEngineResponse.bankResponse.response.description);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_do_payment);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.PAYMENT)) {
            successPayment(requestUrl, (PaymentEngineResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        MessageUtils.toast(mContext, R.string.dialog_error_payment);
    }
}
