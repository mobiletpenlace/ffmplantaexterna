package com.totalplay.fieldforcetpenlace.modules.definition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jorgehdezvilla on 04/01/18.
 * Module
 */

public class FlowDefinition {

    private static HashMap<String, List<String>> FLOWS_APP = null;

    public static HashMap<String, List<String>> GET_FLOWS_APP() {
        if (FLOWS_APP == null)
            FlowDefinition.FLOWS_APP = new HashMap<String, List<String>>() {{
                put(FlowKey.MAIN_FLOW, MAIN_FLOW);
                put(FlowKey.INTERNAL_PLANT_FLOW, INTERNAL_PLANT_FLOW);
                put(FlowKey.INTEGRATOR_FLOW, INTEGRATOR_FLOW);
            }};
        return FLOWS_APP;
    }

    private static final List<String> MAIN_FLOW = new ArrayList<String>() {{
        add(ModuleKey.LOGIN);
        add(ModuleKey.SELFIE_PHOTO);
        add(ModuleKey.INTRODUCCTION_APP);
        add(ModuleKey.ASSISTANTS);
        add(ModuleKey.LOAD_WORK_ORDER);
    }};

    private static final List<String> INTERNAL_PLANT_FLOW = new ArrayList<String>() {{
        add(ModuleKey.WORK_ORDER_MAP_INFO);
        add(ModuleKey.WORK_ORDER_TRAVEL);
        add(ModuleKey.WORK_ORDER_ARRIVE_EARLY);
        add(ModuleKey.WORK_ORDER_ARRIVE_EVIDENCE);
        add(ModuleKey.WORK_ORDER_INFO);
        add(ModuleKey.WORK_ORDER_DEAR_TIME);
        add(ModuleKey.WORK_ORDER_SPLITTERS);
        add(ModuleKey.WORK_ORDER_ACTIVATION);
        add(ModuleKey.WORK_ORDER_CONFIGURE_DISPOSITIVES);
        add(ModuleKey.WORK_ORDER_RESUME);
        add(ModuleKey.WORK_ORDER_PAYMENT);
        add(ModuleKey.WORK_ORDER_SURVEY);
        add(ModuleKey.WORK_ORDER_FINISH_INSTALLATION);
        add(ModuleKey.WORK_ORDER_ACCEPTANCE_ACT);
        add(ModuleKey.WORK_ORDER_EVIDENCES);
        add(ModuleKey.WORK_ORDER_MATERIALS);
        add(ModuleKey.WORK_ORDER_COMPLETED);
    }};

    private static final List<String> INTEGRATOR_FLOW = new ArrayList<String>() {{
        add(ModuleKey.WORK_ORDER_MAP_INFO);
        add(ModuleKey.WORK_ORDER_TRAVEL);
        add(ModuleKey.WORK_ORDER_ARRIVE_EVIDENCE);
        add(ModuleKey.WORK_ORDER_INFO);
        add(ModuleKey.WORK_ORDER_DEAR_TIME);
        add(ModuleKey.WORK_ORDER_CONFIGURE_DISPOSITIVES);
        add(ModuleKey.WORK_ORDER_REPLACE_DISPOSITIVES);
        add(ModuleKey.WORK_ORDER_RESUME);
        // FIRMA
        add(ModuleKey.WORK_ORDER_ACCEPTANCE_ACT);
        add(ModuleKey.WORK_ORDER_EVIDENCES);
        add(ModuleKey.WORK_ORDER_COMPLETED);
    }};



}
