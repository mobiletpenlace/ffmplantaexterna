package com.totalplay.fieldforcetpenlace.modules.enl.load_work_order;


import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.custom.MarkerWorkOrderCustom;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.mvp.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressWarnings("unchecked")
public class WorkOrdersFragment extends BaseFFMFragment implements OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener,
        WorkOrdersPresenter.WorkOrdersCallback {

    @BindView(R.id.fr_my_ots_empty)
    TextView mEmptyTextView;

    public GoogleMap mMap;
    public Marker mMarker;

    private WorkOrdersPresenter mWorkOrdersPresenter;
    private List<WorkOrder> mWorkOrderList = new ArrayList<>();

    public static WorkOrdersFragment newInstance() {
        return new WorkOrdersFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_work_orders, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap != null) {
            mMap.clear();
            onSuccessUpdateLocation(TotalPlayLocationService.getLastLocation());
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEmptyTextView.setVisibility(View.GONE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.fr_work_orders_map);
        mapFragment.getMapAsync(this);

        mWorkOrdersPresenter.loadWorkOrders();
    }

    @Override
    public BasePresenter getPresenter() {
        return mWorkOrdersPresenter = new WorkOrdersPresenter((AppCompatActivity) getActivity(), this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        onSuccessUpdateLocation(TotalPlayLocationService.getLastLocation());
        try {
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.style_json));
        } catch (Resources.NotFoundException ignored) {
        }
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new MarkerWorkOrderCustom(getActivity().getLayoutInflater(), getActivity()));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        mWorkOrdersPresenter.markerClick();
    }

    @Override
    public void onSuccessUpdateLocation(Location location) {
        mMap.clear();
        mMap.setOnMapLoadedCallback(() -> {
            if (location != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17.489744f));
                mMarker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(location.getLatitude(), location.getLongitude()))
                        .title(this.getString(R.string.item_work_order_title_searching))
                        .snippet(String.valueOf(mWorkOrderList.size()))
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_primary_marker)));
                mMarker.showInfoWindow();
            }
        });
    }

    @Override
    public void onSuccessLoadWorkOrderList(List<WorkOrder> workOrderList) {
        mWorkOrderList = workOrderList;
        try {
            mMarker.setSnippet(String.valueOf(mWorkOrderList.size()));
            mMarker.setTitle(getString(R.string.item_work_order_title_start));
            mMarker.showInfoWindow();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessSaveWorkOrder(WorkOrder workOrder) {
        ((BaseFFMActivity) getActivity()).baseModule.finishModule((AppCompatActivity) getActivity(), workOrder);
        getActivity().finish();
    }

    @OnClick(R.id.act_img_refresh)
    public void onRefresh() {
        onSuccessUpdateLocation(TotalPlayLocationService.getLastLocation());
    }
}
