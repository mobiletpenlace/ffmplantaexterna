package com.totalplay.fieldforcetpenlace.modules.enl.auxiliars;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Assistant;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.modules.definition.FlowKey;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleRecyclerView;
import com.totalplay.view.BaseSimpleRecyclerView.RefreshBaseRecyclerCallback;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by totalplay on 11/18/16.
 * FFM
 */

@SuppressWarnings("unchecked")
public class AssistantsActivity extends BaseFFMActivity implements AssistantsPresenter.AssistantsCallback,
        RefreshBaseRecyclerCallback {

    public AssistantAdapter mAssistantAdapter;
    private AssistantsPresenter mAssistantsPresenter;
    private BaseSimpleRecyclerView mBaseSimpleRecyclerView;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, AssistantsParameter assistantsParameter) {
        Intent intent = new Intent(appCompatActivity, AssistantsActivity.class);
        if (assistantsParameter != null)
            intent.putExtra(Keys.ASSISTANT_IS_CHANGING, assistantsParameter.isChanging);
        else
            intent.putExtra(Keys.ASSISTANT_IS_CHANGING, false);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_installation_assistant);
        setTitle(R.string.title_activity_installation_assistant);
        ButterKnife.bind(this);
        mBaseSimpleRecyclerView = new BaseSimpleRecyclerView(this, R.id.act_installation_assistant_list, R.id.act_installation_assistant_list_refresh)
                .setRefreshBaseRecycler(this)
                .setAdapter(mAssistantAdapter = new AssistantAdapter(this))
                .setEmptyView(R.id.view_layout_empty_recycler)
                .addBottomOffsetDecoration(200);

        mAssistantsPresenter.checkAssistantsWorkTeam();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mAssistantsPresenter = new AssistantsPresenter(this, this);
    }

    @OnClick(R.id.act_installation_accept)
    public void onClick() {
        mAssistantsPresenter.continueClick(mAssistantAdapter.mSelectAssistant);
    }

    @Override
    public void onSuccessLoadAssistants(List<Assistant> assistantList) {
        if (assistantList.size() > 0) {
            mBaseSimpleRecyclerView.disableSwipeRefresh();
            mBaseSimpleRecyclerView.update(assistantList);
            mBaseSimpleRecyclerView.mSwipeRefreshLayout.setEnabled(false);
        }
        mBaseSimpleRecyclerView.stopRefresh();
    }

    @Override
    public void onSuccessSaveAssistants() {
        baseModule.finishModule(this, null);
        finish();
    }

    @Override
    public void onRefreshItems() {
        mAssistantsPresenter.loadAssistants();
    }

    @Override
    public void onBackPressed() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage("¿Está seguro de cerrar sesión y regresar a la pantalla de inicio de sesión?")
                    .setPositiveButton("Aceptar", (dialog, which) -> {
                        mFFMDataManager.tx(tx -> tx.delete(User.class));
                        baseModule.startFlow(AssistantsActivity.this, FlowKey.MAIN_FLOW, null);
                        AssistantsActivity.this.finish();
                    })
                    .setNegativeButton("Cancelar", null);
            builder.create().show();
        } else {
            super.onBackPressed();
        }
    }
}
