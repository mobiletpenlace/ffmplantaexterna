package com.totalplay.fieldforcetpenlace.modules.enl.login;

import android.support.v7.app.AppCompatActivity;

import com.library.pojos.DataManager;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;
import com.totalplay.utils.Prefs;

import org.joda.time.DateTime;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public class LoginModule extends BaseModule<ModuleParameter, User> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, ModuleParameter moduleParameter) {
        DataManager ffmDataManager = DataManager.validateConnection(null);
        User user = ffmDataManager.queryWhere(User.class).findFirst();
        if (user == null) {
            LoginActivity.launch(this, appCompatActivity);
        } else {
            long time = Prefs.instance().number(Keys.PREF_LAST_TIME_LOGGED);
            DateTime dateTime = new DateTime(time);
            if (dateTime.plusHours(3).isBeforeNow()) {
                ffmDataManager.tx(tx -> tx.delete(User.class));
                LoginActivity.launch(this, appCompatActivity);
            } else {
                finishModule(appCompatActivity, user);
            }
        }
        ffmDataManager.close();
    }

    @Override
    public void finishModule(AppCompatActivity appCompatActivity, User response) {
        super.finishModule(appCompatActivity, response);
    }
}
