package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 11/01/18.
 * FFM ENLACE
 */

public class SplittersOutput implements Serializable, ModuleParameter {

    public WorkOrder workOrder;
    public Splitter splitter;
    public Reason reason;
    public boolean searchOther;
    public boolean stopWorkOrder;

    public SplittersOutput(WorkOrder workOrder, Splitter splitter, Reason reason, boolean searchOther, boolean stopWorkOrder) {
        this.workOrder = workOrder;
        this.splitter = splitter;
        this.reason = reason;
        this.searchOther = searchOther;
        this.stopWorkOrder = stopWorkOrder;
    }
}
