package com.totalplay.fieldforcetpenlace.modules.enl.travel;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.library.utils.NavigationUtils;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.info_map.StaticMapFragment;
import com.totalplay.fieldforcetpenlace.view.activities.OptionsMenuActivity;
import com.totalplay.mvp.BasePresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressWarnings("unchecked")
public class WorkOrderInfoInRouteActivity extends OptionsMenuActivity
        implements WorkOrderInfoInRoutePresenter.WorkOrderInfoInRouteCallback {

    @BindView(R.id.bottom_sheet_content)
    ViewGroup mAppRutesContent;
    @BindView(R.id.menu_travel_problems)
    Button mTravelProblemsButton;
    @BindView(R.id.menu_travel_continue)
    Button mTravelContinueButton;


    private StaticMapFragment mMapFragment;
    private StaticMapFragment.MapPoint mWorkOrderMapPoint;

    private WorkOrderInfoInRoutePresenter mWorkOrderInfoInRoutePresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isInSite) {
            Intent intent = new Intent(appCompatActivity, WorkOrderInfoInRouteActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.enl_activity_workorder_info_in_route);
        ButterKnife.bind(this);

        mReportImageView.setVisibility(View.INVISIBLE);
        mTitleReportTextView.setTextColor(ContextCompat.getColor(this, R.color.gray_splitter));
        mWorkOrderInfoInRoutePresenter.loadLocation();

        if (mTrainingManager.isTrainingMode()) {
            mTrainingManager.workOrderInfoInRouteActivity(()
                    -> NavigationUtils.startNavigation(this, mWorkOrder.latitude, mWorkOrder.longitude));
        } else {
            mAppRutesContent.setVisibility(View.GONE);
            NavigationUtils.startNavigation(this, mWorkOrder.latitude, mWorkOrder.longitude);
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        return mWorkOrderInfoInRoutePresenter = new WorkOrderInfoInRoutePresenter(this, this);
    }

    @Override
    public void onLoadWorkOrderAddress(Location userLocation, String workOrderLatitude, String workOrderLongitude) {
        mWorkOrderMapPoint = new StaticMapFragment.MapPoint(Double.valueOf(workOrderLatitude), Double.valueOf(workOrderLongitude));
        ArrayList<StaticMapFragment.MapPoint> points = new ArrayList<StaticMapFragment.MapPoint>() {{
            add(new StaticMapFragment.MapPoint(userLocation.getLatitude(), userLocation.getLongitude()));
            add(mWorkOrderMapPoint);
        }};
        mMapFragment = (StaticMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.act_installation_map_fragment);
        mMapFragment.setMapInitialData(new StaticMapFragment.MapInitialData(points));
    }

    @Override
    public void onLoadWorkOrder(WorkOrder workOrder) {
        StaticMapFragment.mWorkOrders = workOrder;
    }

    @Override
    public void onCompletedMetersRequited() {
        mTravelProblemsButton.setVisibility(View.GONE);
        mTravelContinueButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.menu_travel_problems)
    public void menuTravelProblems() {
        mWorkOrderInfoInRoutePresenter.travelProblemsClick();
    }

    @OnClick(R.id.menu_travel_continue)
    public void menuContinue() {
        WorkOrderInfoInSiteActivity.launch(baseModule, this);
        finish();
    }

    @Override
    public void onUpdateLocation(Location location) {
        if (location != null && mWorkOrderMapPoint != null) {
            mMapFragment.removeAllMarkers();
            ArrayList<StaticMapFragment.MapPoint> points = new ArrayList<StaticMapFragment.MapPoint>() {{
                add(new StaticMapFragment.MapPoint(location.getLatitude(), location.getLongitude()));
                add(mWorkOrderMapPoint);
            }};
            mMapFragment.addMarkers(points);
        }
    }

    @OnClick(R.id.act_img_refresh)
    public void onRefresh() {
        onUpdateLocation(TotalPlayLocationService.getLastLocation());
        onCompletedMetersRequited();
    }
}
