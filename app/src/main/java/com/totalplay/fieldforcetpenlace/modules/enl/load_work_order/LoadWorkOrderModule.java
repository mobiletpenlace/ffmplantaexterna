package com.totalplay.fieldforcetpenlace.modules.enl.load_work_order;

import android.support.v7.app.AppCompatActivity;

import com.library.pojos.DataManager;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleManager;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;
import com.totalplay.fieldforcetpenlace.modules.definition.FlowKey;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public class LoadWorkOrderModule extends BaseModule<ModuleParameter, WorkOrder> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, ModuleParameter moduleParameter) {
        DataManager ffmDataManager = DataManager.validateConnection(null);
        WorkOrder workOrder = ffmDataManager.queryWhere(WorkOrder.class).findFirst();
        if (workOrder != null && workOrder.operarioId.equals("TOTALDEMO")) {
            ffmDataManager.deleteWorkOrder();
            workOrder = null;
        }
        if (workOrder != null) {
            if (workOrder.isWait) {
//                OnSuccessCancelWorkOrderEvent(appCompatActivity);
            } else {
                finishModule(appCompatActivity, workOrder);
            }
        } else {
            LoadWorkOrdersActivity.launch(this, appCompatActivity);
        }
        ffmDataManager.close();
    }

    @Override
    public void finishModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
            ModuleManager.getInstance().startFlow(appCompatActivity, FlowKey.INTERNAL_PLANT_FLOW, null);
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
            ModuleManager.getInstance().startFlow(appCompatActivity, FlowKey.INTEGRATOR_FLOW, null);
        }
    }
}
