package com.totalplay.fieldforcetpenlace.modules.enl.dear_time;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

/**
 * Created by jorgehdezvilla on 15/01/18.
 * FFM Enlace
 */

public class DearTimeModule extends BaseModule<WorkOrder, WorkOrder> {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        UpdateDearTimeActivity.launch(this, appCompatActivity, workOrder);
    }
}
