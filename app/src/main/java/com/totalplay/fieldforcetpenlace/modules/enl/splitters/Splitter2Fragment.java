package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.mvp.BaseFragment;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by icortes on 07/07/17.
 * FFM Enlace
 */

public class Splitter2Fragment extends BaseFragment {

    @BindView(R.id.fr2_port1)
    LinearLayout fr2Port1;
    @BindView(R.id.fr2_port2)
    LinearLayout fr2Port2;
    @BindView(R.id.fr2_port3)
    LinearLayout fr2Port3;
    @BindView(R.id.fr2_port4)
    LinearLayout fr2Port4;
    @BindView(R.id.fr2_port5)
    LinearLayout fr2Port5;
    @BindView(R.id.fr2_port6)
    LinearLayout fr2Port6;
    @BindView(R.id.fr2_port7)
    LinearLayout fr2Port7;
    @BindView(R.id.fr2_port8)
    LinearLayout fr2Port8;

    Boolean[] array = new Boolean[8];
    LinearLayout[] imagenes;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_splitter_2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        Arrays.fill(array, Boolean.FALSE);
        imagenes = new LinearLayout[]{fr2Port1, fr2Port2, fr2Port3, fr2Port4, fr2Port5, fr2Port6, fr2Port7, fr2Port8};
    }

    @OnClick({R.id.fr2_port1, R.id.fr2_port2 , R.id.fr2_port3 , R.id.fr2_port4, R.id.fr2_port5, R.id.fr2_port6, R.id.fr2_port7, R.id.fr2_port8})
    public void onPortSelected(View view){
        getActivity();
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(100); Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION); Ringtone r = RingtoneManager.getRingtone(getActivity(), notification); r.play();
        switch (view.getId()){
            case R.id.fr2_port1:
                if (array[0]){Arrays.fill(array, Boolean.FALSE);} else{Arrays.fill(array, Boolean.FALSE); array[0] = true;}
                getPort("1");
                break;
            case R.id.fr2_port2:
                if (array[1]){Arrays.fill(array, Boolean.FALSE);} else{Arrays.fill(array, Boolean.FALSE); array[1] = true;}
                getPort("2");
                break;
            case R.id.fr2_port3:
                if (array[2]){Arrays.fill(array, Boolean.FALSE);} else{Arrays.fill(array, Boolean.FALSE); array[2] = true;}
                getPort("3");
                break;
            case R.id.fr2_port4:
                if (array[3]){Arrays.fill(array, Boolean.FALSE);} else{Arrays.fill(array, Boolean.FALSE); array[3] = true;}
                getPort("4");
                break;
            case R.id.fr2_port5:
                if (array[4]){Arrays.fill(array, Boolean.FALSE);} else{Arrays.fill(array, Boolean.FALSE); array[4] = true;}
                getPort("5");
                break;
            case R.id.fr2_port6:
                if (array[5]){Arrays.fill(array, Boolean.FALSE);} else{Arrays.fill(array, Boolean.FALSE); array[5] = true;}
                getPort("6");
                break;
            case R.id.fr2_port7:
                if (array[6]){Arrays.fill(array, Boolean.FALSE);} else{Arrays.fill(array, Boolean.FALSE); array[6] = true;}
                getPort("7");
                break;
            case R.id.fr2_port8:
                if (array[7]){Arrays.fill(array, Boolean.FALSE);} else{Arrays.fill(array, Boolean.FALSE); array[7] = true;}
                getPort("8");
                break;
        }
    }

    private void getPort (String port){
        SplitterSelectedActivity.numberPort = port;
        for (int i = 0; i < array.length; i++) {
            if (array[i]) {
                imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_asignado_izq));
            } else {
                imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_disponible_izq));
            }
        }
    }

}
