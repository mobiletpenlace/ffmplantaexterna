package com.totalplay.fieldforcetpenlace.modules.enl.selfie_photo;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public class SelfiePhotoModule extends BaseModule<User, User> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, User user) {
        UserProfileActivity.launch(this, appCompatActivity, user);
    }
}
