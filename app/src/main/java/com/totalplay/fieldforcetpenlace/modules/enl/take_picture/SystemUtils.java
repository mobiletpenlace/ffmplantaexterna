package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import android.os.Environment;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

class SystemUtils {
    public static boolean isStorageAvailable() {
        return isExternalStorageWritable() && isExternalStorageReadable();
    }

    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    private static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }


}

