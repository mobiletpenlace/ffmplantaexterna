package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;

/**
 * Created by NS-262 on 14/12/2016.
 * Enlace
 */

public class BankResponseDialogFragment extends DialogFragment {
    public static BankResponseDialogFragment newInstance(String authorizationCode, String bankResponseCode, String descriptionResponse, String standardResponseCode) {
        BankResponseDialogFragment fragment = new BankResponseDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Keys.EXTRA_BANK_RESPONSE_AUTHORIZATION_CODE, authorizationCode);
        bundle.putString(Keys.EXTRA_BANK_RESPONSE_BANK_RESPONSE_CODE, bankResponseCode);
        bundle.putString(Keys.EXTRA_BANK_RESPONSE_DESCRIPTION_RESPONSE, descriptionResponse);
        bundle.putString(Keys.EXTRA_BANK_RESPONSE_STANDARD_RESPONSE_CODE, standardResponseCode);

        fragment.setArguments(bundle);
        return fragment;
    }

    private BankResponseDialogListener listener;

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.enl_dialog_fragment_bank_response, null);
        TextView authorizationCodeTextView = view.findViewById(R.id.fr_bank_response_authorization_code);
        TextView bankResponseCodeTextView = view.findViewById(R.id.fr_bank_response_bank_response_code);
        TextView descriptionResponseTextView = view.findViewById(R.id.fr_bank_response_description_response);
        TextView standardResponseCodeTextView = view.findViewById(R.id.fr_bank_response_standard_response_code);
        view.findViewById(R.id.fr_bank_response_continue_button).setOnClickListener(view1 -> BankResponseDialogFragment.this.dismiss());

        String authorizationCode = getArguments().getString(Keys.EXTRA_BANK_RESPONSE_AUTHORIZATION_CODE);
        String bankResponseCode = getArguments().getString(Keys.EXTRA_BANK_RESPONSE_BANK_RESPONSE_CODE);
        String descriptionResponse = getArguments().getString(Keys.EXTRA_BANK_RESPONSE_DESCRIPTION_RESPONSE);
        String standardResponseCode = getArguments().getString(Keys.EXTRA_BANK_RESPONSE_STANDARD_RESPONSE_CODE);

        authorizationCodeTextView.setText(authorizationCode);
        bankResponseCodeTextView.setText(bankResponseCode);
        descriptionResponseTextView.setText(descriptionResponse);
        standardResponseCodeTextView.setText(standardResponseCode);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setView(view)
                .setCancelable(false);
        return builder.create();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (listener != null){
            listener.onBankResponseDismiss();
        }
    }

    public void setListener(BankResponseDialogListener listener) {
        this.listener = listener;
    }

    public interface BankResponseDialogListener {
        void onBankResponseDismiss();
    }
}