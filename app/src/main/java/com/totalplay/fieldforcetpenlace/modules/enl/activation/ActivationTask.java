package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.content.Context;
import android.os.AsyncTask;

import com.library.pojos.DataManager;
import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateDNsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.UpdateEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.UpdateResponse;
import com.totalplay.fieldforcetpenlace.library.enums.UpdateActivationErrorReason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Olt;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;

import java.util.ArrayList;
import java.util.List;


public class ActivationTask extends AsyncTask<Void, String, Boolean> {

    public interface ActivationTaskCallback {
        void onSuccessUpdate();

        void onErrorUpdate(UpdateActivationErrorReason errorReason, String errorMessage);
    }

    private final ActivationTaskCallback activationServiceUpdateCallback;
    private final UpdateEquipmentRequest updateEquipmentRequestInternet;
    private final UpdateEquipmentRequest updateEquipmentRequest;
    private final ArrayList<UpdateEquipmentRequest> updateEquipmentRequests;
    private List<UpdateDNsRequest> mUpdateDNRequests;
    private UpdateActivationErrorReason errorReason;
    private String errorMessage;

    private BaseWSManager retrofitManager;

    public ActivationTask(Context context, ActivationTaskCallback activationServiceUpdateCallback,
                          UpdateEquipmentRequest updateEquipmentRequestInternet,
                          List<UpdateDNsRequest> updateDNRequests,
                          ArrayList<UpdateEquipmentRequest> updateEquipmentRequests) {
        this.activationServiceUpdateCallback = activationServiceUpdateCallback;
        this.updateEquipmentRequestInternet = updateEquipmentRequestInternet;
        this.mUpdateDNRequests = updateDNRequests;
        this.updateEquipmentRequests = updateEquipmentRequests;
        this.updateEquipmentRequest = null;

        retrofitManager = WSManager.init().settings(context);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        UpdateResponse internetEquipmentResponse;
        DataManager ffmDataManager = DataManager.validateConnection(null);
        WorkOrder workOrder = ffmDataManager.queryWhere(WorkOrder.class).findFirst();
        if (workOrder == null) return false;
        try {

            internetEquipmentResponse = (UpdateResponse) retrofitManager.requestWsSync(UpdateResponse.class, WSManager.WS.UPDATE_EQUIPMENT, WebServices.servicesEnlace().updateEquipment(updateEquipmentRequestInternet));
            if (internetEquipmentResponse == null) return false;
            if (!internetEquipmentResponse.result.result.equals("0")) {
                errorReason = UpdateActivationErrorReason.COULD_NOT_UPDATE_EQUIPMENT;
                errorMessage = internetEquipmentResponse.result.resultDescription;
                ffmDataManager.close();
                return null;
            }
            Olt olt = new Olt();
            olt.name = updateEquipmentRequestInternet.olt;
            olt.frame = updateEquipmentRequestInternet.frame;
            olt.port = updateEquipmentRequestInternet.port;
            olt.slot = updateEquipmentRequestInternet.slot;
            ffmDataManager.tx(tx -> {
                tx.save(olt);
                workOrder.olt = olt;
                tx.save(workOrder);
            });

            if (updateEquipmentRequest != null) {
                updateEquipmentRequests.add(updateEquipmentRequest);
            }
            for (final UpdateEquipmentRequest equipmentRequest : updateEquipmentRequests) {
                UpdateResponse tvEquipmentResponse = (UpdateResponse) retrofitManager.requestWsSync(UpdateResponse.class, WSManager.WS.UPDATE_EQUIPMENT, WebServices.servicesEnlace().updateEquipment(equipmentRequest));
                if (tvEquipmentResponse == null) return false;
                if (!tvEquipmentResponse.result.result.equals("0")) {
                    errorReason = UpdateActivationErrorReason.COULD_NOT_UPDATE_EQUIPMENT_TV;
                    errorMessage = tvEquipmentResponse.result.resultDescription;
                    ffmDataManager.close();
                    return null;
                }
            }
            ffmDataManager.tx(tx -> {
                workOrder.configuredDevices = true;
                tx.save(workOrder);
            });
        } catch (Exception e) {
            ffmDataManager.tx(tx -> {
                workOrder.configuredDevices = false;
                tx.save(workOrder);
            });
            e.printStackTrace();
            ffmDataManager.close();
            return false;
        }
        for (UpdateDNsRequest updateDNsRequest : mUpdateDNRequests) {
            try {
                if (updateDNsRequest != null) {
                    UpdateResponse updateDNResponse = (UpdateResponse) retrofitManager.requestWsSync(UpdateResponse.class, WSManager.WS.UPDATE_DN, WebServices.servicesEnlace().updateDNs(updateDNsRequest));
                    if (updateDNResponse == null) return false;
                    if (!updateDNResponse.result.result.equals("0")) {
                        errorReason = UpdateActivationErrorReason.COULD_NOT_UPDATE_DNS;
                        errorMessage = updateDNResponse.result.resultDescription;
                        ffmDataManager.close();
                        return null;
                    }
                }
                ffmDataManager.tx(tx -> {
                    workOrder.configuredDN = true;
                    tx.save(workOrder);
                });

            } catch (Exception e) {
                ffmDataManager.tx(tx -> {
                    workOrder.configuredDN = false;
                    tx.save(workOrder);
                });
                e.printStackTrace();
                return false;
            }
        }
        ffmDataManager.close();
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (result != null && result) {
            activationServiceUpdateCallback.onSuccessUpdate();
        } else {
            activationServiceUpdateCallback.onErrorUpdate(errorReason, errorMessage);
        }
    }
}
