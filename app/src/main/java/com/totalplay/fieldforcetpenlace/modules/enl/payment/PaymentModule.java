package com.totalplay.fieldforcetpenlace.modules.enl.payment;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.fieldforcetpenlace.background.queue.jobs.SetFlagJob;
import com.totalplay.fieldforcetpenlace.library.enums.FlagType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

/**
 * Created by jorgehdezvilla on 15/01/18.
 * FFM Enlace
 */

public class PaymentModule extends BaseModule<WorkOrder, WorkOrder> {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        PaymentActivity.launch(this, appCompatActivity, workOrder);
    }

    @Override
    public void finishModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.PAID_ACCOUNT, workOrder.id));
        super.finishModule(appCompatActivity, workOrder);
    }
}
