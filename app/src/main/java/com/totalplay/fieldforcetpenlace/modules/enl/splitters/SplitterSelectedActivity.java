package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;
import com.totalplay.fieldforcetpenlace.view.adapters.SplitterTypeAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by icortes on 06/07/17.
 * Enlace
 */

public class SplitterSelectedActivity extends BaseFFMActivity {

    public static final int REQUEST_CODE_PORT = 0x0021;

    SplitterTypeAdapter splitterTypeAdapter;
    public static String numberPort;

    @BindView(R.id.act_splitters_view_pager)
    ViewPager splittersViewPeger;
    @BindView(R.id.act_splitters_view_tabs)
    TabLayout splittersTabLayout;

    public static void launch(AppCompatActivity appCompatActivity){
        Intent intentPort = new Intent(appCompatActivity, SplitterSelectedActivity.class);
        appCompatActivity.startActivityForResult(intentPort, REQUEST_CODE_PORT);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_splitter);
        setTitle("Registrar puerto");
        ButterKnife.bind(this);

        setupViewPager(splittersViewPeger);
        splittersTabLayout.setupWithViewPager(splittersViewPeger);
    }


    private void setupViewPager(ViewPager viewPager) {
        splitterTypeAdapter = new SplitterTypeAdapter(getSupportFragmentManager());
        splitterTypeAdapter.addFragment(new Splitter1Fragment(), "");
        splitterTypeAdapter.addFragment(new Splitter2Fragment(), "");
        splitterTypeAdapter.addFragment(new Splitter3Fragment(), "");
        splitterTypeAdapter.addFragment(new Splitter4Fragment(), "");
        viewPager.setAdapter(splitterTypeAdapter);
    }

    @OnClick(R.id.act_splitter_acpted)
    public void onButonAcpted(){
        Intent intent=new Intent();
        intent.putExtra("portNumber", numberPort);
        setResult(Activity.RESULT_OK,intent);
        finish();
    }

}
