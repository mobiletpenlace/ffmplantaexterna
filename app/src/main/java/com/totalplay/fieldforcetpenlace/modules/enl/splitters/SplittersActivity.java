package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.ReasonDefinition;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.OptionsMenuActivity;
import com.totalplay.utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


@SuppressWarnings("unchecked")
public class SplittersActivity extends OptionsMenuActivity {

    @BindView(R.id.act_splitters_info_splitter_demo)
    ImageView mSplittersDemo;
    @BindView(R.id.act_splitters_add_splitter)
    ImageView mAddSplitterFrameLayout;

    SplittersMapFragment mSplittersMapFragment;

    @BindView(R.id.splitter_content_description)
    CardView mSplitterContent;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder, boolean canAdd) {
        if (!workOrder.isAssignedSplitter) {
            Intent intent = new Intent(appCompatActivity, SplittersActivity.class);
            intent.putExtra(Keys.EXTRA_SPLITTERS_CAN_ADD, canAdd);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, new SplittersOutput(workOrder, null, null, false, false));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.enl_activity_splitters);

        ButterKnife.bind(this);

        boolean canAdd = getIntent().getBooleanExtra(Keys.EXTRA_SPLITTERS_CAN_ADD, false);
        Prefs.instance().putBool(Keys.EXTRA_SPLITTERS_CAN_ADD, canAdd);

        initReport(getString(R.string.dialog_error_splitter), ReasonDefinition.reasonsSplitter);
        mTitleReportTextView.setTextColor(ContextCompat.getColor(this, R.color.gray_splitter));

        if (mTrainingManager.isTrainingMode()) {
            mTrainingManager.splittersActivity(() -> mSplittersDemo.setVisibility(View.GONE));
        } else {
            mSplittersDemo.setVisibility(View.GONE);
        }

        mSplittersMapFragment = (SplittersMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fr_splitters);
    }

    @OnClick(R.id.act_img_refresh)
    public void onRefreshSplitters() {
        mSplittersMapFragment.refreshSplitters();
    }

    @OnClick(R.id.act_splitters_add_splitter)
    public void OnClick(View view) {
        if (!Prefs.instance().bool(Keys.EXTRA_SPLITTERS_CAN_ADD)) {
            AddSplittersActivity.launch(baseModule, this);
        } else {
            if (Prefs.instance().bool(Keys.EXTRA_SPLITTERS_CAN_ADD)) {
                UpdateSplitterActivity.launch(baseModule, this, new Splitter());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UpdateSplitterActivity.REQUEST_CODE_UPDATE_SPLITTER) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }
}

