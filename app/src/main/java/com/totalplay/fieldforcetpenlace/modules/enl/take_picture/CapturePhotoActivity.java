package com.totalplay.fieldforcetpenlace.modules.enl.take_picture;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

import java.io.File;

import io.fotoapparat.Fotoapparat;
import io.fotoapparat.FotoapparatSwitcher;
import io.fotoapparat.parameter.LensPosition;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.result.PendingResult;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.view.CameraView;

import static io.fotoapparat.log.Loggers.fileLogger;
import static io.fotoapparat.log.Loggers.logcat;
import static io.fotoapparat.log.Loggers.loggers;
import static io.fotoapparat.parameter.selector.AspectRatioSelectors.standardRatio;
import static io.fotoapparat.parameter.selector.FlashSelectors.autoFlash;
import static io.fotoapparat.parameter.selector.FlashSelectors.autoRedEye;
import static io.fotoapparat.parameter.selector.FlashSelectors.off;
import static io.fotoapparat.parameter.selector.FlashSelectors.torch;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.autoFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.continuousFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.fixed;
import static io.fotoapparat.parameter.selector.LensPositionSelectors.lensPosition;
import static io.fotoapparat.parameter.selector.Selectors.firstAvailable;
import static io.fotoapparat.parameter.selector.SizeSelectors.smallestSize;

public class CapturePhotoActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_CAPTURE = 0x12;
    private static int mImageCode;
    private CameraView mCameraView;
    private FotoapparatSwitcher fotoapparatSwitcher;
    private String mImagePath;
    private boolean isTake = false;

    public static void launch(BaseModule baseModule, AppCompatActivity activity, String path) {
        Intent intent = new Intent(activity, CapturePhotoActivity.class);
        intent.putExtra(Keys.EXTRA_IMAGE_PATH, path);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        activity.startActivityForResult(intent, CapturePhotoActivity.REQUEST_CODE_CAPTURE);
    }

    public static void launch(BaseModule baseModule, AppCompatActivity activity, String path, int requestCode) {
        Intent intent = new Intent(activity, CapturePhotoActivity.class);
        intent.putExtra(Keys.EXTRA_IMAGE_PATH, path);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        if (requestCode == 33)
            mImageCode = 0;
        else
            mImageCode = 1;
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_capture_photo);

        mImagePath = getIntent().getStringExtra(Keys.EXTRA_IMAGE_PATH);

        mCameraView = findViewById(R.id.act_capture_photo_camera);
        View captureView = findViewById(R.id.act_capture_photo_take);

        Fotoapparat backPhotoApparat;
        if (mImageCode == 0) {
            backPhotoApparat = createFotoapparat(LensPosition.FRONT);
        } else {
            backPhotoApparat = createFotoapparat(LensPosition.BACK);
        }

        fotoapparatSwitcher = FotoapparatSwitcher.withDefault(backPhotoApparat);

        captureView.setOnClickListener(v -> {
            if (!isTake) {
                isTake = true;
                takePicture();
            }
        });

    }

    private Fotoapparat createFotoapparat(LensPosition position) {
        return Fotoapparat
                .with(this)
                .into(mCameraView)
                .photoSize(standardRatio(smallestSize()))
                .lensPosition(lensPosition(position))
                .focusMode(firstAvailable(
                        continuousFocus(),
                        autoFocus(),
                        fixed()
                ))
                .flash(firstAvailable(
                        autoRedEye(),
                        autoFlash(),
                        torch(),
                        off()
                ))
                .frameProcessor(new SampleFrameProcessor())
                .logger(loggers(
                        logcat(),
                        fileLogger(this)
                ))
                .build();
    }

    private void takePicture() {
        try {
            PhotoResult photoResult = fotoapparatSwitcher.getCurrentFotoapparat().takePicture();
            File file = new File(mImagePath);
            PendingResult<Void> pendingResult = photoResult.saveToFile(file);
            pendingResult.whenDone(aVoid -> {
                setResult(RESULT_OK);
                finish();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fotoapparatSwitcher.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        fotoapparatSwitcher.stop();
    }

    private class SampleFrameProcessor implements FrameProcessor {

        @Override
        public void processFrame(Frame frame) {
            // Perform frame processing, if needed
        }

    }

}
