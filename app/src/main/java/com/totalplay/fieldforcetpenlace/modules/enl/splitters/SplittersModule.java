package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.library.pojos.DataManager;
import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.fieldforcetpenlace.background.queue.jobs.SetFlagJob;
import com.totalplay.fieldforcetpenlace.library.enums.FlagType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleManager;
import com.totalplay.fieldforcetpenlace.view.activities.OptionsIncidentsMenuActivity;
import com.totalplay.fieldforcetpenlace.modules.definition.ModuleKey;

/**
 * Created by jorgehdezvilla on 15/01/18.
 * FFM Enlace
 */

public class SplittersModule extends BaseModule<WorkOrder, SplittersOutput> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (getExtras().isEmpty())
            SplittersActivity.launch(this, appCompatActivity, workOrder, true);
        else
            SplittersActivity.launch(this, appCompatActivity, workOrder, (Boolean) getExtras().get(Keys.CAN_ADD));
    }

    @Override
    public void finishModule(AppCompatActivity appCompatActivity, SplittersOutput splittersOutput) {
        DataManager dataManager = DataManager.validateConnection(null);
        QueueManager.add(new SetFlagJob(FlagType.ASSIGNED_SPLITTER, splittersOutput.workOrder.id));

        if (splittersOutput.searchOther) {
            appCompatActivity.finish();
        } else {
            if (splittersOutput.stopWorkOrder) {
                if (splittersOutput.splitter != null && splittersOutput.splitter.latitude != null)
                    OptionsIncidentsMenuActivity.launch(appCompatActivity, splittersOutput.reason, splittersOutput.splitter);
                else
                    OptionsIncidentsMenuActivity.launch(appCompatActivity, splittersOutput.reason);
            } else {
                dataManager.tx(tx -> {
                    splittersOutput.workOrder.isAssignedSplitter = true;
                    tx.save(splittersOutput.workOrder);
                });
                Intent intent = new Intent();
                appCompatActivity.setResult(Activity.RESULT_OK, intent);
                ModuleManager.getInstance().onNextModule(appCompatActivity, ModuleKey.WORK_ORDER_SPLITTERS, splittersOutput.workOrder);
                appCompatActivity.finish();
            }
        }
        dataManager.close();
    }
}
