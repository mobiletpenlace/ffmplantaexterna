package com.totalplay.fieldforcetpenlace.modules.definition;

/**
 * Created by jorgehdezvilla on 04/01/18.
 * Module
 */

public class ModuleKey {

    public static final String LOGIN = "login";
    public static final String SELFIE_PHOTO = "selfiePhoto";
    public static final String INTRODUCCTION_APP = "introducction";
    public static final String ASSISTANTS = "assistant";
    public static final String LOAD_WORK_ORDER = "loadWorkOrder";
    public static final String WORK_ORDER_MAP_INFO = "infoMpap";
    public static final String WORK_ORDER_TRAVEL = "travel";
    public static final String WORK_ORDER_ARRIVE_EARLY = "arriveEarly";
    public static final String WORK_ORDER_ARRIVE_EVIDENCE = "arriveEvidence";
    public static final String WORK_ORDER_INFO = "info";
    public static final String WORK_ORDER_DEAR_TIME = "dearTime";
    public static final String WORK_ORDER_SPLITTERS = "splitters";
    public static final String WORK_ORDER_ACTIVATION = "activation";
    public static final String WORK_ORDER_CONFIGURE_DISPOSITIVES = "configureDispositives";
    public static final String WORK_ORDER_REPLACE_DISPOSITIVES = "replaceDispositives";
    public static final String WORK_ORDER_RESUME = "resume";
    public static final String WORK_ORDER_PAYMENT = "payment";
    public static final String WORK_ORDER_SURVEY = "survey";
    public static final String WORK_ORDER_FINISH_INSTALLATION = "finishInstallation";
    public static final String WORK_ORDER_ACCEPTANCE_ACT = "acceptanceAct";
    public static final String WORK_ORDER_EVIDENCES = "evidences";
    public static final String WORK_ORDER_MATERIALS = "materials";

    public static final String WORK_ORDER_COMPLETED = "workOrderCompleted";


    public static final String MY_PROFILE = "myProfile";
    public static final String OPERATOR_STATUS = "operatorStatus";

    public static final String TAKE_PICTURE = "takePicture";


}
