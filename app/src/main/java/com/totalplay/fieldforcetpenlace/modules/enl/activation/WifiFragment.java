package com.totalplay.fieldforcetpenlace.modules.enl.activation;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WifiModel;
import com.totalplay.fieldforcetpenlace.view.custom.WifiView;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.fieldforcetpenlace.view.utils.UIUtils;
import com.totalplay.mvp.BasePresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

@SuppressWarnings("unchecked")
public class WifiFragment extends BaseFFMFragment {

    @BindView(R.id.act_service_activation_wifis)
    public LinearLayout mTvViewsLayout;
    public ArrayList<WifiView> mWifiViews = new ArrayList<>();

    public static WifiFragment newInstance() {
        return new WifiFragment();
    }

    @Override
    public BasePresenter getPresenter() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_wifi, container, false);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        ArrayList<Equipment> wifiEquipments = mFFMDataManager.queryWhere(Equipment.class).filter("dispositive", EquipmentType.WIFI_EXTENDER).list();
        if (wifiEquipments != null) {
            for (Equipment equipment : wifiEquipments) {
                View _view = LayoutInflater.from(getContext()).inflate(R.layout.enl_item_service_activation_wifi, null);
                WifiView wifiView = new WifiView((AppCompatActivity) getActivity(), _view, mWorkOrder.packageContracted, equipment);
                if (mWorkOrder.configWorkOrder.wifiModels.size() > 0) {
                    for (WifiModel wifiModel : mWorkOrder.configWorkOrder.wifiModels) {
                        if (equipment.servicePlan.equals(wifiModel.servicePlanId)) {
                            wifiView.setup(wifiModel);
                        }
                    }
                }
                mWifiViews.add(wifiView);
                mTvViewsLayout.addView(_view);
            }
        }

        boolean configured = mWorkOrder.configuredDevices && mWorkOrder.configuredDN;
        if (configured) {
            UIUtils.disableEnableControls(false, mTvViewsLayout);
        }
    }

}
