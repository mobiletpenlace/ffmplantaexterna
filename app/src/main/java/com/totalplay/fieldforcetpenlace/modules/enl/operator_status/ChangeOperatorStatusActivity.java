package com.totalplay.fieldforcetpenlace.modules.enl.operator_status;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.library.enums.OperatorStatus;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.UserStatus;
import com.totalplay.fieldforcetpenlace.view.dialogfragments.RecyclerDialogFragment;
import com.totalplay.mvp.BaseActivity;
import com.totalplay.mvp.BasePresenter;

/**
 * Created by TotalPlay on 11/28/16.
 * Enlace
 */
public class ChangeOperatorStatusActivity extends BaseActivity {

    public static final int REQUEST_CODE_CHANGE_STATUS = 0x123;
    private ChangeOperatorStatusPresenter mChangeOperatorStatusPresenter;

    public static void launch(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, ChangeOperatorStatusActivity.class);
        appCompatActivity.startActivityForResult(intent, REQUEST_CODE_CHANGE_STATUS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RecyclerDialogFragment<UserStatus> fragment = RecyclerDialogFragment.newInstance();
        fragment.setArrayList(OperatorStatus.status);
        fragment.setSelectedListener((index, object, comment) -> mChangeOperatorStatusPresenter.sendOperatorStatus(object));
        fragment.show(getSupportFragmentManager(), "recycler_fragment");
    }

    @Override
    protected BasePresenter getPresenter() {
        return mChangeOperatorStatusPresenter = new ChangeOperatorStatusPresenter(this, (user) -> {
            if (user != null) {
                Intent intent = new Intent();
                intent.putExtra(Keys.EXTRA_USER, user);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
