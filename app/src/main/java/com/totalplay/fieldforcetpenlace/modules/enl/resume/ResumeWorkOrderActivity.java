package com.totalplay.fieldforcetpenlace.modules.enl.resume;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.view.activities.BaseFFMActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jorgehdezvilla on 26/06/17.
 * FFM
 */

@SuppressWarnings("unchecked")
public class ResumeWorkOrderActivity extends BaseFFMActivity {

    @BindView(R.id.act_detail_work_order_number)
    TextView mNumberTextView;
    @BindView(R.id.act_detail_work_order_service)
    TextView mServiceTextView;
    @BindView(R.id.act_detail_work_order_package)
    TextView mPackageTextView;
    @BindView(R.id.act_detail_work_order_addons)
    TextView mAddonsTextView;
    @BindView(R.id.act_detail_work_order_promo)
    TextView mPromoTextView;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, ResumeWorkOrderActivity.class);
        intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
        appCompatActivity.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_resume_order_work);
        ButterKnife.bind(this);

        mNumberTextView.setText((mWorkOrder.id != null) ? mWorkOrder.id : "");
        mServiceTextView.setText((mWorkOrder.serviceOrder != null) ? mWorkOrder.serviceOrder : "");
        mPackageTextView.setText((mWorkOrder.packageContracted != null) ? mWorkOrder.packageContracted : "");

        if (mWorkOrder.quotationInfo != null) {
            String addons = "";
            for (ServicePlan servicePlan : mWorkOrder.quotationInfo.servicePlans) {
                if (servicePlan.wasAdditional.equals("true")) {
                    addons += servicePlan.planLabel + "\n";
                }
            }
            mAddonsTextView.setText(addons);
        }
        mTrainingManager.resumeWorkOrderActivity();

    }

    @OnClick(R.id.act_resume_order_work_next)
    void onNextClick() {
        baseModule.finishModule(this, mWorkOrder);
        finish();
    }


}
