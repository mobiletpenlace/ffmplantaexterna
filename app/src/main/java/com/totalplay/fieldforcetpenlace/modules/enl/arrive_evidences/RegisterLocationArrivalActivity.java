package com.totalplay.fieldforcetpenlace.modules.enl.arrive_evidences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.ReasonDefinition;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.library.utils.DateUtils;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.TakePictureInput;
import com.totalplay.fieldforcetpenlace.view.activities.OptionsMenuActivity;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

@SuppressWarnings("unchecked")
public class RegisterLocationArrivalActivity extends OptionsMenuActivity
        implements RegisterLocationArrivalPresenter.RegisterLocationArrivalCallback {

    private static final int REQUEST_CODE_LEFT_CORNER = 0x0012;
    private static final int REQUEST_CODE_OUTSIDE = 0x0013;
    private static final int REQUEST_CODE_RIGHT_CORNER = 0x0014;

    @BindView(R.id.act_arrival_time)
    TextView mTimeTextView;
    @BindView(R.id.act_arrival_left_corner_image)
    ImageView mLeftCornerImageView;
    @BindView(R.id.act_arrival_right_corner_image)
    ImageView mRightCornerImageView;
    @BindView(R.id.act_arrival_outside_image)
    ImageView mOutsideImageView;
    @BindView(R.id.act_arrival_references)
    EditText mReferencesEditText;

    private File mRequestedFile;
    private FormValidator mFormValidator;

    private boolean pic1 = false;
    private boolean pic2 = false;
    private boolean pic3 = false;

    public RegisterLocationArrivalPresenter mRegisterLocationArrivalPresenter;

    public static void launch(BaseModule baseModule, AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isRegisterLocationArrival) {
            Intent intent = new Intent(appCompatActivity, RegisterLocationArrivalActivity.class);
            intent.putExtra(Keys.EXTRA_BASE_MODULE, baseModule);
            appCompatActivity.startActivity(intent);
        } else {
            baseModule.finishModule(appCompatActivity, workOrder);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_arrival);
        ButterKnife.bind(this);
        setTitle("");

        if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
            initReport(getString(R.string.dialog_error_address), ReasonDefinition.reasonsLocation);
        }

        mTimeTextView.setText(String.format("%s hrs", DateUtils.time()));

        mFormValidator = new FormValidator(this);
        mFormValidator.addValidators(
                new EditTextValidator(mReferencesEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );

        mRegisterLocationArrivalPresenter.getPhotosIfPossible();

        mTrainingManager.registerLocationArrivalActivity();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mRegisterLocationArrivalPresenter = new RegisterLocationArrivalPresenter(this, this);
    }

    @OnClick({R.id.act_arrival_left_corner, R.id.act_arrival_right_corner, R.id.act_arrival_outside})
    public void onImageClick(View view) {
        switch (view.getId()) {
            case R.id.act_arrival_left_corner:
                baseModule.startModule(this, new TakePictureInput("LeftCorner", REQUEST_CODE_LEFT_CORNER, true));
                break;
            case R.id.act_arrival_right_corner:
                baseModule.startModule(this, new TakePictureInput("RightCorner", REQUEST_CODE_RIGHT_CORNER, true));
                break;
            case R.id.act_arrival_outside:
                baseModule.startModule(this, new TakePictureInput("Outside", REQUEST_CODE_RIGHT_CORNER, true));
                break;
        }

    }

    @OnClick(R.id.act_arrival_next)
    public void onNextClick() {
        if (mFormValidator.isValid()) {
            if (pic1 && pic2 && pic3) {
                mRegisterLocationArrivalPresenter.uploadPhotos();
            } else {
                MessageUtils.toast(this, "Es necesario tomar las 3 fotografías");
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            ImageView selectedImageView = null;
            if (requestCode == REQUEST_CODE_LEFT_CORNER) {
                selectedImageView = mLeftCornerImageView;
                pic1 = true;
            }
            if (requestCode == REQUEST_CODE_RIGHT_CORNER) {
                selectedImageView = mRightCornerImageView;
                pic2 = true;
            }
            if (requestCode == REQUEST_CODE_OUTSIDE) {
                selectedImageView = mOutsideImageView;
                pic3 = true;
            }
            if (selectedImageView != null && mRequestedFile != null) {
                selectedImageView.setPadding(0, 0, 0, 0);
                Glide.with(this)
                        .load(mRequestedFile)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(selectedImageView);
                mRegisterLocationArrivalPresenter.savePhoto(mRequestedFile.getName(), mRequestedFile.getAbsolutePath());
            }
        }
    }

    @Override
    public void onSuccessLoadEvidences(List<PhotoEvidence> photoEvidences) {
        if (photoEvidences.size() > 0) {
            HashMap<String, ImageView> imageViewHashMap = new HashMap<String, ImageView>() {{
                put("LeftCorner.jpg", mLeftCornerImageView);
                put("RightCorner.jpg", mRightCornerImageView);
                put("Outside.jpg", mOutsideImageView);
            }};
            for (PhotoEvidence photoEvidence : photoEvidences) {
                if (photoEvidence.name == null) continue;
                if (imageViewHashMap.containsKey(photoEvidence.name)) {
                    ImageView imageView = imageViewHashMap.get(photoEvidence.name);
                    imageView.setPadding(0, 0, 0, 0);
                    Glide.with(this)
                            .load(photoEvidence.path)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(imageView);
                    switch (photoEvidence.name) {
                        case "LeftCorner.jpg":
                            pic1 = true;
                            break;
                        case "RightCorner.jpg":
                            pic2 = true;
                            break;
                        case "Outside.jpg":
                            pic3 = true;
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void onSuccessUploadPhotos(WorkOrder workOrder) {
        finish();
        baseModule.finishModule(this, workOrder);
    }
}