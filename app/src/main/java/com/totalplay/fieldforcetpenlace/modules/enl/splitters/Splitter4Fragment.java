package com.totalplay.fieldforcetpenlace.modules.enl.splitters;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.mvp.BaseFragment;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by icortes on 07/07/17.
 * FFM Enlace
 */

public class Splitter4Fragment extends BaseFragment {

    @BindView(R.id.port1_iz)
    LinearLayout port1Izp;
    @BindView(R.id.port2_iz)
    LinearLayout port2Izp;
    @BindView(R.id.port3_iz)
    LinearLayout port3Izp;
    @BindView(R.id.port4_iz)
    LinearLayout port4Izp;
    @BindView(R.id.port5_iz)
    LinearLayout port5Izp;
    @BindView(R.id.port6_iz)
    LinearLayout port6Izp;
    @BindView(R.id.port7_iz)
    LinearLayout port7Izp;
    @BindView(R.id.port8_iz)
    LinearLayout port8Izp;
    @BindView(R.id.port9_der)
    LinearLayout port9Der;
    @BindView(R.id.port10_der)
    LinearLayout port10Der;
    @BindView(R.id.port11_der)
    LinearLayout port11Der;
    @BindView(R.id.port12_der)
    LinearLayout port12Der;
    @BindView(R.id.port13_der)
    LinearLayout port13Der;
    @BindView(R.id.port14_der)
    LinearLayout port14Der;
    @BindView(R.id.port15_der)
    LinearLayout port15Der;
    @BindView(R.id.port16_der)
    LinearLayout port16Der;

    Boolean[] array = new Boolean[16];
    LinearLayout[] imagenes;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_splitter_4, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        Arrays.fill(array, Boolean.FALSE);
        imagenes = new LinearLayout[]{port1Izp, port2Izp, port3Izp, port4Izp, port5Izp, port6Izp, port7Izp, port8Izp, port9Der,
                port10Der, port11Der, port12Der, port13Der, port14Der, port15Der, port16Der};
    }


    @OnClick({R.id.port1_iz, R.id.port2_iz, R.id.port3_iz, R.id.port4_iz, R.id.port5_iz, R.id.port6_iz, R.id.port7_iz, R.id.port8_iz, R.id.port9_der, R.id.port10_der, R.id.port11_der, R.id.port12_der, R.id.port13_der, R.id.port14_der, R.id.port15_der, R.id.port16_der})
    public void onPortSelected(View view) {
        getActivity();
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(100);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getActivity(), notification);
        r.play();
        switch (view.getId()) {

            case R.id.port1_iz:
                if (array[0]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[0] = true;
                }
                getPort("1");
                break;
            case R.id.port2_iz:
                if (array[1]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[1] = true;
                }
                getPort("2");
                break;
            case R.id.port3_iz:
                if (array[2]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[2] = true;
                }
                getPort("3");
                break;
            case R.id.port4_iz:
                if (array[3]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[3] = true;
                }
                getPort("4");
                break;
            case R.id.port5_iz:
                if (array[4]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[4] = true;
                }
                getPort("5");
                break;
            case R.id.port6_iz:
                if (array[5]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[5] = true;
                }
                getPort("6");
                break;
            case R.id.port7_iz:
                if (array[6]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[6] = true;
                }
                getPort("7");
                break;
            case R.id.port8_iz:
                if (array[7]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[7] = true;
                }
                getPort("8");
                break;
            case R.id.port9_der:
                if (array[8]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[8] = true;
                }
                getPort("9");
                break;
            case R.id.port10_der:
                if (array[9]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[9] = true;
                }
                getPort("10");
                break;
            case R.id.port11_der:
                if (array[10]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[10] = true;
                }
                getPort("11");
                break;
            case R.id.port12_der:
                if (array[11]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[11] = true;
                }
                getPort("12");
                break;
            case R.id.port13_der:
                if (array[12]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[12] = true;
                }
                getPort("13");
                break;
            case R.id.port14_der:
                if (array[13]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[13] = true;
                }
                getPort("14");
                break;
            case R.id.port15_der:
                if (array[14]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[14] = true;
                }
                getPort("15");
                break;
            case R.id.port16_der:
                if (array[15]) {
                    Arrays.fill(array, Boolean.FALSE);
                } else {
                    Arrays.fill(array, Boolean.FALSE);
                    array[15] = true;
                }
                getPort("16");
                break;
        }
    }

    private void getPort(String port) {
        SplitterSelectedActivity.numberPort = port;
        for (int i = 0; i < array.length; i++) {
            if (i < 8) {
                if (array[i]) {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_asignado_izq));
                } else {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_disponible_izq));
                }
            } else {
                if (array[i]) {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_asignado_der));
                } else {
                    imagenes[i].setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_disponible_der));
                }
            }
        }
    }
}
