package com.totalplay.fieldforcetpenlace.modules.enl.auxiliars;

import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 11/01/18.
 * FFM ENLACE
 */

public class AssistantsParameter implements Serializable, ModuleParameter {

    public boolean isChanging;

    public AssistantsParameter(boolean isChanging) {
        this.isChanging = isChanging;
    }
}
