package com.totalplay.fieldforcetpenlace.modules.enl.replace_dispositives;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

/**
 * Created by jorgehdezvilla on 17/01/18.
 * FFM ENlace
 */

public class ReplaceDispositivesModule extends BaseModule<WorkOrder, WorkOrder> {
    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        ReplaceEquipmentsActivity.launch(this, appCompatActivity);
    }
}
