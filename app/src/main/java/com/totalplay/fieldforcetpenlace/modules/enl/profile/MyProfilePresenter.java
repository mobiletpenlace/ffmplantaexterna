package com.totalplay.fieldforcetpenlace.modules.enl.profile;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;

/**
 * Created by jorgehdezvilla on 13/11/17.
 * Enlace
 */

public class MyProfilePresenter extends FFMPresenter {

    private String serverFolder = ServerFolders.PROFILE;

    public MyProfilePresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void savePhoto(String name, String path) {
        mFFMDataManager.tx(tx -> {
            tx.removeListInModelByAttribute(PhotoEvidence.class, "type", ImageTypeEnum.PROFILE);
            PhotoEvidence photo = new PhotoEvidence();
            photo.name = name;
            photo.path = path;

            photo.serverFolder = serverFolder;
            photo.baseUrl = "Ventas/Enlace/System/User/";
            photo.type = ImageTypeEnum.PROFILE;

            tx.save(photo);
        });
    }
}
