package com.totalplay.fieldforcetpenlace.modules.enl.travel;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;

/**
 * Created by jorgehdezvilla on 05/01/18.
 * FFM Enlace
 */

public class WorkOrderTravelModule extends BaseModule<WorkOrder, WorkOrder> {

    @Override
    public void startModule(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        WorkOrderInfoInRouteActivity.launch(this, appCompatActivity, workOrder);
    }

}
