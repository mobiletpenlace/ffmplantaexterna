package com.totalplay.fieldforcetpenlace.modules.enl.replace_dispositives;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.fieldforcetpenlace.view.custom.ReplaceDispositiveView;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

/**
 * Created by jorgehdezvilla on 19/12/17.
 * FFM Enlace
 */

public class ReplaceEquipmentsListFragment extends BaseFFMFragment {

    @BindView(R.id.fr_base_linear)
    ViewGroup mViewGroup;
    private List<ReplaceDispositiveView> mReplaceDispositiveViews = new ArrayList<>();

    private Integer mIndex = 0;
    private Disposable mDispositivesDisposable;

    private View.OnClickListener mOnDeleteDispositiveListener = view -> {
        InfoEquipment infoEquipment = (InfoEquipment) view.getTag();
        infoEquipment.wasReplace = false;
        mFFMDataManager.tx(tx -> {
            mFFMDataManager.queryWhere(Equipment.class).filter("servicePlan", infoEquipment.planServiceId).remove();
            tx.save(infoEquipment);
        });
    };

    public static ReplaceEquipmentsListFragment newInstance() {
        ReplaceEquipmentsListFragment settingsEquipmentsListFragment = new ReplaceEquipmentsListFragment();
        Bundle bundle = new Bundle();
        settingsEquipmentsListFragment.setArguments(bundle);
        return settingsEquipmentsListFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_base_linear, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mDispositivesDisposable = mFFMDataManager.listObservable(Equipment.class).subscribe(this::onSuccessLoadDispositives);
    }

    private void onSuccessLoadDispositives(ArrayList<Equipment> equipmentList) {
        mIndex = 0;
        mViewGroup.removeAllViews();
        mReplaceDispositiveViews.clear();
        for (Equipment equipment : equipmentList) addEquipmentForm(equipment);
    }

    public void addDispositive(InfoEquipment infoEquipment) {
        Equipment equipment = new Equipment();
        equipment.servicePlan = infoEquipment.planServiceId;
        mFFMDataManager.tx(tx -> tx.save(equipment));
    }

    public boolean isValid(){
        boolean valid = true;
        for (ReplaceDispositiveView replaceDispositiveView : mReplaceDispositiveViews) valid &= replaceDispositiveView.isValid();
        return valid;
    }

    public List<Equipment> getEquipments(){
        List<Equipment> equipmentList = new ArrayList<>();
        for (ReplaceDispositiveView replaceDispositiveView : mReplaceDispositiveViews) equipmentList.add(replaceDispositiveView.getEquipment());
        return equipmentList;
    }

    private void addEquipmentForm(Equipment equipment) {
        mIndex++;
        View _view = LayoutInflater.from(getContext()).inflate(R.layout.enl_item_replace_equipment, null);
        InfoEquipment infoEquipment = mFFMDataManager.queryWhere(InfoEquipment.class).filter("planServiceId", equipment.servicePlan).findFirst();
        ReplaceDispositiveView replaceDispositiveView = new ReplaceDispositiveView((AppCompatActivity) getActivity(), _view,
                equipment, infoEquipment, mIndex, mOnDeleteDispositiveListener);
        mReplaceDispositiveViews.add(replaceDispositiveView);
        mViewGroup.addView(_view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDispositivesDisposable != null && !mDispositivesDisposable.isDisposed())
            mDispositivesDisposable.dispose();
    }
}
