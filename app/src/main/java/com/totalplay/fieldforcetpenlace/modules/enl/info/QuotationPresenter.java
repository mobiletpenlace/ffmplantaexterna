package com.totalplay.fieldforcetpenlace.modules.enl.info;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.QuotationRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.Quotation;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.utils.MessageUtils;

public class QuotationPresenter extends FFMPresenter implements WSCallback {

    private ActivationServiceCallback mActivationServiceCallback;

    public interface ActivationServiceCallback {
        void onSuccessLoadQuotationInfo(Quotation quotationInfo);
    }

    public QuotationPresenter(AppCompatActivity appCompatActivity, ActivationServiceCallback activationServiceCallback) {
        super(appCompatActivity);
        mActivationServiceCallback = activationServiceCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void loadQuotation() {
        QuotationRequest quotationRequest = new QuotationRequest();
        quotationRequest.accountTicketNumber = mWorkOrder.account;
        mWSManager.requestWs(Quotation.class, WSManager.WS.QUOTATION, getDefinition().quotation(quotationRequest));
    }

    private void successLoadQuotation(String requestUrl, Quotation quotation) {
        if (quotation.result.equals("0")) {
            mFFMDataManager.tx(tx -> {
                tx.delete(Quotation.class);
                tx.delete(ServicePlan.class);
                if (quotation.amount == null) quotation.amount = "0.0";
                tx.save(quotation);
                mWorkOrder.quotationInfo = quotation;
                tx.save(mWorkOrder);
            });
            mActivationServiceCallback.onSuccessLoadQuotationInfo(mWorkOrder.quotationInfo);
        } else {
            onErrorLoadResponse(requestUrl, quotation.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.QUOTATION)) {
            super.onRequestWS(requestUrl);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.QUOTATION)) {
            successLoadQuotation(requestUrl, (Quotation) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        if (messageError.equals("")) {
            super.onErrorLoadResponse(requestUrl, "");
        } else {
            MessageUtils.toast(mContext, messageError);
        }
        mAppCompatActivity.finish();
    }

}
