package com.totalplay.fieldforcetpenlace.routing;

import android.support.v7.app.AppCompatActivity;

import com.library.pojos.DataManager;
import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.fieldforcetpenlace.background.queue.jobs.SetFlagJob;
import com.totalplay.fieldforcetpenlace.library.enums.FlagType;
import com.totalplay.fieldforcetpenlace.library.enums.ReasonDefinition;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderStatus;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.view.activities.InWaitActivity;
import com.totalplay.fieldforcetpenlace.modules.enl.login.LoginActivity;
import com.totalplay.fieldforcetpenlace.view.activities.OptionsIncidentsMenuActivity;
import com.totalplay.fieldforcetpenlace.modules.enl.splitters.SplitterSelectedActivity;
import com.totalplay.utils.Prefs;

import org.joda.time.DateTime;

/**
 * Created by jorgehdezvilla on 06/09/17.
 * FFM
 */

public class RoutingApp {

    public static void OnLoadedSplashScreen(AppCompatActivity appCompatActivity) {
        DataManager ffmDataManager = DataManager.validateConnection(null);
        User user = ffmDataManager.queryWhere(User.class).findFirst();
        if (user == null) {
            appCompatActivity.startActivity(LoginActivity.createIntent(appCompatActivity));
        } else {
            long time = Prefs.instance().number(Keys.PREF_LAST_TIME_LOGGED);
            DateTime dateTime = new DateTime(time);
            if (dateTime.plusHours(3).isBeforeNow()) {
                ffmDataManager.tx(tx -> tx.delete(User.class));
//                LoginActivity.launch(appCompatActivity);
            } else {
                WorkOrder workOrder = ffmDataManager.queryWhere(WorkOrder.class).findFirst();
                if (workOrder != null && workOrder.operarioId.equals("TOTALDEMO")) {
                    ffmDataManager.deleteWorkOrder();
                    workOrder = null;
                }
//                TrainingManager.TRAINING_ENABLED = true;
                if (workOrder != null) {
                    if (workOrder.isWait) {
                        OnSuccessCancelWorkOrderEvent(appCompatActivity);
                    } else {
//                        WorkOrderInfoActivity.launch(appCompatActivity);
                    }
                } else {
//                    RoutingApp.OnSuccessLoginEvent(appCompatActivity, user);
                }
            }
        }
        ffmDataManager.close();
        appCompatActivity.finish();
    }

    public static void OnSuccessLoginEvent(AppCompatActivity appCompatActivity, User user) {
//        UserProfileActivity.launch(appCompatActivity, user);
    }

    public static void OnButtonSplitterEvent(AppCompatActivity appCompatActivity) {
//        SplittersActivity.launch(appCompatActivity, false);
    }

    public static void OnButtonLogoutEvent(AppCompatActivity appCompatActivity) {
        appCompatActivity.finish();
//        LoginActivity.launch(appCompatActivity);
    }

    public static void OnMarkerWorkOrderEvent(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
//        List<Class> classList = new ArrayList<>();
//        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
//            for (String moduleName : TestFlow.FLOWS_APP.get(FlowKey.INTERNAL_PLANT_FLOW)) {
//                for (Class className : ModuleDefinition.MODULE_COMPOSITION.get(moduleName)) {
//                    classList.add(className);
//                }
//            }
//        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//
//        }
//        WorkOrderInfoActivity.launch(appCompatActivity);
    }

    public static void OnSplitterMapEvent(AppCompatActivity appCompatActivity, Splitter splitter) {
        if (Prefs.instance().bool(Keys.EXTRA_SPLITTERS_CAN_ADD)) {
//            UpdateSplitterActivity.launch(appCompatActivity, splitter);
        }
    }


    public static void OnAddSplitterEvent(AppCompatActivity appCompatActivity) {
//        AddSplittersActivity.launch(appCompatActivity);
    }

    public static void OnStopWorkOrderEvent(AppCompatActivity mAppCompatActivity, Reason reason) {
        OptionsIncidentsMenuActivity.launch(mAppCompatActivity, reason);
    }

    public static void OnDontCantAddSplitterEvent(AppCompatActivity appCompatActivity) {
//        LoadWorkOrdersActivity.launch(appCompatActivity);
    }

    public static void OnSuccessFindSplitterEvent(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
//        ActivationServiceActivity.launch(mAppCompatActivity, workOrder);
    }

    public static void OnSuccessChangeStatusEvent(AppCompatActivity appCompatActivity, String status, WorkOrder workOrder) {
        switch (status) {
            case WorkOrderStatus.IN_TRANSIT:
//                WorkOrderInfoInRouteActivity.launch(appCompatActivity, workOrder);
//                ReplaceEquipmentsActivity.launch(appCompatActivity, workOrder);
                break;
            case WorkOrderStatus.IN_SITE:
                if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
                    if (!workOrder.isRegisterTimeArrival) {
//                        ArrivalTimeActivity.launch(appCompatActivity);
                    } else {
//                        RoutingApp.OnSuccessArrivalTimeEvent(appCompatActivity, workOrder);
                    }
                } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//                    RoutingApp.OnSuccessArrivalTimeEvent(appCompatActivity, workOrder);
                }
                break;
            case WorkOrderStatus.WORKING:
//                if (!workOrder.isTimeEstimated)
////                    UpdateDearTimeActivity.launch(appCompatActivity);
//                else
//                    RoutingApp.OnSuccessUpdateDearTime(appCompatActivity, workOrder);
//                break;
            case WorkOrderStatus.FINISHED:
//                LoadWorkOrdersActivity.launch(appCompatActivity);
                break;
            case WorkOrderStatus.FINISHED_IN_WAIT:
//                LoadWorkOrdersActivity.launch(appCompatActivity);
                break;
        }
    }

    public static void OnSuccessArrivalTimeEvent(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
//        RegisterLocationArrivalActivity.launch(mAppCompatActivity, workOrder);
    }

    public static void OnTravelProblemsEvent(AppCompatActivity appCompatActivity) {
        OptionsIncidentsMenuActivity.launch(appCompatActivity, ReasonDefinition.reasonTranslate);
    }

    public static void OnCompletedRequiredDistanceEvent(AppCompatActivity appCompatActivity) {
//        WorkOrderInfoInSiteActivity.launch(appCompatActivity);
    }

    public static void OnSuccessCancelWorkOrderEvent(AppCompatActivity appCompatActivity) {
        InWaitActivity.launch(appCompatActivity);
    }

    public static void OnSuccessRegisterLocationEvent(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.URBAN_REFERENCES, workOrder.id));
//        DetailOrderWorkActivity.launch(mAppCompatActivity);
    }

    public static void OnSuccessUpdateDearTime(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.ESTIMATED_TIME, workOrder.id));
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
            if (!workOrder.isAssignedSplitter) {
//                SplittersActivity.launch(mAppCompatActivity, true);
            } else {
                RoutingApp.OnSuccessUpdateSplitterAndCannotAdd(mAppCompatActivity, workOrder);
            }
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//            ConfigureDispositivesActivity.launch(mAppCompatActivity, workOrder);
        }
    }

    public static void OnSuccessActivatePBXEvent(AppCompatActivity mAppCompatActivity, WorkOrder mWorkOrder) {
//        ConfigureDispositivesActivity.launch(mAppCompatActivity, mWorkOrder);
    }

    public static void OnSuccessUpdateSplitterAndCanAdd(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.ASSIGNED_SPLITTER, workOrder.id));
//        LoadWorkOrdersActivity.launch(mAppCompatActivity);
    }

    public static void OnSuccessUpdateSplitterAndCannotAdd(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.ASSIGNED_SPLITTER, workOrder.id));
//        ActivationServiceActivity.launch(mAppCompatActivity, workOrder);
    }

    public static void OnSuccessActivationEvent(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
//        ConfigureDispositivesActivity.launch(mAppCompatActivity, workOrder);
    }

    public static void OnSuccessSettingEquipments(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
//            ResumeWorkOrderActivity.launch(mAppCompatActivity);
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//            ReplaceEquipmentsActivity.launch(mAppCompatActivity, workOrder);
        }
    }

    public static void OnSuccessReplaceEquipmentsEvent(AppCompatActivity appCompatActivity, WorkOrder mWorkOrder) {
//        ResumeWorkOrderActivity.launch(appCompatActivity);
    }

    public static void OnContinueResumeWorkOrderEvent(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
//            PaymentActivity.launch(appCompatActivity, workOrder);
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//            RoutingApp.OnSuccessPayment(appCompatActivity, workOrder);
        }
    }

    public static void OnSuccessPayment(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
//        QueueManager.add(new SetFlagJob(FlagType.PAID_ACCOUNT, workOrder.id));
//        SignatureActivity.launch(mAppCompatActivity, workOrder);
//        SurveyActivity.launch(appCompatActivity, workOrder);
    }

    public static void OnSuccessSurvey(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
//            FinishInstallationActivity.launch(appCompatActivity);
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
            RoutingApp.OnSuccessSurvey(appCompatActivity, workOrder);
        }
    }

    public static void OnSuccessSignatureEvent(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
//        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
//            FinishInstallationActivity.launch(appCompatActivity);
//        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//            RoutingApp.OnSuccessSurvey(appCompatActivity, workOrder);
//        }
    }

    public static void OnContinueFinishInstallationEvent(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.END_SURVEY, workOrder.id));
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
//            AcceptanceActActivity.launch(appCompatActivity, workOrder);
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//            AcceptanceActActivity.launch(appCompatActivity, workOrder);
        }
    }

    public static void OnSuccessCaptureAcceptanceCardEvidencesEvent(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
//            EvidenceActivity.launch(mAppCompatActivity, workOrder);
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//            EvidenceActivity.launch(mAppCompatActivity, workOrder);
        }
    }

    public static void OnSuccessCaptureEvidencesEvent(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.SENT_EVIDENCE, workOrder.id));
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
//            UsedMaterialActivity.launch(mAppCompatActivity, workOrder);
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
//            WorkOrderCompletedActivity.launch(mAppCompatActivity);
        }
    }

    public static void OnSuccessDiscountMaterials(AppCompatActivity mAppCompatActivity, WorkOrder workOrder) {
        QueueManager.add(new SetFlagJob(FlagType.SEND_MATERIALS, workOrder.id));
//        WorkOrderCompletedActivity.launch(mAppCompatActivity);
    }

    public static void OnUpdateSplitterEvent(AppCompatActivity mAppCompatActivity) {
        SplitterSelectedActivity.launch(mAppCompatActivity);
    }

}
