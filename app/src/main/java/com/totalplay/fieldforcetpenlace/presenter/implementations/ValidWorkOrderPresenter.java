package com.totalplay.fieldforcetpenlace.presenter.implementations;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ValidWorkOrderRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ValidWorkOrderResponse;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderStatus;
import com.totalplay.fieldforcetpenlace.library.utils.Training.TrainingManager;
import com.totalplay.fieldforcetpenlace.newpresenter.FFMPresenter;
import com.totalplay.fieldforcetpenlace.modules.enl.info_map.WorkOrderInfoActivity;
import com.totalplay.utils.MessageUtils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by totalplay on 7/10/17.
 * FieldForceManagement
 */

public class ValidWorkOrderPresenter extends FFMPresenter implements WSCallback {

    private String mLastStatusWorkOrder = WorkOrderStatus.FINISHED_IN_WAIT;
    private Timer mTimer;
    private ValidWorkOrderCallback mValidWorkOrderCallback;

    public interface ValidWorkOrderCallback {
        void onRequestLastStatusWorkOrder();

        void onLoadLastStatusWorkOrder(String status);
    }

    public ValidWorkOrderPresenter(AppCompatActivity appCompatActivity, ValidWorkOrderCallback validWorkOrderCallback) {
        super(appCompatActivity);
        mValidWorkOrderCallback = validWorkOrderCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void loadStatus() {
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mAppCompatActivity.runOnUiThread(() -> validWorkOrder());
            }
        }, 0, 10000);
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            try {
                mTimer.cancel();
            } catch (Exception ignored) {
            }
        }
    }

    private void validWorkOrder() {
        ValidWorkOrderRequest request = new ValidWorkOrderRequest(mWorkOrder.id);
        mWSManager.requestWs(ValidWorkOrderResponse.class, WSManager.WS.VALIDATE_WORK_ORDER, getDefinition().validWorkOrder(request));
    }

    private void successValidateWorkOrder(ValidWorkOrderResponse validWorkOrderResponse) {
        if (validWorkOrderResponse.result != null && validWorkOrderResponse.result.equals("0")) {
            if (validWorkOrderResponse.status != null) {
                mLastStatusWorkOrder = validWorkOrderResponse.statusId;
                mValidWorkOrderCallback.onLoadLastStatusWorkOrder(validWorkOrderResponse.status);
            }
        }
    }

    public void refreshStatus() {
        if (TrainingManager.TRAINING_ENABLED) {
//            LoadWorkOrdersActivity.launch(mAppCompatActivity);
        } else {
            if (mLastStatusWorkOrder != null) {
                switch (mLastStatusWorkOrder) {
                    case WorkOrderStatus.IN_TRANSIT:
//                        WorkOrderInfoActivity.launch(mAppCompatActivity);
                        break;
                    case WorkOrderStatus.WORKING:
//                        DetailOrderWorkActivity.launch(mAppCompatActivity);
                        break;
                    case WorkOrderStatus.IN_SITE:
                        WorkOrderInfoActivity.launch(mAppCompatActivity, "true");
                        break;
                    case WorkOrderStatus.WAITING:
                    case WorkOrderStatus.STAND_BY:
                    case WorkOrderStatus.FINISHED_IN_WAIT:
                        MessageUtils.toast(mContext, "Espere a que despacho revise su Orden de Trabajo");
                        break;
                    case "13":
                    default:
                        mFFMDataManager.deleteWorkOrder();
                        mAppCompatActivity.finish();
//                        LoadWorkOrdersActivity.launch(mAppCompatActivity);
                        break;
                }
            } else {
                MessageUtils.toast(mContext, "Favor de reintentar mas tarde");
            }
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        mValidWorkOrderCallback.onRequestLastStatusWorkOrder();
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.VALIDATE_WORK_ORDER)) {
            successValidateWorkOrder((ValidWorkOrderResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        mValidWorkOrderCallback.onLoadLastStatusWorkOrder("");
    }

    @Override
    public void onErrorConnection() {
        mValidWorkOrderCallback.onLoadLastStatusWorkOrder("");
    }

}
