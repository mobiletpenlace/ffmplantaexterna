package com.totalplay.fieldforcetpenlace.presenter.implementations;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.library.pojos.BaseDataManager;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.bus.BusManager;
import com.totalplay.fieldforcetpenlace.background.queue.QueueManager;
import com.totalplay.utils.Prefs;

import java.security.SecureRandom;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by totalplay on 11/23/16.
 * FieldForceManagement
 */
public class ApplicationPresenter {
    private Context context;

    public ApplicationPresenter(Context context) {
        this.context = context;
    }

    public void setup() {
        byte[] key = new byte[64];
        new SecureRandom(key);
        BaseDataManager.init(context);
        Prefs.setDefaultContext(context);
        Fabric.with(context, new Crashlytics());

        BusManager.init();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/ProximaNova-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        QueueManager.init(context);
    }

}
