package com.totalplay.fieldforcetpenlace.view.custom;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

public interface IconPagerAdapter {
    /**
     * Get icon representing the page at {@code index} in the adapter.
     */
    int getIconResId(int index);

    // From PagerAdapter
    int getCount();
}
