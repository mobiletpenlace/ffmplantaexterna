package com.totalplay.fieldforcetpenlace.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.Quotation;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.enl.configure_dispositives.GetEquipmentsPresenter;
import com.totalplay.fieldforcetpenlace.modules.enl.info.QuotationPresenter;
import com.totalplay.fieldforcetpenlace.view.adapters.ConfigAsmdAdapter;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;
import com.totalplay.fieldforcetpenlace.view.fragments.ConfigSDMAFragment;
import com.totalplay.fieldforcetpenlace.modules.enl.activation.DnsFragment;
import com.totalplay.mvp.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jorgehdezvilla on 09/10/17.
 * Enlace
 */

public class ConfigSDMActivity extends BaseFFMActivity implements QuotationPresenter.ActivationServiceCallback,
        GetEquipmentsPresenter.Callback {

    @BindView(R.id.act_config_sdma_view_pager)
    ViewPager pager;
    @BindView(R.id.act_config_sdma_view_tabs)
    TabLayout tabLayout;

    private ArrayList<BaseFFMFragment> mConfigFragment;

    private QuotationPresenter mQuotationPresenter;
    private GetEquipmentsPresenter mGetEquipmentsPresenter;

    public static void launch(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        Intent intent = new Intent(appCompatActivity, ConfigSDMActivity.class);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_config_sdm);
        setTitle("");
        ButterKnife.bind(this);

        mQuotationPresenter.loadQuotation();
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mQuotationPresenter = new QuotationPresenter(this, this),
                mGetEquipmentsPresenter = new GetEquipmentsPresenter(this, this)
        };
    }

    public void onLoadedTabs() {
        Equipment equipmentPBX = mFFMDataManager.queryWhere(Equipment.class).filter("dispositive", EquipmentType.PBX).findFirst();
        if (equipmentPBX == null) {
            mWorkOrder.isSetttingPXB = true;
            mFFMDataManager.tx(tx -> tx.save(mWorkOrder));
        }
        if (!mWorkOrder.isSetttingPXB) {
            mConfigFragment = new ArrayList<BaseFFMFragment>() {{
                add(ConfigSDMAFragment.newInstance());
                add(DnsFragment.newInstance());
            }};
        } else {
            mConfigFragment = new ArrayList<BaseFFMFragment>() {{
                add(DnsFragment.newInstance());
            }};
        }


        pager.setAdapter(new ConfigAsmdAdapter(getSupportFragmentManager(), mConfigFragment));
        tabLayout.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(pager));
    }

    @Override
    public void onSuccessLoadQuotationInfo(Quotation quotationInfo) {
        mGetEquipmentsPresenter.loadEquipments();
    }

    @Override
    public void onSuccessLoadEquipments(List<Equipment> equipmentList) {
        onLoadedTabs();
    }

    @Override
    public void onErrorGetEquipmentsEvent() {
        finish();
    }

    @Override
    public void onErrorModelsInfoEquipments() {

    }
}
