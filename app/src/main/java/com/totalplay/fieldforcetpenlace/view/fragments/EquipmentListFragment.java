package com.totalplay.fieldforcetpenlace.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.view.custom.ConfigureDispositiveView;
import com.totalplay.mvp.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by NS-TOTALPLAY on 25/09/2017.
 * Enlace
 */

public class EquipmentListFragment extends BaseFFMFragment {

    @BindView(R.id.fr_base_linear)
    ViewGroup mViewGroup;

    private List<ConfigureDispositiveView> mConfigureDispositiveViewList = new ArrayList<>();

    public static EquipmentListFragment newInstance(String equipmentType) {
        Bundle bundle = new Bundle();
        bundle.putString(Keys.EQUIPMENT_TYPE, equipmentType);
        EquipmentListFragment fragment = new EquipmentListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_base_linear, container, false);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (ConfigureDispositiveView view : mConfigureDispositiveViewList) {
            view.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        String equipmentType = getArguments().getString(Keys.EQUIPMENT_TYPE);
        if (equipmentType == null) equipmentType = "";
        List<Equipment> equipmentList;
        if (equipmentType.equals(EquipmentType.PHONES)) {
            equipmentList = new ArrayList<>();
            equipmentList.addAll(mFFMDataManager.queryWhere(Equipment.class)
                    .filter("dispositive", EquipmentType.PBX)
                    .list());
            equipmentList.addAll(mFFMDataManager.queryWhere(Equipment.class)
                    .contain("dispositive", EquipmentType.TEL)
                    .list());
        } else if (equipmentType.equals(EquipmentType.OTHERS)) {
            equipmentList = mFFMDataManager.queryWhere(Equipment.class)
                    .filterWhereNot("dispositive", EquipmentType.ONT)
                    .filterWhereNot("dispositive", EquipmentType.PBX)
                    .filterWhereNot("dispositive", EquipmentType.WIFI_EXTENDER)
                    .list();
        } else {
            equipmentList = mFFMDataManager.queryWhere(Equipment.class).list();
        }

        int position = 0;
        for (Equipment equipment : equipmentList) {
            position++;
            View _view = LayoutInflater.from(getContext()).inflate(R.layout.enl_item_configure_equipment, null);
            ConfigureDispositiveView configureDispositiveView;
            if (equipmentType.equals(EquipmentType.PHONES)) {
                if (WorkOrderOwnerType.INTEGRATOR.contains(mWorkOrder.ownerId)) {
                    configureDispositiveView = new ConfigureDispositiveView((AppCompatActivity) getActivity(), _view, equipment, position, true);
                } else {
                    configureDispositiveView = new ConfigureDispositiveView((AppCompatActivity) getActivity(), _view, equipment, position, false);
                }
            } else {
                configureDispositiveView = new ConfigureDispositiveView((AppCompatActivity) getActivity(), _view, equipment, position, true);
            }

            mConfigureDispositiveViewList.add(configureDispositiveView);
            mViewGroup.addView(_view);
        }
    }

    public boolean isValidInfoDispositives() {
        boolean valid = true;
        for (ConfigureDispositiveView view : mConfigureDispositiveViewList) {
            valid &= view.isValid();
        }
        return valid;
    }

    public List<Equipment> getEquipmentList() {
        List<Equipment> equipmentList = new ArrayList<>();
        for (ConfigureDispositiveView view : mConfigureDispositiveViewList) {
            Equipment equipment = view.getEquipment();
            if (equipment != null)
                equipmentList.add(equipment);
        }
        return equipmentList;
    }


    @Override
    public BasePresenter getPresenter() {
        return null;
    }

}
