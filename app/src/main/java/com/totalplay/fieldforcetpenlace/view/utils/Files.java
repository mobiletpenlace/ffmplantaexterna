package com.totalplay.fieldforcetpenlace.view.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Files {


    /*

    Initialize somewhere in the code

     */
    public static String FILES_PATH;
    public static String STORE_PATH;

    @SuppressWarnings("resource")
    public static Object loadJSON(String path, String name, Class<?> clss) {
        File file = new File(path, name);
        if (file.exists()) {
            try {
                Gson gson = new Gson();
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String cad = "";
                StringBuilder sb = new StringBuilder();
                while ((cad = br.readLine()) != null) {
                    sb.append(cad);
                }
                return gson.fromJson(sb.toString(), clss);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public static void saveJSON(Object response, String name, String path) {
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdir();
        try {
            File file = new File(path + "/" + name);
            if (!file.exists()) {
                file.createNewFile();
            }
            OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(file, false));
            Gson gson = new Gson();
            String json = gson.toJson(response);
            fout.write(json);
            fout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Uri saveImage(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        Uri bmpUri = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static void unzip(File file, Context context) throws IOException {
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        ZipInputStream zin = new ZipInputStream(in);
        ZipEntry e;
        try {
            String filePath = file.getAbsolutePath().
                    substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
            while ((e = zin.getNextEntry()) != null) {
                String s = e.getName();
                File f = new File(filePath + "/" + s);
                Log.d("unzipping", s);
                FileOutputStream out = new FileOutputStream(f);
                byte[] b = new byte[512];
                int len = 0;
                while ((len = zin.read(b)) != -1) {
                    out.write(b, 0, len);
                }
                out.close();
            }
        } catch (IOException ex) {
            if (zin != null) {
                zin.close();
            }
            throw ex;
        }
        zin.close();
    }

    public static byte[] getBytesFromFile(Uri uri) {
        try {
            FileInputStream is = new FileInputStream(new File(uri.getPath()));
            ByteArrayOutputStream os = new ByteArrayOutputStream(1024);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) >= 0) {
                os.write(buffer, 0, len);
            }
            String image = Base64.encodeToString(os.toByteArray(), Base64.NO_WRAP);
            Log.d("Image Compressed", image);
            return os.toByteArray();
        } catch (Exception e) {
            return new byte[0];
        }
    }

    public static void copyFromAssets(Context context, String fileName, String path) {
        InputStream mInput = null;
        try {
            mInput = context.getAssets().open(fileName);
            File f = new File(path + fileName);
            if (!f.exists())
                f.createNewFile();
            OutputStream mOutput = new FileOutputStream(f);
            byte[] mBuffer = new byte[1024];
            int mLength;
            while ((mLength = mInput.read(mBuffer)) > 0) {
                mOutput.write(mBuffer, 0, mLength);
            }
            mOutput.flush();
            mOutput.close();
            mInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFromAssetsAndRename(Context context, String assetName, String destName, String path) {
        InputStream mInput = null;
        try {
            mInput = context.getAssets().open(assetName);
            File f = new File(path + destName);
            if (!f.exists())
                f.createNewFile();
            OutputStream mOutput = new FileOutputStream(f);
            byte[] mBuffer = new byte[1024];
            int mLength;
            while ((mLength = mInput.read(mBuffer)) > 0) {
                mOutput.write(mBuffer, 0, mLength);
            }
            mOutput.flush();
            mOutput.close();
            mInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap getScaledBitmap(byte[] data) {
        int targetW = 250;
        int targetH = 250;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeByteArray(data, 0, data.length, bmOptions);
    }

    public static Bitmap resizeBitmap(Bitmap bitmapToResize, int maxDimension) {

        int bitmapHeight = bitmapToResize.getHeight();
        int bitmapWidth = bitmapToResize.getWidth();

        Bitmap newBitmap;

        if (bitmapWidth > bitmapHeight) {
            int newHeight = (int) ((float) maxDimension * ((float) bitmapHeight / (float)
                    bitmapWidth));
            newBitmap = Bitmap.createScaledBitmap(bitmapToResize, maxDimension, newHeight, false);
        } else if (bitmapWidth < bitmapHeight) {
            int newWidth = (int) ((float) maxDimension * ((float) bitmapWidth / (float)
                    bitmapHeight));
            newBitmap = Bitmap.createScaledBitmap(bitmapToResize, newWidth, maxDimension, false);
        } else {
            newBitmap = Bitmap.createScaledBitmap(bitmapToResize, maxDimension, maxDimension,
                    false);
        }

        return newBitmap;

    }

}