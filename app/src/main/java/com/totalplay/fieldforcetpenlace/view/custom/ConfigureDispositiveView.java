package com.totalplay.fieldforcetpenlace.view.custom;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.EquipmentModel;
import com.totalplay.fieldforcetpenlace.view.activities.SimpleScannerActivity;
import com.totalplay.fieldforcetpenlace.view.utils.MacEditText;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by totalplay on 11/28/16.
 * FieldForceManagement
 */

public class ConfigureDispositiveView {

    @BindView(R.id.item_equipment_type_name)
    TextView mEquipmentTypeNameTextView;
    @BindView(R.id.item_equipment_equipment_name)
    TextView mEquipmentNameTextView;
    @BindView(R.id.item_equipment_serial_number)
    MaterialEditText mSerialNumberTextView;
    @BindView(R.id.item_equipment_mac)
    MacEditText mMacEditTextView;
    @BindView(R.id.item_equipment_model_label)
    TextView mModelCatalogTextView;
    @BindView(R.id.item_equipment_model)
    CatalogEditText<EquipmentModel> mModelCatalogEditText;

    @BindView(R.id.item_equipment_serial_number_scan)
    ImageView mSerialNumberScanImageView;

    @BindView(R.id.item_equipment_mac_content)
    ViewGroup mMacContentViewGroup;

    private AppCompatActivity appCompatActivity;
    private Equipment mEquipment;
    private boolean wasScanNumberSerial = false;

    private FormValidator mRequiredFormValidator;
    public boolean mIsRequired = true;

    public ConfigureDispositiveView(AppCompatActivity appCompatActivity, View rootView, Equipment equipment, Integer position, boolean requiredCaptureInfo) {
        ButterKnife.bind(this, rootView);
        mEquipment = equipment;
        this.appCompatActivity = appCompatActivity;
        this.mEquipment = equipment;

        mMacEditTextView.registerAfterMacTextChangedCallback();
        mEquipmentTypeNameTextView.setText(String.format(Locale.getDefault(), "%d.- %s", position, equipment.dispositive));
        mEquipmentNameTextView.setText(equipment.name);
        mSerialNumberTextView.setText(equipment.serialNumber);
        mMacEditTextView.setText(equipment.mac);
        if (equipment.equipmentModel != null)
            mModelCatalogEditText.setText(equipment.equipmentModel.modelDescription);

        mSerialNumberScanImageView.setTag(equipment);
        mSerialNumberTextView.setTag(equipment);
        mMacEditTextView.setTag(equipment);
        mModelCatalogEditText.setTag(equipment);

        if (equipment.dispositive.equals("Tel IP")) {
            mMacContentViewGroup.setVisibility(View.VISIBLE);
        }

        mModelCatalogEditText.setCatalogs(equipment.equipmentsModels);

        mRequiredFormValidator = new FormValidator(appCompatActivity, true);
        mIsRequired = requiredCaptureInfo;
        if (equipment.dispositive.equals(EquipmentType.PBX)) {
            mRequiredFormValidator.addValidators(
                    new EditTextValidator(mSerialNumberTextView, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                    new EditTextValidator(mSerialNumberTextView, 12, 50, R.string.dialog_error_length_serial_number)
            );
            mModelCatalogEditText.setVisibility(View.GONE);
            mModelCatalogTextView.setVisibility(View.GONE);
        } else {
            mRequiredFormValidator.addValidators(
                    new EditTextValidator(mSerialNumberTextView, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                    new EditTextValidator(mSerialNumberTextView, 12, 50, R.string.dialog_error_length_serial_number),
                    new EditTextValidator(mModelCatalogEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
            );
        }

    }

    public boolean isValid() {
        boolean valid = true;
        if (mIsRequired) {
            valid = mRequiredFormValidator.isValid();
        } else {
            if (!mSerialNumberTextView.getText().toString().trim().equals("")) {
                valid = mRequiredFormValidator.isValid();
            }
        }

        return valid;
    }

    public Equipment getEquipment() {
        if (!mIsRequired && mSerialNumberTextView.getText().toString().trim().equals("")) {
            return null;
        }
        mEquipment.serialNumber = mSerialNumberTextView.getText().toString().trim();
        mEquipment.mac = mMacEditTextView.getText().toString().trim();
        mEquipment.equipmentModel = mModelCatalogEditText.getSelectedValue();
        return mEquipment;
    }

    @OnClick(R.id.item_equipment_serial_number_scan)
    void onScanNumberSerial() {
        wasScanNumberSerial = true;
        SimpleScannerActivity.launch(appCompatActivity);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            String serialNumber = data.getExtras().getString(Keys.SCAN_RESULT);
            if (wasScanNumberSerial) {
                wasScanNumberSerial = false;
                mSerialNumberTextView.setText(serialNumber);
            }
        }
    }

}