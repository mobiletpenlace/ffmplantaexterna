package com.totalplay.fieldforcetpenlace.view.adapters;

import android.view.View;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleAdapter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseViewHolder;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jorgeHdez on 1/5/17.
 * FMP
 */
@SuppressWarnings({"unchecked"})
public class SettingsEquipmentsAdapter extends BaseSimpleAdapter<InfoEquipment, SettingsEquipmentsAdapter.AdapterViewHolder> {

    private View.OnClickListener mOnClickListener;

    public SettingsEquipmentsAdapter(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemLayout() {
        return R.layout.enl_item_setting_equipment;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new AdapterViewHolder(view);
    }

    class AdapterViewHolder extends BaseViewHolder<InfoEquipment> {

        @BindView(R.id.item_replace_equipment_model)
        TextView mModelTextView;
        @BindView(R.id.item_setting_equipment_serial_value)
        TextView mSerialTextView;
        @BindView(R.id.item_replace_equipment_mac_value)
        TextView mMacTextView;

        AdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, InfoEquipment dispositive) {
            mModelTextView.setText(String.format(Locale.getDefault(), "%s - %s", dispositive.type, dispositive.equipmentModel));
            mSerialTextView.setText(dispositive.numSerie);
            mMacTextView.setText(dispositive.mac);
            holder.itemView.setTag(dispositive);
        }
    }


}
