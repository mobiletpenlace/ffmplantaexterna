package com.totalplay.fieldforcetpenlace.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by :v on 8/14/17.
 */
public class SplitterWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private Map<Marker, InfoWindowSplitterContent> markersContent = new HashMap<>();
    private InfoWindowSplitterContent infoWindowSplitterContent;

    public SplitterWindowAdapter(Context context, Map<Marker, InfoWindowSplitterContent> markersContent) {
        this.context = context;
        this.markersContent = markersContent;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoWindow(Marker marker) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        infoWindowSplitterContent = markersContent.get(marker);
        if (infoWindowSplitterContent != null) {
            return loadContent(inflater.inflate(R.layout.enl_item_marker_custom_splitter, null), marker);
        }
        return loadContent(inflater.inflate(R.layout.enl_item_marker_user, null), marker);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        infoWindowSplitterContent = markersContent.get(marker);
        if (infoWindowSplitterContent != null) {
            return loadContent(LayoutInflater.from(context).inflate(R.layout.enl_item_marker_custom_splitter, null), marker);
        }
        return loadContent(LayoutInflater.from(context).inflate(R.layout.enl_item_marker_user, null), marker);
    }

    private View loadContent(View contentView, Marker marker) {
        infoWindowSplitterContent = markersContent.get(marker);
        if (infoWindowSplitterContent != null) {
            TextView titleTextView = contentView.findViewById(R.id.itm_marker_splitter_tag);
            TextView countTextView = contentView.findViewById(R.id.itm_marker_splitter_description_tag);
            ViewGroup statusImageView = contentView.findViewById(R.id.itm_marker_splitter_status);

            infoWindowSplitterContent = markersContent.get(marker);
            titleTextView.setText(infoWindowSplitterContent.label);
            countTextView.setText(infoWindowSplitterContent.number);

            GradientDrawable gradientDrawable = (GradientDrawable) statusImageView.getBackground();
            gradientDrawable.setColor(infoWindowSplitterContent.color);
        }

        return contentView;
    }

    public static class InfoWindowSplitterContent {

        public Splitter splitter;
        public String label;
        public String number;
        public int color;
    }

}
