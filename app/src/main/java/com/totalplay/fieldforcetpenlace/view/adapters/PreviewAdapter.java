package com.totalplay.fieldforcetpenlace.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.PreviewView;
import com.totalplay.fieldforcetpenlace.view.fragments.PresentationFragment;

import java.util.List;

/**
 * Created by reype on 27/10/2017.
 * Enlace
 */

public class PreviewAdapter extends FragmentPagerAdapter {


    private List<PreviewView> mSlides;

    public PreviewAdapter(FragmentManager fragmentManager, List<PreviewView> slides) {
        super(fragmentManager);
        this.mSlides = slides;
    }

    @Override
    public Fragment getItem(int position) {
        return PresentationFragment.newInstance(mSlides.get(position));
    }


    @Override
    public int getCount() {
        return mSlides.size();
    }

}