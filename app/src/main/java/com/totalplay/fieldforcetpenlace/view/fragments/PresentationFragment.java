package com.totalplay.fieldforcetpenlace.view.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PreviewView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by reype on 25/10/2017.
 * Enlace
 */

public class PresentationFragment extends BaseFFMFragment {

    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.description)
    TextView mDescription;
    @BindView(R.id.image)
    ImageView mImage;
    private PreviewView mPreviewView;

    public static PresentationFragment newInstance(PreviewView previewView) {
        PresentationFragment presentationFragment = new PresentationFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Keys.EXTRA_PREVIEW_VIEW, previewView);
        presentationFragment.setArguments(bundle);
        return presentationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_presentation, container, false);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        mPreviewView = (PreviewView) getArguments().getSerializable(Keys.EXTRA_PREVIEW_VIEW);
        mTitle.setText(mPreviewView.title);
        mDescription.setText(mPreviewView.description);
        Glide.with(this)
                .load(mPreviewView.imageDrawable)
                .fitCenter()
                .into(mImage);
//        mImage.setImageResource(mPreviewView.imageDrawable);
    }

}
