package com.totalplay.fieldforcetpenlace.view.adapters;

import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.DN;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleAdapter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseViewHolder;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jorgeHdez on 1/5/17.
 * FMP
 */
@SuppressWarnings({"unchecked"})
public class PhoneServiceAdapter extends BaseSimpleAdapter<ServicePlan, PhoneServiceAdapter.AdapterViewHolder> {

    @Override
    public int getItemLayout() {
        return R.layout.enl_item_service_phone;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new AdapterViewHolder(view);
    }

    class AdapterViewHolder extends BaseViewHolder<ServicePlan> {

        @BindView(R.id.item_service_phone_plan_id)
        EditText mPlanIdEditText;
        @BindView(R.id.item_service_phone_dn_quantity)
        EditText mDnQuantityEditText;
        @BindView(R.id.item_service_phone_dn_one)
        RadioButton mDnOneRadioButton;
        @BindView(R.id.item_service_phone_dn_two)
        RadioButton mDnTwoRadioButton;

        AdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, ServicePlan servicePlan) {
            mPlanIdEditText.setText(servicePlan.planName);
            mDnQuantityEditText.setText(servicePlan.dnAmount);
            if (servicePlan.dns.size() > 0) {
                DN firstDn = servicePlan.dns.get(0);
                if (servicePlan.dns.size() > 1) {
                    DN lastdN = servicePlan.dns.get(servicePlan.dns.size() - 1);
                    mDnTwoRadioButton.setText(lastdN.dn);
                }
                if (servicePlan.wasAdditional.equals("false")) {
                    mDnOneRadioButton.setText(String.format(Locale.getDefault(), "%s  (Línea Principal)", firstDn.dn));
                } else {
                    mDnOneRadioButton.setText(String.format(Locale.getDefault(), "%s ", firstDn.dn));
                }

            }
        }
    }

}
