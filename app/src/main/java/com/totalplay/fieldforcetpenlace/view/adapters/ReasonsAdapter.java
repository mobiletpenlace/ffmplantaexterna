package com.totalplay.fieldforcetpenlace.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;

import java.util.ArrayList;

/**
 * Created by NS-TOTALPLAY on 22/05/2017.
 */

public class ReasonsAdapter extends RecyclerView.Adapter<ReasonsAdapter.ReasonViewHolder> {
    private ArrayList<String> reasonList;
    public Context context;
    private LayoutInflater inflater;
    private static ItemClickListener clickListener;
    public static int select_item = -1;
    private int row_index = -1;

    public ReasonsAdapter(Context context, ArrayList<String> data){
        inflater = LayoutInflater.from(context);
        reasonList = data;
        this.context = context;
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        clickListener = itemClickListener;
    }

    @Override
    public ReasonsAdapter.ReasonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.enl_simple_list_items,parent,false);
        return new ReasonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReasonViewHolder holder, final int position) {
        Log.e("Position=",String.valueOf(position));
        // select_item = positions;
        holder.reasonTxt.setText(reasonList.get(position));
        holder.reasonTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position < reasonList.size())
                {
                    row_index= position;
                    select_item = position;
                    Log.e("Row_Index",String.valueOf(row_index));
                    notifyDataSetChanged();
                }
            }
        });
        holder.reasonTxt.setTextColor(row_index == position ? Color.parseColor("#000000"): Color.parseColor("#44000000") );
        Log.e("Positions",String.valueOf(position));
    }

    @Override
    public int getItemCount() {
        return reasonList.size();
    }

    static class ReasonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView reasonTxt;

        ReasonViewHolder(View itemView) {
            super(itemView);
            reasonTxt = itemView.findViewById(R.id.list_simple_item);
            reasonTxt.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onClick(v, getAdapterPosition());
            }
        }
    }
    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
