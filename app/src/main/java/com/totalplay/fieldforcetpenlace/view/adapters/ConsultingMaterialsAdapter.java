package com.totalplay.fieldforcetpenlace.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMaterials;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by charlssalazar on 28/06/17.
 * FFM
 */

public class ConsultingMaterialsAdapter extends RecyclerView.Adapter<ConsultingMaterialsAdapter.SimpleViewHolder> {

    private ArrayList<GetMaterials.Materials> materialsList;

    public ConsultingMaterialsAdapter() {
        this.materialsList = new ArrayList<>();
    }

    @Override
    public ConsultingMaterialsAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.enl_item_materials, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ConsultingMaterialsAdapter.SimpleViewHolder holder, int position) {
        GetMaterials.Materials materials = materialsList.get(position);
        holder.nameMaterialTextView.setText(materials.description);
        holder.quantityMaterialTextView.setText(materials.quantity);
    }

    @Override
    public int getItemCount() {
        return materialsList.size();
    }

    public void update(ArrayList<GetMaterials.Materials> listMaterials) {
        this.materialsList = listMaterials;
        notifyDataSetChanged();
    }


    static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_material_name_material)
        TextView nameMaterialTextView;
        @BindView(R.id.item_materials_quantity)
        TextView quantityMaterialTextView;
        @BindView(R.id.item_material_material)
        LinearLayout materialLinearLayout;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
