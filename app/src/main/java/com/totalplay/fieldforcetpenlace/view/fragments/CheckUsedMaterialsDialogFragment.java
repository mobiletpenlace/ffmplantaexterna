package com.totalplay.fieldforcetpenlace.view.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.UsedMaterialsResponse;
import com.totalplay.fieldforcetpenlace.library.interfaces.OnCheckableChange;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Material;
import com.totalplay.fieldforcetpenlace.view.adapters.CheckableAdapter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckUsedMaterialsDialogFragment extends DialogFragment {

    @BindView(R.id.dialog_check_used_materials_list)
    ListView mRecyclerView;
    private OnMaterialsCheckedListener onMaterialsCheckedListener;

    public static CheckUsedMaterialsDialogFragment newInstance(UsedMaterialsResponse.MaterialCatalog materialCatalog) {
        CheckUsedMaterialsDialogFragment fragment = new CheckUsedMaterialsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Keys.EXTRA_MATERIALS, materialCatalog);
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_title_select_materials)
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                onMaterialsCheckedListener.onMaterialsSelected(
                                        ((CheckableAdapter<Material>)mRecyclerView.getAdapter()).getCheckedItems());
                            }
                        }
                )
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                onMaterialsCheckedListener.onCancelSelected();
                                dialog.dismiss();
                            }
                        }
                );

        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        @SuppressLint("InflateParams") View view = layoutInflater.inflate(R.layout.enl_dialog_check_used_materials, null);
        ButterKnife.bind(this, view);
        UsedMaterialsResponse.MaterialCatalog materialCatalog = (UsedMaterialsResponse.MaterialCatalog) getArguments().getSerializable(Keys.EXTRA_MATERIALS);
        if (materialCatalog != null) {
            Collections.sort(materialCatalog.materials, new Comparator<Material>() {
                @Override
                public int compare(final Material object1, final Material object2) {
                    return object1.description.compareTo(object2.description);
                }
            });
            mRecyclerView.setAdapter(new CheckableAdapter<>(getContext(), materialCatalog.materials, new OnCheckableChange() {
                @Override
                public void onCheckedChange(CheckBox checkBox, int position, List objects) {

                }
            }));
        }
        builder.setView(view);
        return builder.create();
    }

    public void setOnMaterialsCheckedListener(OnMaterialsCheckedListener onMaterialsCheckedListener) {
        this.onMaterialsCheckedListener = onMaterialsCheckedListener;
    }

    public interface OnMaterialsCheckedListener {
        void onMaterialsSelected(List<Material> mMaterials);

        void onCancelSelected();
    }
}
