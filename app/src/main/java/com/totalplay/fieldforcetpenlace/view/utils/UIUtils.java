package com.totalplay.fieldforcetpenlace.view.utils;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by totalplay on 11/28/16.
 * FieldForceManagement
 */
public class UIUtils {

    public static void disableEnableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls(enable, (ViewGroup)child);
            }
        }
    }
}
