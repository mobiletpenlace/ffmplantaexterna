package com.totalplay.fieldforcetpenlace.view.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.totalplay.fieldforcetpenlace.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoAdapter extends ArrayAdapter<Bitmap> {

    public PhotoAdapter(Context context) {
        super(context, R.layout.enl_item_photo, new ArrayList<Bitmap>());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.enl_item_photo, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.image.setImageBitmap(getItem(position));
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.item_photo_image)
        ImageView image;

        public ViewHolder(View v) {
            ButterKnife.bind(this, v);
        }
    }
}