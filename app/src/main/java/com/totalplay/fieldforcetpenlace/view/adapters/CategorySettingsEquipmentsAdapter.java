package com.totalplay.fieldforcetpenlace.view.adapters;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleAdapter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleRecyclerView;
import com.totalplay.view.BaseSimpleRecyclerView.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jorgeHdez on 1/5/17.
 * FMP
 */
@SuppressWarnings({"unchecked"})
public class CategorySettingsEquipmentsAdapter extends BaseSimpleAdapter<List<InfoEquipment>, CategorySettingsEquipmentsAdapter.AdapterViewHolder> {

    private View.OnClickListener mOnClickListener;
    private List<String> mCategoryShowden = new ArrayList<>();

    private View.OnClickListener mClickListener = view -> {
        String dispositiveCategory = (String) view.getTag();
        if (searchCategory(dispositiveCategory)) {
            mCategoryShowden.remove(dispositiveCategory);
        } else {
            mCategoryShowden.add(dispositiveCategory);
        }
        notifyDataSetChanged();
    };

    private boolean searchCategory(String categoryName) {
        for (String category : mCategoryShowden) {
            if (category.equals(categoryName)) {
                return true;
            }
        }
        return false;
    }

    public CategorySettingsEquipmentsAdapter(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemLayout() {
        return R.layout.enl_item_category_dispositive;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new AdapterViewHolder(view);
    }

    class AdapterViewHolder extends BaseViewHolder<List<InfoEquipment>> {

        @BindView(R.id.item_category_dispositive_arrow)
        ImageView mImageView;
        @BindView(R.id.item_category_dispositive_content)
        ViewGroup mContentView;
        @BindView(R.id.item_category_dispositive_title)
        TextView mCategoryDispositivetitle;

        BaseSimpleRecyclerView mBaseSimpleRecyclerView;

        AdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(mOnClickListener);

            mBaseSimpleRecyclerView = new BaseSimpleRecyclerView(itemView, R.id.item_category_dispositive_list)
                    .setAdapter(new SettingsEquipmentsAdapter(mOnClickListener));
        }

        @Override
        public void populate(BaseViewHolder holder, int position, List<InfoEquipment> dispositiveList) {
            InfoEquipment dispositive = dispositiveList.get(0);
            mCategoryDispositivetitle.setText(String.format(Locale.getDefault(), "%s (%d)", dispositive.type, dispositiveList.size()));
            mContentView.setOnClickListener(mClickListener);

            if (!searchCategory(dispositive.type)) {
                mBaseSimpleRecyclerView.update(new ArrayList());
                mImageView.setImageDrawable(ContextCompat.getDrawable(mContentView.getContext(), R.drawable.ic_arrow_drop_down));
            } else {
                mBaseSimpleRecyclerView.update(dispositiveList);
                mImageView.setImageDrawable(ContextCompat.getDrawable(mContentView.getContext(), R.drawable.ic_arrow__drop_up));
            }

            holder.itemView.setOnClickListener(mClickListener);
            holder.itemView.setTag(dispositive.type);
        }
    }


}
