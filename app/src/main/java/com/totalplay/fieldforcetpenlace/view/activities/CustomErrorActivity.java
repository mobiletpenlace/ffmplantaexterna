package com.totalplay.fieldforcetpenlace.view.activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponse;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.utils.Prefs;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.config.CaocConfig;

public class CustomErrorActivity extends AppCompatActivity implements WSCallback {

    private int mCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_custom_error);

        TextView idEmployee = findViewById(R.id.custom_error_id_employee);
        User user = (User) Prefs.instance().object(Keys.EXTRA_USER, User.class);
        if (user != null && user.operatorId != null && user.employeeId != null) {
            idEmployee.setText(String.format("%s %s", user.employeeId, user.operatorId));
        }

        String errorText = CustomActivityOnCrash.getStackTraceFromIntent(getIntent());

        Button restartButton = findViewById(R.id.restart_button);
        TextView textView = findViewById(R.id.error);

        ImageView imageView = findViewById(R.id.custom_error_id_logo);
        imageView.setOnClickListener(v -> {
            mCount++;
            if (mCount > 6) {
                textView.setVisibility(View.VISIBLE);
            }
        });
        textView.setVisibility(View.GONE);
//        textView.setText(errorText);

        BaseWSManager mWSManager = WSManager.init().settings(this, this);
        mWSManager.requestWs(WSBaseResponse.class, WSManager.WS.CRASH, WebServices.totalApps().crash("FFMEnlace", errorText));

        final CaocConfig config = CustomActivityOnCrash.getConfigFromIntent(getIntent());

        if (config.isShowRestartButton() && config.getRestartActivityClass() != null) {
            restartButton.setText(R.string.error_restart_app);
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Error", errorText);
            clipboard.setPrimaryClip(clip);
            restartButton.setOnClickListener(v -> CustomActivityOnCrash.restartApplication(CustomErrorActivity.this, config));
        } else {
            restartButton.setOnClickListener(v -> CustomActivityOnCrash.closeApplication(CustomErrorActivity.this, config));
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {

    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {

    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {

    }

    @Override
    public void onErrorConnection() {

    }
}