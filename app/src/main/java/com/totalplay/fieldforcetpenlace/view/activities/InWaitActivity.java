package com.totalplay.fieldforcetpenlace.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.presenter.implementations.ValidWorkOrderPresenter;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NS-TOTALPLAY on 29/06/2017.
 * FFM
 */

public class InWaitActivity extends BaseFFMActivity implements ValidWorkOrderPresenter.ValidWorkOrderCallback {

    @BindView(R.id.act_label_status)
    TextView mStatusTextView;
    @BindView(R.id.act_btnRefresh)
    ImageView mRefreshImageView;

    private RotateAnimation mSynchronizationAnimation;

    private ValidWorkOrderPresenter mValidWorkOrderPresenter;

    public static void launch(Context context) {
        context.startActivity(new Intent(context, InWaitActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_in_site);
        ButterKnife.bind(this);

        configureSyncAnimation();

        mRefreshImageView.startAnimation(mSynchronizationAnimation);
        mValidWorkOrderPresenter.loadStatus();
    }

    private void configureSyncAnimation() {
        mSynchronizationAnimation = new RotateAnimation(0.0f, 360.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        mSynchronizationAnimation.setInterpolator(new LinearInterpolator());
        mSynchronizationAnimation.setRepeatCount(Animation.INFINITE);
        mSynchronizationAnimation.setDuration(1000);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mValidWorkOrderPresenter = new ValidWorkOrderPresenter(this, this);
    }

    @Override
    public void onBackPressed() {
        MessageUtils.toast(this, R.string.dialog_error_inwait);
    }

    @OnClick(R.id.act_check_status)
    public void onClickRefreshStatus(View view) {
        mValidWorkOrderPresenter.refreshStatus();
    }

    @Override
    public void onRequestLastStatusWorkOrder() {
        mStatusTextView.setText("Actualizando");
    }

    @Override
    public void onLoadLastStatusWorkOrder(String status) {
        mStatusTextView.setText(status);
    }
}