package com.totalplay.fieldforcetpenlace.view.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleManager;
import com.totalplay.fieldforcetpenlace.modules.definition.FlowKey;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_splash);
        long SPLASH_DELAY = 3000;

        Intent msgIntent = new Intent(this, TotalPlayLocationService.class);
        startService(msgIntent);

//        TimerTask splashTask = new TimerTask() {
//            @Override
//            public void run() {
//
//            }
//        };
//        new Timer().schedule(splashTask, SPLASH_DELAY);
        ModuleManager.getInstance().startFlow(SplashActivity.this, FlowKey.MAIN_FLOW, null);
        finish();
//        finish();
    }
}