package com.totalplay.fieldforcetpenlace.view.adapters.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by jorgehdezvilla on 19/12/17.
 * FFM Enlace
 */

public class SimpleBaseTabAdapter extends FragmentStatePagerAdapter {

    private List<BaseTab> mBaseTabs;

    public SimpleBaseTabAdapter(FragmentManager supportFragmentManager, List<BaseTab> baseTabs) {
        super(supportFragmentManager);
        this.mBaseTabs = baseTabs;
    }

    @Override
    public Fragment getItem(int position) {
        return mBaseTabs.get(position).fragment;
    }

    @Override
    public int getCount() {
        return mBaseTabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mBaseTabs.get(position).title;
    }

}
