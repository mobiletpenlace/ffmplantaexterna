package com.totalplay.fieldforcetpenlace.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.totalplay.fieldforcetpenlace.modules.enl.activation.InternetFragment;
import com.totalplay.fieldforcetpenlace.modules.enl.activation.PhoneFragment;
import com.totalplay.fieldforcetpenlace.modules.enl.activation.WifiFragment;
import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

public class ActivationServiceFragmentAdapter extends FragmentPagerAdapter {

    List<BaseFFMFragment> baseFragments = new ArrayList<>();

    private InternetFragment internetFragment;
    private PhoneFragment phoneFragment;
    private WifiFragment wifiFragment;

    public ActivationServiceFragmentAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void addFragment(BaseFFMFragment baseFFMFragment) {
        baseFragments.add(baseFFMFragment);
    }

    @Override
    public Fragment getItem(int position) {
        return baseFragments.get(position);
    }


    @Override
    public int getCount() {
        return baseFragments.size();
    }

}
