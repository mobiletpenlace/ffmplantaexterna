package com.totalplay.fieldforcetpenlace.view.activities;

import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OptionsMenuActivity extends BaseFFMActivity {

    @BindView(R.id.toolbar_report)
    protected ImageView mReportImageView;
    @BindView(R.id.toolbar_report_title)
    protected TextView mTitleReportTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void initReport(String title, Reason reason) {
        ButterKnife.bind(this);
        mReportImageView.setVisibility(View.VISIBLE);
        mReportImageView.setOnClickListener(v -> OptionsIncidentsMenuActivity.launch(OptionsMenuActivity.this, reason));
        mTitleReportTextView.setText(title);
    }

    public void initReport(String title, ArrayList<Reason> reasons) {
        ButterKnife.bind(this);
        mReportImageView.setVisibility(View.VISIBLE);
        mReportImageView.setOnClickListener(v -> {
            PopupMenu pop = new PopupMenu(this, mReportImageView);
            for (Reason s : reasons) {
                pop.getMenu().add(s.name);
            }
            pop.setOnMenuItemClickListener(item -> {
                String v1 = item.getTitle().toString();
                for (Reason reason : reasons) {
                    if (reason.name.equals(v1)) {
                        if (reason.id != null) {
                            OptionsIncidentsMenuActivity.launch(OptionsMenuActivity.this, reason);
                        }
                        return true;
                    }
                }
                return false;
            });
            pop.show();
        });
        mTitleReportTextView.setText(title);
    }

}
