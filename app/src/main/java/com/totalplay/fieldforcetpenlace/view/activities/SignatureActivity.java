package com.totalplay.fieldforcetpenlace.view.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.ImageTypeEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ServerFolders;
import com.totalplay.fieldforcetpenlace.modules.enl.take_picture.BusinessFiles;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.PhotoEvidence;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.routing.RoutingApp;
import com.totalplay.utils.MessageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignatureActivity extends BaseFFMActivity {

    @BindView(R.id.act_content_signature)
    ViewGroup mContent;
    @BindView(R.id.act_signature_accept)
    CheckBox mAcceptCheckBox;

    Signature mSignature;
    private Bitmap mSignatureBitmap;
    private SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

    public static void launch(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isSigned) {
            appCompatActivity.startActivity(new Intent(appCompatActivity, SignatureActivity.class));
        } else {
            RoutingApp.OnSuccessSignatureEvent(appCompatActivity, workOrder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_actvity_signature);
        setTitle("");
        ButterKnife.bind(this);

        mSignature = new Signature(getApplicationContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        mSignature.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
        ));

        mContent.addView(mSignature);
        mTrainingManager.signatureActivity();
    }

    @OnClick({R.id.act_signature_dialog_save, R.id.act_signature_dialog_clear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.act_signature_dialog_clear:
                mSignature.clear();
                break;
            case R.id.act_signature_dialog_save:
                if (mAcceptCheckBox.isChecked()) {
                    finish();
                    mFFMDataManager.tx(tx -> {
                        mWorkOrder.isSigned = true;
                        tx.save(mWorkOrder);
                    });
                    File signatureFile = generateFile(mContent);
                    savePhoto(signatureFile.getName(), signatureFile.getPath());
                    RoutingApp.OnSuccessSignatureEvent(this, mWorkOrder);
                } else {
                    MessageUtils.toast(getApplicationContext(), R.string.act_signature_accept_privacity);
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @SuppressLint("InflateParams")
    public class Signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private final RectF dirtyRect = new RectF();
        private Paint paint = new Paint();
        private Path path = new Path();
        private float lastTouchX;
        private float lastTouchY;

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public Bitmap save(View v) {
            if (mSignatureBitmap == null) {
                mSignatureBitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(),
                        Bitmap.Config.RGB_565);
            }
            Canvas canvas = new Canvas(mSignatureBitmap);
            v.draw(canvas);

            return mSignatureBitmap;
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(@NonNull MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    public File generateFile(View content) {
        File signatureFile = null;
        try {
            content.setDrawingCacheEnabled(true);
            Bitmap bitmap = content.getDrawingCache();
            signatureFile = BusinessFiles.newDocument("Signature"+ timeStampFormat.format(new Date()));
            if (signatureFile != null) {
                FileOutputStream ostream = new FileOutputStream(signatureFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 10, ostream);
                ostream.close();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return signatureFile;
    }


    public void savePhoto(String name, String path) {
        mFFMDataManager.tx(tx -> {
            PhotoEvidence photo = new PhotoEvidence();
            photo.name = name;
            photo.path = path;

            photo.workOrder = mWorkOrder.id;
            photo.serverFolder = ServerFolders.EVIDENCES;
            photo.baseUrl = mWorkOrder.baseUrl;
            photo.interventionType = mWorkOrder.interventionType;
            photo.type = ImageTypeEnum.SIGNATURE;

            tx.save(photo);
        });
    }
}