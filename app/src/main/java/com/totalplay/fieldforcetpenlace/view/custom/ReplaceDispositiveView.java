package com.totalplay.fieldforcetpenlace.view.custom;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.EquipmentModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.fieldforcetpenlace.view.activities.SimpleScannerActivity;
import com.totalplay.fieldforcetpenlace.view.utils.MacEditText;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by totalplay on 11/28/16.
 * FieldForceManagement
 */

public class ReplaceDispositiveView {

    @BindView(R.id.item_replace_equipment_model)
    TextView mModelTextView;
    @BindView(R.id.item_setting_equipment_serial_value)
    TextView mSerialTextView;
    @BindView(R.id.item_replace_equipment_mac_value)
    TextView mMacTextView;
    @BindView(R.id.item_equipment_model_delete)
    TextView mDeleteTextView;

    @BindView(R.id.item_equipment_type_name)
    TextView mEquipmentTypeNameTextView;
    @BindView(R.id.item_equipment_equipment_name)
    TextView mEquipmentNameTextView;
    @BindView(R.id.item_equipment_serial_number)
    EditText mSerialNumberTextView;
    @BindView(R.id.item_equipment_mac)
    MacEditText mMacEditTextView;
    @BindView(R.id.item_equipment_model_label)
    TextView mModelCatalogTextView;
    @BindView(R.id.item_equipment_model)
    CatalogEditText<EquipmentModel> mModelCatalogEditText;

    @BindView(R.id.item_equipment_serial_number_scan)
    ImageView mSerialNumberScanImageView;

    @BindView(R.id.item_equipment_mac_content)
    ViewGroup mMacContentViewGroup;

    private AppCompatActivity appCompatActivity;
    private FormValidator mFormValidator;
    private Equipment mEquipment;
    private boolean wasScanNumberSerial = false;
    private View mRootView;

    private View.OnClickListener mDeleteClickListener;

    public ReplaceDispositiveView(AppCompatActivity appCompatActivity, View rootView, Equipment equipment, InfoEquipment infoEquipment, Integer position, View.OnClickListener deleteClickListener) {
        ButterKnife.bind(this, rootView);
        mEquipment = equipment;
        this.appCompatActivity = appCompatActivity;
        this.mEquipment = equipment;
        mDeleteClickListener = deleteClickListener;
        mRootView = rootView;

        mMacEditTextView.registerAfterMacTextChangedCallback();
        mModelTextView.setText(String.format(Locale.getDefault(), "%s - %s", infoEquipment.type, infoEquipment.equipmentModel));
        mSerialTextView.setText(infoEquipment.numSerie);
        mMacTextView.setText(infoEquipment.mac);

        mEquipmentTypeNameTextView.setText(String.format(Locale.getDefault(), "%d.- %s", position, equipment.dispositive));
        mEquipmentNameTextView.setText(equipment.name);
        mSerialNumberTextView.setText(equipment.serialNumber);
        mMacEditTextView.setText(equipment.mac);
        if (equipment.equipmentModel != null)
            mModelCatalogEditText.setText(equipment.equipmentModel.modelDescription);

        mSerialNumberScanImageView.setTag(equipment);
        mSerialNumberTextView.setTag(equipment);
        mMacEditTextView.setTag(equipment);
        mModelCatalogEditText.setTag(equipment);

        if (equipment.dispositive.equals("Tel IP")) {
            mMacContentViewGroup.setVisibility(View.VISIBLE);
        }

        if (infoEquipment.availableModels != null)
            mModelCatalogEditText.setCatalogs(infoEquipment.availableModels.equipmentsModels);
        mDeleteTextView.setTag(infoEquipment);

        mFormValidator = new FormValidator(appCompatActivity, true);
        if (infoEquipment.type.equals(EquipmentType.PBX)) {
            mFormValidator.addValidators(
                    new EditTextValidator(mSerialNumberTextView, Regex.NOT_EMPTY, R.string.dialog_error_empty)
            );
            mModelCatalogTextView.setVisibility(View.GONE);
            mModelCatalogEditText.setVisibility(View.GONE);
        } else {
            mFormValidator.addValidators(
                    new EditTextValidator(mSerialNumberTextView, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                    new EditTextValidator(mModelCatalogEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
            );
        }

    }

    public boolean isValid() {
        return mFormValidator.isValid();
    }

    public Equipment getEquipment() {
        mEquipment.serialNumber = mSerialNumberTextView.getText().toString().trim();
        mEquipment.mac = mMacEditTextView.getText().toString().trim();
        mEquipment.equipmentModel = mModelCatalogEditText.getSelectedValue();
        return mEquipment;
    }

    @OnClick(R.id.item_equipment_serial_number_scan)
    void onScanNumberSerial() {
        wasScanNumberSerial = true;
        SimpleScannerActivity.launch(appCompatActivity);
    }

    @OnClick(R.id.item_equipment_model_delete)
    void onDeleteClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(appCompatActivity)
                .setMessage("¿Está seguro de conservar el dispositivo configurado?")
                .setPositiveButton("Aceptar", (dialog, which) -> {
                    ((ViewGroup) mRootView.getParent()).removeView(mRootView);
                    mDeleteClickListener.onClick(mDeleteTextView);
                })
                .setNegativeButton("Cancelar", null);
        builder.create().show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            String serialNumber = data.getExtras().getString(Keys.SCAN_RESULT);
            if (wasScanNumberSerial) {
                wasScanNumberSerial = false;
                mSerialNumberTextView.setText(serialNumber);
            }
        }
    }

}