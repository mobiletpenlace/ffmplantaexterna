package com.totalplay.fieldforcetpenlace.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.newpresenter.OptionsIncidentsMenuPresenter;
import com.totalplay.mvp.BasePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by armando on 26/06/17.
 * FFM
 */

public class OptionsIncidentsMenuActivity extends BaseFFMActivity
        implements OptionsIncidentsMenuPresenter.OptionsIncidentsMenuCallback {

    @BindView(R.id.act_options_menu_incident_title)
    TextView mIncidentTextView;
    @BindView(R.id.act_options_menu_incident_coments)
    EditText mCommentsIncidents;

    private OptionsIncidentsMenuPresenter mOptionsIncidentsMenuPresenter;


    public static void launch(AppCompatActivity appCompatActivity, Reason reason) {
        Intent intent = new Intent(appCompatActivity, OptionsIncidentsMenuActivity.class);
        intent.putExtra(Keys.EXTRA_REASON, reason);
        appCompatActivity.startActivity(intent);
    }

    public static void launch(AppCompatActivity appCompatActivity, Reason reason, Splitter splitter) {
        Intent intent = new Intent(appCompatActivity, OptionsIncidentsMenuActivity.class);
        intent.putExtra(Keys.EXTRA_REASON, reason);
        intent.putExtra(Keys.EXTRA_SPLITTER, splitter);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enl_activity_options_menu_incidents);
        ButterKnife.bind(this);
        setTitle("");

        mOptionsIncidentsMenuPresenter.loadData();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mOptionsIncidentsMenuPresenter = new OptionsIncidentsMenuPresenter(this, this);
    }

    @Override
    public void onSuccessLoadReason(Reason reason) {
        mIncidentTextView.setText(reason.name);
    }

    @OnClick(R.id.act_options_menu_incident_button)
    public void sendStatusIncidents(View view) {
        if (!mCommentsIncidents.getText().toString().equals("") && !mCommentsIncidents.getText().toString().isEmpty()) {
            mOptionsIncidentsMenuPresenter.sendCancelStatus(mCommentsIncidents.getText().toString());
        } else {
            Toast.makeText(this, R.string.error_in_comments, Toast.LENGTH_SHORT).show();
        }
    }

}
