package com.totalplay.fieldforcetpenlace.view.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMacResponse;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.EquipmentModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WifiModel;
import com.totalplay.fieldforcetpenlace.modules.enl.activation.WifiPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.SimpleScannerActivity;
import com.totalplay.fieldforcetpenlace.view.utils.MacEditText;
import com.totalplay.utils.MessageUtils;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.totalplay.fieldforcetpenlace.R.string.dialog_error_serial_required;

/**
 * Created by totalplay on 11/28/16.
 * FieldForceManagement
 */

public class WifiView implements WifiPresenter.WifiCallback {

    private static final int REQUEST_SERIAL_NUMBER_WIFI = 0x102;
    private static final int REQUEST_MAC_WIFI = 0x103;

    @BindView(R.id.item_service_activation_tv_delete_mac)
    ImageView mDeleteMacImageView;
    @BindView(R.id.item_service_activation_tv_plan_id)
    TextView mServicePlanIdTextView;
    @BindView(R.id.item_service_activation_tv_pack)
    TextView mPackTextView;
    @BindView(R.id.item_service_activation_tv_serial)
    EditText mTvSerialEditText;
    @BindView(R.id.item_service_activation_tv_mac)
    MacEditText macTvMaterialEditText;
    @BindView(R.id.item_service_activation_tv_scan_mac)
    ImageView macButton;
    @BindView(R.id.item_service_activation_tv_equipment_type)
    CatalogEditText<String> mTypeEquipmentTvTextView;
    @BindView(R.id.item_service_activation_tv_equipment_model)
    CatalogEditText<EquipmentModel> mModelEquipmentCatalogEditText;

    @BindView(R.id.item_service_activation_tv_search_mac_button)
    Button mSearchMacButton;

    private AppCompatActivity appCompatActivity;
    public FormValidator mFormValidator;

    private String serialNum;

    private Equipment mEquipment;
    private WifiPresenter mWifiPresenter;

    private boolean wasScan = false;

    public WifiView(AppCompatActivity appCompatActivity, View rootView, String packName, Equipment equipment) {
        ButterKnife.bind(this, rootView);
        mEquipment = equipment;

        macTvMaterialEditText.registerAfterMacTextChangedCallback();
        this.appCompatActivity = appCompatActivity;

        mPackTextView.setText(packName);
        mServicePlanIdTextView.setText(String.format("WIFI EXTENDER - %s", equipment.servicePlan));

        mWifiPresenter = new WifiPresenter(appCompatActivity, this);

        mTvSerialEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                InputMethodManager in = (InputMethodManager) appCompatActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(mTvSerialEditText.getWindowToken(), 0);
                searchMac();
                return true;
            }
            return false;
        });

        mTypeEquipmentTvTextView.setCatalogs(new ArrayList<String>() {{
            add("Router");
        }});

        if (equipment != null) {
            if (equipment.equipmentsModels != null)
                mModelEquipmentCatalogEditText.setCatalogs(equipment.equipmentsModels);
        }

        createEditTextValidators();
    }

    @OnClick(R.id.item_service_activation_tv_scan_bar_code)
    public void onClickScanBarCodeTV() {
        SimpleScannerActivity.launch(appCompatActivity, REQUEST_SERIAL_NUMBER_WIFI);
        wasScan = true;
//        appCompatActivity.startActivityForResult(SimpleScannerActivity.createIntent(appCompatActivity, false, mServicePlan.servicePlanId, false), 100);
    }

    @OnClick(R.id.item_service_activation_tv_scan_mac)
    public void onClickScanMacTV() {
        wasScan = true;
        SimpleScannerActivity.launch(appCompatActivity, REQUEST_MAC_WIFI);
//        appCompatActivity.startActivityForResult(SimpleScannerActivity.createIntent(appCompatActivity, false, mServicePlan.servicePlanId, true), 100);
    }

    @OnClick(R.id.item_service_activation_tv_delete_mac)
    public void onClickDeleteMac() {
        macTvMaterialEditText.setText("");
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (wasScan) {
            wasScan = false;
            if (resultCode == Activity.RESULT_OK) {
                String serialNumber = data.getExtras().getString(Keys.SCAN_RESULT);
                if (requestCode == REQUEST_SERIAL_NUMBER_WIFI) {
                    mTvSerialEditText.setText(serialNumber);
                    mTvSerialEditText.clearFocus();
                    searchMac();
                } else if (requestCode == REQUEST_MAC_WIFI) {
                    macTvMaterialEditText.setText(serialNumber);
                }
            }
        }
    }

    @OnClick(R.id.item_service_activation_tv_search_mac_button)
    public void onClickSearchMac() {
        searchMac();
    }

    private void searchMac() {
        if (!TextUtils.isEmpty(mTvSerialEditText.getText())) {
            mWifiPresenter.validWifiExtenderSerialNumber(mTvSerialEditText.getText().toString());
        } else {
            MessageUtils.toast(appCompatActivity, dialog_error_serial_required);
        }
    }

    public WifiModel getTvModel() {
        String serialNumber = mTvSerialEditText.getText().toString();
        if (serialNumber.length() >= 20) {
            serialNum = serialNumber.substring(2, serialNumber.length());
        } else if (serialNumber.length() == 19) {
            serialNum = serialNumber.substring(1, serialNumber.length());
        } else {
            serialNum = serialNumber;
        }

        return new WifiModel(
                mEquipment.servicePlan,
                macTvMaterialEditText.getText().toString().trim(),
                serialNum.replace(":", ""),
                mTypeEquipmentTvTextView.getText().toString().trim(),
                mModelEquipmentCatalogEditText.getSelectedValue()
        );
    }

    public void setup(WifiModel wifiModel) {
        mTvSerialEditText.setText(wifiModel.serialNumber);
        macTvMaterialEditText.setText(wifiModel.mac);
        mTypeEquipmentTvTextView.setText(wifiModel.srvMode);
        if (wifiModel.equipmentModel != null)
            mModelEquipmentCatalogEditText.setText(wifiModel.equipmentModel.modelDescription);
    }

    private String formatMACAddress(String macAddress) {
        if (!macAddress.contains(":")) {
            String insert = ":";
            int period = 2;
            int capacity = macAddress.length() + insert.length() * (macAddress.length() / period) + 1;
            StringBuilder builder = new StringBuilder(capacity);

            int index = 0;
            String prefix = "";
            while (index < macAddress.length()) {
                builder.append(prefix);
                prefix = insert;
                builder.append(macAddress.substring(index,
                        Math.min(index + period, macAddress.length())));
                index += period;
            }
            return builder.toString();
        } else {
            return macAddress.replace(":", "");
        }

    }

    private void createEditTextValidators() {
        mFormValidator = new FormValidator(appCompatActivity, true);
        mFormValidator.addValidators(
                new EditTextValidator(mTypeEquipmentTvTextView, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mTvSerialEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mTvSerialEditText, 12, 50, R.string.dialog_error_length_serial_number),
                new EditTextValidator(macTvMaterialEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mModelEquipmentCatalogEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );

        macTvMaterialEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 12) {
                    String address = formatMACAddress(macTvMaterialEditText.getText().toString());
                    macTvMaterialEditText.setText(address);
                }
                if (s.length() > 17) {
                    String address = macTvMaterialEditText.getText().toString().substring(0, 16);
                    macTvMaterialEditText.setText("");
                    macTvMaterialEditText.append(address);
                }
            }
        });
    }

    @Override
    public void onSuccessLoadMac(GetMacResponse getMacResponse) {
        macTvMaterialEditText.setText(getMacResponse.mac);

    }

    @Override
    public void onErrorLoadMac() {
        macButton.setVisibility(View.VISIBLE);
    }
}