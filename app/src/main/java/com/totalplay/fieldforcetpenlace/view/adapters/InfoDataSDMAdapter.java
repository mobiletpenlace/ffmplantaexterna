package com.totalplay.fieldforcetpenlace.view.adapters;

import android.view.View;
import android.widget.TextView;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ConfigSDMResponse;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleAdapter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jorgeHdez on 1/5/17.
 * Enlace
 */
@SuppressWarnings({"unchecked"})
public class InfoDataSDMAdapter extends BaseSimpleAdapter<ConfigSDMResponse.AttributeList, InfoDataSDMAdapter.AdapterViewHolder> {

    @Override
    public int getItemLayout() {
        return R.layout.enl_item_info_data_sdm;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new AdapterViewHolder(view);
    }

    class AdapterViewHolder extends BaseViewHolder<ConfigSDMResponse.AttributeList> {

        @BindView(R.id.item_info_data_sdm_name)
        TextView mNameTextView;
        @BindView(R.id.item_info_data_sdm_value)
        TextView mValueTextView;


        AdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, ConfigSDMResponse.AttributeList attributeList) {
            mNameTextView.setText(attributeList.name);
            mValueTextView.setText(attributeList.value);
        }
    }

}
