package com.totalplay.fieldforcetpenlace.view.adapters;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.DiscountMaterialsRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMaterials;
import com.totalplay.utils.MessageUtils;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleAdapter;
import com.totalplay.view.BaseSimpleRecyclerView.BaseViewHolder;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by charlssalazar on 30/06/17.
 * FFM
 */

public class UsedMaterialsAdapter extends BaseSimpleAdapter<GetMaterials.Materials, UsedMaterialsAdapter.AdapterViewHolder> {

    private AppCompatActivity appCompatActivity;
    private HashMap<String, DiscountMaterialsRequest.UsedMaterials> mUsedMaterials = new HashMap<>();

    private View.OnClickListener clickListener = v -> {
        GetMaterials.Materials arrMaterials = (GetMaterials.Materials) v.getTag();
        if (!mUsedMaterials.containsKey(arrMaterials.material)) {
            DiscountMaterialsRequest.UsedMaterials usedMaterials = new DiscountMaterialsRequest.UsedMaterials();
            usedMaterials.material = arrMaterials.material;
            usedMaterials.serie = "";
            usedMaterials.rate = arrMaterials.lot;
            usedMaterials.uom = arrMaterials.funit;
            mUsedMaterials.put(arrMaterials.material, usedMaterials);
        }
        MessageUtils.createDialog(appCompatActivity, "Materiales Usados", "Ingresa la cantidad usada:", quantity -> {
            mUsedMaterials.get(arrMaterials.material).quantity = quantity;
            arrMaterials.quantity = quantity;
            notifyDataSetChanged();
        });
    };

    public UsedMaterialsAdapter(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    @Override
    public int getItemLayout() {
        return R.layout.enl_item_used_materials_v2;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new AdapterViewHolder(view);
    }

    class AdapterViewHolder extends BaseViewHolder<GetMaterials.Materials> {

        @BindView(R.id.item_material_name_material)
        public TextView nameMaterialTextView;
        @BindView(R.id.item_materials_quantity)
        public TextView quantityMaterialTextView;
        @BindView(R.id.type_peace_textview)
        public TextView typePeaceTextview;

        public AdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            quantityMaterialTextView.setOnClickListener(clickListener);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, GetMaterials.Materials materials) {
            nameMaterialTextView.setText(materials.description);
            quantityMaterialTextView.setText(materials.quantity);
            typePeaceTextview.setText(materials.funit);
            quantityMaterialTextView.setText(materials.quantity);
            quantityMaterialTextView.setTag(materials);
            quantityMaterialTextView.setTag(materials);
        }
    }

    public ArrayList<DiscountMaterialsRequest.UsedMaterials> getUsedMaterials() {
        return new ArrayList<>(mUsedMaterials.values());
    }

}
