package com.totalplay.fieldforcetpenlace.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ConfigSDMResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMacResponse;
import com.totalplay.fieldforcetpenlace.library.enums.EquipmentType;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.EquipmentModel;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.newpresenter.ConfigSDMPresenter;
import com.totalplay.fieldforcetpenlace.routing.RoutingApp;
import com.totalplay.fieldforcetpenlace.view.activities.ConfigSDMActivity;
import com.totalplay.fieldforcetpenlace.view.activities.SimpleScannerActivity;
import com.totalplay.fieldforcetpenlace.view.adapters.InfoDataSDMAdapter;
import com.totalplay.fieldforcetpenlace.view.custom.CatalogEditText;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;
import com.totalplay.utils.validators.EditTextValidator;
import com.totalplay.utils.validators.FormValidator;
import com.totalplay.utils.validators.Regex;
import com.totalplay.view.BaseSimpleRecyclerView.BaseSimpleRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by charlssalazar on 10/10/17.
 * Estrategia
 */

public class ConfigSDMAFragment extends BaseFFMFragment implements ConfigSDMPresenter.ConfigSDMCallback {

    @BindView(R.id.act_config_sdm_serial_number)
    EditText mSerialNumberEditText;
    @BindView(R.id.act_config_sdm_mac)
    EditText mMacTextView;
    @BindView(R.id.act_config_sdm_sbcproxy)
    TextView mSBCProxyTextview;
    @BindView(R.id.act_config_sdm_sbcname)
    TextView mSBCNameTextView;
    @BindView(R.id.act_config_sdm_mask)
    TextView mSDMMaskTextview;
    @BindView(R.id.act_config_sdm_gateway)
    TextView mSDMGatewayTextView;
    @BindView(R.id.act_config_sdm_model)
    CatalogEditText<EquipmentModel> mModelCatalogEditText;

    private InfoDataSDMAdapter mInfoDataSDMAdapter;
    private ConfigSDMPresenter mConfigSDMPresenter;


    private FormValidator mFormValidator;
    private AppCompatActivity appCompatActivity;

    public static ConfigSDMAFragment newInstance() {
        Bundle bundle = new Bundle();
        ConfigSDMAFragment fragment = new ConfigSDMAFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static void launch(AppCompatActivity appCompatActivity, WorkOrder workOrder) {
        if (!workOrder.isSetttingPXB) {
            Intent intent = new Intent(appCompatActivity, ConfigSDMActivity.class);
            appCompatActivity.startActivity(intent);
        } else {
            RoutingApp.OnSuccessActivatePBXEvent(appCompatActivity, workOrder);
        }
    }

    @Override
    public BasePresenter getPresenter() {
        return null;
    }

    @Override
    public BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mConfigSDMPresenter = new ConfigSDMPresenter((AppCompatActivity) getActivity(), this)
        };
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enl_fragment_config_sdma, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        appCompatActivity = (AppCompatActivity) getActivity();

        new BaseSimpleRecyclerView(appCompatActivity, R.id.act_config_sdm_info_data)
                .setAdapter(mInfoDataSDMAdapter = new InfoDataSDMAdapter())
                .addBottomOffsetDecoration(200);

        mFormValidator = new FormValidator(getActivity(), true);
        mFormValidator.addValidators(
                new EditTextValidator(mSerialNumberEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mModelCatalogEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );

        Equipment equipment = mFFMDataManager.queryWhere(Equipment.class).filter("dispositive", EquipmentType.PBX).findFirst();
        if (equipment != null) mModelCatalogEditText.setCatalogs(equipment.equipmentsModels);
        mConfigSDMPresenter.getConfigSDM();

        mSerialNumberEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                setSerialNumber(mSerialNumberEditText.getText().toString());
                return true;
            }
            return false;
        });
    }

    public void setSerialNumber(String serialNumber) {
        if (!TextUtils.isEmpty(mSerialNumberEditText.getText())) {
            mConfigSDMPresenter.validWifiExtenderSerialNumber(mSerialNumberEditText.getText().toString().trim());
        } else {
            MessageUtils.toast(getActivity(), R.string.dialog_error_serial_required);
        }
        mSerialNumberEditText.clearFocus();
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(mSerialNumberEditText.getWindowToken(), 0);
    }

    @OnClick(R.id.act_config_sdm_setting)
    void onSettingEquipment() {
        reloadWorkOrder();
        if (!mWorkOrder.isSetttingPXB) {
            if (mFormValidator.isValid()) {
                String mac = mMacTextView.getText().toString().trim();
                String serial = mSerialNumberEditText.getText().toString().trim();
                String model = mModelCatalogEditText.getSelectedValue().modelID;
                mConfigSDMPresenter.settingEquipment(mac, serial, model);
            }
        } else {
            RoutingApp.OnSuccessActivatePBXEvent((AppCompatActivity) getActivity(), mWorkOrder);
        }
    }

    @OnClick(R.id.act_config_sdm_serial_number_scan)
    void onClickScanBarCode() {
        SimpleScannerActivity.launch(appCompatActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SimpleScannerActivity.REQUEST_CODE_SERIAL_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                mSerialNumberEditText.setText(data.getStringExtra(Keys.SCAN_RESULT));
                setSerialNumber(mSerialNumberEditText.getText().toString());
            }
        }
    }

    @Override
    public void onSuccessLoadConfigSDM(ConfigSDMResponse configSDMResponse) {
        if (configSDMResponse != null) {
            if (configSDMResponse.getProxyOutput != null) {
                mSBCProxyTextview.setText(configSDMResponse.getProxyOutput.espSbcproxy);
                mSBCNameTextView.setText(configSDMResponse.getProxyOutput.espSbcname);
            }
            if (configSDMResponse.infoClienteSDM != null) {
                mSDMMaskTextview.setText(configSDMResponse.infoClienteSDM.mask);
                mSDMGatewayTextView.setText(configSDMResponse.infoClienteSDM.gateway);
            }
            mInfoDataSDMAdapter.update(configSDMResponse.attributeList);
        }
    }

    @Override
    public void onErrorLoadConfigSDM() {
//        getActivity().finish();
    }

    @Override
    public void onSuccessLoadMac(GetMacResponse getMacResponse) {
        mMacTextView.setText(getMacResponse.mac);
    }

}
