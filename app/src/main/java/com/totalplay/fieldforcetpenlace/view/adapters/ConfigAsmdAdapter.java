package com.totalplay.fieldforcetpenlace.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.totalplay.fieldforcetpenlace.view.fragments.BaseFFMFragment;

import java.util.ArrayList;

/**
 * Created by charlssalazar on 10/10/17.
 */

public class ConfigAsmdAdapter extends FragmentStatePagerAdapter {

    private ArrayList<BaseFFMFragment> mPaymentFragments;

    public ConfigAsmdAdapter(FragmentManager supportFragmentManager, ArrayList<BaseFFMFragment> paymentFragments) {
        super(supportFragmentManager);
        mPaymentFragments = paymentFragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mPaymentFragments.get(position);
    }

    @Override
    public int getCount() {
        return mPaymentFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Configuración PBX";
        } else if (position == 1) {
            return "Dns";
        }
        return null;
    }
}