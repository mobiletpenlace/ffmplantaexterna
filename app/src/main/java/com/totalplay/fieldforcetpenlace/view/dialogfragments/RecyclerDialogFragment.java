package com.totalplay.fieldforcetpenlace.view.dialogfragments;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderStatus;
import com.totalplay.fieldforcetpenlace.library.interfaces.OnDialogOptionSelectedListener;
import com.totalplay.fieldforcetpenlace.view.adapters.ReasonsAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.totalplay.fieldforcetpenlace.R.id.act_reason_recyclerView;

@SuppressWarnings("unchecked")
public class RecyclerDialogFragment<T> extends DialogFragment {

    @BindView(R.id.comment)
    EditText comment;
    @BindView(act_reason_recyclerView)
    RecyclerView recyclerView;

    private OnDialogOptionSelectedListener selectedListener;
    private ArrayList<T> arrayList = new ArrayList<>();
    private boolean commentEnabled = false;

    public static <T> RecyclerDialogFragment<T> newInstance() {
        return new RecyclerDialogFragment<>();
    }

    public void setSelectedListener(OnDialogOptionSelectedListener<T> selectedListener) {
        this.selectedListener = selectedListener;
    }

    public void setCommentEnabled(boolean commentEnabled) {
        this.commentEnabled = commentEnabled;
    }

    public void setArrayList(ArrayList<T> arrayList) {
        if (arrayList != null) {
            this.arrayList = arrayList;
        }
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.enl_dialog_custom_recycler, null);
        ButterKnife.bind(this, view);

        if (commentEnabled) {
            comment.setVisibility(View.VISIBLE);
        }
        ArrayList<String> titles = new ArrayList<>();
        for (T object : arrayList) {
            titles.add(object.toString());
        }

        ReasonsAdapter adapter = new ReasonsAdapter(getActivity(), titles);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        adapter.setClickListener((view1, position) -> Log.e("Msj", String.valueOf(position)));

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics mMetrics = new DisplayMetrics();
        display.getMetrics(mMetrics);
        mBuilder.setView(view);
        mBuilder.setCancelable(false);
        return mBuilder.create();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.submit)
    public void onclick() {
        if (arrayList.size() > 0) {
            String types = String.valueOf(1);//ChangeOperatorStatusActivity.type);
            Log.e("ClassName->", types);
            switch (types) {
                case "1":
                    if (ReasonsAdapter.select_item > -1) {
                        int index = arrayList.indexOf(arrayList.get(ReasonsAdapter.select_item));// > 1 ? ReasonsAdapter.select_item : 0;
                        selectedListener.onOptionSelected(index, arrayList.get(index), " ");
                    } else {
                        Toast.makeText(getActivity(), "Selecciona un elemento Primero", Toast.LENGTH_LONG).show();
                    }
                    break;
                case "2":
                    if (ReasonsAdapter.select_item > -1) {
                        selectedListener.onOptionSelected(ReasonsAdapter.select_item, arrayList.get(ReasonsAdapter.select_item), WorkOrderStatus.Cancel.reasons.get(ReasonsAdapter.select_item).id);
                    } else {
                        Toast.makeText(getActivity(), "Selecciona un elemento Primero", Toast.LENGTH_LONG).show();
                    }
                    break;
                case "3":
                    if (ReasonsAdapter.select_item > -1) {
                        selectedListener.onOptionSelected(ReasonsAdapter.select_item, arrayList.get(ReasonsAdapter.select_item), WorkOrderStatus.Stop.reasons.get(ReasonsAdapter.select_item).id);
                        break;
                    } else {
                        Toast.makeText(getActivity(), "Selecciona un elemento Primero", Toast.LENGTH_LONG).show();
                    }
                    break;
            }

        }
//        getDialog().dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() != null)
            getActivity().finish();
//        if (getActivity() instanceof ChangeOperatorStatusActivity) getActivity().finish();
    }

}