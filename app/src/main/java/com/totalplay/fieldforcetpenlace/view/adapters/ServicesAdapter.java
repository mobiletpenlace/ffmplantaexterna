package com.totalplay.fieldforcetpenlace.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by NS-262 on 23/11/2016.
 */
public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ServicesViewHolder> {
    private ArrayList<ServicePlan> mServices = new ArrayList<>();

    @Override
    public ServicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.enl_item_service, parent, false);
        return new ServicesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ServicesViewHolder holder, int position) {
        ServicePlan servicePlan = mServices.get(position);
        String text = String.format("%s - %s", servicePlan.servicePlanName, servicePlan.servicePlanNameService);
        holder.nameTextView.setText(text);
    }

    @Override
    public int getItemCount() {
        return mServices.size();
    }

    public void update(List<ServicePlan> servicePlans) {
        mServices.addAll(servicePlans);
        notifyDataSetChanged();
    }

    public static class ServicesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_service_name)
        TextView nameTextView;

        public ServicesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
