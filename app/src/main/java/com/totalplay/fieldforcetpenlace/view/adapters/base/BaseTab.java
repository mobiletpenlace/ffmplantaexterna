package com.totalplay.fieldforcetpenlace.view.adapters.base;

import android.support.v4.app.Fragment;

/**
 * Created by jorgehdezvilla on 19/12/17.
 * FFM Enlace
 */

public class BaseTab {

    public String title;
    public Fragment fragment;

    public BaseTab(String title, Fragment fragment) {
        this.title = title;
        this.fragment = fragment;
    }
}
