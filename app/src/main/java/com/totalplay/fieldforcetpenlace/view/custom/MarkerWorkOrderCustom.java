package com.totalplay.fieldforcetpenlace.view.custom;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.totalplay.fieldforcetpenlace.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

public class MarkerWorkOrderCustom implements GoogleMap.InfoWindowAdapter {
    private LayoutInflater inflater = null;
    private Activity activity;

    public MarkerWorkOrderCustom(LayoutInflater inflater, Activity activity) {
        this.inflater = inflater;
        this.activity = activity;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoWindow(Marker marker) {
        ContextThemeWrapper cw = new ContextThemeWrapper(activity, R.style.Transparent);
        LayoutInflater inflater = (LayoutInflater) cw.getSystemService(LAYOUT_INFLATER_SERVICE);
        return loadContent(inflater.inflate(R.layout.enl_item_work_order_map, null), marker);
    }

    private View loadContent(View contentView, Marker marker) {
        TextView titleTextView = contentView.findViewById(R.id.item_work_order_map_title);
        TextView countTextView = contentView.findViewById(R.id.item_work_order_map_count);
        View indicatorView = contentView.findViewById(R.id.item_work_order_map_indicator);
        ProgressBar progressBar = contentView.findViewById(R.id.item_work_order_map_progress);

        titleTextView.setText(marker.getTitle());
        countTextView.setText(marker.getSnippet());

        int count = Integer.parseInt(marker.getSnippet());
        countTextView.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
        indicatorView.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
        progressBar.setVisibility(count > 0 ? View.GONE : View.VISIBLE);
        return contentView;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        return loadContent(inflater.inflate(R.layout.enl_item_work_order_map, null), marker);
    }
}

