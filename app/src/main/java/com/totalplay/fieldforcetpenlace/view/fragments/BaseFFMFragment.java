package com.totalplay.fieldforcetpenlace.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.library.pojos.DataManager;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.mvp.BaseFragment;
import com.totalplay.utils.validators.FormValidator;

/**
 * Created TotalPlay
 * Enlace FFM
 */
@SuppressWarnings("unused")
public abstract class BaseFFMFragment extends BaseFragment {

    public DataManager mFFMDataManager;
    public WorkOrder mWorkOrder;
    public FormValidator mFormValidator;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFFMDataManager = DataManager.validateConnection(mFFMDataManager);
        mWorkOrder = mFFMDataManager.queryWhere(WorkOrder.class).findFirst();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mFFMDataManager != null) mFFMDataManager.close();
    }

    public void reloadWorkOrder(){
        mWorkOrder = mFFMDataManager.queryWhere(WorkOrder.class).findFirst();
    }

}
