package com.totalplay.fieldforcetpenlace.view.activities;

import android.content.Context;
import android.os.Bundle;

import com.library.pojos.DataManager;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.library.utils.Training.TrainingManager;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.fieldforcetpenlace.modules.base.BaseModule;
import com.totalplay.mvp.BaseActivity;
import com.totalplay.utils.validators.FormValidator;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by TotalPlay
 * Enlace
 */
@SuppressWarnings("unused")
public abstract class BaseFFMActivity extends BaseActivity {

    public BaseModule baseModule;
    public DataManager mFFMDataManager;
    public WorkOrder mWorkOrder;
    public User mUser;
    public FormValidator mFormValidator;
    public TrainingManager mTrainingManager;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFFMDataManager = DataManager.validateConnection(mFFMDataManager);
        mWorkOrder = mFFMDataManager.queryWhere(WorkOrder.class).findFirst();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseModule = (BaseModule) getIntent().getSerializableExtra(Keys.EXTRA_BASE_MODULE);
        mFFMDataManager = DataManager.validateConnection(mFFMDataManager);
        mWorkOrder = mFFMDataManager.queryWhere(WorkOrder.class).findFirst();
        mUser = mFFMDataManager.queryWhere(User.class).findFirst();
        mTrainingManager = TrainingManager.init(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mFFMDataManager != null) mFFMDataManager.close();
    }

}
