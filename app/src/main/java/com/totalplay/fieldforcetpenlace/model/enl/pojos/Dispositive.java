package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 02/10/17.
 * Enlace
 */

public class Dispositive implements Serializable {

    @SerializedName("IdPlanServicio")
    public String planServiceId;
    @SerializedName("NoSerie")
    public String numSerie;
    @SerializedName("MAC")
    public String mac;
    @SerializedName("Modelo_equipo")
    public String equipmentModel;
    @SerializedName("Dispositivo")
    public String dispositive;


    public Dispositive(String planServiceId, String numSerie, String mac, String equipmentModel) {
        this.planServiceId = planServiceId;
        this.numSerie = numSerie;
        this.mac = mac;
        this.equipmentModel = equipmentModel;
    }

}
