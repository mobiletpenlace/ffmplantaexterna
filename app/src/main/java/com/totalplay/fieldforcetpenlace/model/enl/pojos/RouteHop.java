package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-262 on 18/01/2017.
 */

public class RouteHop {
    @SerializedName("HopHost")
    public String hopHost;
    @SerializedName("HopHostAddress")
    public String hopHostAddress;
    @SerializedName("HopErrorCode")
    public String hopErrorCode;
    @SerializedName("HopRTTimes")
    public String hopRTTimes;
}
