package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.util.UUID;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

@RealmClass
public class Arrival implements RealmModel {

    @PrimaryKey
    public String id = UUID.randomUUID().toString();
    public String reference;
    public String minutes;
    public String hour;
    public double latitude;
    public double longitude;
}
