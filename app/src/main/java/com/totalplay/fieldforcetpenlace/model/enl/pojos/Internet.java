package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 1/17/17.
 * FieldForceManagement-Android
 */
public class Internet extends RealmObject{

    @PrimaryKey
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("SerialNumber")
    public String serialNumber;
    public String mac;
    @SerializedName("OLT")
    public String olt;
    @SerializedName("IdOLT")
    public String idOlt;
    public String frame;
    @SerializedName("Slot")
    public String slot;
    @SerializedName("Puerto")
    public String port;
    @SerializedName("SRV_Mode")
    public String srvMode;
    public String equipmentModel;

}
