package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-TOTALPLAY on 14/11/2016.
 */

public class SplitterInfo {
    @SerializedName("busy_port")
    public String busyPort;
    @SerializedName("second_level_nombenclature")
    public String secondLevelNombenclature;
    @SerializedName("splitter_id")
    public String splitterId;

    public SplitterInfo(String busyPort, String secondLevelNombenclature, String splitterId) {
        this.busyPort = busyPort;
        this.secondLevelNombenclature = secondLevelNombenclature;
        this.splitterId = splitterId;
    }
}
