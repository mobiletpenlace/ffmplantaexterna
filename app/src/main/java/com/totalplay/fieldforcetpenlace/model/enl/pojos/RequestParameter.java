package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 28/12/17.
 * FFM Enlace
 */

public class RequestParameter implements Serializable {

    @SerializedName("Parametro")
    public RequestParameterValue parameterElement;

    public RequestParameter(String key, String value) {
        this.parameterElement = new RequestParameterValue(key, value);
    }

    public static class RequestParameterValue {

        @SerializedName("identificador")
        public String key;
        @SerializedName("valor")
        public String value;

        public RequestParameterValue(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

}
