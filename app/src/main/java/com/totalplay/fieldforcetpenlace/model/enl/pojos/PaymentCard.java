package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 31/10/17.
 * Enlace
 */

public class PaymentCard implements Serializable {

    public String numbber;
    public String cvv;
    public String name;
    public String lastName;
    public String secondLastName;
    public String expireMonth;
    public String expireYear;
    public String type;

}
