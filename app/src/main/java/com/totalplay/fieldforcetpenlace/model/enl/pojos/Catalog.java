package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 01/08/17.
 * FFMTpe
 */

public class Catalog implements Serializable {

    @SerializedName("ID_P")
    public String catalogId;
    @SerializedName("DESCRIPCION")
    public String description;


    @Override
    public String toString() {
        return description;
    }

}
