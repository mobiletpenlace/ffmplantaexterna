package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 11/23/16.
 * Enlace
 */
public class User extends RealmObject implements Serializable, ModuleParameter {

    @PrimaryKey
    public String employeeId = "";
    public String operatorId = "";
    public String employeeUser = "";
    public String hash = "";
    public String owner = "";
    public String firstAccess = "";
    public String profilePhoto = "";
    public String employeNumber = "";
    public String employeName = "";
    public String lastName = "";
    public String lastMotherName = "";
    public String completeName = "";
    public String entryDate = "";
    public String company = "";
    public String operatorType = "";
    public String country = "";
    public String supervisor = "";

    public UserStatus userStatus;

    public boolean isCompletePreview = false;
    public boolean isCompleteTraningMain = false;

    public User() {
        employeeId = UUID.randomUUID().toString();
        operatorId = UUID.randomUUID().toString();
        hash = UUID.randomUUID().toString();
    }

    public User(String employeeUser, String employeeId, String operatorId, String owner, String firstAccess, String profilePhoto,
                String employeNumber, String employeName, String lastName, String lastMotherName, String completeName,
                String entryDate, String company, String operatorType, String country, String supervisor) {
        this.employeeUser = employeeUser;
        this.employeeId = employeeId;
        this.operatorId = operatorId;
        this.firstAccess = firstAccess;
        this.owner = owner;
        this.profilePhoto = profilePhoto;
        this.employeNumber = employeNumber;
        this.employeName = employeName;
        this.lastName = lastName;
        this.lastMotherName = lastMotherName;
        this.completeName = completeName;
        this.entryDate = entryDate;
        this.company = company;
        this.operatorType = operatorType;
        this.country = country;
        this.supervisor = supervisor;
    }
}
