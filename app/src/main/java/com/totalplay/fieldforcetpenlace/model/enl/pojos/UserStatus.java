package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.io.Serializable;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by jorgehdezvilla on 12/11/17.
 * Enlace
 */

@RealmClass
public class UserStatus implements RealmModel, Serializable {

    public String id;
    public String name;
    public int color;

    public UserStatus() {
    }

    public UserStatus(String id, String name, int color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    @Override
    public String toString() {
        return name;
    }
}