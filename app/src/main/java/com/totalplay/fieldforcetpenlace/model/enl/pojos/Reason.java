package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.io.Serializable;

/**
 * Created by jorgehdezvilla on 04/10/17.
 * Enlace
 */

public class Reason implements Serializable {
    public final String id;
    public final String name;

    public Reason(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}