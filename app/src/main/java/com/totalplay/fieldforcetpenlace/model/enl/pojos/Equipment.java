package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 11/24/16.
 * Enlace
 */
public class Equipment extends RealmObject implements Serializable {

    @PrimaryKey
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("IdCotPlanServicios")
    public String servicePlan = "";
    @SerializedName("IdEquipo")
    public String id = "";
    @SerializedName("NombreEquipo")
    public String name = "";
    @SerializedName("Dispositivo")
    public String dispositive = "";

    public String serialNumber = "";
    public String mac = "";
    public EquipmentModel equipmentModel;

    @SerializedName("modelos")
    public RealmList<EquipmentModel> equipmentsModels = new RealmList<>();

    @Override
    public String toString() {
        return name;
    }


}
