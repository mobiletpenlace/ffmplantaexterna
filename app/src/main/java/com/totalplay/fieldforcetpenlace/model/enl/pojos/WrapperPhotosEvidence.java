package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by charlssalazar on 29/06/17.
 * Enlace
 */

public class WrapperPhotosEvidence extends RealmObject {

    @PrimaryKey
    public String workOrder;
    public String folder;
    public String serverFolder;
    public String path;
    public RealmList<PhotoEvidence> listPhotos = new RealmList<>();

    public WrapperPhotosEvidence() {
        listPhotos = new RealmList<>();
    }

    public WrapperPhotosEvidence(String workOrder, String serverFolder) {
        listPhotos = new RealmList<>();
        this.workOrder = workOrder;
        this.serverFolder = serverFolder;
    }
}
