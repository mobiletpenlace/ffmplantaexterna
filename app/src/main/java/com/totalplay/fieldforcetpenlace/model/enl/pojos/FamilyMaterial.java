package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.util.ArrayList;

/**
 * Created by totalplay on 11/18/16.
 */
public class FamilyMaterial {

    public String name;
    public ArrayList<Material> materials = new ArrayList<>();

}
