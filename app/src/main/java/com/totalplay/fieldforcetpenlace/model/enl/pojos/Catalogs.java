package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jorgehdezvilla on 01/08/17.
 * FFMTpe
 */

public class Catalogs implements Serializable {

    @SerializedName("catalogo")
    public ArrayList<Catalog> catalogs;

}
