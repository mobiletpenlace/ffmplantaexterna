package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by NS-262 on 17/11/2016.
 */
public class Address extends RealmObject {
    @PrimaryKey
    public String _uuid;
    @SerializedName("Calle")
    public String street;
    @SerializedName("NumExterior")
    public String exteriorNumber;
    @SerializedName("NumInterior")
    public String innerNumber;
    @SerializedName("Colonia")
    public String colony;
    @SerializedName("CP")
    public String zipCode;
    @SerializedName("Ciudad")
    public String city;
    @SerializedName("EntreCalles")
    public String betweenStreets;
    @SerializedName("Longitud")
    public String longitude;
    @SerializedName("Latitud")
    public String latitude;

    public Address() {
        _uuid = UUID.randomUUID().toString();
    }
}
