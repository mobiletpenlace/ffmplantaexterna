package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by jorgehdezvilla on 03/01/18.
 * FFM Enlace
 */

@RealmClass
public class AvailableModels implements RealmModel, Serializable {

    @SerializedName("modelo")
    public RealmList<EquipmentModel> equipmentsModels = new RealmList<>();

}
