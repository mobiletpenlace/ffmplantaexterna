package com.totalplay.fieldforcetpenlace.model.enl.pojos;

/**
 * Created by totalplay on 12/14/16.
 */
public class Solution {
    public final String key;
    public final String name;

    public Solution(String key, String name) {
        this.key = key;
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
