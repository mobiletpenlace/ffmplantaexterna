package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 11/17/16.
 */
public class Material extends RealmObject implements Serializable {

    @PrimaryKey
    @SerializedName("sku_id")
    public String skuId;
    public String description;
    public String type;
    public String unit;
    public String family;
    @Expose(serialize = false, deserialize = false)
    public String quantity = "";

    @Override
    public String toString() {
        return description;
    }
}
