package com.totalplay.fieldforcetpenlace.model.enl.pojos;

/**
 * Created by mahegots on 19/10/16.
 */
public class ItemDialog {
    private String title;
    private int imageIcon;


    public ItemDialog(String title, int imageIcon) {
        this.title = title;
        this.imageIcon = imageIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(int imageIcon) {
        this.imageIcon = imageIcon;
    }
}
