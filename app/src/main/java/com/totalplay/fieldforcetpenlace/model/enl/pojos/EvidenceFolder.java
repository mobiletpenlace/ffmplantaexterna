package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by user on 11/24/16.
 */
public class EvidenceFolder extends RealmObject {

    @PrimaryKey
    public String uuid;
    public RealmList<FileWrapper> fileWrappers;
    public String workOrder;


    public EvidenceFolder() {
        this.uuid = UUID.randomUUID().toString();
        this.fileWrappers = new RealmList<>();
    }


}
