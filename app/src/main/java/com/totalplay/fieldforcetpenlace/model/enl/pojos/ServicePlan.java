package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 11/18/16.
 * FFM
 */
public class ServicePlan extends RealmObject implements Serializable {

    @PrimaryKey
    public String id = UUID.randomUUID().toString();
    @SerializedName("IdCotPlanServicio")
    public String servicePlanId;
    @SerializedName("CotSitioName")
    public String siteName;
    @SerializedName("CotSitioPlanName")
    public String sitePlanName;
    @SerializedName("DpPlanName")
    public String planName;
    @SerializedName("CotPlanServicoName")
    public String servicePlanName;
    @SerializedName("CotPlanServicoNServicio")
    public String servicePlanNameService;
    @SerializedName("DpPlanEtiquetaBRM")
    public String planLabel;
    @SerializedName("Tipo")
    public String type;
    @SerializedName("numeroDns")
    public String dnAmount;
    @SerializedName("EsAdicional")
    public String wasAdditional;

    public RealmList<DN> dns = new RealmList<>();

    @Expose(serialize = false, deserialize = false)
    public boolean configured = false;

}
