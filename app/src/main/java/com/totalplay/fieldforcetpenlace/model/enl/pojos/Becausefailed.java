package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by totalplay on 12/14/16.
 */
public class Becausefailed {
    public String key;
    public String name;
    public ArrayList<Solution> solutions = new ArrayList<>();

    public Becausefailed(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public static Becausefailed name(String key, String name) {
        return new Becausefailed(key, name);
    }

    public Becausefailed failures(Solution... solutions) {
        this.solutions.addAll(Arrays.asList(solutions));
        return this;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
