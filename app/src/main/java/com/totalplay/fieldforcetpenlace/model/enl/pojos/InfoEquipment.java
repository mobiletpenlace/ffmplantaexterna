package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by jorgehdezvilla on 02/10/17.
 * Enlace
 */

@RealmClass
public class InfoEquipment implements Serializable, RealmModel {

    @PrimaryKey
    public String id = UUID.randomUUID().toString();


    @SerializedName("idPlanServicio")
    public String planServiceId;
    @SerializedName("idEquipoModelo")
    public String equipmentModelId;
    @SerializedName("modeloEquipo")
    public String equipmentModel;
    @SerializedName("tipo")
    public String type;
    @SerializedName("numeroSerie")
    public String numSerie;
    @SerializedName("mac")
    public String mac;
    @SerializedName("modelos_disponibles")
    public AvailableModels availableModels;

//    @SerializedName("Dispositivo")
//    public String dispositive;

    public boolean wasReplace = false;

    public InfoEquipment() {
    }

    public InfoEquipment(String planServiceId, String numSerie, String mac, String equipmentModel) {
        this.planServiceId = planServiceId;
        this.numSerie = numSerie;
        this.mac = mac;
        this.equipmentModel = equipmentModel;
    }

}
