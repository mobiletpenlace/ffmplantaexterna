package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 11/24/16.
 */
public class DN extends RealmObject implements Serializable{
    @PrimaryKey
    @SerializedName("Dn")
    public String dn;
    @SerializedName("IdStatus")
    public String statusid;
    @SerializedName("IdTransaccion")
    public String transactionId;
}
