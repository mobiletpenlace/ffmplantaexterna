package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import android.support.annotation.DrawableRes;

import java.util.ArrayList;

/**
 * Created by reype on 27/10/2017.
 */

public class PreviewViewRecicler {

    public ArrayList<PreviewVie> listPreview = new ArrayList<>();

    public static class PreviewVie {
        public String title;
        public String description;
        public @DrawableRes  int imageDrawable;

        public PreviewVie(String title, String description, @DrawableRes int imageDrawable){
            this.title = title;
            this.description = description;
            this.imageDrawable = imageDrawable;
        }
    }



}
