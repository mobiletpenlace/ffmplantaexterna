package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by totalplay on 1/6/17.
 * FieldForceManagement-Android
 */
public class Splitter implements Serializable{
    @SerializedName("nombre")
    public String name;
    @SerializedName("longitud")
    public String longitude;
    public String id = UUID.randomUUID().toString();
    @SerializedName("PuertosTotales")
    public String totalPorts;
    @SerializedName("latitud")
    public String latitude;
    @SerializedName("PuestosOcupados")
    public String occupedPorts;
    @SerializedName("Saturado")
    public String saturated;

    public int colorSplitter;

    public Splitter() {
        this.name = "";
    }

    public Splitter(String name, String latitude, String longitude, String totalPorts, String occupedPorts) {
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.totalPorts = totalPorts;
        this.occupedPorts = occupedPorts;
    }

}
