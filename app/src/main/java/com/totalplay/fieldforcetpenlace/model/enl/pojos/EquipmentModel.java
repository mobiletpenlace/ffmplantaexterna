package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;
import com.totalplay.view.CatalogEditText.CatalogObject;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by jorgehdezvilla on 26/10/17.
 * Enlace
 */

@RealmClass
public class EquipmentModel implements RealmModel, Serializable, CatalogObject {

    @PrimaryKey
    public String id = UUID.randomUUID().toString();
    @SerializedName("id_modelo")
    public String modelID;
    @SerializedName(value = "descripcion_modelo", alternate = {"nombre_modelo"})
    public String modelDescription;

    @Override
    public String toString() {
        return modelDescription;
    }
}
