package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.Quotation;
import com.totalplay.fieldforcetpenlace.modules.base.ModuleParameter;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by NS-262 on 02/11/2016.
 * TFFM
 */

@RealmClass
public class WorkOrder implements RealmModel, ModuleParameter {

    @PrimaryKey
    @SerializedName("NumeroOT")
    public String id;
    @SerializedName("Id_Operario")
    public String operarioId;
    @SerializedName("NumeroTicket")
    public String ticket;
    @SerializedName("NumeroOS")
    public String serviceOrder;
    @SerializedName("NumeroCuenta")
    public String account;

    @SerializedName("NombreCliente")
    public String clientName;
    @SerializedName("NombreContacto")
    public String contactName;
    @SerializedName("TelefonoContacto")
    public String contactPhone;
    @SerializedName("TelefonoEmpresa")
    public String businessPhone;
    @SerializedName("Extencion")
    public String phoneExt;

    @SerializedName("CanalVenta")
    public String saleChannel;

    @SerializedName("Calle")
    public String street;
    @SerializedName("NInterior")
    public String interiorNumber;
    @SerializedName("NExterior")
    public String exteriorNumber;
    @SerializedName("Colonia")
    public String colony;
    @SerializedName("Municipio")
    public String town;
    @SerializedName("Ciudad")
    public String city;
    @SerializedName("CodigoPostal")
    public String zipCode;
    @SerializedName("Referencia")
    public String reference;
    @SerializedName("entreCalles")
    public String betweenStreets;
    @SerializedName("Lat")
    public String latitude;
    @SerializedName("Long1")
    public String longitude;

    @SerializedName("PaqueteContratado")
    public String packageContracted;
    @SerializedName("Cluser")
    public String cluster;

    @SerializedName("IdEstatusOT")
    public String statusOt;
    @SerializedName("DescEstatusOT")
    public String statusOtDescription;
    @SerializedName("TipoIntervencion")
    public String interventionType;
    @SerializedName("SubTipoIntervencion")
    public String subInterventionType;
    @SerializedName("idEstadoOT")
    public String stateOtId;
    @SerializedName("descEstadoOT")
    public String descriptionStateOt;
    @SerializedName("idMotivoOT")
    public String reasontId;
    @SerializedName("DescMotivoOT")
    public String reason;
    @SerializedName("UnidadNegocio")
    public String businessUnit;
    @SerializedName("DiasRetraso")
    public String delayedDays;
    @SerializedName("VISITAS")
    public String visits;
    @SerializedName("idPropietario")
    public String ownerId;
    @SerializedName("hora_compromiso")
    public String compromiseTime;
    @SerializedName("url_imagenes")
    public String baseUrl;
    @SerializedName("url_checkList")
    public String baseUrlFileIntegrations;
    @SerializedName("tiempoEstimado")
    public String dearTime;

    @SerializedName("Info")
    public Quotation quotationInfo;
    @SerializedName("configuraciones")
    public ConfigWorkOrder configWorkOrder;
    public Olt olt;

    //// STATUS
    public boolean isTransit = false;
    public boolean isInSite = false;
    public boolean isInInstallation = false;
    public boolean isWait =false;

    //// FLOW
    public boolean isRegisterTimeArrival = false;
    public boolean isRegisterLocationArrival = false;
    public boolean isTimeEstimated = false;
    public boolean configuredDevices = false;
    public boolean configuredDN = false;
    public boolean isActivate = false;
    public boolean isPaid = false;
    public boolean isSigned = false;
    public boolean isSurveyCompleted = false;
    public boolean isEvidencesCaptured = false;
    public boolean isAcceptanceCardEvidencesCaptured = false;
    public boolean isSetttingPXB = false;
    public boolean isSettingsDispositive = false;
    public boolean isAssignedSplitter = false;
    public boolean isCapturedMaterials = false;
}
