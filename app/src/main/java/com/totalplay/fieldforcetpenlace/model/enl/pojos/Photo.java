package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Photo extends RealmObject {

    @SerializedName("imagen")
    private String photo;
    @PrimaryKey
    private String id;
    private String folder;
    @SerializedName("fecha")
    private String date;
    @SerializedName("nombre")
    private String name;
    @Expose(deserialize = false, serialize = false)
    private boolean synced;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Photo() {
        id = UUID.randomUUID().toString();
        synced = false;
    }

    public Photo(String photo) {
        this.photo = photo;
        id = UUID.randomUUID().toString();
        synced = false;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}
