package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import android.support.annotation.DrawableRes;

import java.io.Serializable;

/**
 * Created by reype on 25/10/2017.
 */

public class PreviewView implements Serializable {

    public String title;
    public String description;
    public @DrawableRes  int imageDrawable;

    public PreviewView(String title, String description, @DrawableRes int imageDrawable){
        this.title = title;
        this.description = description;
        this.imageDrawable = imageDrawable;
    }
}
