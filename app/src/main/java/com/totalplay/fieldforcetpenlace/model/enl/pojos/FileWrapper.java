package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FileWrapper extends RealmObject {

    public String folderName;
    public RealmList<Photo> photos;
    private int counter;
    @PrimaryKey
    private String id;
    public String serverFolderName;

    public FileWrapper() {
        id = UUID.randomUUID().toString();
        photos = new RealmList<>();
        counter = 1;
    }

    public FileWrapper(String folderName) {
        this.folderName = folderName;
        id = UUID.randomUUID().toString();
        photos = new RealmList<>();
    }


    public RealmList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(RealmList<Photo> photos) {
        this.photos = photos;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public void setServerFolderName(String serverFolderName) {
        this.serverFolderName = serverFolderName;
    }

    public String getServerFolderName() {
        return serverFolderName;
    }
}
