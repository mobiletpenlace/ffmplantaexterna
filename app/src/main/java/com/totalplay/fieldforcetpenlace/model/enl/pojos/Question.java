package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by totalplay on 12/19/16.
 * TFFM
 */

public class Question extends RealmObject implements Serializable{

    @SerializedName("IdPregunta")
    public String idquestion;
    @SerializedName("IdRespuesta")
    public String idResponse;
    @SerializedName("IdOT")
    public String idOTFFM;
    @SerializedName("IdOperario")
    public String idoperator;
    public String question;

    public Question() {

    }

    public Question(String idquestion, String question, String idResponse) {
        this.idquestion = idquestion;
        this.idResponse = idResponse;
        this.question = question;
    }
}