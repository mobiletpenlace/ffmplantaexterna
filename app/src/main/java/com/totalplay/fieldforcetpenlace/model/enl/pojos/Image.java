package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jorgehdezvilla on 24/09/17.
 * Enlace
 */

public class Image {
    @SerializedName("ID_OT")
    public String idOt;
    @SerializedName("ID_Type")
    public String type;
    @SerializedName("URL")
    public String url;
    @SerializedName("ID_Operario")
    public String idTechical;
    @SerializedName("Name")
    public String name;
    @SerializedName("ID_Origen")
    public String origin;
    @SerializedName("ID_Intervention")
    public String interventionId;
    @SerializedName("Longitud")
    public String longitude;
    @SerializedName("Latitud")
    public String latitude;
}
