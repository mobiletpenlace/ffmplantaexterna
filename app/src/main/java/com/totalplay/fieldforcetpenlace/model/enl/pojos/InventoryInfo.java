package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NS-TOTALPLAY on 14/11/2016.
 */

public class InventoryInfo {
    @SerializedName("operario_id")
    public String userId;
    @SerializedName("ot_id")
    public String workOrderId;
    @SerializedName("sku_id")
    private String skuId;
    private String serialNumber;
    @SerializedName("Intervention_id")
    private String interventionId;
    private String quantity;
    private String company;

    public InventoryInfo(String workOrderId, String skuId, String serialNumber,
                         String interventionId, String quantity, String company, String userId) {
        this.workOrderId = workOrderId;
        this.skuId = skuId;
        this.serialNumber = serialNumber;
        this.interventionId = interventionId;
        this.quantity = quantity;
        this.company = company;
        this.userId = userId;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getInterventionId() {
        return interventionId;
    }

    public void setInterventionId(String interventionId) {
        this.interventionId = interventionId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
