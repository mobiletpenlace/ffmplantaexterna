package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by totalplay on 11/28/16.
 * FieldForceManagement
 */

@RealmClass
public class WifiModel implements RealmModel {

    @PrimaryKey
    public String uuid = UUID.randomUUID().toString();
    public String mac;
    @SerializedName("SerialNumber")
    public String serialNumber;
    public EquipmentModel equipmentModel;
    public String srvMode;
    @SerializedName("IdPlanServicio")
    public String servicePlanId;

    public WifiModel() {

    }

    public WifiModel(String servicePlanId, String mac, String serialNumber, String srvMode, EquipmentModel equipmentModel) {
        this.servicePlanId = servicePlanId;
        this.mac = mac;
        this.serialNumber = serialNumber;
        this.equipmentModel = equipmentModel;
        this.srvMode = srvMode;
    }
}
