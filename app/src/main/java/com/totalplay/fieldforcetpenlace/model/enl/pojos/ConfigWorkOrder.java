package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by mahegots on 11/07/17.
 */

public class ConfigWorkOrder extends RealmObject {

    @PrimaryKey
    public String uuid = UUID.randomUUID().toString();
    public InternetModel internet;
    public RealmList<WifiModel> wifiModels = new RealmList<>();
    @SerializedName("telefono")
    public DNPhones dnPhones = new DNPhones();

}
