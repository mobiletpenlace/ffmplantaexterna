package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.util.ArrayList;

/**
 * Created by NS-TOTALPLAY on 06/06/2017.
 */

public class StopExtern {
    public final String key;
    public final String name;

    private static ArrayList<StopExtern> inciden = new ArrayList<StopExtern>() {{
        add(new StopExtern("1", "CAMBIO ACOMETIDA"));
        add(new StopExtern("2", "CAMBIO CONECTOR SC ROSETA"));
        add(new StopExtern("3", "CAMBIO CONECTOR SC SPLITTER"));
        add(new StopExtern("4", "CAMBIO CONECTORES SC  AMBOS EXTREMOS\t\n"));
        add(new StopExtern("5", "CAMBIO UTP"));
        add(new StopExtern("6", "CONFIGURACIÓN DE EQUIPOS"));
    }};

    public static ArrayList<StopExtern> incidents() {
        return inciden;
    }

    private static ArrayList<StopExtern> internsS = new ArrayList<StopExtern>() {{
        add(new StopExtern("1", "CAMBIO ACOMETIDA"));
        add(new StopExtern("2", "CAMBIO CONECTOR SC ROSETA"));
    }};

    public static ArrayList<StopExtern> getInternsS() {
        return internsS;
    }

    private static ArrayList<StopExtern> clientsI = new ArrayList<StopExtern>() {{
        add(new StopExtern("3", "CAMBIO CONECTOR SC SPLITTER"));
        add(new StopExtern("4", "CAMBIO CONECTORES SC  AMBOS EXTREMOS\t\n"));
        add(new StopExtern("5", "CAMBIO UTP"));
        add(new StopExtern("6", "CONFIGURACIÓN DE EQUIPOS"));
    }};

    public static ArrayList<StopExtern> getClientsI() {
        return clientsI;
    }

    public StopExtern(String key, String name) {
        this.key = key;
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
