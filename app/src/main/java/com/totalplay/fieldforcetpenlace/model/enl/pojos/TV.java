package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 1/17/17.
 * FieldForceManagement-Android
 */
public class TV extends RealmObject {

    @PrimaryKey
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("IdPlanServicio")
    public String servicePlanId;
    public String mac;
    @SerializedName("SerialNumber")
    public String serialNumber;
    public String equipmentModel;
    public String srvMode;

}
