package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by totalplay on 11/22/16.+
 * FFM
 */
@RealmClass
public class Assistant implements RealmModel {

    @PrimaryKey
    @SerializedName("ID_Auxiliar")
    public String auxId = "";
    @SerializedName(value = "Nombre", alternate = {"Nombre_Auxiliar"})
    public String name = "";
    public String date;

    @Override
    public String toString() {
        return name;
    }
}
