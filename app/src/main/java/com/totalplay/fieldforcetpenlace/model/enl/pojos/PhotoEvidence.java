package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by charlssalazar on 29/06/17.
 * Enlace
 */

public class PhotoEvidence extends RealmObject implements Serializable{


    @PrimaryKey
    public String id = UUID.randomUUID().toString();
    //    @SerializedName("imagen")
//    public String photo;
    public String folder = "";
    @SerializedName("fecha")
    public String date = "";
    @SerializedName("nombre")
    public String name = "";
    public String path = "";
    public String type = "";

    public String workOrder = "";
    public String baseUrl = "";
    public String serverFolder = "";
    public String interventionType = "";

    @Expose(deserialize = false, serialize = false)
    public boolean synced = false;


}
