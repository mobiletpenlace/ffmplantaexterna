package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by totalplay on 11/28/16.
 * FieldForceManagement
 */
@RealmClass
public class InternetModel implements RealmModel {

    @PrimaryKey
    public String uuid = UUID.randomUUID().toString();
    @SerializedName("SerialNumber")
    public String serialNumber;
    public String mac;
    public String servicePlanId;
    @SerializedName("OLT")
    public String olt;
    @SerializedName("IdOLT")
    public String oltId;
    public String frame;
    @SerializedName("SRV_Mode")
    public String srvMode;
    @SerializedName("Slot")
    public String slot;
    @SerializedName("Puerto")
    public String port;
    public EquipmentModel equipmentModel;

    public InternetModel(){

    }

    public InternetModel(String servicePlanId, String srvMode, String mac, String olt, String oltId, String port, String slot, String frame, String serialNumber, EquipmentModel equipmentModel) {
        this.servicePlanId = servicePlanId;
        this.srvMode = srvMode;
        this.mac = mac;
        this.olt = olt;
        this.oltId = oltId;
        this.port = port;
        this.slot = slot;
        this.frame = frame;
        this.serialNumber = serialNumber;
        this.equipmentModel = equipmentModel;
    }
}
