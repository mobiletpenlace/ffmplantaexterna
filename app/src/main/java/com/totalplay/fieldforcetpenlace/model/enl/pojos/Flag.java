package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by jorgehdezvilla on 28/09/17.
 * Enlace
 */

@RealmClass
public class Flag implements RealmModel {

    @PrimaryKey
    @SerializedName("ID_Flag")
    public String flagId;
    @SerializedName("Descripcion_Flag")
    public String flagDescription;
    @SerializedName("On_Off")
    public String onOff;

}