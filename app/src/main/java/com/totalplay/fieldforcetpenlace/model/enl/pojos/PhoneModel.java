package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import java.util.List;

/**
 * Created by totalplay on 11/28/16.
 * FieldForceManagement
 */
public class PhoneModel extends BaseModel {

    public List<DN> dns;
    public String mainDN;

    public PhoneModel(String servicePlanId, List<DN> dns, String mainDN) {
        this.servicePlanId = servicePlanId;
        this.dns = dns;
        this.mainDN = mainDN;
    }

}
