package com.totalplay.fieldforcetpenlace.model.enl.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by totalplay on 12/13/16.
 * Enlace
 */
public class Olt extends RealmObject {

    @PrimaryKey
    public String _uuid = UUID.randomUUID().toString();
    @SerializedName("OltName")
    public String name;
    @SerializedName("Frame")
    public String frame;
    @SerializedName("Slot")
    public String slot;
    @SerializedName("Port")
    public String port;

}
