package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by jorgehdezvilla on 01/08/17.
 * FMMTpe
 */

public class SupportCatalogWorkType {

    public static final String INTERNAL = "0";
    public static final String EXTERNAL = "1";

}
