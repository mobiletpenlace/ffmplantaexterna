package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by jorgehdezvilla on 01/08/17.
 * FFMPTpe
 */

public class CatalogSupportType {

    public static final String FAIL_CATALOG = "F";
    public static final String REASON_CATALOG = "R";
    public static final String SOLUTIONS_CATALOG = "S";

}
