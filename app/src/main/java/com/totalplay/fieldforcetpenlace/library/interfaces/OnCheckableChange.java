package com.totalplay.fieldforcetpenlace.library.interfaces;

import android.widget.CheckBox;

import java.util.List;

/**
 * Created by totalplay on 11/17/16.
 */
public interface OnCheckableChange<T> {

    void onCheckedChange(CheckBox checkBox, int position, List<T> objects);
}
