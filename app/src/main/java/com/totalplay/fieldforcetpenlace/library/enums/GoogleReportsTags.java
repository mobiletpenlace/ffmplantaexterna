package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/24/16.
 * Total
 */
public class GoogleReportsTags {
    public static final String REQUEST = "request";
    public static final String RESPONSE = "response";
    public static final String ERROR = "error";
    public static final String INIT = "init";
}
