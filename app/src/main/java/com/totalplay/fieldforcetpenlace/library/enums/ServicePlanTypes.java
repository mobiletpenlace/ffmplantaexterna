package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/18/16.
 */
public class ServicePlanTypes {
    public static final String INTERNET = "internet";
    public static final String TV = "television";
    public static final String PHONE = "telefono";
    public static final String ADDITIONAL_TV = "television_adicional";
}
