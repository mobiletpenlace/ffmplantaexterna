package com.totalplay.fieldforcetpenlace.library.interfaces;

/**
 * Created by mahernandezgom on 18/04/2016.
 */
public interface OnDialogOptionSelectedListener<T> {

    void onOptionSelected(int index, T object, String comment);
}
