package com.totalplay.fieldforcetpenlace.library.utils;

public class Constants {
    public static final long GOOGLE_API_CLIENT_TIMEOUT_S = 10;
    public static final String GOOGLE_API_CLIENT_ERROR_MSG =
            "Failed to connect to GoogleApiClient (error code = %d)";
}