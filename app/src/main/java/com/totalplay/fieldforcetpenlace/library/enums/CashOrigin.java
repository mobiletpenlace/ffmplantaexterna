package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 1/19/17.
 * FieldForceManagement-Android
 */
public class CashOrigin {
    public static final String CASH = "1";
    public static final String CHECK = "2";
    public static final String TRANSFER = "3";
    public static final String CARD = "4";
    public static final String NONE = "5";
}
