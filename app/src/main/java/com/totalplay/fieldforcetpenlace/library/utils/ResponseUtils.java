package com.totalplay.fieldforcetpenlace.library.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by NS-TOTALPLAY on 11/05/2017.
 */

public class ResponseUtils {
    public static String getServerURL(Response<ResponseBody> response) throws IOException {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(response.body().byteStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }
}
