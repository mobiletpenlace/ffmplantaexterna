package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/25/16.
 */
public enum AutoFindErrorReason {
    COULD_NOT_GET_AUTOFIND,
    COULD_NOT_VALID_SERIAL_NUMBER,
    SERIAL_NUMBER_ALREDY_EXISTS,
    COULD_NOT_GET_MAC,
    EMPTY_SERIAL_NUMBER
}
