package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 7/18/17.
 * FieldForceManagement
 */

public class ServerFolders {
    public static final String SPLITTERS = "Splitters";
    public static final String DOCUMENTS = "Documents";
    public static final String EVIDENCES = "Evidences";
    public static final String ACCEPTANCE_CARD = "AcceptanceCard";
    public static final String ARRIVAL = "Arrival";
    public static final String PROFILE = "Profile";
}
