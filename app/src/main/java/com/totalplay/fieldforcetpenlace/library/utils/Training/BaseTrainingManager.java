package com.totalplay.fieldforcetpenlace.library.utils.Training;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;
import uk.co.samuelwall.materialtaptargetprompt.extras.backgrounds.RectanglePromptBackground;
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal;

import static uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt.STATE_DISMISSED;
import static uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt.STATE_FINISHED;
import static uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt.STATE_FOCAL_PRESSED;
import static uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt.STATE_NON_FOCAL_PRESSED;

/**
 * Created by jorgehdezvilla on 11/11/17.
 * Enlace
 */

public abstract class BaseTrainingManager {

    public static boolean TRAINING_ENABLED = false;

    protected AppCompatActivity appCompatActivity;
    private List<ViewTraining> viewTrainingList;
    private List<ViewTraining> viewBlocks = new ArrayList<>();

    int promptIndex = 0;

    BaseTrainingManager(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        viewTrainingList = new ArrayList<>();
    }

    void startTraining() {
        if (TRAINING_ENABLED) {
            if (viewTrainingList.size() > 0) {
                createPrompt(viewTrainingList.get(0));
            }
        }
    }

    void nextPrompt() {
        promptIndex++;
        if (viewTrainingList.size() > promptIndex) createPrompt(viewTrainingList.get(promptIndex));
    }

    void createPrompt(ViewTraining viewTraining) {
        View view = appCompatActivity.findViewById(viewTraining.view);
        if (view != null) {
            disable(view);
        }
        initMaterialTapView(viewTraining).setPromptStateChangeListener((prompt, state) -> enableView(state, viewTraining, viewTraining.trainingManagerCallback)).show();
    }

    public interface TrainingManagerCallback {
        void onNextClick();
    }

    void generateViewTraining(int viewId, String title, String description) {
        ViewTraining viewTraining = new ViewTraining(viewId, title, description, null);
        viewTrainingList.add(viewTraining);
    }

    void generateViewTraining(int viewId, String title, String description, TrainingManagerCallback trainingManagerCallback) {
        ViewTraining viewTraining = new ViewTraining(viewId, title, description, trainingManagerCallback);
        viewTrainingList.add(viewTraining);
    }

    private int oneStatus = 0;
    private int twoStatus = 0;
    private int threeStatus = 0;

    private void enableView(int state, ViewTraining viewTraining, TrainingManagerCallback trainingManagerCallback) {
        if (state == STATE_FOCAL_PRESSED || state == STATE_NON_FOCAL_PRESSED) {
            oneStatus = 0;
            twoStatus = 0;
            threeStatus = 0;
            if (trainingManagerCallback != null)
                trainingManagerCallback.onNextClick();
        }
        if (oneStatus == 0) oneStatus = state;
        else if (twoStatus == 0) twoStatus = state;
        else if (threeStatus == 0) threeStatus = state;

        com.orhanobut.logger.Logger.d(String.format(Locale.getDefault(), "State: %d, One: %d, two: %d, trhee: %d", state, oneStatus, twoStatus, threeStatus));

        if (oneStatus == STATE_FOCAL_PRESSED && threeStatus == STATE_FINISHED) {
            View view = appCompatActivity.findViewById(viewTraining.view);
            if (view != null) {
                enableViews();
            }
        } else if (oneStatus == STATE_NON_FOCAL_PRESSED && (threeStatus == STATE_DISMISSED || threeStatus == STATE_FINISHED)) {
            View view = appCompatActivity.findViewById(viewTraining.view);
            if (view != null) {
                enableViews();
            }
        }
    }

    private void enableViews() {
        for (ViewTraining viewTraining : viewTrainingList) {
            View view = appCompatActivity.findViewById(viewTraining.view);
            if (view != null) {
                enable(view);
            }
        }
        for (ViewTraining viewTraining : viewBlocks) {
            View view = appCompatActivity.findViewById(viewTraining.view);
            if (view != null) {
                enable(view);
            }
        }
    }

    private static void disable(View layout) {
        layout.setEnabled(false);
        if (layout instanceof ViewGroup) {
            ViewGroup viewGroup = ((ViewGroup) layout);
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                if (child instanceof ViewGroup) {
                    disable(child);
                } else {
                    child.setEnabled(false);
                }
            }
        }
    }

    private static void enable(View layout) {
        layout.setEnabled(true);
        if (layout instanceof ViewGroup) {
            ViewGroup viewGroup = ((ViewGroup) layout);
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                if (child instanceof ViewGroup) {
                    enable(child);
                } else {
                    child.setEnabled(true);
                }
            }
        }
    }

    private MaterialTapTargetPrompt.Builder initMaterialTapView(ViewTraining viewTraining) {
        return new MaterialTapTargetPrompt.Builder(appCompatActivity)
                .setTarget(viewTraining.view)
                .setPrimaryText(viewTraining.title)
                .setSecondaryText(viewTraining.description)
                .setPromptBackground(new RectanglePromptBackground())
                .setPromptFocal(new RectanglePromptFocal());
    }

    public class ViewTraining {
        public int view;
        public String title;
        public String description;
        TrainingManagerCallback trainingManagerCallback;

        public ViewTraining(int view, String title, String description, TrainingManagerCallback trainingManagerCallback) {
            this.view = view;
            this.title = title;
            this.description = description;
            this.trainingManagerCallback = trainingManagerCallback;
        }
    }

    public void disableView(int viewId) {
        if (TRAINING_ENABLED) {
            viewBlocks.add(new ViewTraining(viewId, "", "", null));
            View view = appCompatActivity.findViewById(viewId);
            if (view != null) {
                disable(view);
            }
        }
    }

    public boolean isTrainingMode() {
        return TRAINING_ENABLED;
    }

}
