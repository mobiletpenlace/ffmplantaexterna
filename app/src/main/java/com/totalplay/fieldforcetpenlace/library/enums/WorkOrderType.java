package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/22/16.
 * Enlace
 */
public class WorkOrderType {
    public static final String PAST = "past";
    public static final String ADDON = "65";
    public static final String SUPPORT = "55";
    public static final String INSTALLATION = "48";
    public static final String EXTERNAL = "30";
    public static final String SITE = "49";
}
