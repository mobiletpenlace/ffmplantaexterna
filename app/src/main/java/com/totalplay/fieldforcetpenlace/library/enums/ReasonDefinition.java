package com.totalplay.fieldforcetpenlace.library.enums;

import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;

import java.util.ArrayList;

/**
 * Created by NS-TOTALPLAY on 28/06/2017.
 * Enlace
 */

public class ReasonDefinition {

    public final static String id = "113";

    public final static Reason reasonTranslate = new Reason("121", "PROBLEMAS EN EL TRASLADO");

    public final static ArrayList<Reason> reasonsLocation = new ArrayList<Reason>() {{
        add(new Reason("55", "MAL CLIMA"));
        add(new Reason("53", "FALTA DE PERMISOS EN CALLE"));
        add(new Reason("97", "ZONA DE ALTO RIESGO"));
        add(new Reason("116", "NO EXISTE DOMICILIO"));
    }};

    public static ArrayList<Reason> getReasonsWorkOrder(WorkOrder workOrder) {
        ArrayList<Reason> reasonsWorkOrder = new ArrayList<>();
        if (workOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
            reasonsWorkOrder = new ArrayList<Reason>() {{
                add(new Reason("66", "NO HAY SISTEMA"));
                add(new Reason("101", "REPARACIÓN CFE"));
                add(new Reason("109", "PODA DE ÁRBOL"));
                add(new Reason("111", "TRABAJO DE OTROS CARRIER"));
                add(new Reason("49", "FALTA DE PAGO"));
                add(new Reason("52", "CLIENTE REAGENDA"));
                add(new Reason("95", "SIN PERMISOS DE ACCESO"));
                add(new Reason("27", "CLIENTE NO SE ENCUENTRA"));
            }};
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(workOrder.ownerId)) {
            reasonsWorkOrder = new ArrayList<Reason>() {{
                add(new Reason("121", "PROBLEMAS EN EL TRASLADO"));
                add(new Reason("52", "CLIENTE REAGENDA"));
                add(new Reason("95", "SIN PERMISOS DE ACCESO"));
                add(new Reason("27", "CLIENTE NO SE ENCUENTRA"));
            }};
        }
        return reasonsWorkOrder;
    }

    public final static ArrayList<Reason> reasonsRegister = new ArrayList<Reason>() {{
        add(new Reason("69", "SPLITER ATENUADO"));
        add(new Reason("70", "SPLITER SATURADO"));
        add(new Reason("112", "SPLITER NO ILUMINADO"));
        add(new Reason("118", "SPLITTER PUERTOS DAÑADOS"));
        add(new Reason("120", "ANCHO DE BANDA INSUFICIENTE"));
        add(new Reason("98", "AFECTACION MASIVA"));
    }};


    public final static ArrayList<Reason> reasonsSplitter = new ArrayList<Reason>() {{
        add(new Reason("119", "FUERA DE COBERTURA"));
    }};
}

