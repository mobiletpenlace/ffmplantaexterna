package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by jorgehdezvilla on 28/12/17.
 * FFM Enlace
 */

public class ParamsEnum {

    public static final String ID_SPLITTER = "1";
    public static final String LAT_SPLITTER = "2";
    public static final String LONG_SPLITTER = "3";
    public static final String USED_PORTDS = "4";
    public static final String TOTAL_PORTS = "5";

}
