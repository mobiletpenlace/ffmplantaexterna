package com.totalplay.fieldforcetpenlace.library.utils;

/**
 * Created by mahegots on 18/10/16.
 * Enlace
 */
public class Keys {
    public static final String DEBUG = "Stack Debug";
    public static final String SCAN_RESULT = "scan_result";
    public static final String CREDIT_CARD = "credit_card";
    public static final String LAST_DIGITS_NUMBER = "astFourDigitsOfCardNumber";
    public static final String EXPIRY_DATE = "expiryDate";
    public static final String EXTRA_MATERIALS = "extraMaterials";
    public static final String FCM_TOKEN = "fcmToken";
    public static final String PHOTO_SELECTED = "photo_selected";
    public static final String FOLDER_WRAPPER = "folder_wrapper";
    public static final String EXTRA_IS_INTERNET = "extraInternet";
    public static final String EXTRA_CVV_CREDIT_CARD = "extraCvvCreditCard";
    public static final String EXTRA_TYPE_PAYMENT_CARD = "extraTypePaymentCard";
    public static final String EXTRA_CHANGE_STATUS_TYPE = "extraChangeStatus";
    public static final String EXTRA_SERVICE_PLAN_ID = "extraServicePlanId";

    public static final String EXTRA_WORK_ORDER_INSTALLATION_ENABLED = "extraWorkOrderInstallationEnabled";
    public static final String EXTRA_PORT = "extraPort";
    public static final String EXTRA_BANK_RESPONSE_AUTHORIZATION_CODE = "authorizationCode";
    public static final String EXTRA_BANK_RESPONSE_BANK_RESPONSE_CODE = "bankResponseCode";
    public static final String EXTRA_BANK_RESPONSE_DESCRIPTION_RESPONSE = "descriptionResponse";
    public static final String EXTRA_BANK_RESPONSE_STANDARD_RESPONSE_CODE = "standardResponseCode";
    public static final String EXTRA_ETA_FROM_NOTIFICATION = "extraFromNotification";
    public static final String EXTRA_IMAGE_PATH = "extraImagePath";
    public static final String EXTRA_SPLITTERS_CAN_ADD = "extraSplittersCanAdd";
    public static final String EXTRA_SPLITTER = "extraSplitter";
    public static final String EXTRA_REASON = "extraReason";

    public static final String PREF_LAST_TIME_LOGGED = "prefLastTimeLogged";
    public static final String EXTRA_IS_MAC = "extraIsMac";
    public static final String ASSISTANT_IS_CHANGING = "isChanging";
    public static final String EXTRA_USER = "extra_user";
    public static final String READY_TO_ACTIVATE = "readyToActivate";
    public static final String NOTIFICATION_TYPE = "type";
    public static final String CASH_ORIGIN = "cashOrigin";
    public static final String EQUIPMENT_TYPE = "equipmentType";
    public static final String EXTRA_PREVIEW_VIEW = "extraPreviewView";
    public static final String FLOW_TYPE = "flowType";
    public static final String EXTRA_BASE_MODULE = "extraBaseModule";
    public static final String CAN_ADD = "canAdd";
}
