package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/24/16.
 */
public class ActivationStatus {
    public static final String PENDING_UPDATE_INTERNET = "updating_internet";
    public static final String PENDING_UPDATE_TV = "updating_tv";
    public static final String PENDING_UPDATE_DN = "updating_dn";
    public static final String PENDING_ACTIVATE = "activating";
    public static final String ACTIVATED = "activated";
}
