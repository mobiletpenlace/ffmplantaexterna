package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/25/16.
 */
public enum UpdateActivationErrorReason {
    COULD_NOT_UPDATE_EQUIPMENT,
    COULD_NOT_UPDATE_EQUIPMENT_TV,
    COULD_NOT_UPDATE_DNS,
    COULD_NOT_ATIVATE
}
