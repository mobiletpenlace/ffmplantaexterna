package com.totalplay.fieldforcetpenlace.library.enums;

import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.UserStatus;

import java.util.ArrayList;

/**
 * Created by totalplay on 11/22/16.
 * FieldForceManagement
 */
public class OperatorStatus {

    //private static final String SITE = "EN SITIO";
    public static final String AVAILABLE = "DISPONIBLE";
    private static final String BUSY = "OCUPADO";
    private static final String FREE_DAY = "DIA LIBRE";
    private static final String EATING = "EN COMIDA";
    private static final String VACATION = "VACACIONES";
    private static final String DEPOT = "EN ALMACEN";
    public static final String OUT_SERVICE = "FUERA DE SERVICIO";
    public static final String TECHNICAL_SUPPORT = "APOYO TÉCNICO";

    public static ArrayList<UserStatus> status = new ArrayList<UserStatus>() {{
        add(new UserStatus("1", AVAILABLE, R.color.operator_available));
        add(new UserStatus("2", BUSY, R.color.operator_busy));
        add(new UserStatus("3", FREE_DAY, R.color.operator_free_day));
        add(new UserStatus("4", EATING, R.color.operator_eating));
        add(new UserStatus("5", VACATION, R.color.operator_vacation));
        add(new UserStatus("6", DEPOT, R.color.operator_depot));
        add(new UserStatus("7", OUT_SERVICE, R.color.operator_out_service));
        add(new UserStatus("8", TECHNICAL_SUPPORT, R.color.operator_technical_support));
    }};

}
