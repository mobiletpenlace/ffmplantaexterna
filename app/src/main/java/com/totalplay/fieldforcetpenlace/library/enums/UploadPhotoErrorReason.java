package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/25/16.
 */
public enum UploadPhotoErrorReason {
    COULD_NOT_CREATE_FILE,
    COULD_NOT_UPLOAD_FILE
}
