package com.totalplay.fieldforcetpenlace.library.utils.Training;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.totalplay.fieldforcetpenlace.R;

/**
 * Created by jorgehdezvilla on 11/11/17.
 * Enlace
 */

public class TrainingManager extends BaseTrainingManager {

    private TrainingManager(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    public static TrainingManager init(AppCompatActivity appCompatActivity) {
        return new TrainingManager(appCompatActivity);
    }

    public void mainActivity(TrainingManagerCallback trainingManagerCallback) {
        TRAINING_ENABLED = true;
        appCompatActivity.findViewById(R.id.act_main_map_fragment_ot_assigned_test_content).setVisibility(View.VISIBLE);
        generateViewTraining(R.id.act_main_change_status,
                "Cambio de estatus",
                "Puedes especificar si te encuentras disponible o en alguna otra actividad",
                this::nextPrompt);
        generateViewTraining(R.id.act_main_open_menu,
                "Opciones",
                "En éste botón podrás encontrar opciones como: Cambiar auxiliar, Iniciar Capacitación, Ver Introducción o Cerrar sesión",
                this::nextPrompt);
        generateViewTraining(R.id.act_img_refresh,
                "Actualizar ubicación",
                "Si no actualiza de manera automática tu ubicacion, presiona éste botón para actualizar",
                this::nextPrompt);
        generateViewTraining(R.id.act_main_map_fragment_ot_assigned_test,
                "OT Asignada",
                "Cuando tu despacho te asigne una nueva orden de trabajo, el contador aparecerá en 1. Para empezar el flujo de trabajo, presiona el botón \"INICIAR OT\"",
                this::nextPrompt);
        generateViewTraining(R.id.act_main_content_actions_training,
                "¿Tienes duda de cómo utilizar la app?",
                "Presiona este botón para iniciar una Orden de Trabajo de Capacitación y así puedas aclarar tus dudas",
                this::nextPrompt);
        generateViewTraining(R.id.act_main_splitters,
                "Splitters",
                "Te muestra un mapa con los splitters cercanos a tu ubicación",
                this::nextPrompt);
        generateViewTraining(R.id.act_main_profile,
                "Perfil",
                "Podrás observar tu informacion y cambiar tu foto de perfil", () -> {
                    appCompatActivity.findViewById(R.id.act_main_map_fragment_ot_assigned_test_content).setVisibility(View.GONE);
                    TRAINING_ENABLED = false;
                    trainingManagerCallback.onNextClick();
                }
        );
        startTraining();

    }

    public void workOrderInfoActivity() {
        generateViewTraining(R.id.menu2,
                "Nueva Instalación",
                "Se te mostrará la información general de la OT a iniciar",
                this::nextPrompt);
        generateViewTraining(R.id.act_installation_start_trip,
                "Nueva Instalación",
                "Cuando valides la información, podrás continuar con el viaje");
        startTraining();
    }

    public void workOrderInfoInRouteActivity(TrainingManagerCallback trainingManagerCallback) {
        generateViewTraining(R.id.bottom_sheet_content,
                "En Ruta",
                "Por medio de estas dos aplicaciones podrás trazar la ruta más rápida a tu destino, utiliza la de tu agrado",
                () -> {
                    appCompatActivity.findViewById(R.id.bottom_sheet_content).setVisibility(View.GONE);
                    nextPrompt();
                });
        generateViewTraining(R.id.menu_travel_problems,
                "En Ruta",
                "Si tienes problemas en tu viaje puedes pulsar este boton",
                trainingManagerCallback);
        startTraining();
    }

    public void workOrderInfoInSiteActivity() {
        generateViewTraining(R.id.act_installation_arrival,
                "Confimar Ubicación",
                "Cuando te encuentres en el domicilio, se te mostrará nuevamente la información general de la OT",
                this::nextPrompt);
        generateViewTraining(R.id.act_installation_confirm_location,
                "Confimar Ubicación",
                "Cuando valides la información, podrás confirmar tu llegada");
        startTraining();
    }

    public void arrivalTimeActivity() {
        generateViewTraining(R.id.act_arrival_time_image_congratulation,
                "Puntualidad",
                "Se te indicará si has llegado a tiempo a tu OT, recuerda que la puntualidad es importante",
                this::nextPrompt);
        generateViewTraining(R.id.act_good_arrival_next,
                "Continuar",
                "Presiona continuar para continuar con tu OT");
        startTraining();
    }

    public void registerLocationArrivalActivity() {
        generateViewTraining(R.id.photo_layout,
                "Registro de Ubicación",
                "Toma evidencia fotográfica de que has llegado al domicilio de tu OT",
                this::nextPrompt);
        generateViewTraining(R.id.act_arrival_references_content,
                "Registro de Ubicación",
                "Escribe un breve comentario de las referencias urbanas cercanas al domicilio donde te encuentras",
                this::nextPrompt);
        generateViewTraining(R.id.act_arrival_next,
                "Continuar",
                "Cuando hayas capturado toda la información, presiona continuar para continuar con tu OT",
                this::nextPrompt);
        generateViewTraining(R.id.toolbar_report,
                "¿Tuviste algún problema?",
                "Si te encuentras en ésta situación, presiona en este icono para que puedas reportar con despacho el problema que tienes");
        startTraining();
    }

    public void detailOrderWorkActivity() {
        generateViewTraining(R.id.act_detail_work_order_name_title,
                "Orden de trabajo",
                "Se te mostrará información más específica de tu OT que te ayudará en tu proceso de trabajo",
                this::nextPrompt);
        generateViewTraining(R.id.act_work_order_detail_next,
                "¿Toda la información es correcta?",
                "Presiona iniciar instalación para continuar",
                this::nextPrompt);
        generateViewTraining(R.id.toolbar_report,
                "¿Tuviste algún problema?",
                "Si te encuentras en ésta situación, presiona en este icono para que puedas reportar con despacho el problema que tienes");
        startTraining();
    }

    public void updateDearTimeActivity() {
        generateViewTraining(R.id.estimate_time,
                "Tiempo Estimado",
                "Ajusta el tiempo que estimas que te tomará completar la OT, así ayudarás a tu despacho a saber si te encuentras disposible para otra OT",
                this::nextPrompt);
        generateViewTraining(R.id.act_update_estimated_time_save_button,
                "Tiempo Estimado",
                "Una vez estimado tu tiempo, presiona aceptar para continuar");
        startTraining();
    }

    public void splittersActivity(TrainingManagerCallback trainingManagerCallback) {
        generateViewTraining(R.id.splitter_content_description,
                "Splitters",
                "Te aparecerán los splitters más cercanos a tu posición, cada color representa un estado diferente, tal como se menciona",
                this::nextPrompt);
        generateViewTraining(R.id.act_splitters_info_splitter_demo,
                "Splitters - Actualización",
                "Si ya has ubicado splitters, presiónalos para ver su información, en caso de ser el splitter con el que trabajarás, presiona sobre la información para actualizarlo",
                this::nextPrompt);
        generateViewTraining(R.id.act_splitters_add_splitter,
                "Splitters - Hallazgo",
                "En algunos escenarios tendrás que realizar un hallazgo de splitter, para ello, presiona sobre éste botón",
                this::nextPrompt);
        generateViewTraining(R.id.toolbar_report,
                "¿Tuviste algún problema?",
                "Si te encuentras en ésta situación, presiona en este icono para que puedas reportar con despacho el problema que tienes",
                trainingManagerCallback);
        startTraining();
    }

    public void updateSplitterActivity() {
        disableView(R.id.act_register_splitter_scroll_content);
        generateViewTraining(R.id.act_edit_splitter_title,
                "Registro de Splitter",
                "Te aparecerá el siguiente formulario con la información del splitter seleccionado, validarás y actualizarás la información necesaria",
                this::nextPrompt);
        generateViewTraining(R.id.port_capture,
                "Registro de Splitter",
                "Cuando intentes asignar el valor del puerto, se te mostrarán diferentes pantallas donde podrás elegir por medio de imágenes, el puerto que asignaste para tu trabajo",
                this::nextPrompt);
        generateViewTraining(R.id.photo_splitter,
                "Registro de Splitter",
                "Toma evidencia fotográfica de tu trabajo con el splitter",
                () -> {
                    ViewGroup viewGroup = appCompatActivity.findViewById(R.id.act_splitter_actions_content);
                    ScrollView scrollView = appCompatActivity.findViewById(R.id.act_register_splitter_scroll_content);
                    scrollView.scrollTo(0, viewGroup.getBottom());
                    nextPrompt();
                });
        generateViewTraining(R.id.act_splitter_actions_content,
                "Registro de Splitter",
                "Dependiendo de tu splitter y las ordenes de tu despacho, podrás buscar otro splitter en caso de ser necesario, o continuar asignando el puerto al clienre"
                , this::nextPrompt);
        generateViewTraining(R.id.toolbar_report,
                "¿Tuviste algún problema?",
                "Si te encuentras en ésta situación, presiona en este icono para que puedas reportar con despacho el problema que tienes");
        startTraining();
    }

    public void activationServiceActivity() {
        disableView(R.id.act_service_activation_view_pager);
        generateViewTraining(R.id.toolbar_2,
                "Activación de Equipos",
                "Dependiendo de tu paquete a instalar, deberás instalar los diferentes equipos que son: Telefonía, Wifi Extender, Internet. Si el paquete no cuenta con alguno de ellos, el icono correspondiente no aparecerá",
                this::nextPrompt);
        generateViewTraining(R.id.act_service_activation_title,
                "Activación de Equipos",
                "Por cada tipo de equipo a instalar, te aparecerá una pantalla con la información necesaria y los campos para que ingreses los números de serie. ",
                this::nextPrompt);
        generateViewTraining(R.id.act_service_activation_title,
                "Información de Telefonía",
                "Podrás ver el rango de los dns asignados para tu OT, así mismo cual es el dn principal",
                this::nextPrompt
        );
        generateViewTraining(R.id.act_service_settings_equipment_actions_content,
                "Activación de Equipos",
                "Para que puedas navegar entre pantañas tendrás estos iconos para moverte entre la información de los diferentes disposivitos",
                () -> {
                    ViewPager viewPager = appCompatActivity.findViewById(R.id.act_service_activation_view_pager);
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    nextPrompt();
                }
        );
        generateViewTraining(R.id.act_service_activation_title,
                "Información de Wifi Extender",
                "Dependiendo de la cantidad de wifi extender a instalar, te aparecerá un formulario por cada equipo, en donde ingresarás No. de Serie para consultar la información de su MAC",
                () -> {
                    ViewPager viewPager = appCompatActivity.findViewById(R.id.act_service_activation_view_pager);
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    nextPrompt();
                });
        generateViewTraining(R.id.act_service_activation_title,
                "Información de Internet",
                "Para configurar tu ONT, deberás capturar el número de serie del dispositivo para que puedas recuperar la información necesaria para la configuración",
                () -> {
                    ViewPager viewPager = appCompatActivity.findViewById(R.id.act_service_activation_view_pager);
                    viewPager.setCurrentItem(0);
                    nextPrompt();
                });
        startTraining();
    }

    public void evidenceActivity() {
        generateViewTraining(R.id.photo_evidence_01,
                "Evidencia",
                "tomamos de 1 a 6 fotos como evidencia del trabajo realizado en la instalacion",
                this::nextPrompt);
        generateViewTraining(R.id.continueButton,
                "Evidencia",
                "una vez tomada la evidencia continuamos");
        startTraining();
    }

    public void settingEquipmentsActivity() {
        generateViewTraining(R.id.act_equipments_view_pager,
                "Configuracion de addons",
                "introduce los datos de cada dispositivo para configurarlos",
                this::nextPrompt);
        generateViewTraining(R.id.act_equipments_continue,
                "Configuracion de addons",
                "una vez cargada la configuracion de los addons puedes continuar con el flujo");
        startTraining();
    }

    public void surveyActivity() {
        generateViewTraining(R.id.question1,
                "encuaesta ",
                "el cliente pondera segun su opinion cada una de las preguntas",
                this::nextPrompt);
//        generateViewTraining(R.id.act_survey_acept_button,
//                "Encuesta",
//                "una vez que el cliente llene cada pregunta se envia");
        startTraining();
    }

    public void usedMAterialActivity() {
        generateViewTraining(R.id.materials_title,
                "Materiales",
                "se registra el total de materiales que utilizase edurante la instalacion",
                this::nextPrompt);
        generateViewTraining(R.id.act_used_materials_finish_button,
                "Materiales",
                "ya que se hayan registrado la totalidad de materialse usados procedemos a continuar  terminar la orden");
        startTraining();
    }

    public void acceptanceActivity() {
        generateViewTraining(R.id.photo_acta_01,
                "Aceptación",
                "tomamos de 1 a 3 fotos de la carta de aceptacion",
                this::nextPrompt);
        generateViewTraining(R.id.continueButton,
                "Aceptacion",
                "una vez tomada la evidencia continuamos");
        startTraining();
    }

    public void resumeWorkOrderActivity() {
        generateViewTraining(R.id.resume_layout,
                "Resumen de OT",
                "verifica la informacion de la ot que estas terminando",
                this::nextPrompt);
        generateViewTraining(R.id.act_resume_order_work_next,
                "Resumen de OT",
                "una vez que hayas validado la informacion podemos continuar");
        startTraining();
    }

    public void paymentActivity() {
        generateViewTraining(R.id.act_payment_view_pager,
                "Pago",
                "elige tu forma de pago", () -> {
                    ViewGroup viewGroup = appCompatActivity.findViewById(R.id.act_payment_card);
                    ScrollView scrollView = appCompatActivity.findViewById(R.id.scrollpayment);
                    scrollView.scrollTo(0, viewGroup.getBottom());
                    nextPrompt();
                });
        generateViewTraining(R.id.fr_payment_card_pay,
                "Pago",
                "una vez configurado el pago puedes continuar");
        startTraining();
    }

    public void signatureActivity() {
        generateViewTraining(R.id.act_content_signature,
                "Firma",
                "en este apartado el cliente firma digitalmente demostrando su conformidad con la instalacion",
                this::nextPrompt);
        generateViewTraining(R.id.act_signature_accept,
                "firma",
                "ademas debe de marcar el cuadro, aceptando los terminos de la misma",
                this::nextPrompt);
        generateViewTraining(R.id.act_signature_dialog_clear,
                "firma",
                "si el cliente se equivoca al capturar la firma puede borrarse y comenzar de nuevo",
                this::nextPrompt);
        generateViewTraining(R.id.act_signature_dialog_save,
                "firma",
                "si se esta seguro de la firma capturada y esta marcada la casilla de los terminos podemos dar flujo");
        startTraining();
    }

    public void finishInstallationActivity() {
        generateViewTraining(R.id.act_finish_instalation_button,
                "Termino de instalacion",
                "presiona el boton para dar por terminada tu instalacion");
        startTraining();
    }
}
