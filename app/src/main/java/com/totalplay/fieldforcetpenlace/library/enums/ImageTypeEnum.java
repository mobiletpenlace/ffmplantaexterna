package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by jorgehdezvilla on 24/09/17.
 * Enlace
 */

public class ImageTypeEnum {

    public class InternalPlant {
        public static final String SPLITTER = "5";
        public static final String LOCATION = "6";
        public static final String EVIDENCE = "7";
        public static final String ACCEPTANCE_ACT = "19";
    }

    public class ExternalPlant {
        public static final String SPLITTER = "8";
        public static final String LOCATION = "9";
        public static final String EVIDENCE = "10";
    }

    public class Integrator {
        public static final String SPLITTER = "11";
        public static final String LOCATION = "12";
        public static final String EVIDENCE = "13";
        public static final String ACCEPTANCE_ACT = "18";
    }

    public static final String FIND_SPLITTER = "14";
    public static final String TRANSFER_PAYMENT = "15";
    public static final String CHECK_PAYMENT = "16";
    public static final String SIGNATURE = "17";
    public static final String PROFILE = "20";

}
