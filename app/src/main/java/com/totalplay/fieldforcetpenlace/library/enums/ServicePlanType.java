package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by jorgehdezvilla on 12/10/17.
 * Enlace
 */

public class ServicePlanType {

    public static final String PHONE = "Telefonia";
    public static final String INTERNET = "Internet";
    public static final String UPS = "UPS";
    public static final String SWITCH = "Switch";
    public static final String IP_PHONE = "Tel IP";
    public static final String WIFI_EXTENDER = "Wifi Extender";

}
