package com.totalplay.fieldforcetpenlace.library.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;

/**
 * Created by jorgehdezvilla on 06/09/17.
 * FFM
 */

public class BitmapUtils {

    public static Bitmap GetBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

//    public static PhotoEvidence saveBitmapFromImageView(ImageView imageView, String imageType, File mRequestedFile, DataManager mFFMDataManager) {
//        PhotoEvidence photo = new PhotoEvidence();
//        mFFMDataManager.tx(tx -> {
//            BitmapDrawable bitmapDrawable = ((BitmapDrawable) imageView.getDrawable());
//            if (bitmapDrawable == null)
//                return;
//            Bitmap bitmap = bitmapDrawable.getBitmap();
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//            byte[] imageInByte = baos.toByteArray();
//            photo.name = mRequestedFile.getName();
//            photo.path = mRequestedFile.getAbsolutePath();
//            photo.photo = Base64.encodeToString(imageInByte, Base64.DEFAULT);
//            photo.type = imageType;
//
//        });
//        return photo;
//    }

}
