package com.totalplay.fieldforcetpenlace.library.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorgehdezvilla on 25/09/17.
 * Enlace
 */

public class WorkOrderOwnerType {

    public static final String INTERNAL_PLANT = "1";
    public static final String EXTERNAL_PLANT = "2";
    public static final List<String> INTEGRATOR = new ArrayList<String>(){{
        add("7");
        add("8");
        add("9");
        add("10");
    }};
}
