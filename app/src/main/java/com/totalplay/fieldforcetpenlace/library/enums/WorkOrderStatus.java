package com.totalplay.fieldforcetpenlace.library.enums;

import java.util.ArrayList;

/**
 * Created by totalplay on 11/24/16.
 * Enlace
 */
public class WorkOrderStatus {
    public static final String ASSIGNED = "9";
    public static final String IN_TRANSIT = "10";
    public static final String WORKING = "11";
    public static final String FINISHED = "14";
    public static final String FINISHED_IN_WAIT = "16";
    public static final String IN_SITE = "107";
    public static final String STAND_BY = "113";
    public static final String WAITING = "12";

    public static class Cancel {
        public final static String id = "15";
        public final static ArrayList<Reason> reasons = new ArrayList<Reason>() {{
            // Canceled OT
            add(new Reason("58", "OT DUPLICADA")); // Ventas

            add(new Reason("59", "CLIENTE YA NO DESEA EL SERVICIO")); //Causa Cliente

            add(new Reason("60", "INCUMPLIMIENTO PLANTA INTERNA")); //Planta Interna
            add(new Reason("61", "FUERA DE COBERTURA")); //Planta Interna

            add(new Reason("62", "IMPOSIBILIDAD TECNICA")); //Planeacion

            add(new Reason("63", "PREDIO IRREGULAR")); //Planta Interna

            add(new Reason("64", "DOMICILIO MOROSO")); // Ventas


            /*add(new Reason("60", "OT DUPLICADA"));
            add(new Reason("61", "CLIENTE ILOCALIZABLE"));
            add(new Reason("62", "CLIENTE NO RECIBE AL TECNICO"));
            add(new Reason("63", "FALTA DE PAGO"));
            add(new Reason("64", "IMPOSIBLE ACOMETER"));
            add(new Reason("65", "INCONSISTENCIAS ANTES DE LA INSTALACION"));
            add(new Reason("66", "NO HAY SISTEMA"));
            add(new Reason("67", "NO SE TERMINO LA INSTALACION A TIEMPO"));
            add(new Reason("68", "OBRA CIVIL / IMPOSIBILIDAD DENTRO DEL DOMICILIO"));
            add(new Reason("69", "RED CON FALLAS"));
            add(new Reason("70", "RED INSUFICIENTE"));
            add(new Reason("71", "INCUMPLIMIENTO"));
            add(new Reason("73", "IMPUTABLE AL CLIENTE"));*/
        }};

        public static class Reason {
            public final String id;
            public final String name;

            Reason(String id, String name) {
                this.id = id;
                this.name = name;
            }

            @Override
            public String toString() {
                return name;
            }
        }
    }

    public static class Stop {
        public final static String id = "3";
        public final static ArrayList<Reason> reasons = new ArrayList<Reason>() {{

            // Stop OT
            add(new Reason("42", "SPLITTER SATURADO")); //Planta Externa
            add(new Reason("43", "SPLITTER ATENUADO")); //Planta Externa
            add(new Reason("44", "SPLITTER SIN POTENCIA")); //Planta Externa
            add(new Reason("45", "SPLITTER PUERTOS DAÑADOS")); // Planta Externa
            add(new Reason("46", "ANCHO DE BANDA INSUFICIENTE")); //Planta Externa

            add(new Reason("47", "REQUIERE OBRA CIVIL ")); //Proyectos Especiales
            add(new Reason("48", "DUCTOS OBSTRUIDOS ")); //Proyectos Especiales
            add(new Reason("49", "EDIFICIO SIN SPLITTER")); // Proyectos Especiales

            add(new Reason("50", "CLIENTE FALTA DE PAGO")); // Causa Cliente
            add(new Reason("51", "CLIENTE REAGENDA")); //Causa Cliente
            add(new Reason("52", "CLIENTE ILOCALIZABLE")); //Causa Cliente
            add(new Reason("53", "CLIENTE SIN PERMISOS ACCESO")); //Causa Cliente

            add(new Reason("54", "CLIMA")); //Planta Interna
            add(new Reason("55", "FALTA DE PERMISOS EN CALLE")); //Planta Interna
            add(new Reason("56", "ZONA DE ALTO RIESGO")); // Planta Interna
            add(new Reason("57", "NO HAY SISTEMA")); // Planta Interna

        }};

        public static class Reason {
            public final String id;
            public final String name;
            Reason(String id, String name) {
                this.id = id;
                this.name = name;
            }
            @Override
            public String toString() {
                return name;
            }
        }
    }
}
