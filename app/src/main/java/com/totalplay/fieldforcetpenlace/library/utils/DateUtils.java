package com.totalplay.fieldforcetpenlace.library.utils;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by totalplay on 6/20/17.
 * FieldForceManagement
 */

public class DateUtils {

    public static String time() {
        return new DateTime(new Date().getTime()).toString("HH:mm");
    }

    public static String dateTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(c.getTime());
    }

    public static String dateNow() {
        SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return simpleDate.format(DateTime.now().toDate());
    }

}
