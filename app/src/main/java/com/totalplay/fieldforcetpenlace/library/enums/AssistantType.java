package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/22/16.
 */
public class AssistantType {
    public static final String INTERNAL = "1";
    public static final String EXTERNAL = "2";
}
