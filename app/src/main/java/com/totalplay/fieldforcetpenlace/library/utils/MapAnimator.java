package com.totalplay.fieldforcetpenlace.library.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.util.Arrays;
import java.util.List;

/**
 * Created by amal.chandran on 22/12/16.
 */

public class MapAnimator {

    static final int BLUE_END = Color.parseColor("#FF7B30FF");
    static final int BLUE_START = Color.parseColor("#FFC5A4FF");
    private static MapAnimator mapAnimator;
    private Polyline backgroundPolyline;
    private Polyline foregroundPolyline;
    private PolylineOptions optionsForeground;
    private AnimatorSet firstRunAnimSet;
    private AnimatorSet secondLoopRunAnimSet;


    private MapAnimator() {

    }

    public static MapAnimator getInstance() {
        if (mapAnimator == null) mapAnimator = new MapAnimator();
        return mapAnimator;
    }


    public void animateRoute(GoogleMap googleMap, List<LatLng> bangaloreRoute, boolean isPatternEnabled) {
        if (firstRunAnimSet == null) {
            firstRunAnimSet = new AnimatorSet();
        } else {
            firstRunAnimSet.removeAllListeners();
            firstRunAnimSet.end();
            firstRunAnimSet.cancel();

            firstRunAnimSet = new AnimatorSet();
        }
        if (secondLoopRunAnimSet == null) {
            secondLoopRunAnimSet = new AnimatorSet();
        } else {
            secondLoopRunAnimSet.removeAllListeners();
            secondLoopRunAnimSet.end();
            secondLoopRunAnimSet.cancel();

            secondLoopRunAnimSet = new AnimatorSet();
        }
        //Reset the polylines
        if (foregroundPolyline != null) foregroundPolyline.remove();
        if (backgroundPolyline != null) backgroundPolyline.remove();
        PolylineOptions optionsBackground;
        optionsBackground = showCurvedPolyline(bangaloreRoute.get(0), bangaloreRoute.get(1), 0.5).color(BLUE_START).width(4);
        optionsForeground = showCurvedPolyline(bangaloreRoute.get(0), bangaloreRoute.get(1), 0.5).color(BLUE_END).width(4);

        if (isPatternEnabled) {
            List<PatternItem> pattern = Arrays.asList(new Dash(20), new Gap(10));
            optionsForeground.pattern(pattern);
            optionsBackground.pattern(pattern);
        }


        backgroundPolyline = googleMap.addPolyline(optionsBackground);
        foregroundPolyline = googleMap.addPolyline(optionsForeground);

        final ValueAnimator percentageCompletion = ValueAnimator.ofInt(0, 100);
        percentageCompletion.setDuration(2000);
        percentageCompletion.setInterpolator(new DecelerateInterpolator());
        percentageCompletion.addUpdateListener(animation -> {
            List<LatLng> foregroundPoints = backgroundPolyline.getPoints();

            int percentageValue = (int) animation.getAnimatedValue();
            int pointcount = foregroundPoints.size();
            int countTobeRemoved = (int) (pointcount * (percentageValue / 100.0f));
            List<LatLng> subListTobeRemoved = foregroundPoints.subList(0, countTobeRemoved);
            subListTobeRemoved.clear();

            foregroundPolyline.setPoints(foregroundPoints);
        });
        percentageCompletion.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                foregroundPolyline.setColor(BLUE_START);
                foregroundPolyline.setPoints(backgroundPolyline.getPoints());
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), BLUE_START, BLUE_END);
        colorAnimation.setInterpolator(new AccelerateInterpolator());
        colorAnimation.setDuration(1200); // milliseconds

        colorAnimation.addUpdateListener(animator -> foregroundPolyline.setColor((int) animator.getAnimatedValue()));


        firstRunAnimSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                secondLoopRunAnimSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        secondLoopRunAnimSet.playSequentially(colorAnimation,
                percentageCompletion);
        secondLoopRunAnimSet.setStartDelay(200);

        secondLoopRunAnimSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                secondLoopRunAnimSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        firstRunAnimSet.start();
    }

    private PolylineOptions showCurvedPolyline(LatLng initDirection, LatLng destinyDirection, double k) {

        double d = SphericalUtil.computeDistanceBetween(initDirection, destinyDirection);
        double h = SphericalUtil.computeHeading(initDirection, destinyDirection);

        //Midpoint position
        LatLng p = SphericalUtil.computeOffset(initDirection, d * 0.5, h);

        //Apply some mathematics to calculate position of the circle center
        double x = (1 - k * k) * d * 0.5 / (2 * k);
        double r = (1 + k * k) * d * 0.5 / (2 * k);

        LatLng c = SphericalUtil.computeOffset(p, x, h + 90.0);

        //Polyline options
        PolylineOptions options = new PolylineOptions();


        //Calculate heading between circle center and two points
        double h1 = SphericalUtil.computeHeading(c, initDirection);
        double h2 = SphericalUtil.computeHeading(c, destinyDirection);

        //Calculate positions of points on circle border and add them to polyline options
        int nPoints = 100;
        double step = (h2 - h1) / nPoints;

        for (int i = 0; i < nPoints; i++) {
            LatLng pi = SphericalUtil.computeOffset(c, r, h1 + i * step);
            options.add(pi);
        }


        return options;
    }


    public void setRouteIncreaseForward(LatLng endLatLng) {
        List<LatLng> foregroundPoints = foregroundPolyline.getPoints();
        foregroundPoints.add(endLatLng);
        foregroundPolyline.setPoints(foregroundPoints);
    }

}
