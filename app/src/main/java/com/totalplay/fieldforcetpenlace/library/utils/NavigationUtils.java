package com.totalplay.fieldforcetpenlace.library.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.Toast;

import com.totalplay.fieldforcetpenlace.R;

import java.util.List;

public class NavigationUtils {


    private static BottomSheetDialog mBottomSheetDialog;

    private static Intent initNavigationWaze(String latitude, String longitude) {
        final String url = String.format("waze://?ll=%s,%s&navigate=yes", latitude, longitude);
        try {
            return new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse(url));
        } catch (android.content.ActivityNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        Intent intent = new Intent(packageName);
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        return activities.size() > 0;
    }

    public static void startNavigation(final Activity activity, final String latitude, final String longitude) {
        try {
            if (latitude != null && longitude != null && !latitude.isEmpty() && !longitude.isEmpty()
                    && !latitude.equals("0") && !longitude.equals("0")) {
                mBottomSheetDialog = new BottomSheetDialog(activity);
                View sheetView = activity.getLayoutInflater().inflate(R.layout.enl_dialog_navigation, null);
                sheetView.findViewById(R.id.bottom_sheet_maps).setOnClickListener(view -> {
                    if (!isPackageInstalled("com.google.android.apps.maps", view.getContext().getPackageManager())) {
                        launchActivity(activity, NavigationUtils.initNavigationMaps(latitude, longitude),
                                "com.google.android.apps.maps");
                    }
                    mBottomSheetDialog.dismiss();
                });

                sheetView.findViewById(R.id.bottom_sheet_waze).setOnClickListener(view -> {
                    if (!isPackageInstalled("com.waze", view.getContext().getPackageManager())) {
                        launchActivity(activity, NavigationUtils.initNavigationWaze(latitude, longitude),
                                "com.waze");
                    }
                    mBottomSheetDialog.dismiss();
                });

                mBottomSheetDialog.setContentView(sheetView);
                mBottomSheetDialog.setCancelable(false);
                if (!(activity).isFinishing()) {
                    mBottomSheetDialog.show();
                }


            } else {
                Toast.makeText(activity, "Coordenadas no encontradas", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void openMarket(Context context, String appPackageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private static Intent initNavigationMaps(String latitude, String longitude) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        return mapIntent;
    }

    private static void launchActivity(Context context, Intent intent, String packageName) {
        if (intent != null) {
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(context, "La aplicación no está instalada o no está configurada correctamente",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            openMarket(context, packageName);
        }
    }
}
