package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/28/16.
 * Total
 */

public class PaymentType {

    public static final String CREDIT_CARD = "10003";
    public static final String DEBIT_CARD = "10005";

}
