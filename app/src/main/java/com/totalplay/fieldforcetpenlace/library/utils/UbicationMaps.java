package com.totalplay.fieldforcetpenlace.library.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.widget.Toast;
import java.io.IOException;
import java.util.List;

/**
 * Created by tlion93 on 04/08/17.
 */

public class UbicationMaps {

    public static Address obtainUbication(String ciudad, String code, Context context){
        Geocoder gps = new Geocoder(context);
        List<Address> list_directions = null;
        Address directions = null;
        try {
            list_directions = gps.getFromLocationName(ciudad, 3);
        }
        catch(IOException ex){
            Toast.makeText(context,"No se encontraron coordenadas",Toast.LENGTH_SHORT).show();
        }

        if( list_directions != null && list_directions.size() > 0)
        {
            directions = list_directions.get(0);
        }
        else{
            return  null;
        }

        return directions;
    }
}
