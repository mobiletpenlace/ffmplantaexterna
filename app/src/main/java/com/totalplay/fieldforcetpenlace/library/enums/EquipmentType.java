package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 1/19/17.
 * FieldForceManagement-Android
 */
public class EquipmentType {

    public static final String OTHERS = "1";
    public static final String PHONES = "2";

    public static final String ALL = "3";

    public static final String ONT = "ONT";
    public static final String WIFI_EXTENDER = "WIFI";
    public static final String PBX = "PBX";
    public static final String TEL_IP = "Tel IP";
    public static final String TEL_DIGITAL = "Tel Digital";
    public static final String TEL_ANALOG = "Tel Analogico";
    public static final String TEL = "Tel";
    public static final String UPS = "UPS";

}
