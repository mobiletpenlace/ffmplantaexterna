package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by totalplay on 11/28/16.
 */
public class ChangeStatusType {
    public static final Integer OPERATOR = 1;
    public static final Integer CANCEL_WORK_ORDER = 2;
    public static final Integer STOP_WORK_ORDER = 3;
}
