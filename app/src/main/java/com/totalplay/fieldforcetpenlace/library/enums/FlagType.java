package com.totalplay.fieldforcetpenlace.library.enums;

/**
 * Created by tlion93 on 08/08/17.
 * Enlace
 */

public class FlagType {
    public static final String CONFIGURED_EQUIPMENT = "1";
    public static final String CONFIGURED_DNS = "2";
    public static final String ACTIVATED_ACCOUNT = "3";
    public static final String PAID_ACCOUNT = "4";
    public static final String SENT_EVIDENCE = "5";
    public static final String MARK_ASSISTANT = "6";
    public static final String ASSIGNED_SPLITTER = "7";
    public static final String ESTIMATED_TIME = "8";
    public static final String URBAN_REFERENCES = "9";
    public static final String END_SURVEY = "10";
    public static final String SEND_MATERIALS = "11";
}
