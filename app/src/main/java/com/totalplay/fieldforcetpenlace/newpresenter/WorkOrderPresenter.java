package com.totalplay.fieldforcetpenlace.newpresenter;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;

/**
 * Created by jorgehdezvilla on 27/10/17.
 * Enlace
 */

public class WorkOrderPresenter extends FFMPresenter {

    public WorkOrderPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void saveSuccessSettingsEquipments() {
        mFFMDataManager.tx(tx -> {
            tx.delete(Equipment.class);
            mWorkOrder.isSettingsDispositive = true;
            tx.save(mWorkOrder);
        });
    }
}
