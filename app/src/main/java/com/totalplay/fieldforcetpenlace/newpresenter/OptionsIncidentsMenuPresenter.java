package com.totalplay.fieldforcetpenlace.newpresenter;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ChangeStatusRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ChangeStatusResponse;
import com.totalplay.fieldforcetpenlace.library.enums.OriginAppEnum;
import com.totalplay.fieldforcetpenlace.library.enums.ParamsEnum;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderStatus;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Reason;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.RequestParameter;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Splitter;
import com.totalplay.fieldforcetpenlace.routing.RoutingApp;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;

/**
 * Created by jorgehdezvilla on 12/09/17.
 * FFM
 */

public class OptionsIncidentsMenuPresenter extends FFMPresenter implements WSCallback {

    private Reason mReason;
    private Splitter mSplitter;
    private OptionsIncidentsMenuCallback mOptionsIncidentsMenuCallback;

    public interface OptionsIncidentsMenuCallback {
        void onSuccessLoadReason(Reason reason);
    }

    public OptionsIncidentsMenuPresenter(AppCompatActivity appCompatActivity, OptionsIncidentsMenuCallback optionsIncidentsMenuCallback) {
        super(appCompatActivity);
        this.mOptionsIncidentsMenuCallback = optionsIncidentsMenuCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void loadData() {
        mReason = (Reason) mAppCompatActivity.getIntent().getSerializableExtra(Keys.EXTRA_REASON);
        if (mAppCompatActivity.getIntent().hasExtra(Keys.EXTRA_SPLITTER))
            mSplitter = (Splitter) mAppCompatActivity.getIntent().getSerializableExtra(Keys.EXTRA_SPLITTER);
        mOptionsIncidentsMenuCallback.onSuccessLoadReason(mReason);
    }

    public void sendCancelStatus(String comments) {
        ChangeStatusRequest changeStatusRequest = new ChangeStatusRequest();
        if (TotalPlayLocationService.getLastLocation() != null) {
            final Location location = TotalPlayLocationService.getLastLocation();
            changeStatusRequest.latitude = String.valueOf(location.getLatitude());
            changeStatusRequest.longitude = String.valueOf(location.getLongitude());
        } else {
            changeStatusRequest.latitude = "0";
            changeStatusRequest.longitude = "0";
        }
        changeStatusRequest.origin = OriginAppEnum.ORIGIN_APP;
        changeStatusRequest.status = WorkOrderStatus.STAND_BY;
        changeStatusRequest.reason = mReason.id;
        changeStatusRequest.comment = comments;
        changeStatusRequest.operatorId = mUser.operatorId;
        changeStatusRequest.workOrderId = mWorkOrder.id;
        if (mSplitter != null) {
            changeStatusRequest.parameterList = new ArrayList<>();
            changeStatusRequest.parameterList.add(new RequestParameter(ParamsEnum.ID_SPLITTER, mSplitter.id));
            changeStatusRequest.parameterList.add(new RequestParameter(ParamsEnum.LAT_SPLITTER, mSplitter.latitude));
            changeStatusRequest.parameterList.add(new RequestParameter(ParamsEnum.LONG_SPLITTER, mSplitter.longitude));
            changeStatusRequest.parameterList.add(new RequestParameter(ParamsEnum.USED_PORTDS, mSplitter.occupedPorts));
            changeStatusRequest.parameterList.add(new RequestParameter(ParamsEnum.TOTAL_PORTS, mSplitter.totalPorts));
        }

        mWSManager.requestWs(ChangeStatusResponse.class, WSManager.WS.CHANGE_STATUS, getDefinition().changeStatus(changeStatusRequest));
    }

    private void successChangeStatus(String requestUrl, ChangeStatusResponse changeStatusResponse) {
        if (changeStatusResponse.result.equals("0")) {
            Toast.makeText(mContext, R.string.dialog_success_changed_status, Toast.LENGTH_SHORT).show();
            mAppCompatActivity.finish();
            mFFMDataManager.tx(tx -> {
                mWorkOrder.isWait = true;
                tx.save(mWorkOrder);
            });
            RoutingApp.OnSuccessCancelWorkOrderEvent(mAppCompatActivity);
        } else {
            onErrorLoadResponse(requestUrl, changeStatusResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_comments);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.CHANGE_STATUS)) {
            successChangeStatus(requestUrl, (ChangeStatusResponse) baseResponse);
        }
    }


}
