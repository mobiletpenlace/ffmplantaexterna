package com.totalplay.fieldforcetpenlace.newpresenter;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;

import com.library.pojos.DataManager;
import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.bus.BusManager;
import com.totalplay.fieldforcetpenlace.background.bus.events.LocationUpdatedEvent;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;
import com.totalplay.fieldforcetpenlace.background.ws.definitions.EnlaceWebServicesDefinition;
import com.totalplay.fieldforcetpenlace.background.ws.definitions.WebServicesAppDefinition;
import com.totalplay.fieldforcetpenlace.background.ws.definitions.WebServicesImagesDefinition;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.User;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.mvp.BasePresenter;
import com.totalplay.utils.MessageUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


/**
 * Created by jorgehdezvilla on 06/09/17.
 * FFMW
 */

public abstract class FFMPresenter extends BasePresenter implements WSCallback {

    protected DataManager mFFMDataManager;
    protected WorkOrder mWorkOrder;
    protected User mUser;

    protected BaseWSManager mWSManager;

    public FFMPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isGPSEnabled()) {
            if (mContext instanceof AppCompatActivity) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ((AppCompatActivity) mContext).startActivityForResult(intent, 100);
            }
        }
        mFFMDataManager = DataManager.validateConnection(mFFMDataManager);
        mWorkOrder = mFFMDataManager.queryWhere(WorkOrder.class).findFirst();
        if (mWorkOrder != null && mWorkOrder.ownerId == null)
            mWorkOrder.ownerId = WorkOrderOwnerType.INTERNAL_PLANT;
        mUser = mFFMDataManager.queryWhere(User.class).findFirst();
        BusManager.register(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mFFMDataManager = DataManager.validateConnection(mFFMDataManager);
        mWorkOrder = mFFMDataManager.queryWhere(WorkOrder.class).findFirst();
        mUser = mFFMDataManager.queryWhere(User.class).findFirst();
        mWSManager = initWSManager();
//        QueueManager.add(new SynchronizeImagesJob(mUser, mFFMDataManager.queryWhere(PhotoEvidence.class).filter("synced", false).list()));
    }

    private boolean isGPSEnabled() {
        LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public EnlaceWebServicesDefinition getDefinition() {
        return WebServices.servicesEnlace();
    }

    public WebServicesAppDefinition getAppDefinition() {
        return WebServices.totalApps();
    }

    public WebServicesImagesDefinition getImagesDefinition() {
        return WebServices.servicesImages();
    }

    public abstract BaseWSManager initWSManager();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWSManager != null) mWSManager.onDestroy();
        if (mFFMDataManager != null) mFFMDataManager.close();
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        if (messageError.trim().equals("")) {
            MessageUtils.toast(mContext, R.string.dialog_intern_error);
        } else {
            MessageUtils.toast(mContext, messageError);
        }
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(mContext, R.string.dialog_error_connection);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusManager.unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLocationUpdatedEvent(final LocationUpdatedEvent event) {

    }
}
