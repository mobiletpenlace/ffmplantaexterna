package com.totalplay.fieldforcetpenlace.newpresenter;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ChangeStatusResponse;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderStatus;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;
import com.totalplay.utils.MessageUtils;

/**
 * Created by jorgehdezvilla on 18/09/17.
 * FMP
 */

public class WorkOrderInfoInSitePresenter extends ChangeStatusPresenter implements WSCallback {

    private Callback mCallback;

    public WorkOrderInfoInSitePresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    public interface Callback {
        void onSuccessInSite(WorkOrder workOrder);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mStatus = WorkOrderStatus.IN_SITE;
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void confirmArrivalClick() {
        if (!mWorkOrder.isInSite) {
            changeStatus(mStatus);
        } else {
            mCallback.onSuccessInSite(mWorkOrder);
        }
    }

    private void successChangeStatus(String requestUrl, ChangeStatusResponse changeStatusResponse) {
        MessageUtils.stopProgress();
        if (changeStatusResponse.result.equals("0")) {
            MessageUtils.toast(mContext, R.string.dialog_success_changed_status);
            mWorkOrder.isInSite = true;
            mFFMDataManager.tx(tx -> tx.save(mWorkOrder));
            mCallback.onSuccessInSite(mWorkOrder);
        } else {
            onErrorLoadResponse(requestUrl, changeStatusResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.CHANGE_STATUS)) {
            MessageUtils.progress(mAppCompatActivity, R.string.dialog_changing_status);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if (requestUrl.equals(WSManager.WS.CHANGE_STATUS)) {
            successChangeStatus(requestUrl, (ChangeStatusResponse) baseResponse);
        }
    }

}
