package com.totalplay.fieldforcetpenlace.newpresenter;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ChangeStatusRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ChangeStatusResponse;
import com.totalplay.fieldforcetpenlace.library.enums.OriginAppEnum;

/**
 * Created by jorgehdezvilla on 11/09/17.
 * FFM
 */

public abstract class ChangeStatusPresenter extends FFMPresenter implements WSCallback {

    protected String mStatus = "";
    protected String mReasonId = "";

    public ChangeStatusPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mWSManager = WSManager.init().settings(mContext, this);
    }

    public void changeStatus(String status) {
        mStatus = status;
        ChangeStatusRequest changeStatusRequest = new ChangeStatusRequest();
        if (TotalPlayLocationService.getLastLocation() != null) {
            final Location location = TotalPlayLocationService.getLastLocation();
            changeStatusRequest.latitude = String.valueOf(location.getLatitude());
            changeStatusRequest.longitude = String.valueOf(location.getLongitude());
        } else {
            changeStatusRequest.latitude = "0";
            changeStatusRequest.longitude = "0";
        }
        changeStatusRequest.origin = OriginAppEnum.ORIGIN_APP;
        changeStatusRequest.status = mStatus;
        changeStatusRequest.reason = mReasonId;
        changeStatusRequest.comment = "";
        changeStatusRequest.operatorId = mUser.operatorId;

        changeStatusRequest.workOrderId = mWorkOrder.id;

        mWSManager.requestWs(ChangeStatusResponse.class, WSManager.WS.CHANGE_STATUS, getDefinition().changeStatus(changeStatusRequest));
//            mChangeStatusPresenter.changeStatus(false, WorkOrderStatus.IN_TRANSIT, null);
    }

}
