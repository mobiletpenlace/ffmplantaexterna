package com.totalplay.fieldforcetpenlace.newpresenter;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.services.TotalPlayLocationService;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.CompletedWorkOrderRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ChangeStatusResponse;
import com.totalplay.fieldforcetpenlace.library.enums.OriginAppEnum;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderOwnerType;
import com.totalplay.fieldforcetpenlace.library.enums.WorkOrderStatus;
import com.totalplay.utils.MessageUtils;

/**
 * Created by jorgehdezvilla on 03/10/17.
 * Enlace
 */

public class WorkOrderCompletedPresenter extends FFMPresenter implements WSCallback {

    private Callback mCallback;
    private String mStatus = "";

    public WorkOrderCompletedPresenter(AppCompatActivity appCompatActivity, Callback callback) {
        super(appCompatActivity);
        mCallback = callback;
    }

    public interface Callback {
        void onSuccessChangeStatus();
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CompletedWorkOrderRequest changeStatusRequest = new CompletedWorkOrderRequest();
        if (TotalPlayLocationService.getLastLocation() != null) {
            final Location location = TotalPlayLocationService.getLastLocation();
            changeStatusRequest.latitude = String.valueOf(location.getLatitude());
            changeStatusRequest.longitude = String.valueOf(location.getLongitude());
        } else {
            changeStatusRequest.latitude = "0";
            changeStatusRequest.longitude = "0";
        }
        changeStatusRequest.origin = OriginAppEnum.ORIGIN_APP;
        if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
            mStatus = WorkOrderStatus.FINISHED;
        } else if (WorkOrderOwnerType.INTEGRATOR.contains(mWorkOrder.ownerId)) {
            mStatus = WorkOrderStatus.FINISHED_IN_WAIT;
        }
        changeStatusRequest.status = mStatus;
        changeStatusRequest.reason = "";
        changeStatusRequest.operatorId = mUser.operatorId;
        changeStatusRequest.workOrderId = mWorkOrder.id;

        mWSManager.requestWs(ChangeStatusResponse.class, WSManager.WS.FINISH_STATUS, getDefinition().finishStatus(changeStatusRequest));
    }

    private void successChangeStatus(String requestUrl, ChangeStatusResponse changeStatusResponse) {
        if (changeStatusResponse.result.equals("0")) {
            MessageUtils.toast(mContext, R.string.dialog_success_changed_status);
            if (mWorkOrder.ownerId.equals(WorkOrderOwnerType.INTERNAL_PLANT)) {
                mFFMDataManager.deleteWorkOrder();
            } else {
                mWorkOrder.isWait = true;
                mFFMDataManager.tx(tx -> tx.save(mWorkOrder));
            }
            mCallback.onSuccessChangeStatus();
        } else {
            onErrorLoadResponse(requestUrl, changeStatusResponse.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_changing_status);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.FINISH_STATUS)) {
            successChangeStatus(requestUrl, (ChangeStatusResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        if (requestUrl.equals(WSManager.WS.FINISH_STATUS)) {
            mAppCompatActivity.finish();
        }
    }
}
