package com.totalplay.fieldforcetpenlace.newpresenter;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import com.totalplay.background.BaseWSManager;
import com.totalplay.fieldforcetpenlace.BuildConfig;
import com.totalplay.fieldforcetpenlace.background.ws.WebServices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tlion93 on 25/07/17.
 * Enlace
 */

public class ConsultingURLImgPresenter extends FFMPresenter {

    private Call<ResponseBody> imageCall;
    private ConsultingURLImgCallback mConsultingURLImgCallback;

    public interface ConsultingURLImgCallback {
        void onSuccessLoadFile(File file);

        void onErrorLoadImage();
    }

    public ConsultingURLImgPresenter(AppCompatActivity appCompatActivity, ConsultingURLImgCallback consultingURLImgCallback) {
        super(appCompatActivity);
        mConsultingURLImgCallback = consultingURLImgCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return null;
    }

    public void fullImage(String url, String nameFile) {
        if (BuildConfig.DEBUG) {
            imageCall = WebServices.servicesImages().downloadFiles(Base64.encodeToString(url.getBytes(), Base64.NO_WRAP));
        } else {
            imageCall = WebServices.servicesImages().downloadFile(Base64.encodeToString(url.getBytes(), Base64.NO_WRAP));
        }
        imageCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    //you can now get your file in the InputStream
                    InputStream is = response.body().byteStream();
                    File path = Environment.getExternalStorageDirectory();
                    File filePath = new File(path, ".FFM");
                    if (!filePath.exists())
                        filePath.mkdirs();
                    File file = new File(filePath, nameFile);
                    try (OutputStream output = new FileOutputStream(file)) {
                        byte[] buffer = new byte[4 * 1024]; // or other buffer size
                        int read;

                        while ((read = is.read(buffer)) != -1) {
                            output.write(buffer, 0, read);
                        }
                        output.flush();
                        mConsultingURLImgCallback.onSuccessLoadFile(file);
                    } catch (IOException e) {
                        mConsultingURLImgCallback.onErrorLoadImage();
                        e.printStackTrace();
                    }
                } else {
                    mConsultingURLImgCallback.onErrorLoadImage();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mConsultingURLImgCallback.onErrorLoadImage();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (imageCall != null && imageCall.isExecuted()) imageCall.cancel();
    }
}