package com.totalplay.fieldforcetpenlace.newpresenter;

import android.support.v7.app.AppCompatActivity;

import com.totalplay.background.BaseWSManager;
import com.totalplay.background.WSBaseResponseInterface;
import com.totalplay.background.WSCallback;
import com.totalplay.fieldforcetpenlace.R;
import com.totalplay.fieldforcetpenlace.background.tasks.SerialNumberEquipmentTask;
import com.totalplay.fieldforcetpenlace.background.ws.WSManager;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.ConfigSDMRequest;
import com.totalplay.fieldforcetpenlace.background.ws.requests.enl.SettingEquipmentRequest;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.ConfigSDMResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.GetMacResponse;
import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.UpdateResponse;
import com.totalplay.fieldforcetpenlace.library.enums.SerialNumberErrorReason;
import com.totalplay.fieldforcetpenlace.library.enums.ServicePlanType;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Dispositive;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.routing.RoutingApp;
import com.totalplay.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JuanReynaldoEscobar on 05/10/2017.
 * FFM
 */

public class ConfigSDMPresenter extends FFMPresenter implements WSCallback {

    private ConfigSDMCallback mConfigSDMCallback;
    private SerialNumberEquipmentTask mSerialNumberEquipmentTask;


    public interface ConfigSDMCallback {
        void onSuccessLoadConfigSDM(ConfigSDMResponse configSDMResponse);

        void onErrorLoadConfigSDM();

        void onSuccessLoadMac(GetMacResponse mac);
    }


    public ConfigSDMPresenter(AppCompatActivity appCompatActivity, ConfigSDMCallback configSDMCallback) {
        super(appCompatActivity);
        mConfigSDMCallback = configSDMCallback;
    }

    public void getConfigSDM() {
        ConfigSDMRequest configSDMRequest = new ConfigSDMRequest(mWorkOrder.account);
        mWSManager.requestWs(ConfigSDMResponse.class, WSManager.WS.CONFIG_SMD, getDefinition().getConfigSDM(configSDMRequest));
    }

    @Override
    public BaseWSManager initWSManager() {
        return WSManager.init().settings(mContext, this);
    }

    public void settingEquipment(String mac, String serie, String model) {
        String servicePlanId = "";
        for (ServicePlan servicePlan : mWorkOrder.quotationInfo.servicePlans) {
            if (servicePlan.wasAdditional.equals("false") && servicePlan.type.equals(ServicePlanType.PHONE)) {
                servicePlanId = servicePlan.servicePlanId;
            }
        }
        SettingEquipmentRequest settingEquipmentRequest = new SettingEquipmentRequest();
        List<Dispositive> listRequest = new ArrayList<>();
        listRequest.add(new Dispositive(servicePlanId, serie, mac, model));
        settingEquipmentRequest.dispositives = new SettingEquipmentRequest.Dispositivos();
        settingEquipmentRequest.dispositives.dispositivesList = listRequest;
        mWSManager.requestWs(UpdateResponse.class, WSManager.WS.SETTING_EQUIPMENT, getDefinition().settingEquipment(settingEquipmentRequest));
    }

    public void validWifiExtenderSerialNumber(final String serialNumber) {
        boolean flag = false;
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading_data);
        for (int i = 0; i < mWorkOrder.quotationInfo.servicePlans.size(); i++) {
            if (mWorkOrder.quotationInfo.servicePlans.get(i).type.equals((ServicePlanType.WIFI_EXTENDER))) {
                flag = true;
                break;
            }
        }
        String serialNum;
        if (flag) {
            if (serialNumber.length() >= 20) {
                serialNum = serialNumber.substring(2, serialNumber.length());
            } else if (serialNumber.length() == 19) {
                serialNum = serialNumber.substring(1, serialNumber.length());
            } else {
                serialNum = serialNumber;
            }
        } else {
            serialNum = serialNumber;
        }


        mSerialNumberEquipmentTask = new SerialNumberEquipmentTask(mContext, serialNum, new SerialNumberEquipmentTask.SerialNumberTaskListener() {
            @Override
            public void onSuccessLoadedMac(GetMacResponse mac) {
                MessageUtils.stopProgress();
                mConfigSDMCallback.onSuccessLoadMac(mac);
            }

            @Override
            public void onErrorLoadedMac(SerialNumberErrorReason errorReason) {
                MessageUtils.stopProgress();
                if (errorReason != null) {
                    switch (errorReason) {
                        case COULD_NOT_GET_MAC:
                            MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_loaded_mac);
                            break;
                        case COULD_NOT_VALID_SERIAL_NUMBER:
                            MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_invalid_serial_number);
                            break;
                        case SERIAL_NUMBER_ALREDY_EXISTS:
                            MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_already_exist_serial_number);
                            break;
                        default:
                            MessageUtils.toast(mAppCompatActivity, R.string.dialog_error);
                            break;
                    }
                } else {
                    MessageUtils.toast(mAppCompatActivity, R.string.dialog_error_connection);
                }
            }
        });
        mSerialNumberEquipmentTask.execute();
    }

    private void successConfigSDM(String requestUrl, ConfigSDMResponse configSDMResponse) {
        if (configSDMResponse.response.result.equals("0")) {
            mConfigSDMCallback.onSuccessLoadConfigSDM(configSDMResponse);
        } else {
            onErrorLoadResponse(requestUrl, configSDMResponse.response.resultDescription);
        }
    }

    @Override
    public void onRequestWS(String requestUrl) {
        if (requestUrl.equals(WSManager.WS.CONFIG_SMD)) {
            MessageUtils.progress(mAppCompatActivity, "Descargando información");
        } else if (requestUrl.equals(WSManager.WS.SETTING_EQUIPMENT)) {
            MessageUtils.progress(mAppCompatActivity, R.string.dialog_configuration);
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManager.WS.CONFIG_SMD)) {
            successConfigSDM(requestUrl, (ConfigSDMResponse) baseResponse);
        } else if (requestUrl.equals(WSManager.WS.SETTING_EQUIPMENT)) {
            successSettingEquipments(requestUrl, (UpdateResponse) baseResponse);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        super.onErrorLoadResponse(requestUrl, messageError);
        mConfigSDMCallback.onErrorLoadConfigSDM();
    }

    private void successSettingEquipments(String requestUrl, UpdateResponse updateResponse) {
        if (updateResponse.result != null && updateResponse.result.result.equals("0")) {
            MessageUtils.toast(mContext, updateResponse.result.resultDescription);
            mFFMDataManager.tx(tx -> {
                mWorkOrder.isSetttingPXB = true;
                tx.save(mWorkOrder);
            });
            mAppCompatActivity.finish();
            RoutingApp.OnSuccessActivatePBXEvent(mAppCompatActivity, mWorkOrder);
        } else {
            if (updateResponse.result != null)
                onErrorLoadResponse(requestUrl, updateResponse.result.resultDescription);
            else onErrorLoadResponse(requestUrl, "");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSerialNumberEquipmentTask != null) mSerialNumberEquipmentTask.cancel(true);
    }
}
