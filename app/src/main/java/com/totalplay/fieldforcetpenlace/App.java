package com.totalplay.fieldforcetpenlace;


import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.totalplay.fieldforcetpenlace.library.utils.Keys;
import com.totalplay.fieldforcetpenlace.presenter.implementations.ApplicationPresenter;
import com.totalplay.fieldforcetpenlace.view.activities.CustomErrorActivity;
import com.totalplay.fieldforcetpenlace.view.activities.SplashActivity;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.config.CaocConfig;
import io.fabric.sdk.android.Fabric;

public class App extends Application {

    ApplicationPresenter mApplicationPresenter;
    public static String secretKey;

    @Override
    public void onCreate() {
        super.onCreate();
//        CaocConfig.Builder.create()
//                .trackActivities(true) //default: false
//                .restartActivity(SplashActivity.class) //default: null (your app's launch activity)
//                .errorActivity(CustomErrorActivity.class) //default: null (default error activity)
//                .eventListener(new CustomEventListener()) //default: null
//                .apply();
        Logger.addLogAdapter(new AndroidLogAdapter());
        Fabric.with(this, new Crashlytics());
        mApplicationPresenter = new ApplicationPresenter(this);
        mApplicationPresenter.setup();
        secretKey=getResources().getString(R.string.secret_key);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private static class CustomEventListener implements CustomActivityOnCrash.EventListener {
        @Override
        public void onLaunchErrorActivity() {
            Log.i(Keys.DEBUG, "onLaunchErrorActivity()");
        }

        @Override
        public void onRestartAppFromErrorActivity() {
            Log.i(Keys.DEBUG, "onRestartAppFromErrorActivity()");
        }

        @Override
        public void onCloseAppFromErrorActivity() {
            Log.i(Keys.DEBUG, "onCloseAppFromErrorActivity()");
        }
    }

}

