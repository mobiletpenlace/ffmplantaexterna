package com.library.pojos;

import com.totalplay.fieldforcetpenlace.background.ws.responses.enl.Quotation;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.DN;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Equipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.InfoEquipment;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Material;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.Olt;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.ServicePlan;
import com.totalplay.fieldforcetpenlace.model.enl.pojos.WorkOrder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;

/**
 * Created by jorgehdezvilla on 24/08/17.
 * FMP
 */

@SuppressWarnings("unused")
public class DataManager extends BaseDataManager {

    public Transactions mTransactions;

    private DataManager() {
        super();
    }

    public static DataManager open() {
        return new DataManager();
    }

    public static DataManager validateConnection(DataManager manager) {
        if (manager == null)
            manager = DataManager.open();
        manager.validateConnection();
        return manager;
    }

    public void tx(DataManager.TX tx) {
        validateConnection(this);
        mTransactions = new Transactions(mRealm);
        mRealm.executeTransaction(realm -> tx.execute(mTransactions));
    }

    public void deleteAll() {
        tx(tx -> mRealm.deleteAll());
    }

    public void deleteWorkOrder() {
        tx(tx -> {
            mTransactions.delete(WorkOrder.class);
            mTransactions.delete(Material.class);
            mTransactions.delete(Quotation.class);
            mTransactions.delete(Equipment.class);
            mTransactions.delete(ServicePlan.class);
            mTransactions.delete(DN.class);
            mTransactions.delete(Olt.class);
            mTransactions.delete(InfoEquipment.class);
        });
    }

    public <M extends RealmModel> Flowable<ArrayList<M>> listObservable(Class<M> tClass) {
        return mRealm.where(tClass)
                .findAllAsync()
                .asFlowable()
                .filter(RealmResults::isLoaded)
                .map(items -> new ArrayList<>(mRealm.copyFromRealm(items)));
    }

    public <M extends RealmModel> Flowable<ArrayList<M>> listObservable(Class<M> tClass, String filter, boolean value) {
        return mRealm.where(tClass)
                .equalTo(filter, value)
                .findAllAsync()
                .asFlowable()
                .filter(RealmResults::isLoaded)
                .map(items -> new ArrayList<>(mRealm.copyFromRealm(items)));
    }

    /////////////////////////// CATALOGS

    public interface TX {
        void execute(DataManager.Transactions tx);
    }

    public static class Transactions {

        public Realm realm;

        Transactions(Realm realm) {
            this.realm = realm;
        }

        public <T extends RealmModel> T save(T t) {
            return realm.copyToRealmOrUpdate(t);
        }

        public <T extends RealmModel> T saveModel(T t) {
            return realm.copyToRealmOrUpdate(t);
        }

        public <T extends RealmModel> void save(List<T> t) {
            for (T item : t) {
                save(item);
            }
        }

        public <T extends RealmModel> void delete(Class<T> tClass, String id) {
            RealmResults results = realm.where(tClass).equalTo("id", id).findAll();
            if (results != null) results.deleteAllFromRealm();
        }

        public <T extends RealmModel> void delete(Class<T> tClass) {
            realm.delete(tClass);
        }

        public <T extends RealmModel> void removeListInModelByAttribute(Class<T> tClass, String attribute,
                                                                        String value) {
            RealmResults results = realm.where(tClass).equalTo(attribute, value).findAll();
            if (results != null) results.deleteAllFromRealm();
        }

    }


}
